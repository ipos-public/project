# IPos project

This repository is a centralized version of architecture for IPos software (Indoor Positioning Software Framework for Intralogistics Applications).


## Architecture
![](PRG_talk-Layers.jpg)
    - [diagram](https://drive.google.com/file/d/12oIwl0zkeT2bsS8MUbbtSgFbbirG8Sz1/view?usp=sharing)


## How to checkout?
- Cloning repository into a folder named "project" + checking out all submodules of the master branch
	- $ git clone -b master --recurse-submodules https://git-st.inf.tu-dresden.de/ipos-public/project.git project


## How to run?

1. Start [hivemq](http://www.hivemq.com/demos/websocket-client/). It's a client for an external test MQTT-broker.
    1. Establish a connection with the standard parameters (WS connection).
    2. Add new subscriptions with `Add New Topic Subscription` button
        - `test51/subscribe/positions`: responses
        - `test51/publish/positions`: requests
2. Start Spring boot project from this repository
    - from console (`./gradlew bootRun`) or your favorite IDE
3. Return to a web-browser tab with an external MQTT client
    1. Send valid json representation of position in `test51/publish/positions` topic
        - Message example: `{"x": 3.0,"y": 2.0,"z": 3.0,"accuracy": 0.9}`
    2. If successful, the same message should come from `test51/subscribe/positions` topic.


![](demo.v1.jpg)

## Configurations
- External MQTT broker
  - Parameters for a connection are in `src/main/resources/application.yml`  
    - ```yaml
        mqtt:
          automaticReconnect: true
          cleanSession: true
          setKeepAliveInterval: 10
          connectionTimeout: 20
          clientId: ipos.project
          hostname: broker.hivemq.com
          port: 1883
        ```
    - Parameters are used in `src/main/java/ipos/project/config/MqttConfig.java`
      - `@ConfigurationProperties(prefix = "mqtt")` refers to all relevant parameters
      - create `MqttConnectOptions` bean
      - create `MqttAsyncClient` bean. Require `MqttConnectOptions`


### Extra
- Internal database: [localhost:8077/h2-console](http://localhost:8077/h2-console)
  - server 8077 port from `application.yml`
  - JDBC URL: see logs. Example: `jdbc:h2:mem:1a7bd898-e992-4a5c-9a3e-ed8d9eb2ab37`
  - [ ] TODO: fix database schema


## Links
- [Messaging with JMS](https://spring.io/guides/gs/messaging-jms/)
- [Using WebSocket to build an interactive web application](https://spring.io/guides/gs/messaging-stomp-websocket/)
- [Java Message Service API via Apache ActiveMQ Classic](https://spring.io/guides/gs/messaging-jms/)
