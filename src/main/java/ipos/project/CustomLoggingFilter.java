package ipos.project;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

public class CustomLoggingFilter extends Filter<ILoggingEvent> {

    @Override
    public FilterReply decide(ILoggingEvent event) {
        if (event.getMessage().contains("Eventfilter:")) {
            return FilterReply.DENY;
        } else if (event.getMessage().contains("SDF:")) {
            return FilterReply.DENY;
        } else if (event.getMessage().contains("SDF-DEBUG:")) {
            return FilterReply.DENY;
        } else if (event.getMessage().contains("SDF-PUB:")) {
            return FilterReply.DENY;
        } else if (event.getMessage().contains("INDFRO:")){
           return FilterReply.ACCEPT;
        } else if (event.getMessage().contains("INDFRO-DEBUG:")){
            return FilterReply.ACCEPT;
        } else if (event.getMessage().contains("ODO:")){
            return FilterReply.DENY;
        } else if (event.getMessage().contains("Received a Message:")){ // MQTT-library meldet sich
            return FilterReply.DENY;
        } else if (event.getMessage().contains("OP:")){
            return FilterReply.DENY;
        } else if (event.getMessage().contains("TOOZ:")){
            return FilterReply.ACCEPT;
        } else if (event.getMessage().contains("VDA5050:")){
            return FilterReply.DENY;
        } else if (event.getMessage().contains("OSM:")){
            return FilterReply.DENY;
        }else if (event.getMessage().contains("SHELL:")){
            return FilterReply.ACCEPT;
        } else {
            return FilterReply.ACCEPT;
        }

    }
}