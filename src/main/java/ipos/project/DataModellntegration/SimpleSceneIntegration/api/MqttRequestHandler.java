package ipos.project.DataModellntegration.SimpleSceneIntegration.api;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.SimpleScene;
import ipos.models.SimpleScene.IposMonitoringRequest;
import ipos.project.DataModellntegration.SimpleSceneIntegration.service.SimpleSceneTransformer;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.UseCaseController.Administration;
import ipos.project.UseCaseController.PositionMonitoring;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import java.util.List;


// subscribe to the topic. It's example
@MqttListener(PositionMonitoring.TOPIC_ConfigWrapper)
public class MqttRequestHandler implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    JmsTemplate jmsTemplate;

    @Autowired
    public MqttRequestHandler(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    // method that handle new message from the topic
    public void handle(MqttMessage message) {
        try {
            // TODO: Zwischen JSON und Protobuf unterscheiden können
            SimpleScene.IposConfigWrapper iposConfigWrapper = ProtoJsonMap.fromJson(message.toString(), SimpleScene.IposConfigWrapper.class);
            // IposMonitoringRequest monReqProto = IposMonitoringRequest.parseFrom(message.getPayload());
            handleConfigWrapper(iposConfigWrapper);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    public static void handleConfigWrapper(SimpleScene.IposConfigWrapper iposConfigWrapper) {
        LOG.info("IposConfigWrapper received: \n"
                + iposConfigWrapper.toString());
        // Annahme: iposConfigWrapper enthält für leere Felder leere Listen und nicht null
        processMonitoringRequests(iposConfigWrapper);
        processRefSystems(iposConfigWrapper.getRefSystemsList());
        processFrameConfigs(iposConfigWrapper.getFramesList()); // should be done not before refSystem-processing, as it might require one of the processed refSystems
        processObjectConfigs(iposConfigWrapper.getObjectConfigsList());
        processPois(iposConfigWrapper.getPoisList());
        processQueryRequest(iposConfigWrapper.getQueryRequestsList());

        // this.jmsTemplate.convertAndSend("/request123", monReqInternal); // submit request to the internal broker
    }

    private static void processQueryRequest(List<SimpleScene.IposQueryRequest> queryRequestsList) {
        for (SimpleScene.IposQueryRequest proto_qReq : queryRequestsList){
            DataStorageQueryRequest internal_qReq = SimpleSceneTransformer.queryReq_SScene2Internal(proto_qReq);
            LOG.info("INDFRO:");
            LOG.info("INDFRO: Received QueryRequest for trackingTaskId " + internal_qReq.getTrackingTaskId());
            PositionMonitoring.receiveMessage(internal_qReq);
        }
    }

    /*
    private SimpleScene.IposConfigWrapper replaceNullByEmptyList(SimpleScene.IposConfigWrapper configWrapper) {
        SimpleScene.IposConfigWrapper.Builder configWrapper_NoNull = SimpleScene.IposConfigWrapper.newBuilder();
        configWrapper_NoNull.add;
    }
    */

    private static void processPois(List<SimpleScene.POI> poisList) {
        for (SimpleScene.POI poi : poisList){
            POI poi_internal = SimpleSceneTransformer.poi_SScene2Internal(poi);
            Administration.handlePoi(poi_internal);
        }
    }

    private static void processRefSystems(List<SimpleScene.RefSystem> refSystemsList) {
        for (SimpleScene.RefSystem refSystem : refSystemsList){
            ReferenceSystem refSystem_internal = SimpleSceneTransformer.refSystem_SScene2Internal(refSystem);
            Administration.handleRefSystem(refSystem_internal);
        }
    }

    private static void processObjectConfigs(List<SimpleScene.IposObjectConfig> objectConfigsList) {
        for( SimpleScene.IposObjectConfig objConfig : objectConfigsList){
            Agent agent = SimpleSceneTransformer.agent_SScene2Internal(objConfig);
            Administration.handleAgent(agent);
        }
    }

    private static void processFrameConfigs(List<SimpleScene.IposFrameConfig> frames){
        /*
        if (null == frames){
            LOG.info("ConfigWrapper-message contains no Frame-definitions");
            return;
        }
        */
        for( SimpleScene.IposFrameConfig frame : frames){
            Zone zone = SimpleSceneTransformer.zone_SScene2Internal(frame);
            Administration.handleZone(zone);
        }
    }

    private static void processMonitoringRequests(SimpleScene.IposConfigWrapper iposConfigWrapper) {
        for (IposMonitoringRequest monReq : iposConfigWrapper.getMonitoringRequestsList()){
            MonitoringRequest monReqInternal = SimpleSceneTransformer.monReq_SScene2Internal(monReq);
            PositionMonitoring.receiveMessage(monReqInternal);
            // TODO: send request via JMS to MonitoringController
        }
    }
}
