package ipos.project.DataModellntegration.SimpleSceneIntegration.service;

import com.google.protobuf.AbstractMessageLite;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface ExternalPubService {
    void publish(final String topic, final String msg , int qos, boolean retained);
    MqttMessage createMqttMsg(AbstractMessageLite message, int qos, boolean retained);
}
