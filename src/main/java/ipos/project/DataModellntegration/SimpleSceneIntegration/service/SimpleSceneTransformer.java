package ipos.project.DataModellntegration.SimpleSceneIntegration.service;

import com.google.protobuf.ByteString;
import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import com.google.protobuf.ProtocolStringList;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.UseCaseController.PositionMonitoring;
import ipos.project.devkit.utility.OtherUtility;
import org.apache.logging.log4j.LogManager;
import org.eclipse.emf.common.util.EList;

import java.util.*;
import java.util.stream.Collectors;

public class SimpleSceneTransformer {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    private static IPosDevKitFactory devKitFactory = IPosDevKitFactory.eINSTANCE;
    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;

    public static MonitoringRequest monReq_SScene2Internal(SimpleScene.IposMonitoringRequest monReqProto){
        MonitoringRequest monReqInt = devKitFactory.createMonitoringRequest();
        monReqInt.setFrameIds(new LinkedList<>());
        monReqInt.getFrameIds().addAll(monReqProto.getFrameIdsList());
        monReqInt.setDelta(monReqProto.getDelta());
        monReqInt.setUpdateFrequency(monReqProto.getUpdateFrequency());
        monReqInt.setType(toJavaStringList(monReqProto.getTypeList()));
        monReqInt.setId(toJavaStringList(monReqProto.getIdList()));
        monReqInt.setFusionStrategy(monReqProto.getFusionStrategy());
        monReqInt.setExitNotification(monReqProto.getExitNotification());
        monReqInt.setProperties(monReqProto.getPropertiesList());
        monReqInt.setMonitoringTaskId(monReqProto.getMonitoringTaskId());
        monReqInt.setRefSystemId(monReqProto.getRefSystemId());
        monReqInt.setRequestorProtocol(monReqProto.getRequestorProtocol());
        monReqInt.setSerializationType(monReqProto.getSerializationType());
        return monReqInt;
    }

    private static List<String> toJavaStringList(ProtocolStringList protoList){
        List<String> javaList = new ArrayList<>();
        for(int i = 0; i < protoList.size(); i++ ){
            javaList.add(protoList.get(i));
        }
        return javaList;
    }

    public static SimpleScene.IposMonitoringRequest monReq_Internal2SScene(){
        return null;
    }

    public static SimpleScene.IposPositionEvent posEvent_internal2Proto(PositionEvent internalPosEvent, String notificationType){
        SimpleScene.IposObject.Builder protoIposObject = constructProtoIposObjectFromPosEvent(internalPosEvent);
        if (protoIposObject == null) return null;
        SimpleScene.IposPositionEvent.Builder protoIposPosEvent = transformIntoProtoIposPosEvent(notificationType, protoIposObject);
        return protoIposPosEvent.build();
    }

    private static SimpleScene.IposObject.Builder constructProtoIposObjectFromPosEvent(PositionEvent internalPosEvent) {
        LocalizableObject lObject = PositionMonitoring.getLObjectByIdOrNull(internalPosEvent.getLObjectId());
        if (null == lObject || null == lObject.getAgent()) {
            LOG.error("Internal-PositionEvent could not be transformed into protobuf-format. " +
                    "No LocalizableObject with the provided sensor-id could be found, or no agent " +
                    "is associated to the LocalizableObject that has been found.");
            return null;
        }
        SimpleScene.IposPoint3D.Builder protoPoint3D = transformIntoProtoPoint3D(internalPosEvent);
        SimpleScene.IposPosition.Builder protoIposPosition = transformIntoProtoIposPosition(internalPosEvent, protoPoint3D);
        SimpleScene.IposSimpleOrientation.Builder protoOrientation = transformIntoProtoOrientation(internalPosEvent);
        List<SimpleScene.IposZoneDescriptor.Builder> zoneDescriptors = transformIntoProtoZoneDescriptorList(internalPosEvent);
        SimpleScene.IposObject.Builder protoIposObject = transformIntoProtoObject(internalPosEvent, lObject, protoIposPosition, protoOrientation, zoneDescriptors);
        return protoIposObject;
    }

    private static List<SimpleScene.IposZoneDescriptor.Builder> transformIntoProtoZoneDescriptorList(PositionEvent internalPosEvent) {
        return transformIntoProtoZoneDescriptorList(internalPosEvent.getZonedescriptors());
    }

    private static List<SimpleScene.IposZoneDescriptor.Builder> transformIntoProtoZoneDescriptorList(EList<ZoneDescriptor> zoneDescriptors) {
        List<SimpleScene.IposZoneDescriptor.Builder> zoneDescriptorList = new LinkedList<>();
        for ( ZoneDescriptor zoneDescriptor_internal : zoneDescriptors){
            SimpleScene.IposZoneDescriptor.Builder protoIposZoneDescriptor = SimpleScene.IposZoneDescriptor.newBuilder();
            protoIposZoneDescriptor.setZoneId(zoneDescriptor_internal.getZoneId());
            protoIposZoneDescriptor.setNotificationType(zoneDescriptor_internal.getNotificationType());
            zoneDescriptorList.add(protoIposZoneDescriptor);
        }
        return zoneDescriptorList;
    }

    private static SimpleScene.IposPositionEvent.Builder transformIntoProtoIposPosEvent(String notificationType, SimpleScene.IposObject.Builder protoIposObject) {
        SimpleScene.IposPositionEvent.Builder protoIposPosEvent = SimpleScene.IposPositionEvent.newBuilder();
        /*if (!PositionMonitoring.UNDEFINED_TYPE.equals(notificationType)) { // if undefined, protobuf-object will be created without explicit notificationtype-information
            protoIposPosEvent.setType(notificationType);
        }*/
        protoIposPosEvent.addObjects(protoIposObject);
        return protoIposPosEvent;
    }

    private static SimpleScene.IposObject.Builder transformIntoProtoObject(PositionEvent internalPosEvent, LocalizableObject lObject, SimpleScene.IposPosition.Builder protoIposPosition, SimpleScene.IposSimpleOrientation.Builder protoOrientation, List<SimpleScene.IposZoneDescriptor.Builder> zoneDescriptors) {
        SimpleScene.IposObject.Builder protoIposObject = SimpleScene.IposObject.newBuilder();
        protoIposObject.setSensorId(lObject.getId()).setSensorType(lObject.getSensorType()).setId(lObject.getAgent().getId()).setType(lObject.getAgent().getAgentType()).setOrientation(protoOrientation).setPosition(protoIposPosition).setLastPosUpdate(internalPosEvent.getTimeStamp());
        for (SimpleScene.IposZoneDescriptor.Builder zoneDescriptor : zoneDescriptors){
            protoIposObject.addZoneDescriptors(zoneDescriptor); //   getZoneDescriptorsBuilderList().addAll(zoneDescriptors);
        }
        return protoIposObject;
    }

    private static SimpleScene.IposSimpleOrientation.Builder transformIntoProtoOrientation(PositionEvent internalPosEvent) {
        return transformIntoProtoOrientation((Quaternion) internalPosEvent.getPlacing().getOrientation());
    }

    private static SimpleScene.IposSimpleOrientation.Builder transformIntoProtoOrientation(Quaternion internalOrientation) {
        SimpleScene.IposSimpleOrientation.Builder protoOrientation = SimpleScene.IposSimpleOrientation.newBuilder();
        protoOrientation.setX(internalOrientation.getX()).setY(internalOrientation.getY()).setZ(internalOrientation.getZ()).setW(internalOrientation.getW());
        return protoOrientation;
    }

    private static SimpleScene.IposPosition.Builder transformIntoProtoIposPosition(PositionEvent internalPosEvent, SimpleScene.IposPoint3D.Builder protoPoint3D) {
        return transformIntoProtoIposPosition(internalPosEvent.getPlacing().getPosition(), protoPoint3D);
    }

    private static SimpleScene.IposPosition.Builder transformIntoProtoIposPosition(Position internalPos, SimpleScene.IposPoint3D.Builder protoPoint3D) {
        float internalAccuracy = ((Gaussian) internalPos.getAccuracy()).getConfidenceInterval();
        String internalRefSystemId = internalPos.getReferenceSystem().getId();
        SimpleScene.IposPosition.Builder protoIposPosition = SimpleScene.IposPosition.newBuilder();
        protoIposPosition.setAccuracy(internalAccuracy).setRefSystemId(internalRefSystemId).setPoint(protoPoint3D);
        return protoIposPosition;
    }

    private static SimpleScene.IposPoint3D.Builder transformIntoProtoPoint3D(PositionEvent internalPosEvent) {
        return transformIntoProtoPoint3D(internalPosEvent.getPlacing().getPosition());
    }

    private static SimpleScene.IposPoint3D.Builder transformIntoProtoPoint3D(Position internalPos) {
        SimpleScene.IposPoint3D.Builder protoPoint3D = SimpleScene.IposPoint3D.newBuilder();
        Point3D internalPoint3D = (Point3D) internalPos.getPoint();
        protoPoint3D.setX(internalPoint3D.getX()).setY(internalPoint3D.getY()).setZ(internalPoint3D.getZ());
        return protoPoint3D;
    }

    public static Zone zone_SScene2Internal(SimpleScene.IposFrameConfig frame) {
        Zone zone = modelFactory.createZone();
        zone.setId(frame.getId());
        zone.setName(frame.getId());
        for (SimpleScene.IposSpace space_proto : frame.getSpaceList()){
            Space space = space_SScene2Internal(space_proto);
            zone.getSpace().add(space);
        }
        return zone;
    }

    private static Space space_SScene2Internal(SimpleScene.IposSpace space_proto) {
        Space space = modelFactory.createSpace();
        space.setX(space_proto.getX());
        space.setY(space_proto.getY());
        space.setZ(space_proto.getZ());
        space.setCentrePoint(placing_SScene2Internal(space_proto.getPosition(),space_proto.getOrientation()));
        return space;
    }

    private static Placing placing_SScene2Internal(SimpleScene.IposPosition position, SimpleScene.IposSimpleOrientation orientation) {
        Placing placing = modelFactory.createPlacing();
        placing.setPosition(position_SScene2Internal(position));
        placing.setOrientation(orientation_SScene2Internal(orientation));
        return placing;
    }

    private static Orientation orientation_SScene2Internal(SimpleScene.IposSimpleOrientation orientation_proto) {
    Quaternion orientation = modelFactory.createQuaternion();
    orientation.setX(orientation_proto.getX());
    orientation.setY(orientation_proto.getY());
    orientation.setZ(orientation_proto.getZ());
    orientation.setW(orientation_proto.getW());
    return orientation;
    }

    private static Position position_SScene2Internal(SimpleScene.IposPosition position_proto) {
        Position position = modelFactory.createPosition();
        position.setReferenceSystem(PositionMonitoring.getReferenceSystemByIdOrNull(position_proto.getRefSystemId()));
        position.setPoint(point_SScene2Internal(position_proto.getPoint()));
        position.setAccuracy(accuracy_SScene2Internal(position_proto.getAccuracy()));
        return position;
    }

    private static Accuracy accuracy_SScene2Internal(float accuracy_proto) {
        Gaussian accuracy = modelFactory.createGaussian();
        accuracy.setConfidenceInterval(accuracy_proto);
        return accuracy;
    }

    private static Point point_SScene2Internal(SimpleScene.IposPoint3D point_proto) {
        Point3D point = modelFactory.createPoint3D();
        point.setX(point_proto.getX());
        point.setY(point_proto.getY());
        point.setZ(point_proto.getZ());
        return point;
    }

    /**
     * It is the job of this function to create Agent- and LocalizableObject-objects.
     * If is not the job of this function to checker whether agent/lobject-Objects
     * do already exist for the respective ids.
     * @param objConfig
     * @return
     */
    public static Agent agent_SScene2Internal(SimpleScene.IposObjectConfig objConfig) {
        Agent agent = modelFactory.createAgent();
        agent.setAgentType(objConfig.getAgentType());
        agent.setId(objConfig.getAgentId());
        LocalizableObject lObject = modelFactory.createLocalizableObject();
        lObject.setSensorType(objConfig.getSensorType());
        lObject.setId(objConfig.getSensorId());
        lObject.setAgent(agent);
        agent.getLObject().add(lObject);
        return agent;
    }

    public static ReferenceSystem refSystem_SScene2Internal(SimpleScene.RefSystem refSystem_proto) {
        ReferenceSystem refSystem = modelFactory.createReferenceSystem();
        refSystem.setId(refSystem_proto.getId());
        refSystem.setName(refSystem_proto.getId());
        if (null != refSystem_proto.getPosition() && null != refSystem_proto.getOrientation()) {
            refSystem.setOrigin(placing_SScene2Internal(refSystem_proto.getPosition(), refSystem_proto.getOrientation()));
        }
        return refSystem;
    }

    public static POI poi_SScene2Internal(SimpleScene.POI poi_proto) {
        POI poi = modelFactory.createPOI();
        poi.setId(poi_proto.getId());
        poi.setDescription(poi_proto.getDescription());
        poi.setData(poi_proto.getDataMap());
        poi.setPlacing(placing_SScene2Internal(poi_proto.getPosition(), poi_proto.getOrientation()));
        return poi;
    }

    public static DataStorageQueryRequest queryReq_SScene2Internal(SimpleScene.IposQueryRequest proto_qReq) {
        DataStorageQueryRequest internal_queryReq = devKitFactory.createDataStorageQueryRequest();
        internal_queryReq.setTrackingTaskId(proto_qReq.getTrackingTaskId());
        return internal_queryReq;
    }

    public static SimpleScene.IposQueryResponse queryResp_Internal2SScene(DataStorageQueryResponse dsQueryResponse, String trackingTaskId) {
        SimpleScene.IposQueryResponse.Builder iposQueryResponse = SimpleScene.IposQueryResponse.newBuilder();
        iposQueryResponse.setTrackingTaskId(dsQueryResponse.getTrackingTaskId());
        List<SimpleScene.IposObject.Builder> iposObjects = transformIntoProtoObjectList(dsQueryResponse.getPositionEvents());
        for ( SimpleScene.IposObject.Builder iposObject : iposObjects){
            iposQueryResponse.addObjects(iposObject);
        }
        return iposQueryResponse.build();

    }

    private static List<SimpleScene.IposObject.Builder> transformIntoProtoObjectList(EList<PositionEvent> positionEvents) {
        List<SimpleScene.IposObject.Builder> iposObjectList = new LinkedList<>();
        for (PositionEvent posEvent : positionEvents){
            SimpleScene.IposObject.Builder protoIposObject = constructProtoIposObjectFromPosEvent(posEvent);
            if (protoIposObject == null) continue;
            iposObjectList.add(protoIposObject);
        }
        return iposObjectList;
    }

    public static SimpleScene.IposMsgRcvEvent iposMsgRcvEvent_internal2proto(IposMsgRcvEvent iposMsgRcvEvent_internal) {
        SimpleScene.IposMsgRcvEvent.Builder iposMsgRcvEvent_proto = SimpleScene.IposMsgRcvEvent.newBuilder();
        iposMsgRcvEvent_proto.setProtocolName(iposMsgRcvEvent_internal.getProtocolName());
        iposMsgRcvEvent_proto.setSerializedMsg(ByteString.copyFrom(iposMsgRcvEvent_internal.getSerializedMsg()));
        iposMsgRcvEvent_proto.setAgentId(iposMsgRcvEvent_internal.getAgentId());
        iposMsgRcvEvent_proto.setLastPosUpdate(Optional.ofNullable(iposMsgRcvEvent_internal.getLastPosUpdate()).orElse(""));
        List<SimpleScene.Attribute> attributeList = iposMsgRcvEvent_internal.getExtractedAttributes().stream().map(SimpleSceneTransformer::toAttribute_proto).collect(Collectors.toList());
        attributeList.stream().forEach(attribute -> iposMsgRcvEvent_proto.addExtractedAttributes(attribute));
        // iposMsgRcvEvent_proto.getExtractedAttributesList().addAll(attributeList);
        iposMsgRcvEvent_proto.setTimestamp(Optional.ofNullable(iposMsgRcvEvent_internal.getTimestamp()).orElse(""));
        if (null != iposMsgRcvEvent_internal.getLastKnownPosition()) {
            SimpleScene.IposPoint3D.Builder protoPoint3D = transformIntoProtoPoint3D(iposMsgRcvEvent_internal.getLastKnownPosition());
            iposMsgRcvEvent_proto.setLastKnownPosition(transformIntoProtoIposPosition(iposMsgRcvEvent_internal.getLastKnownPosition(), protoPoint3D));
        }
        if (null != iposMsgRcvEvent_internal.getLastKnownOrientation()) iposMsgRcvEvent_proto.setLastKnownOrientation(transformIntoProtoOrientation((Quaternion) iposMsgRcvEvent_internal.getLastKnownOrientation()));
        List<SimpleScene.IposZoneDescriptor> zoneDescriptors_proto = extractZoneDescriptors(iposMsgRcvEvent_internal);
        zoneDescriptors_proto.stream().forEach(zoneDescriptor -> iposMsgRcvEvent_proto.addLastKnownZoneDescriptors(zoneDescriptor));
        //iposMsgRcvEvent_proto.getLastKnownZoneDescriptorsList().addAll(extractZoneDescriptors(iposMsgRcvEvent_internal));
        return iposMsgRcvEvent_proto.build();
    }

    private static List<SimpleScene.IposZoneDescriptor> extractZoneDescriptors(IposMsgRcvEvent iposMsgRcvEvent_internal) {
        List<SimpleScene.IposZoneDescriptor.Builder> zoneDescriptorBuilders = transformIntoProtoZoneDescriptorList(iposMsgRcvEvent_internal.getLastKnownZonedescriptors());
        List<SimpleScene.IposZoneDescriptor> zoneDescriptors = zoneDescriptorBuilders.stream().map(zoneDescriptorBuilder -> zoneDescriptorBuilder.build()).collect(Collectors.toList());
        return zoneDescriptors;
    }

    private static SimpleScene.Attribute toAttribute_proto(HashMap<String, String> stringStringHashMap) {
        SimpleScene.Attribute.Builder attribute_proto = SimpleScene.Attribute.newBuilder();
        attribute_proto.setName(stringStringHashMap.get(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_NAME));
        attribute_proto.setType(stringStringHashMap.get(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_TYPE));
        attribute_proto.setData(stringStringHashMap.get(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_VALUE));
        return attribute_proto.build();
    }

}
