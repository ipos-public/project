package ipos.project.DataModellntegration.VDA5050Processor;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.GenericSensor;
import ipos.models.VDA5050;
import ipos.project.DataModellntegration.VDA5050Processor.api.MqttRequestHandler;
import ipos.project.SensorValueIntegration.api.MqttPositionHandler;
import ipos.project.devkit.utility.OtherUtility;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;

import java.io.File;
import java.util.Scanner;

import static ipos.project.devkit.utility.OtherUtility.waitUntilUserRequestsReadingNextLine;

public class VDA5050Processor {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    public static final String TOPIC_AGVSTATE = "VDA5050_AgvState";
    public static final String PROTOCOL_NAME_VDA5050 = "VDA5050";
    public static final String AGV_ID_CONNECTOR_CHARACTER = "-";
    public static final Object TYPE_STRING = "string";
    public static final String TYPE_FLOAT = "float";
    public static final String TYPE_LIST_OF_INT = "listOfInt";
    public static final CharSequence DELIMITER_CSV = ",";

    public static void processTestData(String path_to_test_data_file){
        File testDataFile = new File(path_to_test_data_file);
        Scanner scanner = new Scanner(System.in);
        try {
            for (String line : OtherUtility.readLines(testDataFile)) {
                VDA5050.AgvState agvState_proto = ProtoJsonMap.fromJson(line, VDA5050.AgvState.class);
                OtherUtility.waitUntilUserRequestsReadingNextLine(scanner);
                MqttRequestHandler.processAgvState(agvState_proto);
            }
        }catch (InvalidProtocolBufferException e) {
            LOG.error("Error trying to read JSON into protobuf-objects: ");
            e.printStackTrace();
        }
    }


}
