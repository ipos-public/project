package ipos.project.DataModellntegration.VDA5050Processor.api;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.SimpleScene;
import ipos.models.SimpleScene.IposMonitoringRequest;
import ipos.models.VDA5050;
import ipos.project.DataModellntegration.SimpleSceneIntegration.service.SimpleSceneTransformer;
import ipos.project.DataModellntegration.VDA5050Processor.VDA5050Processor;
import ipos.project.DataModellntegration.VDA5050Processor.service.VDA5050Transformer;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load;
import ipos.project.Functionality.DataServices;
import ipos.project.UseCaseController.Administration;
import ipos.project.UseCaseController.PositionMonitoring;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.trans.IPos2protoTransformer;
import ipos.project.devkit.utility.OtherUtility;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import java.util.*;
import java.util.stream.Collectors;


// subscribe to the topic. It's example
@MqttListener(VDA5050Processor.TOPIC_AGVSTATE)
public class MqttRequestHandler implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    JmsTemplate jmsTemplate;
    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;

    @Autowired
    public MqttRequestHandler(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    // method that handle new message from the topic
    public void handle(MqttMessage message) {
        try {
            // TODO: Zwischen JSON und Protobuf unterscheiden können
            //SimpleScene.IposConfigWrapper iposConfigWrapper = ProtoJsonMap.fromJson(message.toString(), SimpleScene.IposConfigWrapper.class);
            VDA5050.AgvState agvState = ProtoJsonMap.fromJson(message.toString(), VDA5050.AgvState.class);
            // VDA5050.AgvState agvState_proto = ProtoJsonMap.fromJson(line, VDA5050.AgvState.class);
            // IposMonitoringRequest monReqProto = IposMonitoringRequest.parseFrom(message.getPayload());
            processAgvState(agvState);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            LOG.info("VDA5050-message could not be read. " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void processAgvState(VDA5050.AgvState agvState_proto) {
        LOG.info("VDA5050-AgvState message received: \n" + agvState_proto.toString());
        AgvState agvState = VDA5050Transformer.agvState_proto2internal(agvState_proto);
        MessageReceivedEvent messageReceivedEvent = extractMessageReceivedEventFromAgvState(agvState, ProtoJsonMap.toJson(agvState_proto));
        LOG.info("VDA5050: Corresponding MessageReceivedEvent: " + messageReceivedEvent.toString());
        attemptToHandleAsPositionEvent(agvState);
        attemptToEvaluateLastNodeId(agvState, messageReceivedEvent.getAgentId());
        PositionMonitoring.receiveMessage(messageReceivedEvent);
        // this.jmsTemplate.convertAndSend("/request123", monReqInternal); // submit request to the internal broker
    }

    private static void attemptToEvaluateLastNodeId(AgvState agvState, String agentId) {
        // we prefer current positions over old positions communicated over lastNodeIds
        boolean noCurrentPositionAvailable = !containsValidAgvPosition(agvState);
        boolean lastNodeIdIsAvailable = null != agvState.getLastNodeId();
        if( noCurrentPositionAvailable && lastNodeIdIsAvailable){
            try {
                String lastNodeId = agvState.getLastNodeId();
                POI poi = Optional.ofNullable(DataServices.getPoiByIdOrNull(lastNodeId)).orElseThrow(() -> new NullPointerException("Message contained unknown lastNodeId: " + agvState.getLastNodeId()));
                LocalizableObject lObject = Optional.ofNullable(DataServices.getLObjectByIdOrNull(agentId)).orElseThrow(() -> new NullPointerException("Message was sent on behalf of an unknown agentId: " + agentId));
                if (null == lObject.getLastPosUpdate()){ // assumption: if lastPosUpdate is unset, there is no position information that would be more current than the VDA5050-lastNodeId
                    lObject.setCurrentPlacing(poi.getPlacing());
                    LOG.info("VDA5050: currentPlacing of localizable object " + lObject.getId() + " has been updated.");
                    return;
                }
            }catch(NullPointerException e){
                LOG.info("LastNodeId of VDA5050-message (AgvState) could not be processed. " + e.getMessage());
                return;
            }
        }
    }

    private static void attemptToHandleAsPositionEvent(AgvState agvState) {
        if(containsValidAgvPosition(agvState)){
            try {
                PositionEvent posEvent = extractPositionEvent(agvState);
                PositionMonitoring.receiveMessage(posEvent);
            }catch (NullPointerException e){
                LOG.info("Received message did not receive sufficient information to be handled as PositionEvent. " + e.getMessage());
            }
        }
    }

    private static PositionEvent extractPositionEvent(AgvState agvState) {
        PositionEvent posEvent = modelFactory.createPositionEvent();
        Objects.requireNonNull(agvState.getManufacturer(), "Message missed a required information: manufacturer");
        Objects.requireNonNull(agvState.getSerialNumber(), "Message missed a required information: serialNumber");
        String extractedRefSystem = Optional.ofNullable(agvState.getAgvposition().getMapId()).orElse(PositionMonitoring.SRS_ID_ROOT);
        Objects.requireNonNull(DataServices.getReferenceSystemByIdOrNull(extractedRefSystem), "Message contained a position that is associated an unknown reference system: " + extractedRefSystem);
        posEvent.setTimeStamp(Optional.ofNullable(agvState.getTimeStamp()).orElse(""));
        posEvent.setLObjectId(agvState.getManufacturer() + VDA5050Processor.AGV_ID_CONNECTOR_CHARACTER + agvState.getSerialNumber());
        posEvent.setPlacing(
            IPos2protoTransformer.createPlacing(
                    IPos2protoTransformer.createPosition(
                            IPos2protoTransformer.createPoint3D(
                                    agvState.getAgvposition().getX(),
                                    agvState.getAgvposition().getY(),
                                    0),
                            -1.0f,
                            DataServices.getReferenceSystemByIdOrNull(extractedRefSystem)),
                    IPos2protoTransformer.createOrientation(
                            0,
                            0,
                            0,
                            0
                    )
            )
        );
        return posEvent;
    }

    private static boolean containsValidAgvPosition(AgvState agvState) {
        return null != agvState.getAgvposition();
    }

    private static MessageReceivedEvent extractMessageReceivedEventFromAgvState(AgvState agvState, String serializedMsg) {
        Objects.requireNonNull(agvState.getManufacturer(), "Message missed a required information: manufacturer");
        Objects.requireNonNull(agvState.getSerialNumber(), "Message missed a required information: serialNumber");
        MessageReceivedEvent messageReceivedEvent = modelFactory.createMessageReceivedEvent();
        messageReceivedEvent.setProtocolName(VDA5050Processor.PROTOCOL_NAME_VDA5050);
        messageReceivedEvent.setSerializedMsg(serializedMsg.getBytes());
        messageReceivedEvent.setAgentId(agvState.getManufacturer() + VDA5050Processor.AGV_ID_CONNECTOR_CHARACTER + agvState.getSerialNumber());
        messageReceivedEvent.setExtractedAttributes(extractAgvStateAttributes(agvState));
        messageReceivedEvent.setTimestamp(Optional.ofNullable(agvState.getTimeStamp()).orElse(""));
        return messageReceivedEvent;
    }

    private static LinkedList<HashMap<String, String>> extractAgvStateAttributes(AgvState agvState) {
        LinkedList<HashMap<String, String>> extractedAttributes = new LinkedList<>();
        extractAgvStateBatteryChargeLevel(agvState, extractedAttributes);
        extractAgvStateLoads(agvState, extractedAttributes);
        extractAgvStateErrors(agvState, extractedAttributes);
        extractAgvStateTheta(agvState, extractedAttributes);
        extractAgvStateLastNodeId(agvState, extractedAttributes);
        return extractedAttributes;
    }

    private static void extractAgvStateLastNodeId(AgvState agvState, LinkedList<HashMap<String, String>> extractedAttributes) {
        if(null == agvState.getLastNodeId() || agvState.getLastNodeId().equals("")){
            return;
        }
        HashMap agvStateLastNodeId = new HashMap<String, String>();
        agvStateLastNodeId.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_NAME, "lastNodeId");
        agvStateLastNodeId.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_TYPE, VDA5050Processor.TYPE_STRING);
        agvStateLastNodeId.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_VALUE, agvState.getLastNodeId());
        extractedAttributes.add(agvStateLastNodeId);
    }

    private static void extractAgvStateErrors(AgvState agvState, LinkedList<HashMap<String, String>> extractedAttributes) {
        if(null == agvState.getErrors() || agvState.getErrors().size()==0){
            return;
        }
        HashMap agvStateErrors = new HashMap<String, String>();
        agvStateErrors.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_NAME, "errors");
        agvStateErrors.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_TYPE, VDA5050Processor.TYPE_LIST_OF_INT);
        List<String> errorIdsAsStringList = agvState.getErrors().stream().map(MqttRequestHandler::extractErrorId).collect(Collectors.toList());
        agvStateErrors.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_VALUE, String.join(VDA5050Processor.DELIMITER_CSV, errorIdsAsStringList));
        extractedAttributes.add(agvStateErrors);
    }

    private static void extractAgvStateLoads(AgvState agvState, LinkedList<HashMap<String, String>> extractedAttributes) {
        if(null == agvState.getLoads() || agvState.getLoads().size() == 0){
            return;
        }
        HashMap agvStateLoads = new HashMap<String, String>();
        agvStateLoads.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_NAME, "loads");
        agvStateLoads.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_TYPE, VDA5050Processor.TYPE_LIST_OF_INT);
        List<String> loadIdsAsStringList = agvState.getLoads().stream().map(MqttRequestHandler::extractLoadId).collect(Collectors.toList());
        agvStateLoads.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_VALUE, String.join(VDA5050Processor.DELIMITER_CSV, loadIdsAsStringList));
        extractedAttributes.add(agvStateLoads);
    }

    private static String extractErrorId(Error error) {
        return error.getErrorType();
    }

    private static java.lang.String extractLoadId(Load load) {
        return load.getLoadId();
    }

    private static void extractAgvStateBatteryChargeLevel(AgvState agvState, LinkedList<HashMap<String, String>> extractedAttributes) {
        if(null == agvState.getBatteryState()){
            return;
        }
        HashMap agvStateBatteryCharge = new HashMap<String, String>();
        agvStateBatteryCharge.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_NAME, "batteryCharge");
        agvStateBatteryCharge.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_TYPE, VDA5050Processor.TYPE_FLOAT);
        agvStateBatteryCharge.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_VALUE, "" + agvState.getBatteryState().getBatteryCharge());
        extractedAttributes.add(agvStateBatteryCharge);
    }

    private static void extractAgvStateTheta(AgvState agvState, LinkedList<HashMap<String, String>> extractedAttributes) {
        if(null == agvState.getAgvposition()){
            return;
        }
        HashMap agvStateTheta = new HashMap<String, String>();
        agvStateTheta.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_NAME, "theta");
        agvStateTheta.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_TYPE, VDA5050Processor.TYPE_FLOAT);
        agvStateTheta.put(OtherUtility.KEY_EXTRACTED_ATTRIBUTE_VALUE, "" + agvState.getAgvposition().getTheta());
        extractedAttributes.add(agvStateTheta);
    }

    private static void processQueryRequest(List<SimpleScene.IposQueryRequest> queryRequestsList) {
        for (SimpleScene.IposQueryRequest proto_qReq : queryRequestsList){
            DataStorageQueryRequest internal_qReq = SimpleSceneTransformer.queryReq_SScene2Internal(proto_qReq);
            LOG.info("INDFRO:");
            LOG.info("INDFRO: Received QueryRequest for trackingTaskId " + internal_qReq.getTrackingTaskId());
            PositionMonitoring.receiveMessage(internal_qReq);
        }
    }

    /*
    private SimpleScene.IposConfigWrapper replaceNullByEmptyList(SimpleScene.IposConfigWrapper configWrapper) {
        SimpleScene.IposConfigWrapper.Builder configWrapper_NoNull = SimpleScene.IposConfigWrapper.newBuilder();
        configWrapper_NoNull.add;
    }
    */

    private static void processPois(List<SimpleScene.POI> poisList) {
        for (SimpleScene.POI poi : poisList){
            POI poi_internal = SimpleSceneTransformer.poi_SScene2Internal(poi);
            Administration.handlePoi(poi_internal);
        }
    }

    private static void processRefSystems(List<SimpleScene.RefSystem> refSystemsList) {
        for (SimpleScene.RefSystem refSystem : refSystemsList){
            ReferenceSystem refSystem_internal = SimpleSceneTransformer.refSystem_SScene2Internal(refSystem);
            Administration.handleRefSystem(refSystem_internal);
        }
    }

    private static void processObjectConfigs(List<SimpleScene.IposObjectConfig> objectConfigsList) {
        for( SimpleScene.IposObjectConfig objConfig : objectConfigsList){
            Agent agent = SimpleSceneTransformer.agent_SScene2Internal(objConfig);
            Administration.handleAgent(agent);
        }
    }

    private static void processFrameConfigs(List<SimpleScene.IposFrameConfig> frames){
        /*
        if (null == frames){
            LOG.info("ConfigWrapper-message contains no Frame-definitions");
            return;
        }
        */
        for( SimpleScene.IposFrameConfig frame : frames){
            Zone zone = SimpleSceneTransformer.zone_SScene2Internal(frame);
            Administration.handleZone(zone);
        }
    }

    private static void processMonitoringRequests(SimpleScene.IposConfigWrapper iposConfigWrapper) {
        for (IposMonitoringRequest monReq : iposConfigWrapper.getMonitoringRequestsList()){
            MonitoringRequest monReqInternal = SimpleSceneTransformer.monReq_SScene2Internal(monReq);
            PositionMonitoring.receiveMessage(monReqInternal);
            // TODO: send request via JMS to MonitoringController
        }
    }
}
