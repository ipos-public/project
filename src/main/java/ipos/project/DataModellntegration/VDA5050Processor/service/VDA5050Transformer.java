package ipos.project.DataModellntegration.VDA5050Processor.service;

import ipos.models.VDA5050;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.*;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error;
import org.apache.logging.log4j.LogManager;

import java.util.LinkedList;
import java.util.List;

public class VDA5050Transformer {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    private static VDA5050Factory vda5050Factory = VDA5050Factory.eINSTANCE;

    public static AgvState agvState_proto2internal(VDA5050.AgvState agvState_proto) {
        AgvState agvState_internal = vda5050Factory.createAgvState();
        agvState_internal.setTimeStamp(agvState_proto.getTimeStamp());
        agvState_internal.setSerialNumber(agvState_proto.getSerialNumber());
        agvState_internal.setLastNodeId(agvState_proto.getLastNodeId());
        agvState_internal.setManufacturer(agvState_proto.getManufacturer());
        agvState_internal.setBatteryState(extractBatteryStateOrNull(agvState_proto));
        agvState_internal.getErrors().addAll(extractErrorsOrEmptyList(agvState_proto));
        agvState_internal.getLoads().addAll(extractLoadsOrEmptyList(agvState_proto));
        agvState_internal.setAgvposition(extractAgvPositionOrNull(agvState_proto));
        return agvState_internal;
    }

    private static AgvPosition extractAgvPositionOrNull(VDA5050.AgvState agvState_proto) {
        boolean agvPositionNotPresentInMessage = ipos.models.VDA5050.AgvPosition.getDefaultInstance() == agvState_proto.getAgvPosition();
        if (agvPositionNotPresentInMessage){
            return null;
        }else {
            AgvPosition agvPosition_internal = vda5050Factory.createAgvPosition();
            agvPosition_internal.setX(agvState_proto.getAgvPosition().getX());
            agvPosition_internal.setY(agvState_proto.getAgvPosition().getY());
            agvPosition_internal.setTheta(agvState_proto.getAgvPosition().getTheta());
            return agvPosition_internal;
        }
    }

    private static BatteryState extractBatteryStateOrNull(VDA5050.AgvState agvState_proto) {
        if (ipos.models.VDA5050.BatteryState.getDefaultInstance() == agvState_proto.getBatteryState()){
            return null;
        }else {
            BatteryState batteryState = vda5050Factory.createBatteryState();
            batteryState.setBatteryCharge(agvState_proto.getBatteryState().getBatteryCharge());
            return batteryState;
        }
    }

    private static List<ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error> extractErrorsOrEmptyList(VDA5050.AgvState agvState_proto){
        LinkedList<ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error> errors = new LinkedList<>();
        for (VDA5050.Error error_proto : agvState_proto.getErrorsList()){
            Error error_internal = vda5050Factory.createError();
            error_internal.setErrorType(error_proto.getErrorType());
            errors.add(error_internal);
        }
        return errors;
    }

    private static List<ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load> extractLoadsOrEmptyList(VDA5050.AgvState agvState_proto){
        LinkedList<ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load> loads = new LinkedList<>();
        for (VDA5050.Load load_proto : agvState_proto.getLoadsList()){
            Load load_internal = vda5050Factory.createLoad();
            load_internal.setLoadId(load_proto.getLoadId());
            loads.add(load_internal);
        }
        return loads;
    }
}
