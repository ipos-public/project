/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Agent#getLObject <em>LObject</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Agent#getAgentType <em>Agent Type</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getAgent()
 * @model
 * @generated
 */
public interface Agent extends Entity {
	/**
	 * Returns the value of the '<em><b>LObject</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LObject</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getAgent_LObject()
	 * @model
	 * @generated
	 */
	EList<LocalizableObject> getLObject();

	/**
	 * Returns the value of the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Type</em>' attribute.
	 * @see #setAgentType(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getAgent_AgentType()
	 * @model
	 * @generated
	 */
	String getAgentType();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Agent#getAgentType <em>Agent Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Type</em>' attribute.
	 * @see #getAgentType()
	 * @generated
	 */
	void setAgentType(String value);

} // Agent
