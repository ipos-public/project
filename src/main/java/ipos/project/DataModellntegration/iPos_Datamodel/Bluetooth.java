/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bluetooth</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth#getRss <em>Rss</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getBluetooth()
 * @model
 * @generated
 */
public interface Bluetooth extends Beacon {
	/**
	 * Returns the value of the '<em><b>Rss</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rss</em>' attribute.
	 * @see #setRss(Map)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getBluetooth_Rss()
	 * @model transient="true"
	 * @generated
	 */
	Map<String, Double> getRss();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth#getRss <em>Rss</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rss</em>' attribute.
	 * @see #getRss()
	 * @generated
	 */
	void setRss(Map<String, Double> value);

} // Bluetooth
