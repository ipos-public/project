/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Storage Query Response</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse#getPositionEvents <em>Position Events</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse#getTrackingTaskId <em>Tracking Task Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getDataStorageQueryResponse()
 * @model
 * @generated
 */
public interface DataStorageQueryResponse extends EObject {
	/**
	 * Returns the value of the '<em><b>Position Events</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Events</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getDataStorageQueryResponse_PositionEvents()
	 * @model
	 * @generated
	 */
	EList<PositionEvent> getPositionEvents();

	/**
	 * Returns the value of the '<em><b>Tracking Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tracking Task Id</em>' attribute.
	 * @see #setTrackingTaskId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getDataStorageQueryResponse_TrackingTaskId()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getTrackingTaskId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse#getTrackingTaskId <em>Tracking Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tracking Task Id</em>' attribute.
	 * @see #getTrackingTaskId()
	 * @generated
	 */
	void setTrackingTaskId(String value);

} // DataStorageQueryResponse
