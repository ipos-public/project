/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Filter Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getTimeCondition <em>Time Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getAccuracyCondition <em>Accuracy Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionCondition <em>Position Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getTimeMinInterval <em>Time Min Interval</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionDelta <em>Position Delta</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getSensorIdCondition <em>Sensor Id Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getFilterStructure <em>Filter Structure</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionConditionCells <em>Position Condition Cells</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getIdCondition <em>Id Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getCategoryCondition <em>Category Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPropertyCondition <em>Property Condition</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition()
 * @model
 * @generated
 */
public interface EventFilterCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>Time Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Condition</em>' attribute.
	 * @see #setTimeCondition(ArrayList)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_TimeCondition()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringArray" transient="true"
	 * @generated
	 */
	ArrayList<String[]> getTimeCondition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getTimeCondition <em>Time Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Condition</em>' attribute.
	 * @see #getTimeCondition()
	 * @generated
	 */
	void setTimeCondition(ArrayList<String[]> value);

	/**
	 * Returns the value of the '<em><b>Accuracy Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accuracy Condition</em>' attribute.
	 * @see #setAccuracyCondition(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_AccuracyCondition()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float" transient="true"
	 * @generated
	 */
	float getAccuracyCondition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getAccuracyCondition <em>Accuracy Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accuracy Condition</em>' attribute.
	 * @see #getAccuracyCondition()
	 * @generated
	 */
	void setAccuracyCondition(float value);

	/**
	 * Returns the value of the '<em><b>Position Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Condition</em>' attribute.
	 * @see #setPositionCondition(ArrayList)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_PositionCondition()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.FloatArray" transient="true"
	 * @generated
	 */
	ArrayList<Float[]> getPositionCondition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionCondition <em>Position Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position Condition</em>' attribute.
	 * @see #getPositionCondition()
	 * @generated
	 */
	void setPositionCondition(ArrayList<Float[]> value);

	/**
	 * Returns the value of the '<em><b>Time Min Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Min Interval</em>' attribute.
	 * @see #setTimeMinInterval(int)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_TimeMinInterval()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 * @generated
	 */
	int getTimeMinInterval();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getTimeMinInterval <em>Time Min Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Min Interval</em>' attribute.
	 * @see #getTimeMinInterval()
	 * @generated
	 */
	void setTimeMinInterval(int value);

	/**
	 * Returns the value of the '<em><b>Position Delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Delta</em>' attribute.
	 * @see #setPositionDelta(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_PositionDelta()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float" transient="true"
	 * @generated
	 */
	float getPositionDelta();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionDelta <em>Position Delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position Delta</em>' attribute.
	 * @see #getPositionDelta()
	 * @generated
	 */
	void setPositionDelta(float value);

	/**
	 * Returns the value of the '<em><b>Sensor Id Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sensor Id Condition</em>' attribute.
	 * @see #setSensorIdCondition(List)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_SensorIdCondition()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringList" transient="true"
	 * @generated
	 */
	List<String> getSensorIdCondition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getSensorIdCondition <em>Sensor Id Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sensor Id Condition</em>' attribute.
	 * @see #getSensorIdCondition()
	 * @generated
	 */
	void setSensorIdCondition(List<String> value);

	/**
	 * Returns the value of the '<em><b>Filter Structure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Structure</em>' attribute.
	 * @see #setFilterStructure(boolean[])
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_FilterStructure()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.BooleanList" transient="true"
	 * @generated
	 */
	boolean[] getFilterStructure();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getFilterStructure <em>Filter Structure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Structure</em>' attribute.
	 * @see #getFilterStructure()
	 * @generated
	 */
	void setFilterStructure(boolean[] value);

	/**
	 * Returns the value of the '<em><b>Position Condition Cells</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Condition Cells</em>' attribute.
	 * @see #setPositionConditionCells(Map)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_PositionConditionCells()
	 * @model transient="true"
	 * @generated
	 */
	Map<String, ArrayList<Float[][]>> getPositionConditionCells();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionConditionCells <em>Position Condition Cells</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position Condition Cells</em>' attribute.
	 * @see #getPositionConditionCells()
	 * @generated
	 */
	void setPositionConditionCells(Map<String, ArrayList<Float[][]>> value);

	/**
	 * Returns the value of the '<em><b>Id Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Condition</em>' attribute.
	 * @see #setIdCondition(List)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_IdCondition()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringList"
	 * @generated
	 */
	List<String> getIdCondition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getIdCondition <em>Id Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Condition</em>' attribute.
	 * @see #getIdCondition()
	 * @generated
	 */
	void setIdCondition(List<String> value);

	/**
	 * Returns the value of the '<em><b>Category Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category Condition</em>' attribute.
	 * @see #setCategoryCondition(List)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_CategoryCondition()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringList"
	 * @generated
	 */
	List<String> getCategoryCondition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getCategoryCondition <em>Category Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category Condition</em>' attribute.
	 * @see #getCategoryCondition()
	 * @generated
	 */
	void setCategoryCondition(List<String> value);

	/**
	 * Returns the value of the '<em><b>Property Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Condition</em>' attribute.
	 * @see #setPropertyCondition(List)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterCondition_PropertyCondition()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringList"
	 * @generated
	 */
	List<String> getPropertyCondition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPropertyCondition <em>Property Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Condition</em>' attribute.
	 * @see #getPropertyCondition()
	 * @generated
	 */
	void setPropertyCondition(List<String> value);

} // EventFilterCondition
