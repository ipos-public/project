/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Filter Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getPositionAmbiguityStrategy <em>Position Ambiguity Strategy</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getPositionAmbiguityParameters <em>Position Ambiguity Parameters</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getFilterCriteria <em>Filter Criteria</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getEventfiltercondition <em>Eventfiltercondition</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterConfiguration()
 * @model
 * @generated
 */
public interface EventFilterConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Position Ambiguity Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Ambiguity Strategy</em>' attribute.
	 * @see #setPositionAmbiguityStrategy(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterConfiguration_PositionAmbiguityStrategy()
	 * @model
	 * @generated
	 */
	String getPositionAmbiguityStrategy();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getPositionAmbiguityStrategy <em>Position Ambiguity Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position Ambiguity Strategy</em>' attribute.
	 * @see #getPositionAmbiguityStrategy()
	 * @generated
	 */
	void setPositionAmbiguityStrategy(String value);

	/**
	 * Returns the value of the '<em><b>Position Ambiguity Parameters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Ambiguity Parameters</em>' attribute.
	 * @see #setPositionAmbiguityParameters(Map)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterConfiguration_PositionAmbiguityParameters()
	 * @model transient="true"
	 * @generated
	 */
	Map<String, String> getPositionAmbiguityParameters();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getPositionAmbiguityParameters <em>Position Ambiguity Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position Ambiguity Parameters</em>' attribute.
	 * @see #getPositionAmbiguityParameters()
	 * @generated
	 */
	void setPositionAmbiguityParameters(Map<String, String> value);

	/**
	 * Returns the value of the '<em><b>Filter Criteria</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Criteria</em>' attribute.
	 * @see #setFilterCriteria(Map)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterConfiguration_FilterCriteria()
	 * @model transient="true"
	 * @generated
	 */
	Map<String, EObject> getFilterCriteria();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getFilterCriteria <em>Filter Criteria</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Criteria</em>' attribute.
	 * @see #getFilterCriteria()
	 * @generated
	 */
	void setFilterCriteria(Map<String, EObject> value);

	/**
	 * Returns the value of the '<em><b>Eventfiltercondition</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eventfiltercondition</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getEventFilterConfiguration_Eventfiltercondition()
	 * @model
	 * @generated
	 */
	EList<TrackingTask> getEventfiltercondition();

} // EventFilterConfiguration
