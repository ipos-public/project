/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gaussian</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Gaussian#getConfidenceInterval <em>Confidence Interval</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getGaussian()
 * @model
 * @generated
 */
public interface Gaussian extends Accuracy {
	/**
	 * Returns the value of the '<em><b>Confidence Interval</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Interval</em>' attribute.
	 * @see #setConfidenceInterval(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getGaussian_ConfidenceInterval()
	 * @model default="0.0"
	 * @generated
	 */
	float getConfidenceInterval();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Gaussian#getConfidenceInterval <em>Confidence Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Interval</em>' attribute.
	 * @see #getConfidenceInterval()
	 * @generated
	 */
	void setConfidenceInterval(float value);

} // Gaussian
