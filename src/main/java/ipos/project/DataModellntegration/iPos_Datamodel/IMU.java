/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IMU</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getAngularrate <em>Angularrate</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getAcceleration <em>Acceleration</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getSensorId <em>Sensor Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getIMU()
 * @model
 * @generated
 */
public interface IMU extends RawdataEvent {
	/**
	 * Returns the value of the '<em><b>Angularrate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Angularrate</em>' reference.
	 * @see #setAngularrate(Quaternion)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getIMU_Angularrate()
	 * @model required="true"
	 * @generated
	 */
	Quaternion getAngularrate();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getAngularrate <em>Angularrate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Angularrate</em>' reference.
	 * @see #getAngularrate()
	 * @generated
	 */
	void setAngularrate(Quaternion value);

	/**
	 * Returns the value of the '<em><b>Acceleration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acceleration</em>' reference.
	 * @see #setAcceleration(Acceleration)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getIMU_Acceleration()
	 * @model required="true"
	 * @generated
	 */
	Acceleration getAcceleration();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getAcceleration <em>Acceleration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acceleration</em>' reference.
	 * @see #getAcceleration()
	 * @generated
	 */
	void setAcceleration(Acceleration value);

	/**
	 * Returns the value of the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sensor Id</em>' attribute.
	 * @see #setSensorId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getIMU_SensorId()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getSensorId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getSensorId <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sensor Id</em>' attribute.
	 * @see #getSensorId()
	 * @generated
	 */
	void setSensorId(String value);

} // IMU
