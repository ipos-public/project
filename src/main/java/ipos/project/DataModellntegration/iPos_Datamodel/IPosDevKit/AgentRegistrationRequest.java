/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent Registration Request</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getAgentRegistrationRequest()
 * @model
 * @generated
 */
public interface AgentRegistrationRequest extends EObject {
} // AgentRegistrationRequest
