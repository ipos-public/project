/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Storage Query Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest#getTrackingTaskId <em>Tracking Task Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getDataStorageQueryRequest()
 * @model
 * @generated
 */
public interface DataStorageQueryRequest extends EObject {
	/**
	 * Returns the value of the '<em><b>Tracking Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tracking Task Id</em>' attribute.
	 * @see #setTrackingTaskId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getDataStorageQueryRequest_TrackingTaskId()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getTrackingTaskId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest#getTrackingTaskId <em>Tracking Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tracking Task Id</em>' attribute.
	 * @see #getTrackingTaskId()
	 * @generated
	 */
	void setTrackingTaskId(String value);

} // DataStorageQueryRequest
