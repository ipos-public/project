/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPos Barcode Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosBarcodeEvent()
 * @model
 * @generated
 */
public interface IPosBarcodeEvent extends IPosProximityEvent {
} // IPosBarcodeEvent
