/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPos Beacon Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent#getDistances <em>Distances</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosBeaconEvent()
 * @model
 * @generated
 */
public interface IPosBeaconEvent extends IPosRawdataEvent {
	/**
	 * Returns the value of the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distances</em>' attribute.
	 * @see #setDistances(Map)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosBeaconEvent_Distances()
	 * @model transient="true"
	 * @generated
	 */
	Map<String, Double> getDistances();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent#getDistances <em>Distances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distances</em>' attribute.
	 * @see #getDistances()
	 * @generated
	 */
	void setDistances(Map<String, Double> value);

} // IPosBeaconEvent
