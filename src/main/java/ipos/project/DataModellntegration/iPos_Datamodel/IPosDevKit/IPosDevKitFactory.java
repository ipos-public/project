/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage
 * @generated
 */
public interface IPosDevKitFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IPosDevKitFactory eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Tracking Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tracking Request</em>'.
	 * @generated
	 */
	TrackingRequest createTrackingRequest();

	/**
	 * Returns a new object of class '<em>Monitoring Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitoring Request</em>'.
	 * @generated
	 */
	MonitoringRequest createMonitoringRequest();

	/**
	 * Returns a new object of class '<em>World Model Update Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>World Model Update Request</em>'.
	 * @generated
	 */
	WorldModelUpdateRequest createWorldModelUpdateRequest();

	/**
	 * Returns a new object of class '<em>Data Storage Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Storage Request</em>'.
	 * @generated
	 */
	DataStorageRequest createDataStorageRequest();

	/**
	 * Returns a new object of class '<em>World Model Query Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>World Model Query Request</em>'.
	 * @generated
	 */
	WorldModelQueryRequest createWorldModelQueryRequest();

	/**
	 * Returns a new object of class '<em>Data Storage Query Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Storage Query Request</em>'.
	 * @generated
	 */
	DataStorageQueryRequest createDataStorageQueryRequest();

	/**
	 * Returns a new object of class '<em>Agent Registration Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agent Registration Request</em>'.
	 * @generated
	 */
	AgentRegistrationRequest createAgentRegistrationRequest();

	/**
	 * Returns a new object of class '<em>Sensor Configuration Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sensor Configuration Request</em>'.
	 * @generated
	 */
	SensorConfigurationRequest createSensorConfigurationRequest();

	/**
	 * Returns a new object of class '<em>Ipos Position Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ipos Position Event</em>'.
	 * @generated
	 */
	IposPositionEvent createIposPositionEvent();

	/**
	 * Returns a new object of class '<em>IPos Rawdata Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos Rawdata Event</em>'.
	 * @generated
	 */
	IPosRawdataEvent createIPosRawdataEvent();

	/**
	 * Returns a new object of class '<em>IPos Beacon Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos Beacon Event</em>'.
	 * @generated
	 */
	IPosBeaconEvent createIPosBeaconEvent();

	/**
	 * Returns a new object of class '<em>IPos UWB Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos UWB Event</em>'.
	 * @generated
	 */
	IPosUWBEvent createIPosUWBEvent();

	/**
	 * Returns a new object of class '<em>IPos BT Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos BT Event</em>'.
	 * @generated
	 */
	IPosBTEvent createIPosBTEvent();

	/**
	 * Returns a new object of class '<em>IPos Proximity Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos Proximity Event</em>'.
	 * @generated
	 */
	IPosProximityEvent createIPosProximityEvent();

	/**
	 * Returns a new object of class '<em>IPos NFC Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos NFC Event</em>'.
	 * @generated
	 */
	IPosNFCEvent createIPosNFCEvent();

	/**
	 * Returns a new object of class '<em>IPos Barcode Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos Barcode Event</em>'.
	 * @generated
	 */
	IPosBarcodeEvent createIPosBarcodeEvent();

	/**
	 * Returns a new object of class '<em>IPos RFID Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos RFID Event</em>'.
	 * @generated
	 */
	IPosRFIDEvent createIPosRFIDEvent();

	/**
	 * Returns a new object of class '<em>IPos Other Prox Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos Other Prox Event</em>'.
	 * @generated
	 */
	IPosOtherProxEvent createIPosOtherProxEvent();

	/**
	 * Returns a new object of class '<em>IPos Other Beacon Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPos Other Beacon Event</em>'.
	 * @generated
	 */
	IPosOtherBeaconEvent createIPosOtherBeaconEvent();

	/**
	 * Returns a new object of class '<em>Ipos Msg Rcv Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ipos Msg Rcv Event</em>'.
	 * @generated
	 */
	IposMsgRcvEvent createIposMsgRcvEvent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	IPosDevKitPackage getIPosDevKitPackage();

} //IPosDevKitFactory
