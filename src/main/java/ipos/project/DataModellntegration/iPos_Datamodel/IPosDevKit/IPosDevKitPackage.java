/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory
 * @model kind="package"
 * @generated
 */
public interface IPosDevKitPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "IPosDevKit";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "IDK";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "IDK";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IPosDevKitPackage eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.TrackingRequestImpl <em>Tracking Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.TrackingRequestImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getTrackingRequest()
	 * @generated
	 */
	int TRACKING_REQUEST = 0;

	/**
	 * The number of structural features of the '<em>Tracking Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACKING_REQUEST_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Tracking Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACKING_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl <em>Monitoring Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getMonitoringRequest()
	 * @generated
	 */
	int MONITORING_REQUEST = 1;

	/**
	 * The feature id for the '<em><b>Frame Ids</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__FRAME_IDS = 0;

	/**
	 * The feature id for the '<em><b>Delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__DELTA = 1;

	/**
	 * The feature id for the '<em><b>Update Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__UPDATE_FREQUENCY = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__TYPE = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__ID = 4;

	/**
	 * The feature id for the '<em><b>Fusion Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__FUSION_STRATEGY = 5;

	/**
	 * The feature id for the '<em><b>Exit Notification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__EXIT_NOTIFICATION = 6;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__PROPERTIES = 7;

	/**
	 * The feature id for the '<em><b>Monitoring Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__MONITORING_TASK_ID = 8;

	/**
	 * The feature id for the '<em><b>Requestor Protocol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__REQUESTOR_PROTOCOL = 9;

	/**
	 * The feature id for the '<em><b>Serialization Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__SERIALIZATION_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Ref System Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST__REF_SYSTEM_ID = 11;

	/**
	 * The number of structural features of the '<em>Monitoring Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST_FEATURE_COUNT = 12;

	/**
	 * The number of operations of the '<em>Monitoring Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.WorldModelUpdateRequestImpl <em>World Model Update Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.WorldModelUpdateRequestImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getWorldModelUpdateRequest()
	 * @generated
	 */
	int WORLD_MODEL_UPDATE_REQUEST = 2;

	/**
	 * The number of structural features of the '<em>World Model Update Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL_UPDATE_REQUEST_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>World Model Update Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL_UPDATE_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageRequestImpl <em>Data Storage Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageRequestImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getDataStorageRequest()
	 * @generated
	 */
	int DATA_STORAGE_REQUEST = 3;

	/**
	 * The number of structural features of the '<em>Data Storage Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_REQUEST_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Data Storage Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.WorldModelQueryRequestImpl <em>World Model Query Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.WorldModelQueryRequestImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getWorldModelQueryRequest()
	 * @generated
	 */
	int WORLD_MODEL_QUERY_REQUEST = 4;

	/**
	 * The number of structural features of the '<em>World Model Query Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL_QUERY_REQUEST_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>World Model Query Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL_QUERY_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageQueryRequestImpl <em>Data Storage Query Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageQueryRequestImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getDataStorageQueryRequest()
	 * @generated
	 */
	int DATA_STORAGE_QUERY_REQUEST = 5;

	/**
	 * The feature id for the '<em><b>Tracking Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_QUERY_REQUEST__TRACKING_TASK_ID = 0;

	/**
	 * The number of structural features of the '<em>Data Storage Query Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_QUERY_REQUEST_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Data Storage Query Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_QUERY_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.AgentRegistrationRequestImpl <em>Agent Registration Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.AgentRegistrationRequestImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getAgentRegistrationRequest()
	 * @generated
	 */
	int AGENT_REGISTRATION_REQUEST = 6;

	/**
	 * The number of structural features of the '<em>Agent Registration Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_REGISTRATION_REQUEST_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Agent Registration Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_REGISTRATION_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.SensorConfigurationRequestImpl <em>Sensor Configuration Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.SensorConfigurationRequestImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getSensorConfigurationRequest()
	 * @generated
	 */
	int SENSOR_CONFIGURATION_REQUEST = 7;

	/**
	 * The number of structural features of the '<em>Sensor Configuration Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR_CONFIGURATION_REQUEST_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Sensor Configuration Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR_CONFIGURATION_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl <em>Ipos Position Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIposPositionEvent()
	 * @generated
	 */
	int IPOS_POSITION_EVENT = 8;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT__AGENT_ID = 0;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT__SENSOR_ID = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT__TYPE = 2;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT__SENSOR_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Last Pos Update</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT__LAST_POS_UPDATE = 4;

	/**
	 * The feature id for the '<em><b>Position</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT__POSITION = 5;

	/**
	 * The feature id for the '<em><b>Orientation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT__ORIENTATION = 6;

	/**
	 * The feature id for the '<em><b>Zone Descriptors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT__ZONE_DESCRIPTORS = 7;

	/**
	 * The number of structural features of the '<em>Ipos Position Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Ipos Position Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_POSITION_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl <em>IPos Rawdata Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosRawdataEvent()
	 * @generated
	 */
	int IPOS_RAWDATA_EVENT = 9;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RAWDATA_EVENT__AGENT_ID = 0;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RAWDATA_EVENT__SENSOR_ID = 1;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RAWDATA_EVENT__AGENT_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RAWDATA_EVENT__SENSOR_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RAWDATA_EVENT__TIME_STAMP = 4;

	/**
	 * The number of structural features of the '<em>IPos Rawdata Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RAWDATA_EVENT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>IPos Rawdata Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RAWDATA_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBeaconEventImpl <em>IPos Beacon Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBeaconEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosBeaconEvent()
	 * @generated
	 */
	int IPOS_BEACON_EVENT = 10;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BEACON_EVENT__AGENT_ID = IPOS_RAWDATA_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BEACON_EVENT__SENSOR_ID = IPOS_RAWDATA_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BEACON_EVENT__AGENT_TYPE = IPOS_RAWDATA_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BEACON_EVENT__SENSOR_TYPE = IPOS_RAWDATA_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BEACON_EVENT__TIME_STAMP = IPOS_RAWDATA_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BEACON_EVENT__DISTANCES = IPOS_RAWDATA_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IPos Beacon Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BEACON_EVENT_FEATURE_COUNT = IPOS_RAWDATA_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IPos Beacon Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BEACON_EVENT_OPERATION_COUNT = IPOS_RAWDATA_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosUWBEventImpl <em>IPos UWB Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosUWBEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosUWBEvent()
	 * @generated
	 */
	int IPOS_UWB_EVENT = 11;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_UWB_EVENT__AGENT_ID = IPOS_BEACON_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_UWB_EVENT__SENSOR_ID = IPOS_BEACON_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_UWB_EVENT__AGENT_TYPE = IPOS_BEACON_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_UWB_EVENT__SENSOR_TYPE = IPOS_BEACON_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_UWB_EVENT__TIME_STAMP = IPOS_BEACON_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_UWB_EVENT__DISTANCES = IPOS_BEACON_EVENT__DISTANCES;

	/**
	 * The number of structural features of the '<em>IPos UWB Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_UWB_EVENT_FEATURE_COUNT = IPOS_BEACON_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>IPos UWB Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_UWB_EVENT_OPERATION_COUNT = IPOS_BEACON_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBTEventImpl <em>IPos BT Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBTEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosBTEvent()
	 * @generated
	 */
	int IPOS_BT_EVENT = 12;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT__AGENT_ID = IPOS_BEACON_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT__SENSOR_ID = IPOS_BEACON_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT__AGENT_TYPE = IPOS_BEACON_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT__SENSOR_TYPE = IPOS_BEACON_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT__TIME_STAMP = IPOS_BEACON_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT__DISTANCES = IPOS_BEACON_EVENT__DISTANCES;

	/**
	 * The feature id for the '<em><b>Rss</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT__RSS = IPOS_BEACON_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IPos BT Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT_FEATURE_COUNT = IPOS_BEACON_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IPos BT Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BT_EVENT_OPERATION_COUNT = IPOS_BEACON_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosProximityEventImpl <em>IPos Proximity Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosProximityEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosProximityEvent()
	 * @generated
	 */
	int IPOS_PROXIMITY_EVENT = 13;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_PROXIMITY_EVENT__AGENT_ID = IPOS_RAWDATA_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_PROXIMITY_EVENT__SENSOR_ID = IPOS_RAWDATA_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_PROXIMITY_EVENT__AGENT_TYPE = IPOS_RAWDATA_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_PROXIMITY_EVENT__SENSOR_TYPE = IPOS_RAWDATA_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_PROXIMITY_EVENT__TIME_STAMP = IPOS_RAWDATA_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_PROXIMITY_EVENT__TAG_ID = IPOS_RAWDATA_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IPos Proximity Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_PROXIMITY_EVENT_FEATURE_COUNT = IPOS_RAWDATA_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IPos Proximity Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_PROXIMITY_EVENT_OPERATION_COUNT = IPOS_RAWDATA_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosNFCEventImpl <em>IPos NFC Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosNFCEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosNFCEvent()
	 * @generated
	 */
	int IPOS_NFC_EVENT = 14;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT__AGENT_ID = IPOS_PROXIMITY_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT__SENSOR_ID = IPOS_PROXIMITY_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT__AGENT_TYPE = IPOS_PROXIMITY_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT__SENSOR_TYPE = IPOS_PROXIMITY_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT__TIME_STAMP = IPOS_PROXIMITY_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT__TAG_ID = IPOS_PROXIMITY_EVENT__TAG_ID;

	/**
	 * The feature id for the '<em><b>Tag Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT__TAG_DATA = IPOS_PROXIMITY_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IPos NFC Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT_FEATURE_COUNT = IPOS_PROXIMITY_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IPos NFC Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_NFC_EVENT_OPERATION_COUNT = IPOS_PROXIMITY_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBarcodeEventImpl <em>IPos Barcode Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBarcodeEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosBarcodeEvent()
	 * @generated
	 */
	int IPOS_BARCODE_EVENT = 15;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BARCODE_EVENT__AGENT_ID = IPOS_PROXIMITY_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BARCODE_EVENT__SENSOR_ID = IPOS_PROXIMITY_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BARCODE_EVENT__AGENT_TYPE = IPOS_PROXIMITY_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BARCODE_EVENT__SENSOR_TYPE = IPOS_PROXIMITY_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BARCODE_EVENT__TIME_STAMP = IPOS_PROXIMITY_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BARCODE_EVENT__TAG_ID = IPOS_PROXIMITY_EVENT__TAG_ID;

	/**
	 * The number of structural features of the '<em>IPos Barcode Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BARCODE_EVENT_FEATURE_COUNT = IPOS_PROXIMITY_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>IPos Barcode Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_BARCODE_EVENT_OPERATION_COUNT = IPOS_PROXIMITY_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRFIDEventImpl <em>IPos RFID Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRFIDEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosRFIDEvent()
	 * @generated
	 */
	int IPOS_RFID_EVENT = 16;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT__AGENT_ID = IPOS_PROXIMITY_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT__SENSOR_ID = IPOS_PROXIMITY_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT__AGENT_TYPE = IPOS_PROXIMITY_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT__SENSOR_TYPE = IPOS_PROXIMITY_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT__TIME_STAMP = IPOS_PROXIMITY_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT__TAG_ID = IPOS_PROXIMITY_EVENT__TAG_ID;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT__LOCATION = IPOS_PROXIMITY_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IPos RFID Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT_FEATURE_COUNT = IPOS_PROXIMITY_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IPos RFID Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_RFID_EVENT_OPERATION_COUNT = IPOS_PROXIMITY_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosOtherProxEventImpl <em>IPos Other Prox Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosOtherProxEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosOtherProxEvent()
	 * @generated
	 */
	int IPOS_OTHER_PROX_EVENT = 17;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT__AGENT_ID = IPOS_PROXIMITY_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT__SENSOR_ID = IPOS_PROXIMITY_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT__AGENT_TYPE = IPOS_PROXIMITY_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT__SENSOR_TYPE = IPOS_PROXIMITY_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT__TIME_STAMP = IPOS_PROXIMITY_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT__TAG_ID = IPOS_PROXIMITY_EVENT__TAG_ID;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT__DATA = IPOS_PROXIMITY_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IPos Other Prox Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT_FEATURE_COUNT = IPOS_PROXIMITY_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IPos Other Prox Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_PROX_EVENT_OPERATION_COUNT = IPOS_PROXIMITY_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosOtherBeaconEventImpl <em>IPos Other Beacon Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosOtherBeaconEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosOtherBeaconEvent()
	 * @generated
	 */
	int IPOS_OTHER_BEACON_EVENT = 18;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT__AGENT_ID = IPOS_BEACON_EVENT__AGENT_ID;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT__SENSOR_ID = IPOS_BEACON_EVENT__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT__AGENT_TYPE = IPOS_BEACON_EVENT__AGENT_TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT__SENSOR_TYPE = IPOS_BEACON_EVENT__SENSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT__TIME_STAMP = IPOS_BEACON_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT__DISTANCES = IPOS_BEACON_EVENT__DISTANCES;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT__DATA = IPOS_BEACON_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IPos Other Beacon Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT_FEATURE_COUNT = IPOS_BEACON_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IPos Other Beacon Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_OTHER_BEACON_EVENT_OPERATION_COUNT = IPOS_BEACON_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl <em>Ipos Msg Rcv Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIposMsgRcvEvent()
	 * @generated
	 */
	int IPOS_MSG_RCV_EVENT = 19;

	/**
	 * The feature id for the '<em><b>Protocol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__PROTOCOL_NAME = 0;

	/**
	 * The feature id for the '<em><b>Serialized Msg</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__SERIALIZED_MSG = 1;

	/**
	 * The feature id for the '<em><b>Last Pos Update</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__LAST_POS_UPDATE = 2;

	/**
	 * The feature id for the '<em><b>Last Known Position</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION = 3;

	/**
	 * The feature id for the '<em><b>Last Known Orientation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION = 4;

	/**
	 * The feature id for the '<em><b>Last Known Zonedescriptors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__LAST_KNOWN_ZONEDESCRIPTORS = 5;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__AGENT_ID = 6;

	/**
	 * The feature id for the '<em><b>Extracted Attributes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__EXTRACTED_ATTRIBUTES = 7;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT__TIMESTAMP = 8;

	/**
	 * The number of structural features of the '<em>Ipos Msg Rcv Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Ipos Msg Rcv Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPOS_MSG_RCV_EVENT_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.TrackingRequest <em>Tracking Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tracking Request</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.TrackingRequest
	 * @generated
	 */
	EClass getTrackingRequest();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest <em>Monitoring Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitoring Request</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest
	 * @generated
	 */
	EClass getMonitoringRequest();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getFrameIds <em>Frame Ids</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frame Ids</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getFrameIds()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_FrameIds();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getDelta <em>Delta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delta</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getDelta()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_Delta();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getUpdateFrequency <em>Update Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Update Frequency</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getUpdateFrequency()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_UpdateFrequency();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getType()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_Type();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getId()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_Id();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getFusionStrategy <em>Fusion Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fusion Strategy</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getFusionStrategy()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_FusionStrategy();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#isExitNotification <em>Exit Notification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exit Notification</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#isExitNotification()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_ExitNotification();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getProperties()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_Properties();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getMonitoringTaskId <em>Monitoring Task Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Monitoring Task Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getMonitoringTaskId()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_MonitoringTaskId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getRequestorProtocol <em>Requestor Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Requestor Protocol</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getRequestorProtocol()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_RequestorProtocol();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getSerializationType <em>Serialization Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Serialization Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getSerializationType()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_SerializationType();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getRefSystemId <em>Ref System Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ref System Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getRefSystemId()
	 * @see #getMonitoringRequest()
	 * @generated
	 */
	EAttribute getMonitoringRequest_RefSystemId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelUpdateRequest <em>World Model Update Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>World Model Update Request</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelUpdateRequest
	 * @generated
	 */
	EClass getWorldModelUpdateRequest();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageRequest <em>Data Storage Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Storage Request</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageRequest
	 * @generated
	 */
	EClass getDataStorageRequest();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelQueryRequest <em>World Model Query Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>World Model Query Request</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelQueryRequest
	 * @generated
	 */
	EClass getWorldModelQueryRequest();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest <em>Data Storage Query Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Storage Query Request</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest
	 * @generated
	 */
	EClass getDataStorageQueryRequest();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest#getTrackingTaskId <em>Tracking Task Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tracking Task Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest#getTrackingTaskId()
	 * @see #getDataStorageQueryRequest()
	 * @generated
	 */
	EAttribute getDataStorageQueryRequest_TrackingTaskId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.AgentRegistrationRequest <em>Agent Registration Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent Registration Request</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.AgentRegistrationRequest
	 * @generated
	 */
	EClass getAgentRegistrationRequest();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.SensorConfigurationRequest <em>Sensor Configuration Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sensor Configuration Request</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.SensorConfigurationRequest
	 * @generated
	 */
	EClass getSensorConfigurationRequest();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent <em>Ipos Position Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ipos Position Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent
	 * @generated
	 */
	EClass getIposPositionEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getAgentId <em>Agent Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Agent Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getAgentId()
	 * @see #getIposPositionEvent()
	 * @generated
	 */
	EAttribute getIposPositionEvent_AgentId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getSensorId <em>Sensor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getSensorId()
	 * @see #getIposPositionEvent()
	 * @generated
	 */
	EAttribute getIposPositionEvent_SensorId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getType()
	 * @see #getIposPositionEvent()
	 * @generated
	 */
	EAttribute getIposPositionEvent_Type();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getSensorType <em>Sensor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getSensorType()
	 * @see #getIposPositionEvent()
	 * @generated
	 */
	EAttribute getIposPositionEvent_SensorType();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getLastPosUpdate <em>Last Pos Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Pos Update</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getLastPosUpdate()
	 * @see #getIposPositionEvent()
	 * @generated
	 */
	EAttribute getIposPositionEvent_LastPosUpdate();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Position</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getPosition()
	 * @see #getIposPositionEvent()
	 * @generated
	 */
	EReference getIposPositionEvent_Position();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getOrientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Orientation</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getOrientation()
	 * @see #getIposPositionEvent()
	 * @generated
	 */
	EReference getIposPositionEvent_Orientation();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getZoneDescriptors <em>Zone Descriptors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Zone Descriptors</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent#getZoneDescriptors()
	 * @see #getIposPositionEvent()
	 * @generated
	 */
	EReference getIposPositionEvent_ZoneDescriptors();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent <em>IPos Rawdata Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos Rawdata Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent
	 * @generated
	 */
	EClass getIPosRawdataEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getAgentId <em>Agent Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Agent Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getAgentId()
	 * @see #getIPosRawdataEvent()
	 * @generated
	 */
	EAttribute getIPosRawdataEvent_AgentId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getSensorId <em>Sensor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getSensorId()
	 * @see #getIPosRawdataEvent()
	 * @generated
	 */
	EAttribute getIPosRawdataEvent_SensorId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getAgentType <em>Agent Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Agent Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getAgentType()
	 * @see #getIPosRawdataEvent()
	 * @generated
	 */
	EAttribute getIPosRawdataEvent_AgentType();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getSensorType <em>Sensor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getSensorType()
	 * @see #getIPosRawdataEvent()
	 * @generated
	 */
	EAttribute getIPosRawdataEvent_SensorType();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getTimeStamp <em>Time Stamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Stamp</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getTimeStamp()
	 * @see #getIPosRawdataEvent()
	 * @generated
	 */
	EAttribute getIPosRawdataEvent_TimeStamp();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent <em>IPos Beacon Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos Beacon Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent
	 * @generated
	 */
	EClass getIPosBeaconEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent#getDistances <em>Distances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distances</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent#getDistances()
	 * @see #getIPosBeaconEvent()
	 * @generated
	 */
	EAttribute getIPosBeaconEvent_Distances();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent <em>IPos UWB Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos UWB Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent
	 * @generated
	 */
	EClass getIPosUWBEvent();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent <em>IPos BT Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos BT Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent
	 * @generated
	 */
	EClass getIPosBTEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent#getRss <em>Rss</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rss</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent#getRss()
	 * @see #getIPosBTEvent()
	 * @generated
	 */
	EAttribute getIPosBTEvent_Rss();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent <em>IPos Proximity Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos Proximity Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent
	 * @generated
	 */
	EClass getIPosProximityEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent#getTagId <em>Tag Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent#getTagId()
	 * @see #getIPosProximityEvent()
	 * @generated
	 */
	EAttribute getIPosProximityEvent_TagId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent <em>IPos NFC Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos NFC Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent
	 * @generated
	 */
	EClass getIPosNFCEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent#getTagData <em>Tag Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag Data</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent#getTagData()
	 * @see #getIPosNFCEvent()
	 * @generated
	 */
	EAttribute getIPosNFCEvent_TagData();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBarcodeEvent <em>IPos Barcode Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos Barcode Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBarcodeEvent
	 * @generated
	 */
	EClass getIPosBarcodeEvent();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRFIDEvent <em>IPos RFID Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos RFID Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRFIDEvent
	 * @generated
	 */
	EClass getIPosRFIDEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRFIDEvent#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRFIDEvent#getLocation()
	 * @see #getIPosRFIDEvent()
	 * @generated
	 */
	EAttribute getIPosRFIDEvent_Location();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherProxEvent <em>IPos Other Prox Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos Other Prox Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherProxEvent
	 * @generated
	 */
	EClass getIPosOtherProxEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherProxEvent#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherProxEvent#getData()
	 * @see #getIPosOtherProxEvent()
	 * @generated
	 */
	EAttribute getIPosOtherProxEvent_Data();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherBeaconEvent <em>IPos Other Beacon Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPos Other Beacon Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherBeaconEvent
	 * @generated
	 */
	EClass getIPosOtherBeaconEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherBeaconEvent#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherBeaconEvent#getData()
	 * @see #getIPosOtherBeaconEvent()
	 * @generated
	 */
	EAttribute getIPosOtherBeaconEvent_Data();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent <em>Ipos Msg Rcv Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ipos Msg Rcv Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent
	 * @generated
	 */
	EClass getIposMsgRcvEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getProtocolName <em>Protocol Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol Name</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getProtocolName()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EAttribute getIposMsgRcvEvent_ProtocolName();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getSerializedMsg <em>Serialized Msg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Serialized Msg</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getSerializedMsg()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EAttribute getIposMsgRcvEvent_SerializedMsg();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastPosUpdate <em>Last Pos Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Pos Update</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastPosUpdate()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EAttribute getIposMsgRcvEvent_LastPosUpdate();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownPosition <em>Last Known Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Last Known Position</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownPosition()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EReference getIposMsgRcvEvent_LastKnownPosition();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownOrientation <em>Last Known Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Last Known Orientation</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownOrientation()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EReference getIposMsgRcvEvent_LastKnownOrientation();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownZonedescriptors <em>Last Known Zonedescriptors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Last Known Zonedescriptors</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownZonedescriptors()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EReference getIposMsgRcvEvent_LastKnownZonedescriptors();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getAgentId <em>Agent Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Agent Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getAgentId()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EAttribute getIposMsgRcvEvent_AgentId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getExtractedAttributes <em>Extracted Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extracted Attributes</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getExtractedAttributes()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EAttribute getIposMsgRcvEvent_ExtractedAttributes();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getTimestamp()
	 * @see #getIposMsgRcvEvent()
	 * @generated
	 */
	EAttribute getIposMsgRcvEvent_Timestamp();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IPosDevKitFactory getIPosDevKitFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.TrackingRequestImpl <em>Tracking Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.TrackingRequestImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getTrackingRequest()
		 * @generated
		 */
		EClass TRACKING_REQUEST = eINSTANCE.getTrackingRequest();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl <em>Monitoring Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getMonitoringRequest()
		 * @generated
		 */
		EClass MONITORING_REQUEST = eINSTANCE.getMonitoringRequest();

		/**
		 * The meta object literal for the '<em><b>Frame Ids</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__FRAME_IDS = eINSTANCE.getMonitoringRequest_FrameIds();

		/**
		 * The meta object literal for the '<em><b>Delta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__DELTA = eINSTANCE.getMonitoringRequest_Delta();

		/**
		 * The meta object literal for the '<em><b>Update Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__UPDATE_FREQUENCY = eINSTANCE.getMonitoringRequest_UpdateFrequency();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__TYPE = eINSTANCE.getMonitoringRequest_Type();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__ID = eINSTANCE.getMonitoringRequest_Id();

		/**
		 * The meta object literal for the '<em><b>Fusion Strategy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__FUSION_STRATEGY = eINSTANCE.getMonitoringRequest_FusionStrategy();

		/**
		 * The meta object literal for the '<em><b>Exit Notification</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__EXIT_NOTIFICATION = eINSTANCE.getMonitoringRequest_ExitNotification();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__PROPERTIES = eINSTANCE.getMonitoringRequest_Properties();

		/**
		 * The meta object literal for the '<em><b>Monitoring Task Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__MONITORING_TASK_ID = eINSTANCE.getMonitoringRequest_MonitoringTaskId();

		/**
		 * The meta object literal for the '<em><b>Requestor Protocol</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__REQUESTOR_PROTOCOL = eINSTANCE.getMonitoringRequest_RequestorProtocol();

		/**
		 * The meta object literal for the '<em><b>Serialization Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__SERIALIZATION_TYPE = eINSTANCE.getMonitoringRequest_SerializationType();

		/**
		 * The meta object literal for the '<em><b>Ref System Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_REQUEST__REF_SYSTEM_ID = eINSTANCE.getMonitoringRequest_RefSystemId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.WorldModelUpdateRequestImpl <em>World Model Update Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.WorldModelUpdateRequestImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getWorldModelUpdateRequest()
		 * @generated
		 */
		EClass WORLD_MODEL_UPDATE_REQUEST = eINSTANCE.getWorldModelUpdateRequest();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageRequestImpl <em>Data Storage Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageRequestImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getDataStorageRequest()
		 * @generated
		 */
		EClass DATA_STORAGE_REQUEST = eINSTANCE.getDataStorageRequest();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.WorldModelQueryRequestImpl <em>World Model Query Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.WorldModelQueryRequestImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getWorldModelQueryRequest()
		 * @generated
		 */
		EClass WORLD_MODEL_QUERY_REQUEST = eINSTANCE.getWorldModelQueryRequest();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageQueryRequestImpl <em>Data Storage Query Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageQueryRequestImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getDataStorageQueryRequest()
		 * @generated
		 */
		EClass DATA_STORAGE_QUERY_REQUEST = eINSTANCE.getDataStorageQueryRequest();

		/**
		 * The meta object literal for the '<em><b>Tracking Task Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_STORAGE_QUERY_REQUEST__TRACKING_TASK_ID = eINSTANCE.getDataStorageQueryRequest_TrackingTaskId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.AgentRegistrationRequestImpl <em>Agent Registration Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.AgentRegistrationRequestImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getAgentRegistrationRequest()
		 * @generated
		 */
		EClass AGENT_REGISTRATION_REQUEST = eINSTANCE.getAgentRegistrationRequest();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.SensorConfigurationRequestImpl <em>Sensor Configuration Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.SensorConfigurationRequestImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getSensorConfigurationRequest()
		 * @generated
		 */
		EClass SENSOR_CONFIGURATION_REQUEST = eINSTANCE.getSensorConfigurationRequest();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl <em>Ipos Position Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIposPositionEvent()
		 * @generated
		 */
		EClass IPOS_POSITION_EVENT = eINSTANCE.getIposPositionEvent();

		/**
		 * The meta object literal for the '<em><b>Agent Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_POSITION_EVENT__AGENT_ID = eINSTANCE.getIposPositionEvent_AgentId();

		/**
		 * The meta object literal for the '<em><b>Sensor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_POSITION_EVENT__SENSOR_ID = eINSTANCE.getIposPositionEvent_SensorId();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_POSITION_EVENT__TYPE = eINSTANCE.getIposPositionEvent_Type();

		/**
		 * The meta object literal for the '<em><b>Sensor Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_POSITION_EVENT__SENSOR_TYPE = eINSTANCE.getIposPositionEvent_SensorType();

		/**
		 * The meta object literal for the '<em><b>Last Pos Update</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_POSITION_EVENT__LAST_POS_UPDATE = eINSTANCE.getIposPositionEvent_LastPosUpdate();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IPOS_POSITION_EVENT__POSITION = eINSTANCE.getIposPositionEvent_Position();

		/**
		 * The meta object literal for the '<em><b>Orientation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IPOS_POSITION_EVENT__ORIENTATION = eINSTANCE.getIposPositionEvent_Orientation();

		/**
		 * The meta object literal for the '<em><b>Zone Descriptors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IPOS_POSITION_EVENT__ZONE_DESCRIPTORS = eINSTANCE.getIposPositionEvent_ZoneDescriptors();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl <em>IPos Rawdata Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosRawdataEvent()
		 * @generated
		 */
		EClass IPOS_RAWDATA_EVENT = eINSTANCE.getIPosRawdataEvent();

		/**
		 * The meta object literal for the '<em><b>Agent Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_RAWDATA_EVENT__AGENT_ID = eINSTANCE.getIPosRawdataEvent_AgentId();

		/**
		 * The meta object literal for the '<em><b>Sensor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_RAWDATA_EVENT__SENSOR_ID = eINSTANCE.getIPosRawdataEvent_SensorId();

		/**
		 * The meta object literal for the '<em><b>Agent Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_RAWDATA_EVENT__AGENT_TYPE = eINSTANCE.getIPosRawdataEvent_AgentType();

		/**
		 * The meta object literal for the '<em><b>Sensor Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_RAWDATA_EVENT__SENSOR_TYPE = eINSTANCE.getIPosRawdataEvent_SensorType();

		/**
		 * The meta object literal for the '<em><b>Time Stamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_RAWDATA_EVENT__TIME_STAMP = eINSTANCE.getIPosRawdataEvent_TimeStamp();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBeaconEventImpl <em>IPos Beacon Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBeaconEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosBeaconEvent()
		 * @generated
		 */
		EClass IPOS_BEACON_EVENT = eINSTANCE.getIPosBeaconEvent();

		/**
		 * The meta object literal for the '<em><b>Distances</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_BEACON_EVENT__DISTANCES = eINSTANCE.getIPosBeaconEvent_Distances();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosUWBEventImpl <em>IPos UWB Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosUWBEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosUWBEvent()
		 * @generated
		 */
		EClass IPOS_UWB_EVENT = eINSTANCE.getIPosUWBEvent();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBTEventImpl <em>IPos BT Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBTEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosBTEvent()
		 * @generated
		 */
		EClass IPOS_BT_EVENT = eINSTANCE.getIPosBTEvent();

		/**
		 * The meta object literal for the '<em><b>Rss</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_BT_EVENT__RSS = eINSTANCE.getIPosBTEvent_Rss();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosProximityEventImpl <em>IPos Proximity Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosProximityEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosProximityEvent()
		 * @generated
		 */
		EClass IPOS_PROXIMITY_EVENT = eINSTANCE.getIPosProximityEvent();

		/**
		 * The meta object literal for the '<em><b>Tag Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_PROXIMITY_EVENT__TAG_ID = eINSTANCE.getIPosProximityEvent_TagId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosNFCEventImpl <em>IPos NFC Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosNFCEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosNFCEvent()
		 * @generated
		 */
		EClass IPOS_NFC_EVENT = eINSTANCE.getIPosNFCEvent();

		/**
		 * The meta object literal for the '<em><b>Tag Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_NFC_EVENT__TAG_DATA = eINSTANCE.getIPosNFCEvent_TagData();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBarcodeEventImpl <em>IPos Barcode Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosBarcodeEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosBarcodeEvent()
		 * @generated
		 */
		EClass IPOS_BARCODE_EVENT = eINSTANCE.getIPosBarcodeEvent();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRFIDEventImpl <em>IPos RFID Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRFIDEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosRFIDEvent()
		 * @generated
		 */
		EClass IPOS_RFID_EVENT = eINSTANCE.getIPosRFIDEvent();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_RFID_EVENT__LOCATION = eINSTANCE.getIPosRFIDEvent_Location();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosOtherProxEventImpl <em>IPos Other Prox Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosOtherProxEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosOtherProxEvent()
		 * @generated
		 */
		EClass IPOS_OTHER_PROX_EVENT = eINSTANCE.getIPosOtherProxEvent();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_OTHER_PROX_EVENT__DATA = eINSTANCE.getIPosOtherProxEvent_Data();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosOtherBeaconEventImpl <em>IPos Other Beacon Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosOtherBeaconEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIPosOtherBeaconEvent()
		 * @generated
		 */
		EClass IPOS_OTHER_BEACON_EVENT = eINSTANCE.getIPosOtherBeaconEvent();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_OTHER_BEACON_EVENT__DATA = eINSTANCE.getIPosOtherBeaconEvent_Data();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl <em>Ipos Msg Rcv Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl#getIposMsgRcvEvent()
		 * @generated
		 */
		EClass IPOS_MSG_RCV_EVENT = eINSTANCE.getIposMsgRcvEvent();

		/**
		 * The meta object literal for the '<em><b>Protocol Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_MSG_RCV_EVENT__PROTOCOL_NAME = eINSTANCE.getIposMsgRcvEvent_ProtocolName();

		/**
		 * The meta object literal for the '<em><b>Serialized Msg</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_MSG_RCV_EVENT__SERIALIZED_MSG = eINSTANCE.getIposMsgRcvEvent_SerializedMsg();

		/**
		 * The meta object literal for the '<em><b>Last Pos Update</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_MSG_RCV_EVENT__LAST_POS_UPDATE = eINSTANCE.getIposMsgRcvEvent_LastPosUpdate();

		/**
		 * The meta object literal for the '<em><b>Last Known Position</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION = eINSTANCE.getIposMsgRcvEvent_LastKnownPosition();

		/**
		 * The meta object literal for the '<em><b>Last Known Orientation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION = eINSTANCE.getIposMsgRcvEvent_LastKnownOrientation();

		/**
		 * The meta object literal for the '<em><b>Last Known Zonedescriptors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IPOS_MSG_RCV_EVENT__LAST_KNOWN_ZONEDESCRIPTORS = eINSTANCE
				.getIposMsgRcvEvent_LastKnownZonedescriptors();

		/**
		 * The meta object literal for the '<em><b>Agent Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_MSG_RCV_EVENT__AGENT_ID = eINSTANCE.getIposMsgRcvEvent_AgentId();

		/**
		 * The meta object literal for the '<em><b>Extracted Attributes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_MSG_RCV_EVENT__EXTRACTED_ATTRIBUTES = eINSTANCE.getIposMsgRcvEvent_ExtractedAttributes();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IPOS_MSG_RCV_EVENT__TIMESTAMP = eINSTANCE.getIposMsgRcvEvent_Timestamp();

	}

} //IPosDevKitPackage
