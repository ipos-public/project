/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPos NFC Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent#getTagData <em>Tag Data</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosNFCEvent()
 * @model
 * @generated
 */
public interface IPosNFCEvent extends IPosProximityEvent {
	/**
	 * Returns the value of the '<em><b>Tag Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Data</em>' attribute.
	 * @see #setTagData(Map)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosNFCEvent_TagData()
	 * @model transient="true"
	 * @generated
	 */
	Map<String, Object> getTagData();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent#getTagData <em>Tag Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag Data</em>' attribute.
	 * @see #getTagData()
	 * @generated
	 */
	void setTagData(Map<String, Object> value);

} // IPosNFCEvent
