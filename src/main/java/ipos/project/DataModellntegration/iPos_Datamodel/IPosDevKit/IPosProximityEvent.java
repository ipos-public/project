/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPos Proximity Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent#getTagId <em>Tag Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosProximityEvent()
 * @model
 * @generated
 */
public interface IPosProximityEvent extends IPosRawdataEvent {
	/**
	 * Returns the value of the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Id</em>' attribute.
	 * @see #setTagId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosProximityEvent_TagId()
	 * @model
	 * @generated
	 */
	String getTagId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent#getTagId <em>Tag Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag Id</em>' attribute.
	 * @see #getTagId()
	 * @generated
	 */
	void setTagId(String value);

} // IPosProximityEvent
