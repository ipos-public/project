/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPos Rawdata Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getAgentId <em>Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getSensorId <em>Sensor Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getAgentType <em>Agent Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getSensorType <em>Sensor Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getTimeStamp <em>Time Stamp</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosRawdataEvent()
 * @model
 * @generated
 */
public interface IPosRawdataEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Id</em>' attribute.
	 * @see #setAgentId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosRawdataEvent_AgentId()
	 * @model
	 * @generated
	 */
	String getAgentId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getAgentId <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Id</em>' attribute.
	 * @see #getAgentId()
	 * @generated
	 */
	void setAgentId(String value);

	/**
	 * Returns the value of the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sensor Id</em>' attribute.
	 * @see #setSensorId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosRawdataEvent_SensorId()
	 * @model
	 * @generated
	 */
	String getSensorId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getSensorId <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sensor Id</em>' attribute.
	 * @see #getSensorId()
	 * @generated
	 */
	void setSensorId(String value);

	/**
	 * Returns the value of the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Type</em>' attribute.
	 * @see #setAgentType(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosRawdataEvent_AgentType()
	 * @model
	 * @generated
	 */
	String getAgentType();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getAgentType <em>Agent Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Type</em>' attribute.
	 * @see #getAgentType()
	 * @generated
	 */
	void setAgentType(String value);

	/**
	 * Returns the value of the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sensor Type</em>' attribute.
	 * @see #setSensorType(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosRawdataEvent_SensorType()
	 * @model
	 * @generated
	 */
	String getSensorType();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getSensorType <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sensor Type</em>' attribute.
	 * @see #getSensorType()
	 * @generated
	 */
	void setSensorType(String value);

	/**
	 * Returns the value of the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Stamp</em>' attribute.
	 * @see #setTimeStamp(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosRawdataEvent_TimeStamp()
	 * @model
	 * @generated
	 */
	String getTimeStamp();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent#getTimeStamp <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Stamp</em>' attribute.
	 * @see #getTimeStamp()
	 * @generated
	 */
	void setTimeStamp(String value);

} // IPosRawdataEvent
