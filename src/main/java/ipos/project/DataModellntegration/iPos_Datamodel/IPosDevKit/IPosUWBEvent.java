/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPos UWB Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIPosUWBEvent()
 * @model
 * @generated
 */
public interface IPosUWBEvent extends IPosBeaconEvent {
} // IPosUWBEvent
