/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import ipos.project.DataModellntegration.iPos_Datamodel.Orientation;
import ipos.project.DataModellntegration.iPos_Datamodel.Position;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;

import java.util.HashMap;
import java.util.LinkedList;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ipos Msg Rcv Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getProtocolName <em>Protocol Name</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getSerializedMsg <em>Serialized Msg</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastPosUpdate <em>Last Pos Update</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownPosition <em>Last Known Position</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownOrientation <em>Last Known Orientation</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownZonedescriptors <em>Last Known Zonedescriptors</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getAgentId <em>Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getExtractedAttributes <em>Extracted Attributes</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getTimestamp <em>Timestamp</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent()
 * @model
 * @generated
 */
public interface IposMsgRcvEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Protocol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protocol Name</em>' attribute.
	 * @see #setProtocolName(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_ProtocolName()
	 * @model required="true"
	 * @generated
	 */
	String getProtocolName();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getProtocolName <em>Protocol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protocol Name</em>' attribute.
	 * @see #getProtocolName()
	 * @generated
	 */
	void setProtocolName(String value);

	/**
	 * Returns the value of the '<em><b>Serialized Msg</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serialized Msg</em>' attribute.
	 * @see #setSerializedMsg(byte[])
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_SerializedMsg()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.ByteArray"
	 * @generated
	 */
	byte[] getSerializedMsg();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getSerializedMsg <em>Serialized Msg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Serialized Msg</em>' attribute.
	 * @see #getSerializedMsg()
	 * @generated
	 */
	void setSerializedMsg(byte[] value);

	/**
	 * Returns the value of the '<em><b>Last Pos Update</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Pos Update</em>' attribute.
	 * @see #setLastPosUpdate(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_LastPosUpdate()
	 * @model
	 * @generated
	 */
	String getLastPosUpdate();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastPosUpdate <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Pos Update</em>' attribute.
	 * @see #getLastPosUpdate()
	 * @generated
	 */
	void setLastPosUpdate(String value);

	/**
	 * Returns the value of the '<em><b>Last Known Position</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Known Position</em>' reference.
	 * @see #setLastKnownPosition(Position)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_LastKnownPosition()
	 * @model
	 * @generated
	 */
	Position getLastKnownPosition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownPosition <em>Last Known Position</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Known Position</em>' reference.
	 * @see #getLastKnownPosition()
	 * @generated
	 */
	void setLastKnownPosition(Position value);

	/**
	 * Returns the value of the '<em><b>Last Known Orientation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Known Orientation</em>' reference.
	 * @see #setLastKnownOrientation(Orientation)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_LastKnownOrientation()
	 * @model
	 * @generated
	 */
	Orientation getLastKnownOrientation();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getLastKnownOrientation <em>Last Known Orientation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Known Orientation</em>' reference.
	 * @see #getLastKnownOrientation()
	 * @generated
	 */
	void setLastKnownOrientation(Orientation value);

	/**
	 * Returns the value of the '<em><b>Last Known Zonedescriptors</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Known Zonedescriptors</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_LastKnownZonedescriptors()
	 * @model
	 * @generated
	 */
	EList<ZoneDescriptor> getLastKnownZonedescriptors();

	/**
	 * Returns the value of the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Id</em>' attribute.
	 * @see #setAgentId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_AgentId()
	 * @model required="true"
	 * @generated
	 */
	String getAgentId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getAgentId <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Id</em>' attribute.
	 * @see #getAgentId()
	 * @generated
	 */
	void setAgentId(String value);

	/**
	 * Returns the value of the '<em><b>Extracted Attributes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extracted Attributes</em>' attribute.
	 * @see #setExtractedAttributes(LinkedList)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_ExtractedAttributes()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.ListOfStringMaps"
	 * @generated
	 */
	LinkedList<HashMap<String, String>> getExtractedAttributes();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getExtractedAttributes <em>Extracted Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extracted Attributes</em>' attribute.
	 * @see #getExtractedAttributes()
	 * @generated
	 */
	void setExtractedAttributes(LinkedList<HashMap<String, String>> value);

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getIposMsgRcvEvent_Timestamp()
	 * @model
	 * @generated
	 */
	String getTimestamp();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(String value);

} // IposMsgRcvEvent
