/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitoring Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getFrameIds <em>Frame Ids</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getDelta <em>Delta</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getUpdateFrequency <em>Update Frequency</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getType <em>Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getId <em>Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getFusionStrategy <em>Fusion Strategy</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#isExitNotification <em>Exit Notification</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getProperties <em>Properties</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getMonitoringTaskId <em>Monitoring Task Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getRequestorProtocol <em>Requestor Protocol</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getSerializationType <em>Serialization Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getRefSystemId <em>Ref System Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest()
 * @model
 * @generated
 */
public interface MonitoringRequest extends EObject {
	/**
	 * Returns the value of the '<em><b>Frame Ids</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Frame Ids</em>' attribute.
	 * @see #setFrameIds(List)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_FrameIds()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringList"
	 * @generated
	 */
	List<String> getFrameIds();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getFrameIds <em>Frame Ids</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Frame Ids</em>' attribute.
	 * @see #getFrameIds()
	 * @generated
	 */
	void setFrameIds(List<String> value);

	/**
	 * Returns the value of the '<em><b>Delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta</em>' attribute.
	 * @see #setDelta(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_Delta()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float"
	 * @generated
	 */
	float getDelta();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getDelta <em>Delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delta</em>' attribute.
	 * @see #getDelta()
	 * @generated
	 */
	void setDelta(float value);

	/**
	 * Returns the value of the '<em><b>Update Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Update Frequency</em>' attribute.
	 * @see #setUpdateFrequency(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_UpdateFrequency()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float"
	 * @generated
	 */
	float getUpdateFrequency();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getUpdateFrequency <em>Update Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Update Frequency</em>' attribute.
	 * @see #getUpdateFrequency()
	 * @generated
	 */
	void setUpdateFrequency(float value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(List)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_Type()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringList"
	 * @generated
	 */
	List<String> getType();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(List<String> value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(List)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_Id()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringList"
	 * @generated
	 */
	List<String> getId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(List<String> value);

	/**
	 * Returns the value of the '<em><b>Fusion Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fusion Strategy</em>' attribute.
	 * @see #setFusionStrategy(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_FusionStrategy()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getFusionStrategy();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getFusionStrategy <em>Fusion Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fusion Strategy</em>' attribute.
	 * @see #getFusionStrategy()
	 * @generated
	 */
	void setFusionStrategy(String value);

	/**
	 * Returns the value of the '<em><b>Exit Notification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exit Notification</em>' attribute.
	 * @see #setExitNotification(boolean)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_ExitNotification()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isExitNotification();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#isExitNotification <em>Exit Notification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exit Notification</em>' attribute.
	 * @see #isExitNotification()
	 * @generated
	 */
	void setExitNotification(boolean value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' attribute.
	 * @see #setProperties(List)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_Properties()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.StringList"
	 * @generated
	 */
	List<String> getProperties();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getProperties <em>Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Properties</em>' attribute.
	 * @see #getProperties()
	 * @generated
	 */
	void setProperties(List<String> value);

	/**
	 * Returns the value of the '<em><b>Monitoring Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitoring Task Id</em>' attribute.
	 * @see #setMonitoringTaskId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_MonitoringTaskId()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getMonitoringTaskId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getMonitoringTaskId <em>Monitoring Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monitoring Task Id</em>' attribute.
	 * @see #getMonitoringTaskId()
	 * @generated
	 */
	void setMonitoringTaskId(String value);

	/**
	 * Returns the value of the '<em><b>Requestor Protocol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requestor Protocol</em>' attribute.
	 * @see #setRequestorProtocol(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_RequestorProtocol()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getRequestorProtocol();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getRequestorProtocol <em>Requestor Protocol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requestor Protocol</em>' attribute.
	 * @see #getRequestorProtocol()
	 * @generated
	 */
	void setRequestorProtocol(String value);

	/**
	 * Returns the value of the '<em><b>Serialization Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serialization Type</em>' attribute.
	 * @see #setSerializationType(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_SerializationType()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getSerializationType();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getSerializationType <em>Serialization Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Serialization Type</em>' attribute.
	 * @see #getSerializationType()
	 * @generated
	 */
	void setSerializationType(String value);

	/**
	 * Returns the value of the '<em><b>Ref System Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref System Id</em>' attribute.
	 * @see #setRefSystemId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getMonitoringRequest_RefSystemId()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getRefSystemId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest#getRefSystemId <em>Ref System Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref System Id</em>' attribute.
	 * @see #getRefSystemId()
	 * @generated
	 */
	void setRefSystemId(String value);

} // MonitoringRequest
