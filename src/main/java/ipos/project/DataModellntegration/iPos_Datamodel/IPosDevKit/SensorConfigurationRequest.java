/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sensor Configuration Request</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getSensorConfigurationRequest()
 * @model
 * @generated
 */
public interface SensorConfigurationRequest extends EObject {
} // SensorConfigurationRequest
