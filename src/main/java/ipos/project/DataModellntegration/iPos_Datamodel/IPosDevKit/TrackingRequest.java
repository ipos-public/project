/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tracking Request</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#getTrackingRequest()
 * @model
 * @generated
 */
public interface TrackingRequest extends EObject {
} // TrackingRequest
