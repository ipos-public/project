/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.AgentRegistrationRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent Registration Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AgentRegistrationRequestImpl extends MinimalEObjectImpl.Container implements AgentRegistrationRequest {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentRegistrationRequestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.AGENT_REGISTRATION_REQUEST;
	}

} //AgentRegistrationRequestImpl
