/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Storage Query Request</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.DataStorageQueryRequestImpl#getTrackingTaskId <em>Tracking Task Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataStorageQueryRequestImpl extends MinimalEObjectImpl.Container implements DataStorageQueryRequest {
	/**
	 * The default value of the '{@link #getTrackingTaskId() <em>Tracking Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackingTaskId()
	 * @generated
	 * @ordered
	 */
	protected static final String TRACKING_TASK_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTrackingTaskId() <em>Tracking Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackingTaskId()
	 * @generated
	 * @ordered
	 */
	protected String trackingTaskId = TRACKING_TASK_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataStorageQueryRequestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.DATA_STORAGE_QUERY_REQUEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTrackingTaskId() {
		return trackingTaskId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrackingTaskId(String newTrackingTaskId) {
		String oldTrackingTaskId = trackingTaskId;
		trackingTaskId = newTrackingTaskId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.DATA_STORAGE_QUERY_REQUEST__TRACKING_TASK_ID, oldTrackingTaskId, trackingTaskId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPosDevKitPackage.DATA_STORAGE_QUERY_REQUEST__TRACKING_TASK_ID:
			return getTrackingTaskId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPosDevKitPackage.DATA_STORAGE_QUERY_REQUEST__TRACKING_TASK_ID:
			setTrackingTaskId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.DATA_STORAGE_QUERY_REQUEST__TRACKING_TASK_ID:
			setTrackingTaskId(TRACKING_TASK_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.DATA_STORAGE_QUERY_REQUEST__TRACKING_TASK_ID:
			return TRACKING_TASK_ID_EDEFAULT == null ? trackingTaskId != null
					: !TRACKING_TASK_ID_EDEFAULT.equals(trackingTaskId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (trackingTaskId: ");
		result.append(trackingTaskId);
		result.append(')');
		return result.toString();
	}

} //DataStorageQueryRequestImpl
