/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Storage Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DataStorageRequestImpl extends MinimalEObjectImpl.Container implements DataStorageRequest {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataStorageRequestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.DATA_STORAGE_REQUEST;
	}

} //DataStorageRequestImpl
