/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBarcodeEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPos Barcode Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IPosBarcodeEventImpl extends IPosProximityEventImpl implements IPosBarcodeEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPosBarcodeEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.IPOS_BARCODE_EVENT;
	}

} //IPosBarcodeEventImpl
