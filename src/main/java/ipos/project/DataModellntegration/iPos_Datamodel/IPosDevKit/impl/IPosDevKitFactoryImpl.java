/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IPosDevKitFactoryImpl extends EFactoryImpl implements IPosDevKitFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static IPosDevKitFactory init() {
		try {
			IPosDevKitFactory theIPosDevKitFactory = (IPosDevKitFactory) EPackage.Registry.INSTANCE
					.getEFactory(IPosDevKitPackage.eNS_URI);
			if (theIPosDevKitFactory != null) {
				return theIPosDevKitFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new IPosDevKitFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosDevKitFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case IPosDevKitPackage.TRACKING_REQUEST:
			return createTrackingRequest();
		case IPosDevKitPackage.MONITORING_REQUEST:
			return createMonitoringRequest();
		case IPosDevKitPackage.WORLD_MODEL_UPDATE_REQUEST:
			return createWorldModelUpdateRequest();
		case IPosDevKitPackage.DATA_STORAGE_REQUEST:
			return createDataStorageRequest();
		case IPosDevKitPackage.WORLD_MODEL_QUERY_REQUEST:
			return createWorldModelQueryRequest();
		case IPosDevKitPackage.DATA_STORAGE_QUERY_REQUEST:
			return createDataStorageQueryRequest();
		case IPosDevKitPackage.AGENT_REGISTRATION_REQUEST:
			return createAgentRegistrationRequest();
		case IPosDevKitPackage.SENSOR_CONFIGURATION_REQUEST:
			return createSensorConfigurationRequest();
		case IPosDevKitPackage.IPOS_POSITION_EVENT:
			return createIposPositionEvent();
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT:
			return createIPosRawdataEvent();
		case IPosDevKitPackage.IPOS_BEACON_EVENT:
			return createIPosBeaconEvent();
		case IPosDevKitPackage.IPOS_UWB_EVENT:
			return createIPosUWBEvent();
		case IPosDevKitPackage.IPOS_BT_EVENT:
			return createIPosBTEvent();
		case IPosDevKitPackage.IPOS_PROXIMITY_EVENT:
			return createIPosProximityEvent();
		case IPosDevKitPackage.IPOS_NFC_EVENT:
			return createIPosNFCEvent();
		case IPosDevKitPackage.IPOS_BARCODE_EVENT:
			return createIPosBarcodeEvent();
		case IPosDevKitPackage.IPOS_RFID_EVENT:
			return createIPosRFIDEvent();
		case IPosDevKitPackage.IPOS_OTHER_PROX_EVENT:
			return createIPosOtherProxEvent();
		case IPosDevKitPackage.IPOS_OTHER_BEACON_EVENT:
			return createIPosOtherBeaconEvent();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT:
			return createIposMsgRcvEvent();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrackingRequest createTrackingRequest() {
		TrackingRequestImpl trackingRequest = new TrackingRequestImpl();
		return trackingRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoringRequest createMonitoringRequest() {
		MonitoringRequestImpl monitoringRequest = new MonitoringRequestImpl();
		return monitoringRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldModelUpdateRequest createWorldModelUpdateRequest() {
		WorldModelUpdateRequestImpl worldModelUpdateRequest = new WorldModelUpdateRequestImpl();
		return worldModelUpdateRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataStorageRequest createDataStorageRequest() {
		DataStorageRequestImpl dataStorageRequest = new DataStorageRequestImpl();
		return dataStorageRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldModelQueryRequest createWorldModelQueryRequest() {
		WorldModelQueryRequestImpl worldModelQueryRequest = new WorldModelQueryRequestImpl();
		return worldModelQueryRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataStorageQueryRequest createDataStorageQueryRequest() {
		DataStorageQueryRequestImpl dataStorageQueryRequest = new DataStorageQueryRequestImpl();
		return dataStorageQueryRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentRegistrationRequest createAgentRegistrationRequest() {
		AgentRegistrationRequestImpl agentRegistrationRequest = new AgentRegistrationRequestImpl();
		return agentRegistrationRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SensorConfigurationRequest createSensorConfigurationRequest() {
		SensorConfigurationRequestImpl sensorConfigurationRequest = new SensorConfigurationRequestImpl();
		return sensorConfigurationRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IposPositionEvent createIposPositionEvent() {
		IposPositionEventImpl iposPositionEvent = new IposPositionEventImpl();
		return iposPositionEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosRawdataEvent createIPosRawdataEvent() {
		IPosRawdataEventImpl iPosRawdataEvent = new IPosRawdataEventImpl();
		return iPosRawdataEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosBeaconEvent createIPosBeaconEvent() {
		IPosBeaconEventImpl iPosBeaconEvent = new IPosBeaconEventImpl();
		return iPosBeaconEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosUWBEvent createIPosUWBEvent() {
		IPosUWBEventImpl iPosUWBEvent = new IPosUWBEventImpl();
		return iPosUWBEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosBTEvent createIPosBTEvent() {
		IPosBTEventImpl iPosBTEvent = new IPosBTEventImpl();
		return iPosBTEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosProximityEvent createIPosProximityEvent() {
		IPosProximityEventImpl iPosProximityEvent = new IPosProximityEventImpl();
		return iPosProximityEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosNFCEvent createIPosNFCEvent() {
		IPosNFCEventImpl iPosNFCEvent = new IPosNFCEventImpl();
		return iPosNFCEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosBarcodeEvent createIPosBarcodeEvent() {
		IPosBarcodeEventImpl iPosBarcodeEvent = new IPosBarcodeEventImpl();
		return iPosBarcodeEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosRFIDEvent createIPosRFIDEvent() {
		IPosRFIDEventImpl iPosRFIDEvent = new IPosRFIDEventImpl();
		return iPosRFIDEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosOtherProxEvent createIPosOtherProxEvent() {
		IPosOtherProxEventImpl iPosOtherProxEvent = new IPosOtherProxEventImpl();
		return iPosOtherProxEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosOtherBeaconEvent createIPosOtherBeaconEvent() {
		IPosOtherBeaconEventImpl iPosOtherBeaconEvent = new IPosOtherBeaconEventImpl();
		return iPosOtherBeaconEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IposMsgRcvEvent createIposMsgRcvEvent() {
		IposMsgRcvEventImpl iposMsgRcvEvent = new IposMsgRcvEventImpl();
		return iposMsgRcvEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosDevKitPackage getIPosDevKitPackage() {
		return (IPosDevKitPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static IPosDevKitPackage getPackage() {
		return IPosDevKitPackage.eINSTANCE;
	}

} //IPosDevKitFactoryImpl
