/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.AgentRegistrationRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBarcodeEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherBeaconEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherProxEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRFIDEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.SensorConfigurationRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.TrackingRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelQueryRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelUpdateRequest;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IPosDevKitPackageImpl extends EPackageImpl implements IPosDevKitPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass trackingRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitoringRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass worldModelUpdateRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataStorageRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass worldModelQueryRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataStorageQueryRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentRegistrationRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sensorConfigurationRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iposPositionEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosRawdataEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosBeaconEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosUWBEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosBTEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosProximityEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosNFCEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosBarcodeEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosRFIDEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosOtherProxEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPosOtherBeaconEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iposMsgRcvEventEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private IPosDevKitPackageImpl() {
		super(eNS_URI, IPosDevKitFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link IPosDevKitPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static IPosDevKitPackage init() {
		if (isInited)
			return (IPosDevKitPackage) EPackage.Registry.INSTANCE.getEPackage(IPosDevKitPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredIPosDevKitPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		IPosDevKitPackageImpl theIPosDevKitPackage = registeredIPosDevKitPackage instanceof IPosDevKitPackageImpl
				? (IPosDevKitPackageImpl) registeredIPosDevKitPackage
				: new IPosDevKitPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPos_DatamodelPackage.eNS_URI);
		IPos_DatamodelPackageImpl theIPos_DatamodelPackage = (IPos_DatamodelPackageImpl) (registeredPackage instanceof IPos_DatamodelPackageImpl
				? registeredPackage
				: IPos_DatamodelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OFBizPackage.eNS_URI);
		OFBizPackageImpl theOFBizPackage = (OFBizPackageImpl) (registeredPackage instanceof OFBizPackageImpl
				? registeredPackage
				: OFBizPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ToozPackage.eNS_URI);
		ToozPackageImpl theToozPackage = (ToozPackageImpl) (registeredPackage instanceof ToozPackageImpl
				? registeredPackage
				: ToozPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VDA5050Package.eNS_URI);
		VDA5050PackageImpl theVDA5050Package = (VDA5050PackageImpl) (registeredPackage instanceof VDA5050PackageImpl
				? registeredPackage
				: VDA5050Package.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OSMPackage.eNS_URI);
		OSMPackageImpl theOSMPackage = (OSMPackageImpl) (registeredPackage instanceof OSMPackageImpl ? registeredPackage
				: OSMPackage.eINSTANCE);

		// Create package meta-data objects
		theIPosDevKitPackage.createPackageContents();
		theIPos_DatamodelPackage.createPackageContents();
		theOFBizPackage.createPackageContents();
		theToozPackage.createPackageContents();
		theVDA5050Package.createPackageContents();
		theOSMPackage.createPackageContents();

		// Initialize created meta-data
		theIPosDevKitPackage.initializePackageContents();
		theIPos_DatamodelPackage.initializePackageContents();
		theOFBizPackage.initializePackageContents();
		theToozPackage.initializePackageContents();
		theVDA5050Package.initializePackageContents();
		theOSMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theIPosDevKitPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(IPosDevKitPackage.eNS_URI, theIPosDevKitPackage);
		return theIPosDevKitPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTrackingRequest() {
		return trackingRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitoringRequest() {
		return monitoringRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_FrameIds() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_Delta() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_UpdateFrequency() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_Type() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_Id() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_FusionStrategy() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_ExitNotification() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_Properties() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_MonitoringTaskId() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_RequestorProtocol() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_SerializationType() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringRequest_RefSystemId() {
		return (EAttribute) monitoringRequestEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorldModelUpdateRequest() {
		return worldModelUpdateRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataStorageRequest() {
		return dataStorageRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorldModelQueryRequest() {
		return worldModelQueryRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataStorageQueryRequest() {
		return dataStorageQueryRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataStorageQueryRequest_TrackingTaskId() {
		return (EAttribute) dataStorageQueryRequestEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgentRegistrationRequest() {
		return agentRegistrationRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSensorConfigurationRequest() {
		return sensorConfigurationRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIposPositionEvent() {
		return iposPositionEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposPositionEvent_AgentId() {
		return (EAttribute) iposPositionEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposPositionEvent_SensorId() {
		return (EAttribute) iposPositionEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposPositionEvent_Type() {
		return (EAttribute) iposPositionEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposPositionEvent_SensorType() {
		return (EAttribute) iposPositionEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposPositionEvent_LastPosUpdate() {
		return (EAttribute) iposPositionEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIposPositionEvent_Position() {
		return (EReference) iposPositionEventEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIposPositionEvent_Orientation() {
		return (EReference) iposPositionEventEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIposPositionEvent_ZoneDescriptors() {
		return (EReference) iposPositionEventEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosRawdataEvent() {
		return iPosRawdataEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosRawdataEvent_AgentId() {
		return (EAttribute) iPosRawdataEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosRawdataEvent_SensorId() {
		return (EAttribute) iPosRawdataEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosRawdataEvent_AgentType() {
		return (EAttribute) iPosRawdataEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosRawdataEvent_SensorType() {
		return (EAttribute) iPosRawdataEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosRawdataEvent_TimeStamp() {
		return (EAttribute) iPosRawdataEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosBeaconEvent() {
		return iPosBeaconEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosBeaconEvent_Distances() {
		return (EAttribute) iPosBeaconEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosUWBEvent() {
		return iPosUWBEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosBTEvent() {
		return iPosBTEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosBTEvent_Rss() {
		return (EAttribute) iPosBTEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosProximityEvent() {
		return iPosProximityEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosProximityEvent_TagId() {
		return (EAttribute) iPosProximityEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosNFCEvent() {
		return iPosNFCEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosNFCEvent_TagData() {
		return (EAttribute) iPosNFCEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosBarcodeEvent() {
		return iPosBarcodeEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosRFIDEvent() {
		return iPosRFIDEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosRFIDEvent_Location() {
		return (EAttribute) iPosRFIDEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosOtherProxEvent() {
		return iPosOtherProxEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosOtherProxEvent_Data() {
		return (EAttribute) iPosOtherProxEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPosOtherBeaconEvent() {
		return iPosOtherBeaconEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIPosOtherBeaconEvent_Data() {
		return (EAttribute) iPosOtherBeaconEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIposMsgRcvEvent() {
		return iposMsgRcvEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposMsgRcvEvent_ProtocolName() {
		return (EAttribute) iposMsgRcvEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposMsgRcvEvent_SerializedMsg() {
		return (EAttribute) iposMsgRcvEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposMsgRcvEvent_LastPosUpdate() {
		return (EAttribute) iposMsgRcvEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIposMsgRcvEvent_LastKnownPosition() {
		return (EReference) iposMsgRcvEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIposMsgRcvEvent_LastKnownOrientation() {
		return (EReference) iposMsgRcvEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIposMsgRcvEvent_LastKnownZonedescriptors() {
		return (EReference) iposMsgRcvEventEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposMsgRcvEvent_AgentId() {
		return (EAttribute) iposMsgRcvEventEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposMsgRcvEvent_ExtractedAttributes() {
		return (EAttribute) iposMsgRcvEventEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIposMsgRcvEvent_Timestamp() {
		return (EAttribute) iposMsgRcvEventEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosDevKitFactory getIPosDevKitFactory() {
		return (IPosDevKitFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		trackingRequestEClass = createEClass(TRACKING_REQUEST);

		monitoringRequestEClass = createEClass(MONITORING_REQUEST);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__FRAME_IDS);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__DELTA);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__UPDATE_FREQUENCY);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__TYPE);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__ID);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__FUSION_STRATEGY);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__EXIT_NOTIFICATION);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__PROPERTIES);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__MONITORING_TASK_ID);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__REQUESTOR_PROTOCOL);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__SERIALIZATION_TYPE);
		createEAttribute(monitoringRequestEClass, MONITORING_REQUEST__REF_SYSTEM_ID);

		worldModelUpdateRequestEClass = createEClass(WORLD_MODEL_UPDATE_REQUEST);

		dataStorageRequestEClass = createEClass(DATA_STORAGE_REQUEST);

		worldModelQueryRequestEClass = createEClass(WORLD_MODEL_QUERY_REQUEST);

		dataStorageQueryRequestEClass = createEClass(DATA_STORAGE_QUERY_REQUEST);
		createEAttribute(dataStorageQueryRequestEClass, DATA_STORAGE_QUERY_REQUEST__TRACKING_TASK_ID);

		agentRegistrationRequestEClass = createEClass(AGENT_REGISTRATION_REQUEST);

		sensorConfigurationRequestEClass = createEClass(SENSOR_CONFIGURATION_REQUEST);

		iposPositionEventEClass = createEClass(IPOS_POSITION_EVENT);
		createEAttribute(iposPositionEventEClass, IPOS_POSITION_EVENT__AGENT_ID);
		createEAttribute(iposPositionEventEClass, IPOS_POSITION_EVENT__SENSOR_ID);
		createEAttribute(iposPositionEventEClass, IPOS_POSITION_EVENT__TYPE);
		createEAttribute(iposPositionEventEClass, IPOS_POSITION_EVENT__SENSOR_TYPE);
		createEAttribute(iposPositionEventEClass, IPOS_POSITION_EVENT__LAST_POS_UPDATE);
		createEReference(iposPositionEventEClass, IPOS_POSITION_EVENT__POSITION);
		createEReference(iposPositionEventEClass, IPOS_POSITION_EVENT__ORIENTATION);
		createEReference(iposPositionEventEClass, IPOS_POSITION_EVENT__ZONE_DESCRIPTORS);

		iPosRawdataEventEClass = createEClass(IPOS_RAWDATA_EVENT);
		createEAttribute(iPosRawdataEventEClass, IPOS_RAWDATA_EVENT__AGENT_ID);
		createEAttribute(iPosRawdataEventEClass, IPOS_RAWDATA_EVENT__SENSOR_ID);
		createEAttribute(iPosRawdataEventEClass, IPOS_RAWDATA_EVENT__AGENT_TYPE);
		createEAttribute(iPosRawdataEventEClass, IPOS_RAWDATA_EVENT__SENSOR_TYPE);
		createEAttribute(iPosRawdataEventEClass, IPOS_RAWDATA_EVENT__TIME_STAMP);

		iPosBeaconEventEClass = createEClass(IPOS_BEACON_EVENT);
		createEAttribute(iPosBeaconEventEClass, IPOS_BEACON_EVENT__DISTANCES);

		iPosUWBEventEClass = createEClass(IPOS_UWB_EVENT);

		iPosBTEventEClass = createEClass(IPOS_BT_EVENT);
		createEAttribute(iPosBTEventEClass, IPOS_BT_EVENT__RSS);

		iPosProximityEventEClass = createEClass(IPOS_PROXIMITY_EVENT);
		createEAttribute(iPosProximityEventEClass, IPOS_PROXIMITY_EVENT__TAG_ID);

		iPosNFCEventEClass = createEClass(IPOS_NFC_EVENT);
		createEAttribute(iPosNFCEventEClass, IPOS_NFC_EVENT__TAG_DATA);

		iPosBarcodeEventEClass = createEClass(IPOS_BARCODE_EVENT);

		iPosRFIDEventEClass = createEClass(IPOS_RFID_EVENT);
		createEAttribute(iPosRFIDEventEClass, IPOS_RFID_EVENT__LOCATION);

		iPosOtherProxEventEClass = createEClass(IPOS_OTHER_PROX_EVENT);
		createEAttribute(iPosOtherProxEventEClass, IPOS_OTHER_PROX_EVENT__DATA);

		iPosOtherBeaconEventEClass = createEClass(IPOS_OTHER_BEACON_EVENT);
		createEAttribute(iPosOtherBeaconEventEClass, IPOS_OTHER_BEACON_EVENT__DATA);

		iposMsgRcvEventEClass = createEClass(IPOS_MSG_RCV_EVENT);
		createEAttribute(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__PROTOCOL_NAME);
		createEAttribute(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__SERIALIZED_MSG);
		createEAttribute(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__LAST_POS_UPDATE);
		createEReference(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION);
		createEReference(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION);
		createEReference(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__LAST_KNOWN_ZONEDESCRIPTORS);
		createEAttribute(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__AGENT_ID);
		createEAttribute(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__EXTRACTED_ATTRIBUTES);
		createEAttribute(iposMsgRcvEventEClass, IPOS_MSG_RCV_EVENT__TIMESTAMP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		IPos_DatamodelPackage theIPos_DatamodelPackage = (IPos_DatamodelPackage) EPackage.Registry.INSTANCE
				.getEPackage(IPos_DatamodelPackage.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE
				.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		iPosBeaconEventEClass.getESuperTypes().add(this.getIPosRawdataEvent());
		iPosUWBEventEClass.getESuperTypes().add(this.getIPosBeaconEvent());
		iPosBTEventEClass.getESuperTypes().add(this.getIPosBeaconEvent());
		iPosProximityEventEClass.getESuperTypes().add(this.getIPosRawdataEvent());
		iPosNFCEventEClass.getESuperTypes().add(this.getIPosProximityEvent());
		iPosBarcodeEventEClass.getESuperTypes().add(this.getIPosProximityEvent());
		iPosRFIDEventEClass.getESuperTypes().add(this.getIPosProximityEvent());
		iPosOtherProxEventEClass.getESuperTypes().add(this.getIPosProximityEvent());
		iPosOtherBeaconEventEClass.getESuperTypes().add(this.getIPosBeaconEvent());

		// Initialize classes, features, and operations; add parameters
		initEClass(trackingRequestEClass, TrackingRequest.class, "TrackingRequest", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(monitoringRequestEClass, MonitoringRequest.class, "MonitoringRequest", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMonitoringRequest_FrameIds(), theIPos_DatamodelPackage.getStringList(), "frameIds", null, 0,
				1, MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_Delta(), theXMLTypePackage.getFloat(), "delta", null, 0, 1,
				MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_UpdateFrequency(), theXMLTypePackage.getFloat(), "updateFrequency", null, 0,
				1, MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_Type(), theIPos_DatamodelPackage.getStringList(), "type", null, 0, 1,
				MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_Id(), theIPos_DatamodelPackage.getStringList(), "id", null, 0, 1,
				MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_FusionStrategy(), theXMLTypePackage.getString(), "fusionStrategy", null, 0,
				1, MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_ExitNotification(), theXMLTypePackage.getBoolean(), "exitNotification",
				null, 0, 1, MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_Properties(), theIPos_DatamodelPackage.getStringList(), "properties", null,
				0, 1, MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_MonitoringTaskId(), theXMLTypePackage.getString(), "monitoringTaskId", null,
				0, 1, MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_RequestorProtocol(), theXMLTypePackage.getString(), "requestorProtocol",
				null, 0, 1, MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_SerializationType(), theXMLTypePackage.getString(), "serializationType",
				null, 0, 1, MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoringRequest_RefSystemId(), theXMLTypePackage.getString(), "refSystemId", null, 0, 1,
				MonitoringRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(worldModelUpdateRequestEClass, WorldModelUpdateRequest.class, "WorldModelUpdateRequest",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataStorageRequestEClass, DataStorageRequest.class, "DataStorageRequest", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(worldModelQueryRequestEClass, WorldModelQueryRequest.class, "WorldModelQueryRequest", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataStorageQueryRequestEClass, DataStorageQueryRequest.class, "DataStorageQueryRequest",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataStorageQueryRequest_TrackingTaskId(), theXMLTypePackage.getString(), "trackingTaskId",
				null, 0, 1, DataStorageQueryRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agentRegistrationRequestEClass, AgentRegistrationRequest.class, "AgentRegistrationRequest",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sensorConfigurationRequestEClass, SensorConfigurationRequest.class, "SensorConfigurationRequest",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(iposPositionEventEClass, IposPositionEvent.class, "IposPositionEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIposPositionEvent_AgentId(), theXMLTypePackage.getString(), "agentId", null, 0, 1,
				IposPositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposPositionEvent_SensorId(), theXMLTypePackage.getString(), "sensorId", null, 0, 1,
				IposPositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposPositionEvent_Type(), theXMLTypePackage.getString(), "type", null, 0, 1,
				IposPositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposPositionEvent_SensorType(), theXMLTypePackage.getString(), "sensorType", null, 0, 1,
				IposPositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposPositionEvent_LastPosUpdate(), theXMLTypePackage.getString(), "lastPosUpdate", null, 0, 1,
				IposPositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getIposPositionEvent_Position(), theIPos_DatamodelPackage.getPosition(), null, "position", null,
				1, 1, IposPositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIposPositionEvent_Orientation(), theIPos_DatamodelPackage.getOrientation(), null,
				"orientation", null, 1, 1, IposPositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIposPositionEvent_ZoneDescriptors(), theIPos_DatamodelPackage.getZoneDescriptor(), null,
				"zoneDescriptors", null, 0, -1, IposPositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iPosRawdataEventEClass, IPosRawdataEvent.class, "IPosRawdataEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIPosRawdataEvent_AgentId(), ecorePackage.getEString(), "agentId", null, 0, 1,
				IPosRawdataEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIPosRawdataEvent_SensorId(), ecorePackage.getEString(), "sensorId", null, 0, 1,
				IPosRawdataEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIPosRawdataEvent_AgentType(), ecorePackage.getEString(), "agentType", null, 0, 1,
				IPosRawdataEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIPosRawdataEvent_SensorType(), ecorePackage.getEString(), "sensorType", null, 0, 1,
				IPosRawdataEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIPosRawdataEvent_TimeStamp(), ecorePackage.getEString(), "timeStamp", null, 0, 1,
				IPosRawdataEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(iPosBeaconEventEClass, IPosBeaconEvent.class, "IPosBeaconEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(theXMLTypePackage.getString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theXMLTypePackage.getDoubleObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getIPosBeaconEvent_Distances(), g1, "distances", null, 0, 1, IPosBeaconEvent.class, IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iPosUWBEventEClass, IPosUWBEvent.class, "IPosUWBEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(iPosBTEventEClass, IPosBTEvent.class, "IPosBTEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theXMLTypePackage.getDoubleObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getIPosBTEvent_Rss(), g1, "rss", null, 0, 1, IPosBTEvent.class, IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iPosProximityEventEClass, IPosProximityEvent.class, "IPosProximityEvent", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIPosProximityEvent_TagId(), ecorePackage.getEString(), "tagId", null, 0, 1,
				IPosProximityEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(iPosNFCEventEClass, IPosNFCEvent.class, "IPosNFCEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getIPosNFCEvent_TagData(), g1, "tagData", null, 0, 1, IPosNFCEvent.class, IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iPosBarcodeEventEClass, IPosBarcodeEvent.class, "IPosBarcodeEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(iPosRFIDEventEClass, IPosRFIDEvent.class, "IPosRFIDEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIPosRFIDEvent_Location(), ecorePackage.getEString(), "location", null, 0, 1,
				IPosRFIDEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(iPosOtherProxEventEClass, IPosOtherProxEvent.class, "IPosOtherProxEvent", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getIPosOtherProxEvent_Data(), g1, "data", null, 0, 1, IPosOtherProxEvent.class, IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iPosOtherBeaconEventEClass, IPosOtherBeaconEvent.class, "IPosOtherBeaconEvent", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getIPosOtherBeaconEvent_Data(), g1, "data", null, 0, 1, IPosOtherBeaconEvent.class, IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iposMsgRcvEventEClass, IposMsgRcvEvent.class, "IposMsgRcvEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIposMsgRcvEvent_ProtocolName(), ecorePackage.getEString(), "protocolName", null, 1, 1,
				IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposMsgRcvEvent_SerializedMsg(), theIPos_DatamodelPackage.getByteArray(), "serializedMsg",
				null, 0, 1, IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposMsgRcvEvent_LastPosUpdate(), ecorePackage.getEString(), "lastPosUpdate", null, 0, 1,
				IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getIposMsgRcvEvent_LastKnownPosition(), theIPos_DatamodelPackage.getPosition(), null,
				"lastKnownPosition", null, 0, 1, IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIposMsgRcvEvent_LastKnownOrientation(), theIPos_DatamodelPackage.getOrientation(), null,
				"lastKnownOrientation", null, 0, 1, IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIposMsgRcvEvent_LastKnownZonedescriptors(), theIPos_DatamodelPackage.getZoneDescriptor(),
				null, "lastKnownZonedescriptors", null, 0, -1, IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposMsgRcvEvent_AgentId(), ecorePackage.getEString(), "agentId", null, 1, 1,
				IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposMsgRcvEvent_ExtractedAttributes(), theIPos_DatamodelPackage.getListOfStringMaps(),
				"extractedAttributes", null, 0, 1, IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIposMsgRcvEvent_Timestamp(), ecorePackage.getEString(), "timestamp", null, 0, 1,
				IposMsgRcvEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
	}

} //IPosDevKitPackageImpl
