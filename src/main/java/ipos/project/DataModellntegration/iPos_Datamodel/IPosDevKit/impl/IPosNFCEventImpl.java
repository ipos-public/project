/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPos NFC Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosNFCEventImpl#getTagData <em>Tag Data</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IPosNFCEventImpl extends IPosProximityEventImpl implements IPosNFCEvent {
	/**
	 * The cached value of the '{@link #getTagData() <em>Tag Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTagData()
	 * @generated
	 * @ordered
	 */
	protected Map<String, Object> tagData;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPosNFCEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.IPOS_NFC_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<String, Object> getTagData() {
		return tagData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTagData(Map<String, Object> newTagData) {
		Map<String, Object> oldTagData = tagData;
		tagData = newTagData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_NFC_EVENT__TAG_DATA,
					oldTagData, tagData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_NFC_EVENT__TAG_DATA:
			return getTagData();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_NFC_EVENT__TAG_DATA:
			setTagData((Map<String, Object>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_NFC_EVENT__TAG_DATA:
			setTagData((Map<String, Object>) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_NFC_EVENT__TAG_DATA:
			return tagData != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (tagData: ");
		result.append(tagData);
		result.append(')');
		return result.toString();
	}

} //IPosNFCEventImpl
