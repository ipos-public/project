/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPos Rawdata Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl#getAgentId <em>Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl#getSensorId <em>Sensor Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl#getAgentType <em>Agent Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl#getSensorType <em>Sensor Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosRawdataEventImpl#getTimeStamp <em>Time Stamp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IPosRawdataEventImpl extends MinimalEObjectImpl.Container implements IPosRawdataEvent {
	/**
	 * The default value of the '{@link #getAgentId() <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentId()
	 * @generated
	 * @ordered
	 */
	protected static final String AGENT_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAgentId() <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentId()
	 * @generated
	 * @ordered
	 */
	protected String agentId = AGENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSensorId() <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorId()
	 * @generated
	 * @ordered
	 */
	protected static final String SENSOR_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorId() <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorId()
	 * @generated
	 * @ordered
	 */
	protected String sensorId = SENSOR_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getAgentType() <em>Agent Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentType()
	 * @generated
	 * @ordered
	 */
	protected static final String AGENT_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAgentType() <em>Agent Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentType()
	 * @generated
	 * @ordered
	 */
	protected String agentType = AGENT_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSensorType() <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorType()
	 * @generated
	 * @ordered
	 */
	protected static final String SENSOR_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorType() <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorType()
	 * @generated
	 * @ordered
	 */
	protected String sensorType = SENSOR_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected static final String TIME_STAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected String timeStamp = TIME_STAMP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPosRawdataEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.IPOS_RAWDATA_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentId(String newAgentId) {
		String oldAgentId = agentId;
		agentId = newAgentId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_ID,
					oldAgentId, agentId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSensorId() {
		return sensorId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorId(String newSensorId) {
		String oldSensorId = sensorId;
		sensorId = newSensorId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_ID,
					oldSensorId, sensorId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAgentType() {
		return agentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentType(String newAgentType) {
		String oldAgentType = agentType;
		agentType = newAgentType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_TYPE,
					oldAgentType, agentType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSensorType() {
		return sensorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorType(String newSensorType) {
		String oldSensorType = sensorType;
		sensorType = newSensorType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_TYPE,
					oldSensorType, sensorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeStamp(String newTimeStamp) {
		String oldTimeStamp = timeStamp;
		timeStamp = newTimeStamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_RAWDATA_EVENT__TIME_STAMP,
					oldTimeStamp, timeStamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_ID:
			return getAgentId();
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_ID:
			return getSensorId();
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_TYPE:
			return getAgentType();
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_TYPE:
			return getSensorType();
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__TIME_STAMP:
			return getTimeStamp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_ID:
			setAgentId((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_ID:
			setSensorId((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_TYPE:
			setAgentType((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_TYPE:
			setSensorType((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__TIME_STAMP:
			setTimeStamp((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_ID:
			setAgentId(AGENT_ID_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_ID:
			setSensorId(SENSOR_ID_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_TYPE:
			setAgentType(AGENT_TYPE_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_TYPE:
			setSensorType(SENSOR_TYPE_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__TIME_STAMP:
			setTimeStamp(TIME_STAMP_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_ID:
			return AGENT_ID_EDEFAULT == null ? agentId != null : !AGENT_ID_EDEFAULT.equals(agentId);
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_ID:
			return SENSOR_ID_EDEFAULT == null ? sensorId != null : !SENSOR_ID_EDEFAULT.equals(sensorId);
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__AGENT_TYPE:
			return AGENT_TYPE_EDEFAULT == null ? agentType != null : !AGENT_TYPE_EDEFAULT.equals(agentType);
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__SENSOR_TYPE:
			return SENSOR_TYPE_EDEFAULT == null ? sensorType != null : !SENSOR_TYPE_EDEFAULT.equals(sensorType);
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT__TIME_STAMP:
			return TIME_STAMP_EDEFAULT == null ? timeStamp != null : !TIME_STAMP_EDEFAULT.equals(timeStamp);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (agentId: ");
		result.append(agentId);
		result.append(", sensorId: ");
		result.append(sensorId);
		result.append(", agentType: ");
		result.append(agentType);
		result.append(", sensorType: ");
		result.append(sensorType);
		result.append(", timeStamp: ");
		result.append(timeStamp);
		result.append(')');
		return result.toString();
	}

} //IPosRawdataEventImpl
