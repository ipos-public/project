/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPos UWB Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IPosUWBEventImpl extends IPosBeaconEventImpl implements IPosUWBEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPosUWBEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.IPOS_UWB_EVENT;
	}

} //IPosUWBEventImpl
