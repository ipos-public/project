/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent;

import ipos.project.DataModellntegration.iPos_Datamodel.Orientation;
import ipos.project.DataModellntegration.iPos_Datamodel.Position;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ipos Msg Rcv Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getProtocolName <em>Protocol Name</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getSerializedMsg <em>Serialized Msg</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getLastPosUpdate <em>Last Pos Update</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getLastKnownPosition <em>Last Known Position</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getLastKnownOrientation <em>Last Known Orientation</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getLastKnownZonedescriptors <em>Last Known Zonedescriptors</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getAgentId <em>Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getExtractedAttributes <em>Extracted Attributes</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposMsgRcvEventImpl#getTimestamp <em>Timestamp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IposMsgRcvEventImpl extends MinimalEObjectImpl.Container implements IposMsgRcvEvent {
	/**
	 * The default value of the '{@link #getProtocolName() <em>Protocol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolName()
	 * @generated
	 * @ordered
	 */
	protected static final String PROTOCOL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProtocolName() <em>Protocol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolName()
	 * @generated
	 * @ordered
	 */
	protected String protocolName = PROTOCOL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSerializedMsg() <em>Serialized Msg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerializedMsg()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] SERIALIZED_MSG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSerializedMsg() <em>Serialized Msg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerializedMsg()
	 * @generated
	 * @ordered
	 */
	protected byte[] serializedMsg = SERIALIZED_MSG_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastPosUpdate() <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPosUpdate()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_POS_UPDATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastPosUpdate() <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPosUpdate()
	 * @generated
	 * @ordered
	 */
	protected String lastPosUpdate = LAST_POS_UPDATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLastKnownPosition() <em>Last Known Position</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastKnownPosition()
	 * @generated
	 * @ordered
	 */
	protected Position lastKnownPosition;

	/**
	 * The cached value of the '{@link #getLastKnownOrientation() <em>Last Known Orientation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastKnownOrientation()
	 * @generated
	 * @ordered
	 */
	protected Orientation lastKnownOrientation;

	/**
	 * The cached value of the '{@link #getLastKnownZonedescriptors() <em>Last Known Zonedescriptors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastKnownZonedescriptors()
	 * @generated
	 * @ordered
	 */
	protected EList<ZoneDescriptor> lastKnownZonedescriptors;

	/**
	 * The default value of the '{@link #getAgentId() <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentId()
	 * @generated
	 * @ordered
	 */
	protected static final String AGENT_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAgentId() <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentId()
	 * @generated
	 * @ordered
	 */
	protected String agentId = AGENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtractedAttributes() <em>Extracted Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtractedAttributes()
	 * @generated
	 * @ordered
	 */
	protected static final LinkedList<HashMap<String, String>> EXTRACTED_ATTRIBUTES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtractedAttributes() <em>Extracted Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtractedAttributes()
	 * @generated
	 * @ordered
	 */
	protected LinkedList<HashMap<String, String>> extractedAttributes = EXTRACTED_ATTRIBUTES_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final String TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected String timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IposMsgRcvEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.IPOS_MSG_RCV_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProtocolName() {
		return protocolName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtocolName(String newProtocolName) {
		String oldProtocolName = protocolName;
		protocolName = newProtocolName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_MSG_RCV_EVENT__PROTOCOL_NAME,
					oldProtocolName, protocolName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte[] getSerializedMsg() {
		return serializedMsg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSerializedMsg(byte[] newSerializedMsg) {
		byte[] oldSerializedMsg = serializedMsg;
		serializedMsg = newSerializedMsg;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_MSG_RCV_EVENT__SERIALIZED_MSG,
					oldSerializedMsg, serializedMsg));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastPosUpdate() {
		return lastPosUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastPosUpdate(String newLastPosUpdate) {
		String oldLastPosUpdate = lastPosUpdate;
		lastPosUpdate = newLastPosUpdate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_POS_UPDATE,
					oldLastPosUpdate, lastPosUpdate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Position getLastKnownPosition() {
		if (lastKnownPosition != null && lastKnownPosition.eIsProxy()) {
			InternalEObject oldLastKnownPosition = (InternalEObject) lastKnownPosition;
			lastKnownPosition = (Position) eResolveProxy(oldLastKnownPosition);
			if (lastKnownPosition != oldLastKnownPosition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION, oldLastKnownPosition,
							lastKnownPosition));
			}
		}
		return lastKnownPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Position basicGetLastKnownPosition() {
		return lastKnownPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastKnownPosition(Position newLastKnownPosition) {
		Position oldLastKnownPosition = lastKnownPosition;
		lastKnownPosition = newLastKnownPosition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION, oldLastKnownPosition,
					lastKnownPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Orientation getLastKnownOrientation() {
		if (lastKnownOrientation != null && lastKnownOrientation.eIsProxy()) {
			InternalEObject oldLastKnownOrientation = (InternalEObject) lastKnownOrientation;
			lastKnownOrientation = (Orientation) eResolveProxy(oldLastKnownOrientation);
			if (lastKnownOrientation != oldLastKnownOrientation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION, oldLastKnownOrientation,
							lastKnownOrientation));
			}
		}
		return lastKnownOrientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Orientation basicGetLastKnownOrientation() {
		return lastKnownOrientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastKnownOrientation(Orientation newLastKnownOrientation) {
		Orientation oldLastKnownOrientation = lastKnownOrientation;
		lastKnownOrientation = newLastKnownOrientation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION, oldLastKnownOrientation,
					lastKnownOrientation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ZoneDescriptor> getLastKnownZonedescriptors() {
		if (lastKnownZonedescriptors == null) {
			lastKnownZonedescriptors = new EObjectResolvingEList<ZoneDescriptor>(ZoneDescriptor.class, this,
					IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ZONEDESCRIPTORS);
		}
		return lastKnownZonedescriptors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentId(String newAgentId) {
		String oldAgentId = agentId;
		agentId = newAgentId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_MSG_RCV_EVENT__AGENT_ID,
					oldAgentId, agentId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkedList<HashMap<String, String>> getExtractedAttributes() {
		return extractedAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtractedAttributes(LinkedList<HashMap<String, String>> newExtractedAttributes) {
		LinkedList<HashMap<String, String>> oldExtractedAttributes = extractedAttributes;
		extractedAttributes = newExtractedAttributes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.IPOS_MSG_RCV_EVENT__EXTRACTED_ATTRIBUTES, oldExtractedAttributes,
					extractedAttributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimestamp(String newTimestamp) {
		String oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_MSG_RCV_EVENT__TIMESTAMP,
					oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__PROTOCOL_NAME:
			return getProtocolName();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__SERIALIZED_MSG:
			return getSerializedMsg();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_POS_UPDATE:
			return getLastPosUpdate();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION:
			if (resolve)
				return getLastKnownPosition();
			return basicGetLastKnownPosition();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION:
			if (resolve)
				return getLastKnownOrientation();
			return basicGetLastKnownOrientation();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ZONEDESCRIPTORS:
			return getLastKnownZonedescriptors();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__AGENT_ID:
			return getAgentId();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__EXTRACTED_ATTRIBUTES:
			return getExtractedAttributes();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__TIMESTAMP:
			return getTimestamp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__PROTOCOL_NAME:
			setProtocolName((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__SERIALIZED_MSG:
			setSerializedMsg((byte[]) newValue);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_POS_UPDATE:
			setLastPosUpdate((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION:
			setLastKnownPosition((Position) newValue);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION:
			setLastKnownOrientation((Orientation) newValue);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ZONEDESCRIPTORS:
			getLastKnownZonedescriptors().clear();
			getLastKnownZonedescriptors().addAll((Collection<? extends ZoneDescriptor>) newValue);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__AGENT_ID:
			setAgentId((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__EXTRACTED_ATTRIBUTES:
			setExtractedAttributes((LinkedList<HashMap<String, String>>) newValue);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__TIMESTAMP:
			setTimestamp((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__PROTOCOL_NAME:
			setProtocolName(PROTOCOL_NAME_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__SERIALIZED_MSG:
			setSerializedMsg(SERIALIZED_MSG_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_POS_UPDATE:
			setLastPosUpdate(LAST_POS_UPDATE_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION:
			setLastKnownPosition((Position) null);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION:
			setLastKnownOrientation((Orientation) null);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ZONEDESCRIPTORS:
			getLastKnownZonedescriptors().clear();
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__AGENT_ID:
			setAgentId(AGENT_ID_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__EXTRACTED_ATTRIBUTES:
			setExtractedAttributes(EXTRACTED_ATTRIBUTES_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__TIMESTAMP:
			setTimestamp(TIMESTAMP_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__PROTOCOL_NAME:
			return PROTOCOL_NAME_EDEFAULT == null ? protocolName != null : !PROTOCOL_NAME_EDEFAULT.equals(protocolName);
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__SERIALIZED_MSG:
			return SERIALIZED_MSG_EDEFAULT == null ? serializedMsg != null
					: !SERIALIZED_MSG_EDEFAULT.equals(serializedMsg);
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_POS_UPDATE:
			return LAST_POS_UPDATE_EDEFAULT == null ? lastPosUpdate != null
					: !LAST_POS_UPDATE_EDEFAULT.equals(lastPosUpdate);
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_POSITION:
			return lastKnownPosition != null;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ORIENTATION:
			return lastKnownOrientation != null;
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__LAST_KNOWN_ZONEDESCRIPTORS:
			return lastKnownZonedescriptors != null && !lastKnownZonedescriptors.isEmpty();
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__AGENT_ID:
			return AGENT_ID_EDEFAULT == null ? agentId != null : !AGENT_ID_EDEFAULT.equals(agentId);
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__EXTRACTED_ATTRIBUTES:
			return EXTRACTED_ATTRIBUTES_EDEFAULT == null ? extractedAttributes != null
					: !EXTRACTED_ATTRIBUTES_EDEFAULT.equals(extractedAttributes);
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT__TIMESTAMP:
			return TIMESTAMP_EDEFAULT == null ? timestamp != null : !TIMESTAMP_EDEFAULT.equals(timestamp);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (protocolName: ");
		result.append(protocolName);
		result.append(", serializedMsg: ");
		result.append(serializedMsg);
		result.append(", lastPosUpdate: ");
		result.append(lastPosUpdate);
		result.append(", agentId: ");
		result.append(agentId);
		result.append(", extractedAttributes: ");
		result.append(extractedAttributes);
		result.append(", timestamp: ");
		result.append(timestamp);
		result.append(')');
		return result.toString();
	}

} //IposMsgRcvEventImpl
