/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;

import ipos.project.DataModellntegration.iPos_Datamodel.Orientation;
import ipos.project.DataModellntegration.iPos_Datamodel.Position;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ipos Position Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl#getAgentId <em>Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl#getSensorId <em>Sensor Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl#getType <em>Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl#getSensorType <em>Sensor Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl#getLastPosUpdate <em>Last Pos Update</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl#getOrientation <em>Orientation</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IposPositionEventImpl#getZoneDescriptors <em>Zone Descriptors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IposPositionEventImpl extends MinimalEObjectImpl.Container implements IposPositionEvent {
	/**
	 * The default value of the '{@link #getAgentId() <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentId()
	 * @generated
	 * @ordered
	 */
	protected static final String AGENT_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAgentId() <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentId()
	 * @generated
	 * @ordered
	 */
	protected String agentId = AGENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSensorId() <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorId()
	 * @generated
	 * @ordered
	 */
	protected static final String SENSOR_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorId() <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorId()
	 * @generated
	 * @ordered
	 */
	protected String sensorId = SENSOR_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSensorType() <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorType()
	 * @generated
	 * @ordered
	 */
	protected static final String SENSOR_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorType() <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorType()
	 * @generated
	 * @ordered
	 */
	protected String sensorType = SENSOR_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastPosUpdate() <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPosUpdate()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_POS_UPDATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastPosUpdate() <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPosUpdate()
	 * @generated
	 * @ordered
	 */
	protected String lastPosUpdate = LAST_POS_UPDATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected Position position;

	/**
	 * The cached value of the '{@link #getOrientation() <em>Orientation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrientation()
	 * @generated
	 * @ordered
	 */
	protected Orientation orientation;

	/**
	 * The cached value of the '{@link #getZoneDescriptors() <em>Zone Descriptors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZoneDescriptors()
	 * @generated
	 * @ordered
	 */
	protected EList<ZoneDescriptor> zoneDescriptors;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IposPositionEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.IPOS_POSITION_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentId(String newAgentId) {
		String oldAgentId = agentId;
		agentId = newAgentId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_POSITION_EVENT__AGENT_ID,
					oldAgentId, agentId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSensorId() {
		return sensorId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorId(String newSensorId) {
		String oldSensorId = sensorId;
		sensorId = newSensorId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_ID,
					oldSensorId, sensorId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_POSITION_EVENT__TYPE, oldType,
					type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSensorType() {
		return sensorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorType(String newSensorType) {
		String oldSensorType = sensorType;
		sensorType = newSensorType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_TYPE,
					oldSensorType, sensorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastPosUpdate() {
		return lastPosUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastPosUpdate(String newLastPosUpdate) {
		String oldLastPosUpdate = lastPosUpdate;
		lastPosUpdate = newLastPosUpdate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.IPOS_POSITION_EVENT__LAST_POS_UPDATE, oldLastPosUpdate, lastPosUpdate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Position getPosition() {
		if (position != null && position.eIsProxy()) {
			InternalEObject oldPosition = (InternalEObject) position;
			position = (Position) eResolveProxy(oldPosition);
			if (position != oldPosition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPosDevKitPackage.IPOS_POSITION_EVENT__POSITION, oldPosition, position));
			}
		}
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Position basicGetPosition() {
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(Position newPosition) {
		Position oldPosition = position;
		position = newPosition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_POSITION_EVENT__POSITION,
					oldPosition, position));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Orientation getOrientation() {
		if (orientation != null && orientation.eIsProxy()) {
			InternalEObject oldOrientation = (InternalEObject) orientation;
			orientation = (Orientation) eResolveProxy(oldOrientation);
			if (orientation != oldOrientation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPosDevKitPackage.IPOS_POSITION_EVENT__ORIENTATION, oldOrientation, orientation));
			}
		}
		return orientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Orientation basicGetOrientation() {
		return orientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrientation(Orientation newOrientation) {
		Orientation oldOrientation = orientation;
		orientation = newOrientation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.IPOS_POSITION_EVENT__ORIENTATION,
					oldOrientation, orientation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ZoneDescriptor> getZoneDescriptors() {
		if (zoneDescriptors == null) {
			zoneDescriptors = new EObjectResolvingEList<ZoneDescriptor>(ZoneDescriptor.class, this,
					IPosDevKitPackage.IPOS_POSITION_EVENT__ZONE_DESCRIPTORS);
		}
		return zoneDescriptors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_POSITION_EVENT__AGENT_ID:
			return getAgentId();
		case IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_ID:
			return getSensorId();
		case IPosDevKitPackage.IPOS_POSITION_EVENT__TYPE:
			return getType();
		case IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_TYPE:
			return getSensorType();
		case IPosDevKitPackage.IPOS_POSITION_EVENT__LAST_POS_UPDATE:
			return getLastPosUpdate();
		case IPosDevKitPackage.IPOS_POSITION_EVENT__POSITION:
			if (resolve)
				return getPosition();
			return basicGetPosition();
		case IPosDevKitPackage.IPOS_POSITION_EVENT__ORIENTATION:
			if (resolve)
				return getOrientation();
			return basicGetOrientation();
		case IPosDevKitPackage.IPOS_POSITION_EVENT__ZONE_DESCRIPTORS:
			return getZoneDescriptors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_POSITION_EVENT__AGENT_ID:
			setAgentId((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_ID:
			setSensorId((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__TYPE:
			setType((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_TYPE:
			setSensorType((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__LAST_POS_UPDATE:
			setLastPosUpdate((String) newValue);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__POSITION:
			setPosition((Position) newValue);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__ORIENTATION:
			setOrientation((Orientation) newValue);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__ZONE_DESCRIPTORS:
			getZoneDescriptors().clear();
			getZoneDescriptors().addAll((Collection<? extends ZoneDescriptor>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_POSITION_EVENT__AGENT_ID:
			setAgentId(AGENT_ID_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_ID:
			setSensorId(SENSOR_ID_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_TYPE:
			setSensorType(SENSOR_TYPE_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__LAST_POS_UPDATE:
			setLastPosUpdate(LAST_POS_UPDATE_EDEFAULT);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__POSITION:
			setPosition((Position) null);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__ORIENTATION:
			setOrientation((Orientation) null);
			return;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__ZONE_DESCRIPTORS:
			getZoneDescriptors().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.IPOS_POSITION_EVENT__AGENT_ID:
			return AGENT_ID_EDEFAULT == null ? agentId != null : !AGENT_ID_EDEFAULT.equals(agentId);
		case IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_ID:
			return SENSOR_ID_EDEFAULT == null ? sensorId != null : !SENSOR_ID_EDEFAULT.equals(sensorId);
		case IPosDevKitPackage.IPOS_POSITION_EVENT__TYPE:
			return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		case IPosDevKitPackage.IPOS_POSITION_EVENT__SENSOR_TYPE:
			return SENSOR_TYPE_EDEFAULT == null ? sensorType != null : !SENSOR_TYPE_EDEFAULT.equals(sensorType);
		case IPosDevKitPackage.IPOS_POSITION_EVENT__LAST_POS_UPDATE:
			return LAST_POS_UPDATE_EDEFAULT == null ? lastPosUpdate != null
					: !LAST_POS_UPDATE_EDEFAULT.equals(lastPosUpdate);
		case IPosDevKitPackage.IPOS_POSITION_EVENT__POSITION:
			return position != null;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__ORIENTATION:
			return orientation != null;
		case IPosDevKitPackage.IPOS_POSITION_EVENT__ZONE_DESCRIPTORS:
			return zoneDescriptors != null && !zoneDescriptors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (agentId: ");
		result.append(agentId);
		result.append(", sensorId: ");
		result.append(sensorId);
		result.append(", type: ");
		result.append(type);
		result.append(", sensorType: ");
		result.append(sensorType);
		result.append(", lastPosUpdate: ");
		result.append(lastPosUpdate);
		result.append(')');
		return result.toString();
	}

} //IposPositionEventImpl
