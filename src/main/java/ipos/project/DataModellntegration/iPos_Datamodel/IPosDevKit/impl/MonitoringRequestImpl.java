/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitoring Request</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getFrameIds <em>Frame Ids</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getDelta <em>Delta</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getUpdateFrequency <em>Update Frequency</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getType <em>Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getId <em>Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getFusionStrategy <em>Fusion Strategy</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#isExitNotification <em>Exit Notification</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getMonitoringTaskId <em>Monitoring Task Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getRequestorProtocol <em>Requestor Protocol</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getSerializationType <em>Serialization Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.MonitoringRequestImpl#getRefSystemId <em>Ref System Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MonitoringRequestImpl extends MinimalEObjectImpl.Container implements MonitoringRequest {
	/**
	 * The default value of the '{@link #getFrameIds() <em>Frame Ids</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameIds()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> FRAME_IDS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFrameIds() <em>Frame Ids</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameIds()
	 * @generated
	 * @ordered
	 */
	protected List<String> frameIds = FRAME_IDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getDelta() <em>Delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelta()
	 * @generated
	 * @ordered
	 */
	protected static final float DELTA_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getDelta() <em>Delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelta()
	 * @generated
	 * @ordered
	 */
	protected float delta = DELTA_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpdateFrequency() <em>Update Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdateFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final float UPDATE_FREQUENCY_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getUpdateFrequency() <em>Update Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdateFrequency()
	 * @generated
	 * @ordered
	 */
	protected float updateFrequency = UPDATE_FREQUENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected List<String> type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected List<String> id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getFusionStrategy() <em>Fusion Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFusionStrategy()
	 * @generated
	 * @ordered
	 */
	protected static final String FUSION_STRATEGY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFusionStrategy() <em>Fusion Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFusionStrategy()
	 * @generated
	 * @ordered
	 */
	protected String fusionStrategy = FUSION_STRATEGY_EDEFAULT;

	/**
	 * The default value of the '{@link #isExitNotification() <em>Exit Notification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExitNotification()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXIT_NOTIFICATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExitNotification() <em>Exit Notification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExitNotification()
	 * @generated
	 * @ordered
	 */
	protected boolean exitNotification = EXIT_NOTIFICATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getProperties() <em>Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> PROPERTIES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected List<String> properties = PROPERTIES_EDEFAULT;

	/**
	 * The default value of the '{@link #getMonitoringTaskId() <em>Monitoring Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringTaskId()
	 * @generated
	 * @ordered
	 */
	protected static final String MONITORING_TASK_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMonitoringTaskId() <em>Monitoring Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringTaskId()
	 * @generated
	 * @ordered
	 */
	protected String monitoringTaskId = MONITORING_TASK_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getRequestorProtocol() <em>Requestor Protocol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequestorProtocol()
	 * @generated
	 * @ordered
	 */
	protected static final String REQUESTOR_PROTOCOL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRequestorProtocol() <em>Requestor Protocol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequestorProtocol()
	 * @generated
	 * @ordered
	 */
	protected String requestorProtocol = REQUESTOR_PROTOCOL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSerializationType() <em>Serialization Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerializationType()
	 * @generated
	 * @ordered
	 */
	protected static final String SERIALIZATION_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSerializationType() <em>Serialization Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerializationType()
	 * @generated
	 * @ordered
	 */
	protected String serializationType = SERIALIZATION_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRefSystemId() <em>Ref System Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefSystemId()
	 * @generated
	 * @ordered
	 */
	protected static final String REF_SYSTEM_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRefSystemId() <em>Ref System Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefSystemId()
	 * @generated
	 * @ordered
	 */
	protected String refSystemId = REF_SYSTEM_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoringRequestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.MONITORING_REQUEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getFrameIds() {
		return frameIds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrameIds(List<String> newFrameIds) {
		List<String> oldFrameIds = frameIds;
		frameIds = newFrameIds;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.MONITORING_REQUEST__FRAME_IDS,
					oldFrameIds, frameIds));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getDelta() {
		return delta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDelta(float newDelta) {
		float oldDelta = delta;
		delta = newDelta;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.MONITORING_REQUEST__DELTA, oldDelta,
					delta));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getUpdateFrequency() {
		return updateFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpdateFrequency(float newUpdateFrequency) {
		float oldUpdateFrequency = updateFrequency;
		updateFrequency = newUpdateFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.MONITORING_REQUEST__UPDATE_FREQUENCY, oldUpdateFrequency, updateFrequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(List<String> newType) {
		List<String> oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.MONITORING_REQUEST__TYPE, oldType,
					type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(List<String> newId) {
		List<String> oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.MONITORING_REQUEST__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFusionStrategy() {
		return fusionStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFusionStrategy(String newFusionStrategy) {
		String oldFusionStrategy = fusionStrategy;
		fusionStrategy = newFusionStrategy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.MONITORING_REQUEST__FUSION_STRATEGY,
					oldFusionStrategy, fusionStrategy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExitNotification() {
		return exitNotification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExitNotification(boolean newExitNotification) {
		boolean oldExitNotification = exitNotification;
		exitNotification = newExitNotification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.MONITORING_REQUEST__EXIT_NOTIFICATION, oldExitNotification, exitNotification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getProperties() {
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperties(List<String> newProperties) {
		List<String> oldProperties = properties;
		properties = newProperties;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.MONITORING_REQUEST__PROPERTIES,
					oldProperties, properties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMonitoringTaskId() {
		return monitoringTaskId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitoringTaskId(String newMonitoringTaskId) {
		String oldMonitoringTaskId = monitoringTaskId;
		monitoringTaskId = newMonitoringTaskId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.MONITORING_REQUEST__MONITORING_TASK_ID, oldMonitoringTaskId, monitoringTaskId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRequestorProtocol() {
		return requestorProtocol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequestorProtocol(String newRequestorProtocol) {
		String oldRequestorProtocol = requestorProtocol;
		requestorProtocol = newRequestorProtocol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.MONITORING_REQUEST__REQUESTOR_PROTOCOL, oldRequestorProtocol, requestorProtocol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSerializationType() {
		return serializationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSerializationType(String newSerializationType) {
		String oldSerializationType = serializationType;
		serializationType = newSerializationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPosDevKitPackage.MONITORING_REQUEST__SERIALIZATION_TYPE, oldSerializationType, serializationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRefSystemId() {
		return refSystemId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefSystemId(String newRefSystemId) {
		String oldRefSystemId = refSystemId;
		refSystemId = newRefSystemId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPosDevKitPackage.MONITORING_REQUEST__REF_SYSTEM_ID,
					oldRefSystemId, refSystemId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPosDevKitPackage.MONITORING_REQUEST__FRAME_IDS:
			return getFrameIds();
		case IPosDevKitPackage.MONITORING_REQUEST__DELTA:
			return getDelta();
		case IPosDevKitPackage.MONITORING_REQUEST__UPDATE_FREQUENCY:
			return getUpdateFrequency();
		case IPosDevKitPackage.MONITORING_REQUEST__TYPE:
			return getType();
		case IPosDevKitPackage.MONITORING_REQUEST__ID:
			return getId();
		case IPosDevKitPackage.MONITORING_REQUEST__FUSION_STRATEGY:
			return getFusionStrategy();
		case IPosDevKitPackage.MONITORING_REQUEST__EXIT_NOTIFICATION:
			return isExitNotification();
		case IPosDevKitPackage.MONITORING_REQUEST__PROPERTIES:
			return getProperties();
		case IPosDevKitPackage.MONITORING_REQUEST__MONITORING_TASK_ID:
			return getMonitoringTaskId();
		case IPosDevKitPackage.MONITORING_REQUEST__REQUESTOR_PROTOCOL:
			return getRequestorProtocol();
		case IPosDevKitPackage.MONITORING_REQUEST__SERIALIZATION_TYPE:
			return getSerializationType();
		case IPosDevKitPackage.MONITORING_REQUEST__REF_SYSTEM_ID:
			return getRefSystemId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPosDevKitPackage.MONITORING_REQUEST__FRAME_IDS:
			setFrameIds((List<String>) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__DELTA:
			setDelta((Float) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__UPDATE_FREQUENCY:
			setUpdateFrequency((Float) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__TYPE:
			setType((List<String>) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__ID:
			setId((List<String>) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__FUSION_STRATEGY:
			setFusionStrategy((String) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__EXIT_NOTIFICATION:
			setExitNotification((Boolean) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__PROPERTIES:
			setProperties((List<String>) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__MONITORING_TASK_ID:
			setMonitoringTaskId((String) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__REQUESTOR_PROTOCOL:
			setRequestorProtocol((String) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__SERIALIZATION_TYPE:
			setSerializationType((String) newValue);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__REF_SYSTEM_ID:
			setRefSystemId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.MONITORING_REQUEST__FRAME_IDS:
			setFrameIds(FRAME_IDS_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__DELTA:
			setDelta(DELTA_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__UPDATE_FREQUENCY:
			setUpdateFrequency(UPDATE_FREQUENCY_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__ID:
			setId(ID_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__FUSION_STRATEGY:
			setFusionStrategy(FUSION_STRATEGY_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__EXIT_NOTIFICATION:
			setExitNotification(EXIT_NOTIFICATION_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__PROPERTIES:
			setProperties(PROPERTIES_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__MONITORING_TASK_ID:
			setMonitoringTaskId(MONITORING_TASK_ID_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__REQUESTOR_PROTOCOL:
			setRequestorProtocol(REQUESTOR_PROTOCOL_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__SERIALIZATION_TYPE:
			setSerializationType(SERIALIZATION_TYPE_EDEFAULT);
			return;
		case IPosDevKitPackage.MONITORING_REQUEST__REF_SYSTEM_ID:
			setRefSystemId(REF_SYSTEM_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPosDevKitPackage.MONITORING_REQUEST__FRAME_IDS:
			return FRAME_IDS_EDEFAULT == null ? frameIds != null : !FRAME_IDS_EDEFAULT.equals(frameIds);
		case IPosDevKitPackage.MONITORING_REQUEST__DELTA:
			return delta != DELTA_EDEFAULT;
		case IPosDevKitPackage.MONITORING_REQUEST__UPDATE_FREQUENCY:
			return updateFrequency != UPDATE_FREQUENCY_EDEFAULT;
		case IPosDevKitPackage.MONITORING_REQUEST__TYPE:
			return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		case IPosDevKitPackage.MONITORING_REQUEST__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case IPosDevKitPackage.MONITORING_REQUEST__FUSION_STRATEGY:
			return FUSION_STRATEGY_EDEFAULT == null ? fusionStrategy != null
					: !FUSION_STRATEGY_EDEFAULT.equals(fusionStrategy);
		case IPosDevKitPackage.MONITORING_REQUEST__EXIT_NOTIFICATION:
			return exitNotification != EXIT_NOTIFICATION_EDEFAULT;
		case IPosDevKitPackage.MONITORING_REQUEST__PROPERTIES:
			return PROPERTIES_EDEFAULT == null ? properties != null : !PROPERTIES_EDEFAULT.equals(properties);
		case IPosDevKitPackage.MONITORING_REQUEST__MONITORING_TASK_ID:
			return MONITORING_TASK_ID_EDEFAULT == null ? monitoringTaskId != null
					: !MONITORING_TASK_ID_EDEFAULT.equals(monitoringTaskId);
		case IPosDevKitPackage.MONITORING_REQUEST__REQUESTOR_PROTOCOL:
			return REQUESTOR_PROTOCOL_EDEFAULT == null ? requestorProtocol != null
					: !REQUESTOR_PROTOCOL_EDEFAULT.equals(requestorProtocol);
		case IPosDevKitPackage.MONITORING_REQUEST__SERIALIZATION_TYPE:
			return SERIALIZATION_TYPE_EDEFAULT == null ? serializationType != null
					: !SERIALIZATION_TYPE_EDEFAULT.equals(serializationType);
		case IPosDevKitPackage.MONITORING_REQUEST__REF_SYSTEM_ID:
			return REF_SYSTEM_ID_EDEFAULT == null ? refSystemId != null : !REF_SYSTEM_ID_EDEFAULT.equals(refSystemId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (frameIds: ");
		result.append(frameIds);
		result.append(", delta: ");
		result.append(delta);
		result.append(", updateFrequency: ");
		result.append(updateFrequency);
		result.append(", type: ");
		result.append(type);
		result.append(", id: ");
		result.append(id);
		result.append(", fusionStrategy: ");
		result.append(fusionStrategy);
		result.append(", exitNotification: ");
		result.append(exitNotification);
		result.append(", properties: ");
		result.append(properties);
		result.append(", monitoringTaskId: ");
		result.append(monitoringTaskId);
		result.append(", requestorProtocol: ");
		result.append(requestorProtocol);
		result.append(", serializationType: ");
		result.append(serializationType);
		result.append(", refSystemId: ");
		result.append(refSystemId);
		result.append(')');
		return result.toString();
	}

} //MonitoringRequestImpl
