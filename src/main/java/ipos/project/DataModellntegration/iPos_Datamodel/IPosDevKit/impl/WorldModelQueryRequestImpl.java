/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelQueryRequest;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>World Model Query Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WorldModelQueryRequestImpl extends MinimalEObjectImpl.Container implements WorldModelQueryRequest {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorldModelQueryRequestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPosDevKitPackage.Literals.WORLD_MODEL_QUERY_REQUEST;
	}

} //WorldModelQueryRequestImpl
