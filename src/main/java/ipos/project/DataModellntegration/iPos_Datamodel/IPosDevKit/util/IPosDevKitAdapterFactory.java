/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.util;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage
 * @generated
 */
public class IPosDevKitAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static IPosDevKitPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosDevKitAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = IPosDevKitPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPosDevKitSwitch<Adapter> modelSwitch = new IPosDevKitSwitch<Adapter>() {
		@Override
		public Adapter caseTrackingRequest(TrackingRequest object) {
			return createTrackingRequestAdapter();
		}

		@Override
		public Adapter caseMonitoringRequest(MonitoringRequest object) {
			return createMonitoringRequestAdapter();
		}

		@Override
		public Adapter caseWorldModelUpdateRequest(WorldModelUpdateRequest object) {
			return createWorldModelUpdateRequestAdapter();
		}

		@Override
		public Adapter caseDataStorageRequest(DataStorageRequest object) {
			return createDataStorageRequestAdapter();
		}

		@Override
		public Adapter caseWorldModelQueryRequest(WorldModelQueryRequest object) {
			return createWorldModelQueryRequestAdapter();
		}

		@Override
		public Adapter caseDataStorageQueryRequest(DataStorageQueryRequest object) {
			return createDataStorageQueryRequestAdapter();
		}

		@Override
		public Adapter caseAgentRegistrationRequest(AgentRegistrationRequest object) {
			return createAgentRegistrationRequestAdapter();
		}

		@Override
		public Adapter caseSensorConfigurationRequest(SensorConfigurationRequest object) {
			return createSensorConfigurationRequestAdapter();
		}

		@Override
		public Adapter caseIposPositionEvent(IposPositionEvent object) {
			return createIposPositionEventAdapter();
		}

		@Override
		public Adapter caseIPosRawdataEvent(IPosRawdataEvent object) {
			return createIPosRawdataEventAdapter();
		}

		@Override
		public Adapter caseIPosBeaconEvent(IPosBeaconEvent object) {
			return createIPosBeaconEventAdapter();
		}

		@Override
		public Adapter caseIPosUWBEvent(IPosUWBEvent object) {
			return createIPosUWBEventAdapter();
		}

		@Override
		public Adapter caseIPosBTEvent(IPosBTEvent object) {
			return createIPosBTEventAdapter();
		}

		@Override
		public Adapter caseIPosProximityEvent(IPosProximityEvent object) {
			return createIPosProximityEventAdapter();
		}

		@Override
		public Adapter caseIPosNFCEvent(IPosNFCEvent object) {
			return createIPosNFCEventAdapter();
		}

		@Override
		public Adapter caseIPosBarcodeEvent(IPosBarcodeEvent object) {
			return createIPosBarcodeEventAdapter();
		}

		@Override
		public Adapter caseIPosRFIDEvent(IPosRFIDEvent object) {
			return createIPosRFIDEventAdapter();
		}

		@Override
		public Adapter caseIPosOtherProxEvent(IPosOtherProxEvent object) {
			return createIPosOtherProxEventAdapter();
		}

		@Override
		public Adapter caseIPosOtherBeaconEvent(IPosOtherBeaconEvent object) {
			return createIPosOtherBeaconEventAdapter();
		}

		@Override
		public Adapter caseIposMsgRcvEvent(IposMsgRcvEvent object) {
			return createIposMsgRcvEventAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.TrackingRequest <em>Tracking Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.TrackingRequest
	 * @generated
	 */
	public Adapter createTrackingRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest <em>Monitoring Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest
	 * @generated
	 */
	public Adapter createMonitoringRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelUpdateRequest <em>World Model Update Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelUpdateRequest
	 * @generated
	 */
	public Adapter createWorldModelUpdateRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageRequest <em>Data Storage Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageRequest
	 * @generated
	 */
	public Adapter createDataStorageRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelQueryRequest <em>World Model Query Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.WorldModelQueryRequest
	 * @generated
	 */
	public Adapter createWorldModelQueryRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest <em>Data Storage Query Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest
	 * @generated
	 */
	public Adapter createDataStorageQueryRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.AgentRegistrationRequest <em>Agent Registration Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.AgentRegistrationRequest
	 * @generated
	 */
	public Adapter createAgentRegistrationRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.SensorConfigurationRequest <em>Sensor Configuration Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.SensorConfigurationRequest
	 * @generated
	 */
	public Adapter createSensorConfigurationRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent <em>Ipos Position Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent
	 * @generated
	 */
	public Adapter createIposPositionEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent <em>IPos Rawdata Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent
	 * @generated
	 */
	public Adapter createIPosRawdataEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent <em>IPos Beacon Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent
	 * @generated
	 */
	public Adapter createIPosBeaconEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent <em>IPos UWB Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent
	 * @generated
	 */
	public Adapter createIPosUWBEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent <em>IPos BT Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent
	 * @generated
	 */
	public Adapter createIPosBTEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent <em>IPos Proximity Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosProximityEvent
	 * @generated
	 */
	public Adapter createIPosProximityEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent <em>IPos NFC Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosNFCEvent
	 * @generated
	 */
	public Adapter createIPosNFCEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBarcodeEvent <em>IPos Barcode Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBarcodeEvent
	 * @generated
	 */
	public Adapter createIPosBarcodeEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRFIDEvent <em>IPos RFID Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRFIDEvent
	 * @generated
	 */
	public Adapter createIPosRFIDEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherProxEvent <em>IPos Other Prox Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherProxEvent
	 * @generated
	 */
	public Adapter createIPosOtherProxEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherBeaconEvent <em>IPos Other Beacon Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosOtherBeaconEvent
	 * @generated
	 */
	public Adapter createIPosOtherBeaconEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent <em>Ipos Msg Rcv Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent
	 * @generated
	 */
	public Adapter createIposMsgRcvEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //IPosDevKitAdapterFactory
