/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.util;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage
 * @generated
 */
public class IPosDevKitSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static IPosDevKitPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPosDevKitSwitch() {
		if (modelPackage == null) {
			modelPackage = IPosDevKitPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case IPosDevKitPackage.TRACKING_REQUEST: {
			TrackingRequest trackingRequest = (TrackingRequest) theEObject;
			T result = caseTrackingRequest(trackingRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.MONITORING_REQUEST: {
			MonitoringRequest monitoringRequest = (MonitoringRequest) theEObject;
			T result = caseMonitoringRequest(monitoringRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.WORLD_MODEL_UPDATE_REQUEST: {
			WorldModelUpdateRequest worldModelUpdateRequest = (WorldModelUpdateRequest) theEObject;
			T result = caseWorldModelUpdateRequest(worldModelUpdateRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.DATA_STORAGE_REQUEST: {
			DataStorageRequest dataStorageRequest = (DataStorageRequest) theEObject;
			T result = caseDataStorageRequest(dataStorageRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.WORLD_MODEL_QUERY_REQUEST: {
			WorldModelQueryRequest worldModelQueryRequest = (WorldModelQueryRequest) theEObject;
			T result = caseWorldModelQueryRequest(worldModelQueryRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.DATA_STORAGE_QUERY_REQUEST: {
			DataStorageQueryRequest dataStorageQueryRequest = (DataStorageQueryRequest) theEObject;
			T result = caseDataStorageQueryRequest(dataStorageQueryRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.AGENT_REGISTRATION_REQUEST: {
			AgentRegistrationRequest agentRegistrationRequest = (AgentRegistrationRequest) theEObject;
			T result = caseAgentRegistrationRequest(agentRegistrationRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.SENSOR_CONFIGURATION_REQUEST: {
			SensorConfigurationRequest sensorConfigurationRequest = (SensorConfigurationRequest) theEObject;
			T result = caseSensorConfigurationRequest(sensorConfigurationRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_POSITION_EVENT: {
			IposPositionEvent iposPositionEvent = (IposPositionEvent) theEObject;
			T result = caseIposPositionEvent(iposPositionEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_RAWDATA_EVENT: {
			IPosRawdataEvent iPosRawdataEvent = (IPosRawdataEvent) theEObject;
			T result = caseIPosRawdataEvent(iPosRawdataEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_BEACON_EVENT: {
			IPosBeaconEvent iPosBeaconEvent = (IPosBeaconEvent) theEObject;
			T result = caseIPosBeaconEvent(iPosBeaconEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosBeaconEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_UWB_EVENT: {
			IPosUWBEvent iPosUWBEvent = (IPosUWBEvent) theEObject;
			T result = caseIPosUWBEvent(iPosUWBEvent);
			if (result == null)
				result = caseIPosBeaconEvent(iPosUWBEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosUWBEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_BT_EVENT: {
			IPosBTEvent iPosBTEvent = (IPosBTEvent) theEObject;
			T result = caseIPosBTEvent(iPosBTEvent);
			if (result == null)
				result = caseIPosBeaconEvent(iPosBTEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosBTEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_PROXIMITY_EVENT: {
			IPosProximityEvent iPosProximityEvent = (IPosProximityEvent) theEObject;
			T result = caseIPosProximityEvent(iPosProximityEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosProximityEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_NFC_EVENT: {
			IPosNFCEvent iPosNFCEvent = (IPosNFCEvent) theEObject;
			T result = caseIPosNFCEvent(iPosNFCEvent);
			if (result == null)
				result = caseIPosProximityEvent(iPosNFCEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosNFCEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_BARCODE_EVENT: {
			IPosBarcodeEvent iPosBarcodeEvent = (IPosBarcodeEvent) theEObject;
			T result = caseIPosBarcodeEvent(iPosBarcodeEvent);
			if (result == null)
				result = caseIPosProximityEvent(iPosBarcodeEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosBarcodeEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_RFID_EVENT: {
			IPosRFIDEvent iPosRFIDEvent = (IPosRFIDEvent) theEObject;
			T result = caseIPosRFIDEvent(iPosRFIDEvent);
			if (result == null)
				result = caseIPosProximityEvent(iPosRFIDEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosRFIDEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_OTHER_PROX_EVENT: {
			IPosOtherProxEvent iPosOtherProxEvent = (IPosOtherProxEvent) theEObject;
			T result = caseIPosOtherProxEvent(iPosOtherProxEvent);
			if (result == null)
				result = caseIPosProximityEvent(iPosOtherProxEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosOtherProxEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_OTHER_BEACON_EVENT: {
			IPosOtherBeaconEvent iPosOtherBeaconEvent = (IPosOtherBeaconEvent) theEObject;
			T result = caseIPosOtherBeaconEvent(iPosOtherBeaconEvent);
			if (result == null)
				result = caseIPosBeaconEvent(iPosOtherBeaconEvent);
			if (result == null)
				result = caseIPosRawdataEvent(iPosOtherBeaconEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPosDevKitPackage.IPOS_MSG_RCV_EVENT: {
			IposMsgRcvEvent iposMsgRcvEvent = (IposMsgRcvEvent) theEObject;
			T result = caseIposMsgRcvEvent(iposMsgRcvEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tracking Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tracking Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrackingRequest(TrackingRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Monitoring Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Monitoring Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonitoringRequest(MonitoringRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>World Model Update Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>World Model Update Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorldModelUpdateRequest(WorldModelUpdateRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Storage Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Storage Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataStorageRequest(DataStorageRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>World Model Query Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>World Model Query Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorldModelQueryRequest(WorldModelQueryRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Storage Query Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Storage Query Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataStorageQueryRequest(DataStorageQueryRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Agent Registration Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Agent Registration Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAgentRegistrationRequest(AgentRegistrationRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sensor Configuration Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sensor Configuration Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSensorConfigurationRequest(SensorConfigurationRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ipos Position Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ipos Position Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIposPositionEvent(IposPositionEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos Rawdata Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos Rawdata Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosRawdataEvent(IPosRawdataEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos Beacon Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos Beacon Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosBeaconEvent(IPosBeaconEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos UWB Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos UWB Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosUWBEvent(IPosUWBEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos BT Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos BT Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosBTEvent(IPosBTEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos Proximity Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos Proximity Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosProximityEvent(IPosProximityEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos NFC Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos NFC Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosNFCEvent(IPosNFCEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos Barcode Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos Barcode Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosBarcodeEvent(IPosBarcodeEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos RFID Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos RFID Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosRFIDEvent(IPosRFIDEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos Other Prox Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos Other Prox Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosOtherProxEvent(IPosOtherProxEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPos Other Beacon Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPos Other Beacon Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPosOtherBeaconEvent(IPosOtherBeaconEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ipos Msg Rcv Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ipos Msg Rcv Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIposMsgRcvEvent(IposMsgRcvEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //IPosDevKitSwitch
