/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage
 * @generated
 */
public interface IPos_DatamodelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IPos_DatamodelFactory eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agent</em>'.
	 * @generated
	 */
	Agent createAgent();

	/**
	 * Returns a new object of class '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entity</em>'.
	 * @generated
	 */
	Entity createEntity();

	/**
	 * Returns a new object of class '<em>Localizable Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Localizable Object</em>'.
	 * @generated
	 */
	LocalizableObject createLocalizableObject();

	/**
	 * Returns a new object of class '<em>Placing</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Placing</em>'.
	 * @generated
	 */
	Placing createPlacing();

	/**
	 * Returns a new object of class '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Position</em>'.
	 * @generated
	 */
	Position createPosition();

	/**
	 * Returns a new object of class '<em>Orientation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Orientation</em>'.
	 * @generated
	 */
	Orientation createOrientation();

	/**
	 * Returns a new object of class '<em>WGS84 Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>WGS84 Point</em>'.
	 * @generated
	 */
	WGS84Point createWGS84Point();

	/**
	 * Returns a new object of class '<em>Accuracy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Accuracy</em>'.
	 * @generated
	 */
	Accuracy createAccuracy();

	/**
	 * Returns a new object of class '<em>Point2 D</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Point2 D</em>'.
	 * @generated
	 */
	Point2D createPoint2D();

	/**
	 * Returns a new object of class '<em>Point3 D</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Point3 D</em>'.
	 * @generated
	 */
	Point3D createPoint3D();

	/**
	 * Returns a new object of class '<em>Reference System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference System</em>'.
	 * @generated
	 */
	ReferenceSystem createReferenceSystem();

	/**
	 * Returns a new object of class '<em>Zone</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Zone</em>'.
	 * @generated
	 */
	Zone createZone();

	/**
	 * Returns a new object of class '<em>Zone Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Zone Map</em>'.
	 * @generated
	 */
	ZoneMap createZoneMap();

	/**
	 * Returns a new object of class '<em>Space</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Space</em>'.
	 * @generated
	 */
	Space createSpace();

	/**
	 * Returns a new object of class '<em>Map Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Map Type</em>'.
	 * @generated
	 */
	MapType createMapType();

	/**
	 * Returns a new object of class '<em>Quaternion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quaternion</em>'.
	 * @generated
	 */
	Quaternion createQuaternion();

	/**
	 * Returns a new object of class '<em>Gaussian</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gaussian</em>'.
	 * @generated
	 */
	Gaussian createGaussian();

	/**
	 * Returns a new object of class '<em>Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Point</em>'.
	 * @generated
	 */
	Point createPoint();

	/**
	 * Returns a new object of class '<em>Rawdata Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rawdata Event</em>'.
	 * @generated
	 */
	RawdataEvent createRawdataEvent();

	/**
	 * Returns a new object of class '<em>Proximity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Proximity</em>'.
	 * @generated
	 */
	Proximity createProximity();

	/**
	 * Returns a new object of class '<em>RFID</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RFID</em>'.
	 * @generated
	 */
	RFID createRFID();

	/**
	 * Returns a new object of class '<em>NFC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NFC</em>'.
	 * @generated
	 */
	NFC createNFC();

	/**
	 * Returns a new object of class '<em>IMU</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IMU</em>'.
	 * @generated
	 */
	IMU createIMU();

	/**
	 * Returns a new object of class '<em>Acceleration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acceleration</em>'.
	 * @generated
	 */
	Acceleration createAcceleration();

	/**
	 * Returns a new object of class '<em>Beacon</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Beacon</em>'.
	 * @generated
	 */
	Beacon createBeacon();

	/**
	 * Returns a new object of class '<em>Position Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Position Event</em>'.
	 * @generated
	 */
	PositionEvent createPositionEvent();

	/**
	 * Returns a new object of class '<em>Event Filter Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Filter Configuration</em>'.
	 * @generated
	 */
	EventFilterConfiguration createEventFilterConfiguration();

	/**
	 * Returns a new object of class '<em>Monitoring Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitoring Task</em>'.
	 * @generated
	 */
	MonitoringTask createMonitoringTask();

	/**
	 * Returns a new object of class '<em>Tracking Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tracking Task</em>'.
	 * @generated
	 */
	TrackingTask createTrackingTask();

	/**
	 * Returns a new object of class '<em>World Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>World Model</em>'.
	 * @generated
	 */
	WorldModel createWorldModel();

	/**
	 * Returns a new object of class '<em>Barcode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Barcode</em>'.
	 * @generated
	 */
	Barcode createBarcode();

	/**
	 * Returns a new object of class '<em>Other Prox</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Other Prox</em>'.
	 * @generated
	 */
	OtherProx createOtherProx();

	/**
	 * Returns a new object of class '<em>Bluetooth</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bluetooth</em>'.
	 * @generated
	 */
	Bluetooth createBluetooth();

	/**
	 * Returns a new object of class '<em>UWB</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UWB</em>'.
	 * @generated
	 */
	UWB createUWB();

	/**
	 * Returns a new object of class '<em>Other Beacon</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Other Beacon</em>'.
	 * @generated
	 */
	OtherBeacon createOtherBeacon();

	/**
	 * Returns a new object of class '<em>POI</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>POI</em>'.
	 * @generated
	 */
	POI createPOI();

	/**
	 * Returns a new object of class '<em>Event Filter Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Filter Condition</em>'.
	 * @generated
	 */
	EventFilterCondition createEventFilterCondition();

	/**
	 * Returns a new object of class '<em>Zone Descriptor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Zone Descriptor</em>'.
	 * @generated
	 */
	ZoneDescriptor createZoneDescriptor();

	/**
	 * Returns a new object of class '<em>Data Storage Query Response</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Storage Query Response</em>'.
	 * @generated
	 */
	DataStorageQueryResponse createDataStorageQueryResponse();

	/**
	 * Returns a new object of class '<em>Message Received Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Message Received Event</em>'.
	 * @generated
	 */
	MessageReceivedEvent createMessageReceivedEvent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	IPos_DatamodelPackage getIPos_DatamodelPackage();

} //IPos_DatamodelFactory
