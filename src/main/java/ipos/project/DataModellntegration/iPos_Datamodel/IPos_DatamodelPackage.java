/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelFactory
 * @model kind="package"
 * @generated
 */
public interface IPos_DatamodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "iPos_Datamodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/iPos_Datamodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "iPos_Datamodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IPos_DatamodelPackage eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.EntityImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ID = 0;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.AgentImpl <em>Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.AgentImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getAgent()
	 * @generated
	 */
	int AGENT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__ID = ENTITY__ID;

	/**
	 * The feature id for the '<em><b>LObject</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__LOBJECT = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Agent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__AGENT_TYPE = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.LocalizableObjectImpl <em>Localizable Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.LocalizableObjectImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getLocalizableObject()
	 * @generated
	 */
	int LOCALIZABLE_OBJECT = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZABLE_OBJECT__ID = ENTITY__ID;

	/**
	 * The feature id for the '<em><b>Last Pos Update</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZABLE_OBJECT__LAST_POS_UPDATE = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZABLE_OBJECT__SENSOR_TYPE = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZABLE_OBJECT__AGENT = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Current Placing</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZABLE_OBJECT__CURRENT_PLACING = ENTITY_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Localizable Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZABLE_OBJECT_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Localizable Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZABLE_OBJECT_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PlacingImpl <em>Placing</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.PlacingImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPlacing()
	 * @generated
	 */
	int PLACING = 3;

	/**
	 * The feature id for the '<em><b>Position</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACING__POSITION = 0;

	/**
	 * The feature id for the '<em><b>Orientation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACING__ORIENTATION = 1;

	/**
	 * The number of structural features of the '<em>Placing</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACING_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Placing</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionImpl <em>Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPosition()
	 * @generated
	 */
	int POSITION = 4;

	/**
	 * The feature id for the '<em><b>Accuracy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__ACCURACY = 0;

	/**
	 * The feature id for the '<em><b>Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__POINT = 1;

	/**
	 * The feature id for the '<em><b>Reference System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__REFERENCE_SYSTEM = 2;

	/**
	 * The number of structural features of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.OrientationImpl <em>Orientation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.OrientationImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getOrientation()
	 * @generated
	 */
	int ORIENTATION = 5;

	/**
	 * The number of structural features of the '<em>Orientation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORIENTATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Orientation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORIENTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PointImpl <em>Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.PointImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPoint()
	 * @generated
	 */
	int POINT = 17;

	/**
	 * The number of structural features of the '<em>Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.WGS84PointImpl <em>WGS84 Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.WGS84PointImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getWGS84Point()
	 * @generated
	 */
	int WGS84_POINT = 6;

	/**
	 * The feature id for the '<em><b>Longitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WGS84_POINT__LONGITUDE = POINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Latitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WGS84_POINT__LATITUDE = POINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Altitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WGS84_POINT__ALTITUDE = POINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>WGS84 Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WGS84_POINT_FEATURE_COUNT = POINT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>WGS84 Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WGS84_POINT_OPERATION_COUNT = POINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.AccuracyImpl <em>Accuracy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.AccuracyImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getAccuracy()
	 * @generated
	 */
	int ACCURACY = 7;

	/**
	 * The number of structural features of the '<em>Accuracy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCURACY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Accuracy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCURACY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.Point2DImpl <em>Point2 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.Point2DImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPoint2D()
	 * @generated
	 */
	int POINT2_D = 8;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT2_D__X = POINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT2_D__Y = POINT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Point2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT2_D_FEATURE_COUNT = POINT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Point2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT2_D_OPERATION_COUNT = POINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.Point3DImpl <em>Point3 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.Point3DImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPoint3D()
	 * @generated
	 */
	int POINT3_D = 9;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT3_D__X = POINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT3_D__Y = POINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT3_D__Z = POINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Point3 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT3_D_FEATURE_COUNT = POINT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Point3 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT3_D_OPERATION_COUNT = POINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ReferenceSystemImpl <em>Reference System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ReferenceSystemImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getReferenceSystem()
	 * @generated
	 */
	int REFERENCE_SYSTEM = 10;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SYSTEM__ORIGIN = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SYSTEM__NAME = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SYSTEM__ID = 2;

	/**
	 * The number of structural features of the '<em>Reference System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SYSTEM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Reference System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SYSTEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneImpl <em>Zone</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getZone()
	 * @generated
	 */
	int ZONE = 11;

	/**
	 * The feature id for the '<em><b>Space</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE__SPACE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE__ID = 2;

	/**
	 * The number of structural features of the '<em>Zone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Zone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MapTypeImpl <em>Map Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.MapTypeImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getMapType()
	 * @generated
	 */
	int MAP_TYPE = 14;

	/**
	 * The number of structural features of the '<em>Map Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TYPE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Map Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneMapImpl <em>Zone Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneMapImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getZoneMap()
	 * @generated
	 */
	int ZONE_MAP = 12;

	/**
	 * The feature id for the '<em><b>Zone</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_MAP__ZONE = MAP_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Zone Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_MAP_FEATURE_COUNT = MAP_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Zone Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_MAP_OPERATION_COUNT = MAP_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.SpaceImpl <em>Space</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.SpaceImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getSpace()
	 * @generated
	 */
	int SPACE = 13;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACE__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACE__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACE__Z = 2;

	/**
	 * The feature id for the '<em><b>Centre Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACE__CENTRE_POINT = 3;

	/**
	 * The number of structural features of the '<em>Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPACE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.QuaternionImpl <em>Quaternion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.QuaternionImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getQuaternion()
	 * @generated
	 */
	int QUATERNION = 15;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUATERNION__X = ORIENTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUATERNION__Y = ORIENTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUATERNION__Z = ORIENTATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>W</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUATERNION__W = ORIENTATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Quaternion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUATERNION_FEATURE_COUNT = ORIENTATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Quaternion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUATERNION_OPERATION_COUNT = ORIENTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.GaussianImpl <em>Gaussian</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.GaussianImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getGaussian()
	 * @generated
	 */
	int GAUSSIAN = 16;

	/**
	 * The feature id for the '<em><b>Confidence Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN__CONFIDENCE_INTERVAL = ACCURACY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Gaussian</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_FEATURE_COUNT = ACCURACY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Gaussian</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_OPERATION_COUNT = ACCURACY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.RawdataEventImpl <em>Rawdata Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.RawdataEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getRawdataEvent()
	 * @generated
	 */
	int RAWDATA_EVENT = 18;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAWDATA_EVENT__TIME_STAMP = 0;

	/**
	 * The number of structural features of the '<em>Rawdata Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAWDATA_EVENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Rawdata Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAWDATA_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ProximityImpl <em>Proximity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ProximityImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getProximity()
	 * @generated
	 */
	int PROXIMITY = 19;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROXIMITY__TIME_STAMP = RAWDATA_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROXIMITY__TAG_ID = RAWDATA_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROXIMITY__TYPE = RAWDATA_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Scanner Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROXIMITY__SCANNER_ID = RAWDATA_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Proximity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROXIMITY_FEATURE_COUNT = RAWDATA_EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Proximity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROXIMITY_OPERATION_COUNT = RAWDATA_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.RFIDImpl <em>RFID</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.RFIDImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getRFID()
	 * @generated
	 */
	int RFID = 20;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RFID__TIME_STAMP = PROXIMITY__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RFID__TAG_ID = PROXIMITY__TAG_ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RFID__TYPE = PROXIMITY__TYPE;

	/**
	 * The feature id for the '<em><b>Scanner Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RFID__SCANNER_ID = PROXIMITY__SCANNER_ID;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RFID__LOCATION = PROXIMITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>RFID</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RFID_FEATURE_COUNT = PROXIMITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>RFID</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RFID_OPERATION_COUNT = PROXIMITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.NFCImpl <em>NFC</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.NFCImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getNFC()
	 * @generated
	 */
	int NFC = 21;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFC__TIME_STAMP = PROXIMITY__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFC__TAG_ID = PROXIMITY__TAG_ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFC__TYPE = PROXIMITY__TYPE;

	/**
	 * The feature id for the '<em><b>Scanner Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFC__SCANNER_ID = PROXIMITY__SCANNER_ID;

	/**
	 * The feature id for the '<em><b>Tag Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFC__TAG_DATA = PROXIMITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NFC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFC_FEATURE_COUNT = PROXIMITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>NFC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFC_OPERATION_COUNT = PROXIMITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.IMUImpl <em>IMU</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IMUImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getIMU()
	 * @generated
	 */
	int IMU = 22;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMU__TIME_STAMP = RAWDATA_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Angularrate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMU__ANGULARRATE = RAWDATA_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Acceleration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMU__ACCELERATION = RAWDATA_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMU__SENSOR_ID = RAWDATA_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>IMU</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMU_FEATURE_COUNT = RAWDATA_EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>IMU</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMU_OPERATION_COUNT = RAWDATA_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.AccelerationImpl <em>Acceleration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.AccelerationImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getAcceleration()
	 * @generated
	 */
	int ACCELERATION = 23;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCELERATION__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCELERATION__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCELERATION__Z = 2;

	/**
	 * The number of structural features of the '<em>Acceleration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCELERATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Acceleration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCELERATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.BeaconImpl <em>Beacon</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.BeaconImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getBeacon()
	 * @generated
	 */
	int BEACON = 24;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEACON__TIME_STAMP = RAWDATA_EVENT__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEACON__DISTANCES = RAWDATA_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEACON__TYPE = RAWDATA_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEACON__SENSOR_ID = RAWDATA_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Beacon</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEACON_FEATURE_COUNT = RAWDATA_EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Beacon</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEACON_OPERATION_COUNT = RAWDATA_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionEventImpl <em>Position Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPositionEvent()
	 * @generated
	 */
	int POSITION_EVENT = 25;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_EVENT__TIME_STAMP = 0;

	/**
	 * The feature id for the '<em><b>LObject Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_EVENT__LOBJECT_ID = 1;

	/**
	 * The feature id for the '<em><b>Zonedescriptors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_EVENT__ZONEDESCRIPTORS = 2;

	/**
	 * The feature id for the '<em><b>Placing</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_EVENT__PLACING = 3;

	/**
	 * The number of structural features of the '<em>Position Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_EVENT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Position Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConfigurationImpl <em>Event Filter Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConfigurationImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getEventFilterConfiguration()
	 * @generated
	 */
	int EVENT_FILTER_CONFIGURATION = 26;

	/**
	 * The feature id for the '<em><b>Position Ambiguity Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_STRATEGY = 0;

	/**
	 * The feature id for the '<em><b>Position Ambiguity Parameters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_PARAMETERS = 1;

	/**
	 * The feature id for the '<em><b>Filter Criteria</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONFIGURATION__FILTER_CRITERIA = 2;

	/**
	 * The feature id for the '<em><b>Eventfiltercondition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONFIGURATION__EVENTFILTERCONDITION = 3;

	/**
	 * The number of structural features of the '<em>Event Filter Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONFIGURATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Event Filter Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MonitoringTaskImpl <em>Monitoring Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.MonitoringTaskImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getMonitoringTask()
	 * @generated
	 */
	int MONITORING_TASK = 27;

	/**
	 * The feature id for the '<em><b>Eventfilterconfiguration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_TASK__EVENTFILTERCONFIGURATION = 0;

	/**
	 * The number of structural features of the '<em>Monitoring Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_TASK_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Monitoring Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_TASK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.TrackingTaskImpl <em>Tracking Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.TrackingTaskImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getTrackingTask()
	 * @generated
	 */
	int TRACKING_TASK = 28;

	/**
	 * The feature id for the '<em><b>Eventfilterconfiguration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACKING_TASK__EVENTFILTERCONFIGURATION = 0;

	/**
	 * The number of structural features of the '<em>Tracking Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACKING_TASK_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Tracking Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACKING_TASK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.WorldModelImpl <em>World Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.WorldModelImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getWorldModel()
	 * @generated
	 */
	int WORLD_MODEL = 29;

	/**
	 * The feature id for the '<em><b>Agent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL__AGENT = 0;

	/**
	 * The feature id for the '<em><b>Zone Map</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL__ZONE_MAP = 1;

	/**
	 * The feature id for the '<em><b>Pois</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL__POIS = 2;

	/**
	 * The feature id for the '<em><b>Reference System</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL__REFERENCE_SYSTEM = 3;

	/**
	 * The number of structural features of the '<em>World Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>World Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.BarcodeImpl <em>Barcode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.BarcodeImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getBarcode()
	 * @generated
	 */
	int BARCODE = 30;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARCODE__TIME_STAMP = PROXIMITY__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARCODE__TAG_ID = PROXIMITY__TAG_ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARCODE__TYPE = PROXIMITY__TYPE;

	/**
	 * The feature id for the '<em><b>Scanner Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARCODE__SCANNER_ID = PROXIMITY__SCANNER_ID;

	/**
	 * The number of structural features of the '<em>Barcode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARCODE_FEATURE_COUNT = PROXIMITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Barcode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARCODE_OPERATION_COUNT = PROXIMITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.OtherProxImpl <em>Other Prox</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.OtherProxImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getOtherProx()
	 * @generated
	 */
	int OTHER_PROX = 31;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_PROX__TIME_STAMP = PROXIMITY__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_PROX__TAG_ID = PROXIMITY__TAG_ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_PROX__TYPE = PROXIMITY__TYPE;

	/**
	 * The feature id for the '<em><b>Scanner Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_PROX__SCANNER_ID = PROXIMITY__SCANNER_ID;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_PROX__DATA = PROXIMITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Other Prox</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_PROX_FEATURE_COUNT = PROXIMITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Other Prox</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_PROX_OPERATION_COUNT = PROXIMITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.BluetoothImpl <em>Bluetooth</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.BluetoothImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getBluetooth()
	 * @generated
	 */
	int BLUETOOTH = 32;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLUETOOTH__TIME_STAMP = BEACON__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLUETOOTH__DISTANCES = BEACON__DISTANCES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLUETOOTH__TYPE = BEACON__TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLUETOOTH__SENSOR_ID = BEACON__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Rss</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLUETOOTH__RSS = BEACON_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Bluetooth</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLUETOOTH_FEATURE_COUNT = BEACON_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Bluetooth</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLUETOOTH_OPERATION_COUNT = BEACON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.UWBImpl <em>UWB</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.UWBImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getUWB()
	 * @generated
	 */
	int UWB = 33;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UWB__TIME_STAMP = BEACON__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UWB__DISTANCES = BEACON__DISTANCES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UWB__TYPE = BEACON__TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UWB__SENSOR_ID = BEACON__SENSOR_ID;

	/**
	 * The number of structural features of the '<em>UWB</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UWB_FEATURE_COUNT = BEACON_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>UWB</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UWB_OPERATION_COUNT = BEACON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.OtherBeaconImpl <em>Other Beacon</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.OtherBeaconImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getOtherBeacon()
	 * @generated
	 */
	int OTHER_BEACON = 34;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_BEACON__TIME_STAMP = BEACON__TIME_STAMP;

	/**
	 * The feature id for the '<em><b>Distances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_BEACON__DISTANCES = BEACON__DISTANCES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_BEACON__TYPE = BEACON__TYPE;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_BEACON__SENSOR_ID = BEACON__SENSOR_ID;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_BEACON__DATA = BEACON_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Other Beacon</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_BEACON_FEATURE_COUNT = BEACON_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Other Beacon</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHER_BEACON_OPERATION_COUNT = BEACON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.POIImpl <em>POI</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.POIImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPOI()
	 * @generated
	 */
	int POI = 35;

	/**
	 * The feature id for the '<em><b>Placing</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POI__PLACING = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POI__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POI__DATA = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POI__ID = 3;

	/**
	 * The number of structural features of the '<em>POI</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POI_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>POI</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POI_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl <em>Event Filter Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getEventFilterCondition()
	 * @generated
	 */
	int EVENT_FILTER_CONDITION = 36;

	/**
	 * The feature id for the '<em><b>Time Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__TIME_CONDITION = 0;

	/**
	 * The feature id for the '<em><b>Accuracy Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__ACCURACY_CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Position Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__POSITION_CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Time Min Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__TIME_MIN_INTERVAL = 3;

	/**
	 * The feature id for the '<em><b>Position Delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__POSITION_DELTA = 4;

	/**
	 * The feature id for the '<em><b>Sensor Id Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__SENSOR_ID_CONDITION = 5;

	/**
	 * The feature id for the '<em><b>Filter Structure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__FILTER_STRUCTURE = 6;

	/**
	 * The feature id for the '<em><b>Position Condition Cells</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__POSITION_CONDITION_CELLS = 7;

	/**
	 * The feature id for the '<em><b>Id Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__ID_CONDITION = 8;

	/**
	 * The feature id for the '<em><b>Category Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__CATEGORY_CONDITION = 9;

	/**
	 * The feature id for the '<em><b>Property Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION__PROPERTY_CONDITION = 10;

	/**
	 * The number of structural features of the '<em>Event Filter Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Event Filter Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FILTER_CONDITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneDescriptorImpl <em>Zone Descriptor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneDescriptorImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getZoneDescriptor()
	 * @generated
	 */
	int ZONE_DESCRIPTOR = 37;

	/**
	 * The feature id for the '<em><b>Zone Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_DESCRIPTOR__ZONE_ID = 0;

	/**
	 * The feature id for the '<em><b>Notification Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_DESCRIPTOR__NOTIFICATION_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Zone Descriptor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_DESCRIPTOR_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Zone Descriptor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZONE_DESCRIPTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.DataStorageQueryResponseImpl <em>Data Storage Query Response</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.DataStorageQueryResponseImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getDataStorageQueryResponse()
	 * @generated
	 */
	int DATA_STORAGE_QUERY_RESPONSE = 38;

	/**
	 * The feature id for the '<em><b>Position Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_QUERY_RESPONSE__POSITION_EVENTS = 0;

	/**
	 * The feature id for the '<em><b>Tracking Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_QUERY_RESPONSE__TRACKING_TASK_ID = 1;

	/**
	 * The number of structural features of the '<em>Data Storage Query Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_QUERY_RESPONSE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Data Storage Query Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STORAGE_QUERY_RESPONSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl <em>Message Received Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getMessageReceivedEvent()
	 * @generated
	 */
	int MESSAGE_RECEIVED_EVENT = 39;

	/**
	 * The feature id for the '<em><b>Protocol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_RECEIVED_EVENT__PROTOCOL_NAME = 0;

	/**
	 * The feature id for the '<em><b>Serialized Msg</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_RECEIVED_EVENT__SERIALIZED_MSG = 1;

	/**
	 * The feature id for the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_RECEIVED_EVENT__AGENT_ID = 2;

	/**
	 * The feature id for the '<em><b>Extracted Attributes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_RECEIVED_EVENT__EXTRACTED_ATTRIBUTES = 3;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_RECEIVED_EVENT__TIMESTAMP = 4;

	/**
	 * The number of structural features of the '<em>Message Received Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_RECEIVED_EVENT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Message Received Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_RECEIVED_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '<em>String List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getStringList()
	 * @generated
	 */
	int STRING_LIST = 40;

	/**
	 * The meta object id for the '<em>String Array</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getStringArray()
	 * @generated
	 */
	int STRING_ARRAY = 41;

	/**
	 * The meta object id for the '<em>Float Array3d</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getFloatArray3d()
	 * @generated
	 */
	int FLOAT_ARRAY3D = 42;

	/**
	 * The meta object id for the '<em>Boolean List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getBooleanList()
	 * @generated
	 */
	int BOOLEAN_LIST = 43;

	/**
	 * The meta object id for the '<em>Float Array</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getFloatArray()
	 * @generated
	 */
	int FLOAT_ARRAY = 44;

	/**
	 * The meta object id for the '<em>Byte Array</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getByteArray()
	 * @generated
	 */
	int BYTE_ARRAY = 45;

	/**
	 * The meta object id for the '<em>List Of String Maps</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.LinkedList
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getListOfStringMaps()
	 * @generated
	 */
	int LIST_OF_STRING_MAPS = 46;

	/**
	 * The meta object id for the '<em>Int Array</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getIntArray()
	 * @generated
	 */
	int INT_ARRAY = 47;

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Agent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Agent
	 * @generated
	 */
	EClass getAgent();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.Agent#getLObject <em>LObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>LObject</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Agent#getLObject()
	 * @see #getAgent()
	 * @generated
	 */
	EReference getAgent_LObject();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Agent#getAgentType <em>Agent Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Agent Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Agent#getAgentType()
	 * @see #getAgent()
	 * @generated
	 */
	EAttribute getAgent_AgentType();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Entity#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Entity#getId()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Id();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject <em>Localizable Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Localizable Object</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject
	 * @generated
	 */
	EClass getLocalizableObject();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getLastPosUpdate <em>Last Pos Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Pos Update</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getLastPosUpdate()
	 * @see #getLocalizableObject()
	 * @generated
	 */
	EAttribute getLocalizableObject_LastPosUpdate();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getSensorType <em>Sensor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getSensorType()
	 * @see #getLocalizableObject()
	 * @generated
	 */
	EAttribute getLocalizableObject_SensorType();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Agent</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getAgent()
	 * @see #getLocalizableObject()
	 * @generated
	 */
	EReference getLocalizableObject_Agent();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getCurrentPlacing <em>Current Placing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Placing</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getCurrentPlacing()
	 * @see #getLocalizableObject()
	 * @generated
	 */
	EReference getLocalizableObject_CurrentPlacing();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Placing <em>Placing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Placing</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Placing
	 * @generated
	 */
	EClass getPlacing();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.Placing#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Position</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Placing#getPosition()
	 * @see #getPlacing()
	 * @generated
	 */
	EReference getPlacing_Position();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.Placing#getOrientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Orientation</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Placing#getOrientation()
	 * @see #getPlacing()
	 * @generated
	 */
	EReference getPlacing_Orientation();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Position <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Position</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Position
	 * @generated
	 */
	EClass getPosition();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getAccuracy <em>Accuracy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Accuracy</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Position#getAccuracy()
	 * @see #getPosition()
	 * @generated
	 */
	EReference getPosition_Accuracy();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getPoint <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Point</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Position#getPoint()
	 * @see #getPosition()
	 * @generated
	 */
	EReference getPosition_Point();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getReferenceSystem <em>Reference System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference System</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Position#getReferenceSystem()
	 * @see #getPosition()
	 * @generated
	 */
	EReference getPosition_ReferenceSystem();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Orientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Orientation</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Orientation
	 * @generated
	 */
	EClass getOrientation();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point <em>WGS84 Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>WGS84 Point</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point
	 * @generated
	 */
	EClass getWGS84Point();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point#getLongitude <em>Longitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Longitude</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point#getLongitude()
	 * @see #getWGS84Point()
	 * @generated
	 */
	EAttribute getWGS84Point_Longitude();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point#getLatitude <em>Latitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Latitude</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point#getLatitude()
	 * @see #getWGS84Point()
	 * @generated
	 */
	EAttribute getWGS84Point_Latitude();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point#getAltitude <em>Altitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Altitude</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point#getAltitude()
	 * @see #getWGS84Point()
	 * @generated
	 */
	EAttribute getWGS84Point_Altitude();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Accuracy <em>Accuracy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Accuracy</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Accuracy
	 * @generated
	 */
	EClass getAccuracy();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point2D <em>Point2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point2 D</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point2D
	 * @generated
	 */
	EClass getPoint2D();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point2D#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point2D#getX()
	 * @see #getPoint2D()
	 * @generated
	 */
	EAttribute getPoint2D_X();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point2D#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point2D#getY()
	 * @see #getPoint2D()
	 * @generated
	 */
	EAttribute getPoint2D_Y();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point3D <em>Point3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point3 D</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point3D
	 * @generated
	 */
	EClass getPoint3D();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point3D#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point3D#getX()
	 * @see #getPoint3D()
	 * @generated
	 */
	EAttribute getPoint3D_X();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point3D#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point3D#getY()
	 * @see #getPoint3D()
	 * @generated
	 */
	EAttribute getPoint3D_Y();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point3D#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point3D#getZ()
	 * @see #getPoint3D()
	 * @generated
	 */
	EAttribute getPoint3D_Z();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem <em>Reference System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference System</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem
	 * @generated
	 */
	EClass getReferenceSystem();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Origin</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getOrigin()
	 * @see #getReferenceSystem()
	 * @generated
	 */
	EReference getReferenceSystem_Origin();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getName()
	 * @see #getReferenceSystem()
	 * @generated
	 */
	EAttribute getReferenceSystem_Name();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getId()
	 * @see #getReferenceSystem()
	 * @generated
	 */
	EAttribute getReferenceSystem_Id();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Zone <em>Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Zone</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Zone
	 * @generated
	 */
	EClass getZone();

	/**
	 * Returns the meta object for the containment reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.Zone#getSpace <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Space</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Zone#getSpace()
	 * @see #getZone()
	 * @generated
	 */
	EReference getZone_Space();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Zone#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Zone#getName()
	 * @see #getZone()
	 * @generated
	 */
	EAttribute getZone_Name();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Zone#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Zone#getId()
	 * @see #getZone()
	 * @generated
	 */
	EAttribute getZone_Id();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap <em>Zone Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Zone Map</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap
	 * @generated
	 */
	EClass getZoneMap();

	/**
	 * Returns the meta object for the containment reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap#getZone <em>Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Zone</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap#getZone()
	 * @see #getZoneMap()
	 * @generated
	 */
	EReference getZoneMap_Zone();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Space</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Space
	 * @generated
	 */
	EClass getSpace();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Space#getX()
	 * @see #getSpace()
	 * @generated
	 */
	EAttribute getSpace_X();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Space#getY()
	 * @see #getSpace()
	 * @generated
	 */
	EAttribute getSpace_Y();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Space#getZ()
	 * @see #getSpace()
	 * @generated
	 */
	EAttribute getSpace_Z();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getCentrePoint <em>Centre Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Centre Point</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Space#getCentrePoint()
	 * @see #getSpace()
	 * @generated
	 */
	EReference getSpace_CentrePoint();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.MapType <em>Map Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MapType
	 * @generated
	 */
	EClass getMapType();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion <em>Quaternion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quaternion</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Quaternion
	 * @generated
	 */
	EClass getQuaternion();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getX()
	 * @see #getQuaternion()
	 * @generated
	 */
	EAttribute getQuaternion_X();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getY()
	 * @see #getQuaternion()
	 * @generated
	 */
	EAttribute getQuaternion_Y();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getZ()
	 * @see #getQuaternion()
	 * @generated
	 */
	EAttribute getQuaternion_Z();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getW <em>W</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>W</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getW()
	 * @see #getQuaternion()
	 * @generated
	 */
	EAttribute getQuaternion_W();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Gaussian <em>Gaussian</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gaussian</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Gaussian
	 * @generated
	 */
	EClass getGaussian();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Gaussian#getConfidenceInterval <em>Confidence Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confidence Interval</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Gaussian#getConfidenceInterval()
	 * @see #getGaussian()
	 * @generated
	 */
	EAttribute getGaussian_ConfidenceInterval();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point
	 * @generated
	 */
	EClass getPoint();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.RawdataEvent <em>Rawdata Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rawdata Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.RawdataEvent
	 * @generated
	 */
	EClass getRawdataEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.RawdataEvent#getTimeStamp <em>Time Stamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Stamp</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.RawdataEvent#getTimeStamp()
	 * @see #getRawdataEvent()
	 * @generated
	 */
	EAttribute getRawdataEvent_TimeStamp();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity <em>Proximity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Proximity</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Proximity
	 * @generated
	 */
	EClass getProximity();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getTagId <em>Tag Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getTagId()
	 * @see #getProximity()
	 * @generated
	 */
	EAttribute getProximity_TagId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getType()
	 * @see #getProximity()
	 * @generated
	 */
	EAttribute getProximity_Type();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getScannerId <em>Scanner Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scanner Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getScannerId()
	 * @see #getProximity()
	 * @generated
	 */
	EAttribute getProximity_ScannerId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.RFID <em>RFID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RFID</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.RFID
	 * @generated
	 */
	EClass getRFID();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.RFID#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.RFID#getLocation()
	 * @see #getRFID()
	 * @generated
	 */
	EAttribute getRFID_Location();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.NFC <em>NFC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NFC</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.NFC
	 * @generated
	 */
	EClass getNFC();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.NFC#getTagData <em>Tag Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag Data</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.NFC#getTagData()
	 * @see #getNFC()
	 * @generated
	 */
	EAttribute getNFC_TagData();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU <em>IMU</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IMU</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IMU
	 * @generated
	 */
	EClass getIMU();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getAngularrate <em>Angularrate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Angularrate</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IMU#getAngularrate()
	 * @see #getIMU()
	 * @generated
	 */
	EReference getIMU_Angularrate();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getAcceleration <em>Acceleration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Acceleration</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IMU#getAcceleration()
	 * @see #getIMU()
	 * @generated
	 */
	EReference getIMU_Acceleration();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU#getSensorId <em>Sensor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IMU#getSensorId()
	 * @see #getIMU()
	 * @generated
	 */
	EAttribute getIMU_SensorId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Acceleration <em>Acceleration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Acceleration</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Acceleration
	 * @generated
	 */
	EClass getAcceleration();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Acceleration#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Acceleration#getX()
	 * @see #getAcceleration()
	 * @generated
	 */
	EAttribute getAcceleration_X();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Acceleration#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Acceleration#getY()
	 * @see #getAcceleration()
	 * @generated
	 */
	EAttribute getAcceleration_Y();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Acceleration#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Acceleration#getZ()
	 * @see #getAcceleration()
	 * @generated
	 */
	EAttribute getAcceleration_Z();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Beacon <em>Beacon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Beacon</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Beacon
	 * @generated
	 */
	EClass getBeacon();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Beacon#getDistances <em>Distances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distances</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Beacon#getDistances()
	 * @see #getBeacon()
	 * @generated
	 */
	EAttribute getBeacon_Distances();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Beacon#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Beacon#getType()
	 * @see #getBeacon()
	 * @generated
	 */
	EAttribute getBeacon_Type();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Beacon#getSensorId <em>Sensor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Beacon#getSensorId()
	 * @see #getBeacon()
	 * @generated
	 */
	EAttribute getBeacon_SensorId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent <em>Position Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Position Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent
	 * @generated
	 */
	EClass getPositionEvent();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getPlacing <em>Placing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Placing</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getPlacing()
	 * @see #getPositionEvent()
	 * @generated
	 */
	EReference getPositionEvent_Placing();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getTimeStamp <em>Time Stamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Stamp</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getTimeStamp()
	 * @see #getPositionEvent()
	 * @generated
	 */
	EAttribute getPositionEvent_TimeStamp();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getLObjectId <em>LObject Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>LObject Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getLObjectId()
	 * @see #getPositionEvent()
	 * @generated
	 */
	EAttribute getPositionEvent_LObjectId();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getZonedescriptors <em>Zonedescriptors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Zonedescriptors</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getZonedescriptors()
	 * @see #getPositionEvent()
	 * @generated
	 */
	EReference getPositionEvent_Zonedescriptors();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration <em>Event Filter Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Filter Configuration</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration
	 * @generated
	 */
	EClass getEventFilterConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getPositionAmbiguityStrategy <em>Position Ambiguity Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position Ambiguity Strategy</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getPositionAmbiguityStrategy()
	 * @see #getEventFilterConfiguration()
	 * @generated
	 */
	EAttribute getEventFilterConfiguration_PositionAmbiguityStrategy();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getPositionAmbiguityParameters <em>Position Ambiguity Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position Ambiguity Parameters</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getPositionAmbiguityParameters()
	 * @see #getEventFilterConfiguration()
	 * @generated
	 */
	EAttribute getEventFilterConfiguration_PositionAmbiguityParameters();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getFilterCriteria <em>Filter Criteria</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Criteria</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getFilterCriteria()
	 * @see #getEventFilterConfiguration()
	 * @generated
	 */
	EAttribute getEventFilterConfiguration_FilterCriteria();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getEventfiltercondition <em>Eventfiltercondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Eventfiltercondition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration#getEventfiltercondition()
	 * @see #getEventFilterConfiguration()
	 * @generated
	 */
	EReference getEventFilterConfiguration_Eventfiltercondition();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask <em>Monitoring Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitoring Task</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask
	 * @generated
	 */
	EClass getMonitoringTask();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask#getEventfilterconfiguration <em>Eventfilterconfiguration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Eventfilterconfiguration</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask#getEventfilterconfiguration()
	 * @see #getMonitoringTask()
	 * @generated
	 */
	EReference getMonitoringTask_Eventfilterconfiguration();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask <em>Tracking Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tracking Task</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask
	 * @generated
	 */
	EClass getTrackingTask();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask#getEventfilterconfiguration <em>Eventfilterconfiguration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Eventfilterconfiguration</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask#getEventfilterconfiguration()
	 * @see #getTrackingTask()
	 * @generated
	 */
	EReference getTrackingTask_Eventfilterconfiguration();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel <em>World Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>World Model</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WorldModel
	 * @generated
	 */
	EClass getWorldModel();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Agent</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getAgent()
	 * @see #getWorldModel()
	 * @generated
	 */
	EReference getWorldModel_Agent();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getZoneMap <em>Zone Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Zone Map</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getZoneMap()
	 * @see #getWorldModel()
	 * @generated
	 */
	EReference getWorldModel_ZoneMap();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getPois <em>Pois</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Pois</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getPois()
	 * @see #getWorldModel()
	 * @generated
	 */
	EReference getWorldModel_Pois();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getReferenceSystem <em>Reference System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reference System</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getReferenceSystem()
	 * @see #getWorldModel()
	 * @generated
	 */
	EReference getWorldModel_ReferenceSystem();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Barcode <em>Barcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Barcode</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Barcode
	 * @generated
	 */
	EClass getBarcode();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OtherProx <em>Other Prox</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Other Prox</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OtherProx
	 * @generated
	 */
	EClass getOtherProx();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OtherProx#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OtherProx#getData()
	 * @see #getOtherProx()
	 * @generated
	 */
	EAttribute getOtherProx_Data();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth <em>Bluetooth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bluetooth</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth
	 * @generated
	 */
	EClass getBluetooth();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth#getRss <em>Rss</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rss</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth#getRss()
	 * @see #getBluetooth()
	 * @generated
	 */
	EAttribute getBluetooth_Rss();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.UWB <em>UWB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UWB</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.UWB
	 * @generated
	 */
	EClass getUWB();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OtherBeacon <em>Other Beacon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Other Beacon</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OtherBeacon
	 * @generated
	 */
	EClass getOtherBeacon();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OtherBeacon#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OtherBeacon#getData()
	 * @see #getOtherBeacon()
	 * @generated
	 */
	EAttribute getOtherBeacon_Data();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.POI <em>POI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>POI</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.POI
	 * @generated
	 */
	EClass getPOI();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.POI#getPlacing <em>Placing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Placing</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.POI#getPlacing()
	 * @see #getPOI()
	 * @generated
	 */
	EReference getPOI_Placing();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.POI#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.POI#getDescription()
	 * @see #getPOI()
	 * @generated
	 */
	EAttribute getPOI_Description();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.POI#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.POI#getData()
	 * @see #getPOI()
	 * @generated
	 */
	EAttribute getPOI_Data();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.POI#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.POI#getId()
	 * @see #getPOI()
	 * @generated
	 */
	EAttribute getPOI_Id();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition <em>Event Filter Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Filter Condition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition
	 * @generated
	 */
	EClass getEventFilterCondition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getTimeCondition <em>Time Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Condition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getTimeCondition()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_TimeCondition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getAccuracyCondition <em>Accuracy Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Accuracy Condition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getAccuracyCondition()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_AccuracyCondition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionCondition <em>Position Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position Condition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionCondition()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_PositionCondition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getTimeMinInterval <em>Time Min Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Min Interval</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getTimeMinInterval()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_TimeMinInterval();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionDelta <em>Position Delta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position Delta</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionDelta()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_PositionDelta();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getSensorIdCondition <em>Sensor Id Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Id Condition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getSensorIdCondition()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_SensorIdCondition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getFilterStructure <em>Filter Structure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Structure</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getFilterStructure()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_FilterStructure();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionConditionCells <em>Position Condition Cells</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position Condition Cells</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPositionConditionCells()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_PositionConditionCells();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getIdCondition <em>Id Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Condition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getIdCondition()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_IdCondition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getCategoryCondition <em>Category Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category Condition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getCategoryCondition()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_CategoryCondition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPropertyCondition <em>Property Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Condition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition#getPropertyCondition()
	 * @see #getEventFilterCondition()
	 * @generated
	 */
	EAttribute getEventFilterCondition_PropertyCondition();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor <em>Zone Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Zone Descriptor</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor
	 * @generated
	 */
	EClass getZoneDescriptor();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor#getZoneId <em>Zone Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Zone Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor#getZoneId()
	 * @see #getZoneDescriptor()
	 * @generated
	 */
	EAttribute getZoneDescriptor_ZoneId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor#getNotificationType <em>Notification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Notification Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor#getNotificationType()
	 * @see #getZoneDescriptor()
	 * @generated
	 */
	EAttribute getZoneDescriptor_NotificationType();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse <em>Data Storage Query Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Storage Query Response</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse
	 * @generated
	 */
	EClass getDataStorageQueryResponse();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse#getPositionEvents <em>Position Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Position Events</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse#getPositionEvents()
	 * @see #getDataStorageQueryResponse()
	 * @generated
	 */
	EReference getDataStorageQueryResponse_PositionEvents();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse#getTrackingTaskId <em>Tracking Task Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tracking Task Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse#getTrackingTaskId()
	 * @see #getDataStorageQueryResponse()
	 * @generated
	 */
	EAttribute getDataStorageQueryResponse_TrackingTaskId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent <em>Message Received Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Received Event</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent
	 * @generated
	 */
	EClass getMessageReceivedEvent();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getProtocolName <em>Protocol Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol Name</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getProtocolName()
	 * @see #getMessageReceivedEvent()
	 * @generated
	 */
	EAttribute getMessageReceivedEvent_ProtocolName();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getSerializedMsg <em>Serialized Msg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Serialized Msg</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getSerializedMsg()
	 * @see #getMessageReceivedEvent()
	 * @generated
	 */
	EAttribute getMessageReceivedEvent_SerializedMsg();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getAgentId <em>Agent Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Agent Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getAgentId()
	 * @see #getMessageReceivedEvent()
	 * @generated
	 */
	EAttribute getMessageReceivedEvent_AgentId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getExtractedAttributes <em>Extracted Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extracted Attributes</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getExtractedAttributes()
	 * @see #getMessageReceivedEvent()
	 * @generated
	 */
	EAttribute getMessageReceivedEvent_ExtractedAttributes();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getTimestamp()
	 * @see #getMessageReceivedEvent()
	 * @generated
	 */
	EAttribute getMessageReceivedEvent_Timestamp();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>String List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String List</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List&lt;java.lang.String&gt;"
	 * @generated
	 */
	EDataType getStringList();

	/**
	 * Returns the meta object for data type '<em>String Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String Array</em>'.
	 * @model instanceClass="java.util.ArrayList&lt;java.lang.String[]&gt;"
	 * @generated
	 */
	EDataType getStringArray();

	/**
	 * Returns the meta object for data type '<em>Float Array3d</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Float Array3d</em>'.
	 * @model instanceClass="java.util.ArrayList&lt;java.lang.Float[][]&gt;"
	 * @generated
	 */
	EDataType getFloatArray3d();

	/**
	 * Returns the meta object for data type '<em>Boolean List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Boolean List</em>'.
	 * @model instanceClass="java.lang.boolean[]"
	 * @generated
	 */
	EDataType getBooleanList();

	/**
	 * Returns the meta object for data type '<em>Float Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Float Array</em>'.
	 * @model instanceClass="java.util.ArrayList&lt;java.lang.Float[]&gt;"
	 * @generated
	 */
	EDataType getFloatArray();

	/**
	 * Returns the meta object for data type '<em>Byte Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Byte Array</em>'.
	 * @model instanceClass="java.lang.byte[]"
	 * @generated
	 */
	EDataType getByteArray();

	/**
	 * Returns the meta object for data type '{@link java.util.LinkedList <em>List Of String Maps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>List Of String Maps</em>'.
	 * @see java.util.LinkedList
	 * @model instanceClass="java.util.LinkedList&lt;java.util.HashMap&lt;java.lang.String, java.lang.String&gt;&gt;"
	 * @generated
	 */
	EDataType getListOfStringMaps();

	/**
	 * Returns the meta object for data type '<em>Int Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Int Array</em>'.
	 * @model instanceClass="int[]"
	 * @generated
	 */
	EDataType getIntArray();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IPos_DatamodelFactory getIPos_DatamodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.AgentImpl <em>Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.AgentImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getAgent()
		 * @generated
		 */
		EClass AGENT = eINSTANCE.getAgent();

		/**
		 * The meta object literal for the '<em><b>LObject</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT__LOBJECT = eINSTANCE.getAgent_LObject();

		/**
		 * The meta object literal for the '<em><b>Agent Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT__AGENT_TYPE = eINSTANCE.getAgent_AgentType();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.EntityImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__ID = eINSTANCE.getEntity_Id();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.LocalizableObjectImpl <em>Localizable Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.LocalizableObjectImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getLocalizableObject()
		 * @generated
		 */
		EClass LOCALIZABLE_OBJECT = eINSTANCE.getLocalizableObject();

		/**
		 * The meta object literal for the '<em><b>Last Pos Update</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCALIZABLE_OBJECT__LAST_POS_UPDATE = eINSTANCE.getLocalizableObject_LastPosUpdate();

		/**
		 * The meta object literal for the '<em><b>Sensor Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCALIZABLE_OBJECT__SENSOR_TYPE = eINSTANCE.getLocalizableObject_SensorType();

		/**
		 * The meta object literal for the '<em><b>Agent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCALIZABLE_OBJECT__AGENT = eINSTANCE.getLocalizableObject_Agent();

		/**
		 * The meta object literal for the '<em><b>Current Placing</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCALIZABLE_OBJECT__CURRENT_PLACING = eINSTANCE.getLocalizableObject_CurrentPlacing();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PlacingImpl <em>Placing</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.PlacingImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPlacing()
		 * @generated
		 */
		EClass PLACING = eINSTANCE.getPlacing();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLACING__POSITION = eINSTANCE.getPlacing_Position();

		/**
		 * The meta object literal for the '<em><b>Orientation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLACING__ORIENTATION = eINSTANCE.getPlacing_Orientation();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionImpl <em>Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPosition()
		 * @generated
		 */
		EClass POSITION = eINSTANCE.getPosition();

		/**
		 * The meta object literal for the '<em><b>Accuracy</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POSITION__ACCURACY = eINSTANCE.getPosition_Accuracy();

		/**
		 * The meta object literal for the '<em><b>Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POSITION__POINT = eINSTANCE.getPosition_Point();

		/**
		 * The meta object literal for the '<em><b>Reference System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POSITION__REFERENCE_SYSTEM = eINSTANCE.getPosition_ReferenceSystem();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.OrientationImpl <em>Orientation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.OrientationImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getOrientation()
		 * @generated
		 */
		EClass ORIENTATION = eINSTANCE.getOrientation();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.WGS84PointImpl <em>WGS84 Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.WGS84PointImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getWGS84Point()
		 * @generated
		 */
		EClass WGS84_POINT = eINSTANCE.getWGS84Point();

		/**
		 * The meta object literal for the '<em><b>Longitude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WGS84_POINT__LONGITUDE = eINSTANCE.getWGS84Point_Longitude();

		/**
		 * The meta object literal for the '<em><b>Latitude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WGS84_POINT__LATITUDE = eINSTANCE.getWGS84Point_Latitude();

		/**
		 * The meta object literal for the '<em><b>Altitude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WGS84_POINT__ALTITUDE = eINSTANCE.getWGS84Point_Altitude();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.AccuracyImpl <em>Accuracy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.AccuracyImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getAccuracy()
		 * @generated
		 */
		EClass ACCURACY = eINSTANCE.getAccuracy();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.Point2DImpl <em>Point2 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.Point2DImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPoint2D()
		 * @generated
		 */
		EClass POINT2_D = eINSTANCE.getPoint2D();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT2_D__X = eINSTANCE.getPoint2D_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT2_D__Y = eINSTANCE.getPoint2D_Y();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.Point3DImpl <em>Point3 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.Point3DImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPoint3D()
		 * @generated
		 */
		EClass POINT3_D = eINSTANCE.getPoint3D();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT3_D__X = eINSTANCE.getPoint3D_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT3_D__Y = eINSTANCE.getPoint3D_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT3_D__Z = eINSTANCE.getPoint3D_Z();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ReferenceSystemImpl <em>Reference System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ReferenceSystemImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getReferenceSystem()
		 * @generated
		 */
		EClass REFERENCE_SYSTEM = eINSTANCE.getReferenceSystem();

		/**
		 * The meta object literal for the '<em><b>Origin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_SYSTEM__ORIGIN = eINSTANCE.getReferenceSystem_Origin();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCE_SYSTEM__NAME = eINSTANCE.getReferenceSystem_Name();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCE_SYSTEM__ID = eINSTANCE.getReferenceSystem_Id();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneImpl <em>Zone</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getZone()
		 * @generated
		 */
		EClass ZONE = eINSTANCE.getZone();

		/**
		 * The meta object literal for the '<em><b>Space</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ZONE__SPACE = eINSTANCE.getZone_Space();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ZONE__NAME = eINSTANCE.getZone_Name();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ZONE__ID = eINSTANCE.getZone_Id();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneMapImpl <em>Zone Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneMapImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getZoneMap()
		 * @generated
		 */
		EClass ZONE_MAP = eINSTANCE.getZoneMap();

		/**
		 * The meta object literal for the '<em><b>Zone</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ZONE_MAP__ZONE = eINSTANCE.getZoneMap_Zone();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.SpaceImpl <em>Space</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.SpaceImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getSpace()
		 * @generated
		 */
		EClass SPACE = eINSTANCE.getSpace();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPACE__X = eINSTANCE.getSpace_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPACE__Y = eINSTANCE.getSpace_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPACE__Z = eINSTANCE.getSpace_Z();

		/**
		 * The meta object literal for the '<em><b>Centre Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPACE__CENTRE_POINT = eINSTANCE.getSpace_CentrePoint();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MapTypeImpl <em>Map Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.MapTypeImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getMapType()
		 * @generated
		 */
		EClass MAP_TYPE = eINSTANCE.getMapType();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.QuaternionImpl <em>Quaternion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.QuaternionImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getQuaternion()
		 * @generated
		 */
		EClass QUATERNION = eINSTANCE.getQuaternion();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUATERNION__X = eINSTANCE.getQuaternion_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUATERNION__Y = eINSTANCE.getQuaternion_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUATERNION__Z = eINSTANCE.getQuaternion_Z();

		/**
		 * The meta object literal for the '<em><b>W</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUATERNION__W = eINSTANCE.getQuaternion_W();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.GaussianImpl <em>Gaussian</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.GaussianImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getGaussian()
		 * @generated
		 */
		EClass GAUSSIAN = eINSTANCE.getGaussian();

		/**
		 * The meta object literal for the '<em><b>Confidence Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAUSSIAN__CONFIDENCE_INTERVAL = eINSTANCE.getGaussian_ConfidenceInterval();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PointImpl <em>Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.PointImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPoint()
		 * @generated
		 */
		EClass POINT = eINSTANCE.getPoint();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.RawdataEventImpl <em>Rawdata Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.RawdataEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getRawdataEvent()
		 * @generated
		 */
		EClass RAWDATA_EVENT = eINSTANCE.getRawdataEvent();

		/**
		 * The meta object literal for the '<em><b>Time Stamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RAWDATA_EVENT__TIME_STAMP = eINSTANCE.getRawdataEvent_TimeStamp();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ProximityImpl <em>Proximity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ProximityImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getProximity()
		 * @generated
		 */
		EClass PROXIMITY = eINSTANCE.getProximity();

		/**
		 * The meta object literal for the '<em><b>Tag Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROXIMITY__TAG_ID = eINSTANCE.getProximity_TagId();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROXIMITY__TYPE = eINSTANCE.getProximity_Type();

		/**
		 * The meta object literal for the '<em><b>Scanner Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROXIMITY__SCANNER_ID = eINSTANCE.getProximity_ScannerId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.RFIDImpl <em>RFID</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.RFIDImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getRFID()
		 * @generated
		 */
		EClass RFID = eINSTANCE.getRFID();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RFID__LOCATION = eINSTANCE.getRFID_Location();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.NFCImpl <em>NFC</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.NFCImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getNFC()
		 * @generated
		 */
		EClass NFC = eINSTANCE.getNFC();

		/**
		 * The meta object literal for the '<em><b>Tag Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NFC__TAG_DATA = eINSTANCE.getNFC_TagData();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.IMUImpl <em>IMU</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IMUImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getIMU()
		 * @generated
		 */
		EClass IMU = eINSTANCE.getIMU();

		/**
		 * The meta object literal for the '<em><b>Angularrate</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMU__ANGULARRATE = eINSTANCE.getIMU_Angularrate();

		/**
		 * The meta object literal for the '<em><b>Acceleration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMU__ACCELERATION = eINSTANCE.getIMU_Acceleration();

		/**
		 * The meta object literal for the '<em><b>Sensor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMU__SENSOR_ID = eINSTANCE.getIMU_SensorId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.AccelerationImpl <em>Acceleration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.AccelerationImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getAcceleration()
		 * @generated
		 */
		EClass ACCELERATION = eINSTANCE.getAcceleration();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCELERATION__X = eINSTANCE.getAcceleration_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCELERATION__Y = eINSTANCE.getAcceleration_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCELERATION__Z = eINSTANCE.getAcceleration_Z();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.BeaconImpl <em>Beacon</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.BeaconImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getBeacon()
		 * @generated
		 */
		EClass BEACON = eINSTANCE.getBeacon();

		/**
		 * The meta object literal for the '<em><b>Distances</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BEACON__DISTANCES = eINSTANCE.getBeacon_Distances();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BEACON__TYPE = eINSTANCE.getBeacon_Type();

		/**
		 * The meta object literal for the '<em><b>Sensor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BEACON__SENSOR_ID = eINSTANCE.getBeacon_SensorId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionEventImpl <em>Position Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPositionEvent()
		 * @generated
		 */
		EClass POSITION_EVENT = eINSTANCE.getPositionEvent();

		/**
		 * The meta object literal for the '<em><b>Placing</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POSITION_EVENT__PLACING = eINSTANCE.getPositionEvent_Placing();

		/**
		 * The meta object literal for the '<em><b>Time Stamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION_EVENT__TIME_STAMP = eINSTANCE.getPositionEvent_TimeStamp();

		/**
		 * The meta object literal for the '<em><b>LObject Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION_EVENT__LOBJECT_ID = eINSTANCE.getPositionEvent_LObjectId();

		/**
		 * The meta object literal for the '<em><b>Zonedescriptors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POSITION_EVENT__ZONEDESCRIPTORS = eINSTANCE.getPositionEvent_Zonedescriptors();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConfigurationImpl <em>Event Filter Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConfigurationImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getEventFilterConfiguration()
		 * @generated
		 */
		EClass EVENT_FILTER_CONFIGURATION = eINSTANCE.getEventFilterConfiguration();

		/**
		 * The meta object literal for the '<em><b>Position Ambiguity Strategy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_STRATEGY = eINSTANCE
				.getEventFilterConfiguration_PositionAmbiguityStrategy();

		/**
		 * The meta object literal for the '<em><b>Position Ambiguity Parameters</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_PARAMETERS = eINSTANCE
				.getEventFilterConfiguration_PositionAmbiguityParameters();

		/**
		 * The meta object literal for the '<em><b>Filter Criteria</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONFIGURATION__FILTER_CRITERIA = eINSTANCE.getEventFilterConfiguration_FilterCriteria();

		/**
		 * The meta object literal for the '<em><b>Eventfiltercondition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_FILTER_CONFIGURATION__EVENTFILTERCONDITION = eINSTANCE
				.getEventFilterConfiguration_Eventfiltercondition();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MonitoringTaskImpl <em>Monitoring Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.MonitoringTaskImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getMonitoringTask()
		 * @generated
		 */
		EClass MONITORING_TASK = eINSTANCE.getMonitoringTask();

		/**
		 * The meta object literal for the '<em><b>Eventfilterconfiguration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORING_TASK__EVENTFILTERCONFIGURATION = eINSTANCE.getMonitoringTask_Eventfilterconfiguration();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.TrackingTaskImpl <em>Tracking Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.TrackingTaskImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getTrackingTask()
		 * @generated
		 */
		EClass TRACKING_TASK = eINSTANCE.getTrackingTask();

		/**
		 * The meta object literal for the '<em><b>Eventfilterconfiguration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRACKING_TASK__EVENTFILTERCONFIGURATION = eINSTANCE.getTrackingTask_Eventfilterconfiguration();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.WorldModelImpl <em>World Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.WorldModelImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getWorldModel()
		 * @generated
		 */
		EClass WORLD_MODEL = eINSTANCE.getWorldModel();

		/**
		 * The meta object literal for the '<em><b>Agent</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORLD_MODEL__AGENT = eINSTANCE.getWorldModel_Agent();

		/**
		 * The meta object literal for the '<em><b>Zone Map</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORLD_MODEL__ZONE_MAP = eINSTANCE.getWorldModel_ZoneMap();

		/**
		 * The meta object literal for the '<em><b>Pois</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORLD_MODEL__POIS = eINSTANCE.getWorldModel_Pois();

		/**
		 * The meta object literal for the '<em><b>Reference System</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORLD_MODEL__REFERENCE_SYSTEM = eINSTANCE.getWorldModel_ReferenceSystem();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.BarcodeImpl <em>Barcode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.BarcodeImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getBarcode()
		 * @generated
		 */
		EClass BARCODE = eINSTANCE.getBarcode();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.OtherProxImpl <em>Other Prox</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.OtherProxImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getOtherProx()
		 * @generated
		 */
		EClass OTHER_PROX = eINSTANCE.getOtherProx();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OTHER_PROX__DATA = eINSTANCE.getOtherProx_Data();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.BluetoothImpl <em>Bluetooth</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.BluetoothImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getBluetooth()
		 * @generated
		 */
		EClass BLUETOOTH = eINSTANCE.getBluetooth();

		/**
		 * The meta object literal for the '<em><b>Rss</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLUETOOTH__RSS = eINSTANCE.getBluetooth_Rss();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.UWBImpl <em>UWB</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.UWBImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getUWB()
		 * @generated
		 */
		EClass UWB = eINSTANCE.getUWB();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.OtherBeaconImpl <em>Other Beacon</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.OtherBeaconImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getOtherBeacon()
		 * @generated
		 */
		EClass OTHER_BEACON = eINSTANCE.getOtherBeacon();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OTHER_BEACON__DATA = eINSTANCE.getOtherBeacon_Data();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.POIImpl <em>POI</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.POIImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getPOI()
		 * @generated
		 */
		EClass POI = eINSTANCE.getPOI();

		/**
		 * The meta object literal for the '<em><b>Placing</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POI__PLACING = eINSTANCE.getPOI_Placing();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POI__DESCRIPTION = eINSTANCE.getPOI_Description();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POI__DATA = eINSTANCE.getPOI_Data();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POI__ID = eINSTANCE.getPOI_Id();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl <em>Event Filter Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getEventFilterCondition()
		 * @generated
		 */
		EClass EVENT_FILTER_CONDITION = eINSTANCE.getEventFilterCondition();

		/**
		 * The meta object literal for the '<em><b>Time Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__TIME_CONDITION = eINSTANCE.getEventFilterCondition_TimeCondition();

		/**
		 * The meta object literal for the '<em><b>Accuracy Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__ACCURACY_CONDITION = eINSTANCE.getEventFilterCondition_AccuracyCondition();

		/**
		 * The meta object literal for the '<em><b>Position Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__POSITION_CONDITION = eINSTANCE.getEventFilterCondition_PositionCondition();

		/**
		 * The meta object literal for the '<em><b>Time Min Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__TIME_MIN_INTERVAL = eINSTANCE.getEventFilterCondition_TimeMinInterval();

		/**
		 * The meta object literal for the '<em><b>Position Delta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__POSITION_DELTA = eINSTANCE.getEventFilterCondition_PositionDelta();

		/**
		 * The meta object literal for the '<em><b>Sensor Id Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__SENSOR_ID_CONDITION = eINSTANCE.getEventFilterCondition_SensorIdCondition();

		/**
		 * The meta object literal for the '<em><b>Filter Structure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__FILTER_STRUCTURE = eINSTANCE.getEventFilterCondition_FilterStructure();

		/**
		 * The meta object literal for the '<em><b>Position Condition Cells</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__POSITION_CONDITION_CELLS = eINSTANCE
				.getEventFilterCondition_PositionConditionCells();

		/**
		 * The meta object literal for the '<em><b>Id Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__ID_CONDITION = eINSTANCE.getEventFilterCondition_IdCondition();

		/**
		 * The meta object literal for the '<em><b>Category Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__CATEGORY_CONDITION = eINSTANCE.getEventFilterCondition_CategoryCondition();

		/**
		 * The meta object literal for the '<em><b>Property Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_FILTER_CONDITION__PROPERTY_CONDITION = eINSTANCE.getEventFilterCondition_PropertyCondition();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneDescriptorImpl <em>Zone Descriptor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.ZoneDescriptorImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getZoneDescriptor()
		 * @generated
		 */
		EClass ZONE_DESCRIPTOR = eINSTANCE.getZoneDescriptor();

		/**
		 * The meta object literal for the '<em><b>Zone Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ZONE_DESCRIPTOR__ZONE_ID = eINSTANCE.getZoneDescriptor_ZoneId();

		/**
		 * The meta object literal for the '<em><b>Notification Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ZONE_DESCRIPTOR__NOTIFICATION_TYPE = eINSTANCE.getZoneDescriptor_NotificationType();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.DataStorageQueryResponseImpl <em>Data Storage Query Response</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.DataStorageQueryResponseImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getDataStorageQueryResponse()
		 * @generated
		 */
		EClass DATA_STORAGE_QUERY_RESPONSE = eINSTANCE.getDataStorageQueryResponse();

		/**
		 * The meta object literal for the '<em><b>Position Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_STORAGE_QUERY_RESPONSE__POSITION_EVENTS = eINSTANCE
				.getDataStorageQueryResponse_PositionEvents();

		/**
		 * The meta object literal for the '<em><b>Tracking Task Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_STORAGE_QUERY_RESPONSE__TRACKING_TASK_ID = eINSTANCE
				.getDataStorageQueryResponse_TrackingTaskId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl <em>Message Received Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getMessageReceivedEvent()
		 * @generated
		 */
		EClass MESSAGE_RECEIVED_EVENT = eINSTANCE.getMessageReceivedEvent();

		/**
		 * The meta object literal for the '<em><b>Protocol Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_RECEIVED_EVENT__PROTOCOL_NAME = eINSTANCE.getMessageReceivedEvent_ProtocolName();

		/**
		 * The meta object literal for the '<em><b>Serialized Msg</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_RECEIVED_EVENT__SERIALIZED_MSG = eINSTANCE.getMessageReceivedEvent_SerializedMsg();

		/**
		 * The meta object literal for the '<em><b>Agent Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_RECEIVED_EVENT__AGENT_ID = eINSTANCE.getMessageReceivedEvent_AgentId();

		/**
		 * The meta object literal for the '<em><b>Extracted Attributes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_RECEIVED_EVENT__EXTRACTED_ATTRIBUTES = eINSTANCE
				.getMessageReceivedEvent_ExtractedAttributes();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_RECEIVED_EVENT__TIMESTAMP = eINSTANCE.getMessageReceivedEvent_Timestamp();

		/**
		 * The meta object literal for the '<em>String List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getStringList()
		 * @generated
		 */
		EDataType STRING_LIST = eINSTANCE.getStringList();

		/**
		 * The meta object literal for the '<em>String Array</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getStringArray()
		 * @generated
		 */
		EDataType STRING_ARRAY = eINSTANCE.getStringArray();

		/**
		 * The meta object literal for the '<em>Float Array3d</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getFloatArray3d()
		 * @generated
		 */
		EDataType FLOAT_ARRAY3D = eINSTANCE.getFloatArray3d();

		/**
		 * The meta object literal for the '<em>Boolean List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getBooleanList()
		 * @generated
		 */
		EDataType BOOLEAN_LIST = eINSTANCE.getBooleanList();

		/**
		 * The meta object literal for the '<em>Float Array</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getFloatArray()
		 * @generated
		 */
		EDataType FLOAT_ARRAY = eINSTANCE.getFloatArray();

		/**
		 * The meta object literal for the '<em>Byte Array</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getByteArray()
		 * @generated
		 */
		EDataType BYTE_ARRAY = eINSTANCE.getByteArray();

		/**
		 * The meta object literal for the '<em>List Of String Maps</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.LinkedList
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getListOfStringMaps()
		 * @generated
		 */
		EDataType LIST_OF_STRING_MAPS = eINSTANCE.getListOfStringMaps();

		/**
		 * The meta object literal for the '<em>Int Array</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl#getIntArray()
		 * @generated
		 */
		EDataType INT_ARRAY = eINSTANCE.getIntArray();

	}

} //IPos_DatamodelPackage
