/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Localizable Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getLastPosUpdate <em>Last Pos Update</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getSensorType <em>Sensor Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getAgent <em>Agent</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getCurrentPlacing <em>Current Placing</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getLocalizableObject()
 * @model
 * @generated
 */
public interface LocalizableObject extends Entity {
	/**
	 * Returns the value of the '<em><b>Last Pos Update</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Pos Update</em>' attribute.
	 * @see #setLastPosUpdate(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getLocalizableObject_LastPosUpdate()
	 * @model
	 * @generated
	 */
	String getLastPosUpdate();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getLastPosUpdate <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Pos Update</em>' attribute.
	 * @see #getLastPosUpdate()
	 * @generated
	 */
	void setLastPosUpdate(String value);

	/**
	 * Returns the value of the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sensor Type</em>' attribute.
	 * @see #setSensorType(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getLocalizableObject_SensorType()
	 * @model
	 * @generated
	 */
	String getSensorType();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getSensorType <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sensor Type</em>' attribute.
	 * @see #getSensorType()
	 * @generated
	 */
	void setSensorType(String value);

	/**
	 * Returns the value of the '<em><b>Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent</em>' reference.
	 * @see #setAgent(Agent)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getLocalizableObject_Agent()
	 * @model
	 * @generated
	 */
	Agent getAgent();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getAgent <em>Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent</em>' reference.
	 * @see #getAgent()
	 * @generated
	 */
	void setAgent(Agent value);

	/**
	 * Returns the value of the '<em><b>Current Placing</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Placing</em>' reference.
	 * @see #setCurrentPlacing(Placing)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getLocalizableObject_CurrentPlacing()
	 * @model
	 * @generated
	 */
	Placing getCurrentPlacing();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject#getCurrentPlacing <em>Current Placing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Placing</em>' reference.
	 * @see #getCurrentPlacing()
	 * @generated
	 */
	void setCurrentPlacing(Placing value);

} // LocalizableObject
