/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMapType()
 * @model
 * @generated
 */
public interface MapType extends EObject {
} // MapType
