/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import java.util.HashMap;
import java.util.LinkedList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Received Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getProtocolName <em>Protocol Name</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getSerializedMsg <em>Serialized Msg</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getAgentId <em>Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getExtractedAttributes <em>Extracted Attributes</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getTimestamp <em>Timestamp</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMessageReceivedEvent()
 * @model
 * @generated
 */
public interface MessageReceivedEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Protocol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protocol Name</em>' attribute.
	 * @see #setProtocolName(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMessageReceivedEvent_ProtocolName()
	 * @model
	 * @generated
	 */
	String getProtocolName();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getProtocolName <em>Protocol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protocol Name</em>' attribute.
	 * @see #getProtocolName()
	 * @generated
	 */
	void setProtocolName(String value);

	/**
	 * Returns the value of the '<em><b>Serialized Msg</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serialized Msg</em>' attribute.
	 * @see #setSerializedMsg(byte[])
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMessageReceivedEvent_SerializedMsg()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.ByteArray"
	 * @generated
	 */
	byte[] getSerializedMsg();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getSerializedMsg <em>Serialized Msg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Serialized Msg</em>' attribute.
	 * @see #getSerializedMsg()
	 * @generated
	 */
	void setSerializedMsg(byte[] value);

	/**
	 * Returns the value of the '<em><b>Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Id</em>' attribute.
	 * @see #setAgentId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMessageReceivedEvent_AgentId()
	 * @model
	 * @generated
	 */
	String getAgentId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getAgentId <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Id</em>' attribute.
	 * @see #getAgentId()
	 * @generated
	 */
	void setAgentId(String value);

	/**
	 * Returns the value of the '<em><b>Extracted Attributes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extracted Attributes</em>' attribute.
	 * @see #setExtractedAttributes(LinkedList)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMessageReceivedEvent_ExtractedAttributes()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.ListOfStringMaps"
	 * @generated
	 */
	LinkedList<HashMap<String, String>> getExtractedAttributes();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getExtractedAttributes <em>Extracted Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extracted Attributes</em>' attribute.
	 * @see #getExtractedAttributes()
	 * @generated
	 */
	void setExtractedAttributes(LinkedList<HashMap<String, String>> value);

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMessageReceivedEvent_Timestamp()
	 * @model
	 * @generated
	 */
	String getTimestamp();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(String value);

} // MessageReceivedEvent
