/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitoring Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask#getEventfilterconfiguration <em>Eventfilterconfiguration</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMonitoringTask()
 * @model
 * @generated
 */
public interface MonitoringTask extends EObject {
	/**
	 * Returns the value of the '<em><b>Eventfilterconfiguration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eventfilterconfiguration</em>' reference.
	 * @see #setEventfilterconfiguration(EventFilterConfiguration)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getMonitoringTask_Eventfilterconfiguration()
	 * @model required="true"
	 * @generated
	 */
	EventFilterConfiguration getEventfilterconfiguration();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask#getEventfilterconfiguration <em>Eventfilterconfiguration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Eventfilterconfiguration</em>' reference.
	 * @see #getEventfilterconfiguration()
	 * @generated
	 */
	void setEventfilterconfiguration(EventFilterConfiguration value);

} // MonitoringTask
