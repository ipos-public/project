/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage
 * @generated
 */
public interface OFBizFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OFBizFactory eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Picklist</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Picklist</em>'.
	 * @generated
	 */
	Picklist createPicklist();

	/**
	 * Returns a new object of class '<em>Picklist Bin</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Picklist Bin</em>'.
	 * @generated
	 */
	PicklistBin createPicklistBin();

	/**
	 * Returns a new object of class '<em>Picklist Item</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Picklist Item</em>'.
	 * @generated
	 */
	PicklistItem createPicklistItem();

	/**
	 * Returns a new object of class '<em>Inventory Item</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inventory Item</em>'.
	 * @generated
	 */
	InventoryItem createInventoryItem();

	/**
	 * Returns a new object of class '<em>Picklist Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Picklist Role</em>'.
	 * @generated
	 */
	PicklistRole createPicklistRole();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OFBizPackage getOFBizPackage();

} //OFBizFactory
