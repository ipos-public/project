/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizFactory
 * @model kind="package"
 * @generated
 */
public interface OFBizPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "OFBiz";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ofbiz";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ofbiz";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OFBizPackage eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl.init();

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistImpl <em>Picklist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getPicklist()
	 * @generated
	 */
	int PICKLIST = 0;

	/**
	 * The feature id for the '<em><b>Picklist Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST__PICKLIST_ID = 0;

	/**
	 * The feature id for the '<em><b>Picklist Roles</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST__PICKLIST_ROLES = 1;

	/**
	 * The feature id for the '<em><b>Picklist Bins</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST__PICKLIST_BINS = 2;

	/**
	 * The number of structural features of the '<em>Picklist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Picklist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistBinImpl <em>Picklist Bin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistBinImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getPicklistBin()
	 * @generated
	 */
	int PICKLIST_BIN = 1;

	/**
	 * The feature id for the '<em><b>Picklist Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_BIN__PICKLIST_ID = 0;

	/**
	 * The feature id for the '<em><b>Bin Location Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_BIN__BIN_LOCATION_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Picklist Items</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_BIN__PICKLIST_ITEMS = 2;

	/**
	 * The number of structural features of the '<em>Picklist Bin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_BIN_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Picklist Bin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_BIN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistItemImpl <em>Picklist Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistItemImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getPicklistItem()
	 * @generated
	 */
	int PICKLIST_ITEM = 2;

	/**
	 * The feature id for the '<em><b>Inventory Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_ITEM__INVENTORY_ITEM = 0;

	/**
	 * The number of structural features of the '<em>Picklist Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_ITEM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Picklist Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_ITEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.InventoryItemImpl <em>Inventory Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.InventoryItemImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getInventoryItem()
	 * @generated
	 */
	int INVENTORY_ITEM = 3;

	/**
	 * The feature id for the '<em><b>Inventory Item Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVENTORY_ITEM__INVENTORY_ITEM_ID = 0;

	/**
	 * The feature id for the '<em><b>Container Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVENTORY_ITEM__CONTAINER_ID = 1;

	/**
	 * The feature id for the '<em><b>Product Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVENTORY_ITEM__PRODUCT_ID = 2;

	/**
	 * The number of structural features of the '<em>Inventory Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVENTORY_ITEM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Inventory Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVENTORY_ITEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistRoleImpl <em>Picklist Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistRoleImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getPicklistRole()
	 * @generated
	 */
	int PICKLIST_ROLE = 4;

	/**
	 * The feature id for the '<em><b>Picklist Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_ROLE__PICKLIST_ID = 0;

	/**
	 * The feature id for the '<em><b>Party Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_ROLE__PARTY_ID = 1;

	/**
	 * The feature id for the '<em><b>Role Type Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_ROLE__ROLE_TYPE_ID = 2;

	/**
	 * The number of structural features of the '<em>Picklist Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_ROLE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Picklist Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PICKLIST_ROLE_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist <em>Picklist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Picklist</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist
	 * @generated
	 */
	EClass getPicklist();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistId <em>Picklist Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Picklist Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistId()
	 * @see #getPicklist()
	 * @generated
	 */
	EAttribute getPicklist_PicklistId();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistRoles <em>Picklist Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Picklist Roles</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistRoles()
	 * @see #getPicklist()
	 * @generated
	 */
	EReference getPicklist_PicklistRoles();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistBins <em>Picklist Bins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Picklist Bins</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistBins()
	 * @see #getPicklist()
	 * @generated
	 */
	EReference getPicklist_PicklistBins();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin <em>Picklist Bin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Picklist Bin</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin
	 * @generated
	 */
	EClass getPicklistBin();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getPicklistId <em>Picklist Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Picklist Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getPicklistId()
	 * @see #getPicklistBin()
	 * @generated
	 */
	EAttribute getPicklistBin_PicklistId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getBinLocationNumber <em>Bin Location Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bin Location Number</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getBinLocationNumber()
	 * @see #getPicklistBin()
	 * @generated
	 */
	EAttribute getPicklistBin_BinLocationNumber();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getPicklistItems <em>Picklist Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Picklist Items</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getPicklistItems()
	 * @see #getPicklistBin()
	 * @generated
	 */
	EReference getPicklistBin_PicklistItems();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistItem <em>Picklist Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Picklist Item</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistItem
	 * @generated
	 */
	EClass getPicklistItem();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistItem#getInventoryItem <em>Inventory Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Inventory Item</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistItem#getInventoryItem()
	 * @see #getPicklistItem()
	 * @generated
	 */
	EReference getPicklistItem_InventoryItem();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem <em>Inventory Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inventory Item</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem
	 * @generated
	 */
	EClass getInventoryItem();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem#getInventoryItemId <em>Inventory Item Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inventory Item Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem#getInventoryItemId()
	 * @see #getInventoryItem()
	 * @generated
	 */
	EAttribute getInventoryItem_InventoryItemId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem#getContainerId <em>Container Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Container Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem#getContainerId()
	 * @see #getInventoryItem()
	 * @generated
	 */
	EAttribute getInventoryItem_ContainerId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem#getProductId <em>Product Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Product Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem#getProductId()
	 * @see #getInventoryItem()
	 * @generated
	 */
	EAttribute getInventoryItem_ProductId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole <em>Picklist Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Picklist Role</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole
	 * @generated
	 */
	EClass getPicklistRole();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole#getPicklistId <em>Picklist Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Picklist Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole#getPicklistId()
	 * @see #getPicklistRole()
	 * @generated
	 */
	EAttribute getPicklistRole_PicklistId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole#getPartyId <em>Party Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Party Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole#getPartyId()
	 * @see #getPicklistRole()
	 * @generated
	 */
	EAttribute getPicklistRole_PartyId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole#getRoleTypeId <em>Role Type Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role Type Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole#getRoleTypeId()
	 * @see #getPicklistRole()
	 * @generated
	 */
	EAttribute getPicklistRole_RoleTypeId();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OFBizFactory getOFBizFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistImpl <em>Picklist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getPicklist()
		 * @generated
		 */
		EClass PICKLIST = eINSTANCE.getPicklist();

		/**
		 * The meta object literal for the '<em><b>Picklist Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PICKLIST__PICKLIST_ID = eINSTANCE.getPicklist_PicklistId();

		/**
		 * The meta object literal for the '<em><b>Picklist Roles</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PICKLIST__PICKLIST_ROLES = eINSTANCE.getPicklist_PicklistRoles();

		/**
		 * The meta object literal for the '<em><b>Picklist Bins</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PICKLIST__PICKLIST_BINS = eINSTANCE.getPicklist_PicklistBins();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistBinImpl <em>Picklist Bin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistBinImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getPicklistBin()
		 * @generated
		 */
		EClass PICKLIST_BIN = eINSTANCE.getPicklistBin();

		/**
		 * The meta object literal for the '<em><b>Picklist Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PICKLIST_BIN__PICKLIST_ID = eINSTANCE.getPicklistBin_PicklistId();

		/**
		 * The meta object literal for the '<em><b>Bin Location Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PICKLIST_BIN__BIN_LOCATION_NUMBER = eINSTANCE.getPicklistBin_BinLocationNumber();

		/**
		 * The meta object literal for the '<em><b>Picklist Items</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PICKLIST_BIN__PICKLIST_ITEMS = eINSTANCE.getPicklistBin_PicklistItems();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistItemImpl <em>Picklist Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistItemImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getPicklistItem()
		 * @generated
		 */
		EClass PICKLIST_ITEM = eINSTANCE.getPicklistItem();

		/**
		 * The meta object literal for the '<em><b>Inventory Item</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PICKLIST_ITEM__INVENTORY_ITEM = eINSTANCE.getPicklistItem_InventoryItem();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.InventoryItemImpl <em>Inventory Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.InventoryItemImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getInventoryItem()
		 * @generated
		 */
		EClass INVENTORY_ITEM = eINSTANCE.getInventoryItem();

		/**
		 * The meta object literal for the '<em><b>Inventory Item Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INVENTORY_ITEM__INVENTORY_ITEM_ID = eINSTANCE.getInventoryItem_InventoryItemId();

		/**
		 * The meta object literal for the '<em><b>Container Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INVENTORY_ITEM__CONTAINER_ID = eINSTANCE.getInventoryItem_ContainerId();

		/**
		 * The meta object literal for the '<em><b>Product Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INVENTORY_ITEM__PRODUCT_ID = eINSTANCE.getInventoryItem_ProductId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistRoleImpl <em>Picklist Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistRoleImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl#getPicklistRole()
		 * @generated
		 */
		EClass PICKLIST_ROLE = eINSTANCE.getPicklistRole();

		/**
		 * The meta object literal for the '<em><b>Picklist Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PICKLIST_ROLE__PICKLIST_ID = eINSTANCE.getPicklistRole_PicklistId();

		/**
		 * The meta object literal for the '<em><b>Party Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PICKLIST_ROLE__PARTY_ID = eINSTANCE.getPicklistRole_PartyId();

		/**
		 * The meta object literal for the '<em><b>Role Type Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PICKLIST_ROLE__ROLE_TYPE_ID = eINSTANCE.getPicklistRole_RoleTypeId();

	}

} //OFBizPackage
