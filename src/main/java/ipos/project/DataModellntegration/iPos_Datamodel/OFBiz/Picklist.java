/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Picklist</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistId <em>Picklist Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistRoles <em>Picklist Roles</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistBins <em>Picklist Bins</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#getPicklist()
 * @model
 * @generated
 */
public interface Picklist extends EObject {
	/**
	 * Returns the value of the '<em><b>Picklist Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picklist Id</em>' attribute.
	 * @see #setPicklistId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#getPicklist_PicklistId()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getPicklistId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist#getPicklistId <em>Picklist Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Picklist Id</em>' attribute.
	 * @see #getPicklistId()
	 * @generated
	 */
	void setPicklistId(String value);

	/**
	 * Returns the value of the '<em><b>Picklist Roles</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picklist Roles</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#getPicklist_PicklistRoles()
	 * @model
	 * @generated
	 */
	EList<PicklistRole> getPicklistRoles();

	/**
	 * Returns the value of the '<em><b>Picklist Bins</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picklist Bins</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#getPicklist_PicklistBins()
	 * @model
	 * @generated
	 */
	EList<PicklistBin> getPicklistBins();

} // Picklist
