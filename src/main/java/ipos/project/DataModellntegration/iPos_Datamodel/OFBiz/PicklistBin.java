/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Picklist Bin</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getPicklistId <em>Picklist Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getBinLocationNumber <em>Bin Location Number</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getPicklistItems <em>Picklist Items</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#getPicklistBin()
 * @model
 * @generated
 */
public interface PicklistBin extends EObject {
	/**
	 * Returns the value of the '<em><b>Picklist Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picklist Id</em>' attribute.
	 * @see #setPicklistId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#getPicklistBin_PicklistId()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getPicklistId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getPicklistId <em>Picklist Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Picklist Id</em>' attribute.
	 * @see #getPicklistId()
	 * @generated
	 */
	void setPicklistId(String value);

	/**
	 * Returns the value of the '<em><b>Bin Location Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bin Location Number</em>' attribute.
	 * @see #setBinLocationNumber(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#getPicklistBin_BinLocationNumber()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getBinLocationNumber();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin#getBinLocationNumber <em>Bin Location Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bin Location Number</em>' attribute.
	 * @see #getBinLocationNumber()
	 * @generated
	 */
	void setBinLocationNumber(String value);

	/**
	 * Returns the value of the '<em><b>Picklist Items</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistItem}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Picklist Items</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#getPicklistBin_PicklistItems()
	 * @model
	 * @generated
	 */
	EList<PicklistItem> getPicklistItems();

} // PicklistBin
