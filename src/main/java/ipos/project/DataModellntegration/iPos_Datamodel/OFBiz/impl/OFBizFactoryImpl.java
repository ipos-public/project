/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OFBizFactoryImpl extends EFactoryImpl implements OFBizFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OFBizFactory init() {
		try {
			OFBizFactory theOFBizFactory = (OFBizFactory) EPackage.Registry.INSTANCE.getEFactory(OFBizPackage.eNS_URI);
			if (theOFBizFactory != null) {
				return theOFBizFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OFBizFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OFBizFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case OFBizPackage.PICKLIST:
			return createPicklist();
		case OFBizPackage.PICKLIST_BIN:
			return createPicklistBin();
		case OFBizPackage.PICKLIST_ITEM:
			return createPicklistItem();
		case OFBizPackage.INVENTORY_ITEM:
			return createInventoryItem();
		case OFBizPackage.PICKLIST_ROLE:
			return createPicklistRole();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Picklist createPicklist() {
		PicklistImpl picklist = new PicklistImpl();
		return picklist;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PicklistBin createPicklistBin() {
		PicklistBinImpl picklistBin = new PicklistBinImpl();
		return picklistBin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PicklistItem createPicklistItem() {
		PicklistItemImpl picklistItem = new PicklistItemImpl();
		return picklistItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InventoryItem createInventoryItem() {
		InventoryItemImpl inventoryItem = new InventoryItemImpl();
		return inventoryItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PicklistRole createPicklistRole() {
		PicklistRoleImpl picklistRole = new PicklistRoleImpl();
		return picklistRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OFBizPackage getOFBizPackage() {
		return (OFBizPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OFBizPackage getPackage() {
		return OFBizPackage.eINSTANCE;
	}

} //OFBizFactoryImpl
