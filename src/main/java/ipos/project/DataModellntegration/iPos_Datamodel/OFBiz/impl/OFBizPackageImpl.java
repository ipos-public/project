/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistItem;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OFBizPackageImpl extends EPackageImpl implements OFBizPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass picklistEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass picklistBinEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass picklistItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inventoryItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass picklistRoleEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OFBizPackageImpl() {
		super(eNS_URI, OFBizFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OFBizPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OFBizPackage init() {
		if (isInited)
			return (OFBizPackage) EPackage.Registry.INSTANCE.getEPackage(OFBizPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOFBizPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OFBizPackageImpl theOFBizPackage = registeredOFBizPackage instanceof OFBizPackageImpl
				? (OFBizPackageImpl) registeredOFBizPackage
				: new OFBizPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPos_DatamodelPackage.eNS_URI);
		IPos_DatamodelPackageImpl theIPos_DatamodelPackage = (IPos_DatamodelPackageImpl) (registeredPackage instanceof IPos_DatamodelPackageImpl
				? registeredPackage
				: IPos_DatamodelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPosDevKitPackage.eNS_URI);
		IPosDevKitPackageImpl theIPosDevKitPackage = (IPosDevKitPackageImpl) (registeredPackage instanceof IPosDevKitPackageImpl
				? registeredPackage
				: IPosDevKitPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ToozPackage.eNS_URI);
		ToozPackageImpl theToozPackage = (ToozPackageImpl) (registeredPackage instanceof ToozPackageImpl
				? registeredPackage
				: ToozPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VDA5050Package.eNS_URI);
		VDA5050PackageImpl theVDA5050Package = (VDA5050PackageImpl) (registeredPackage instanceof VDA5050PackageImpl
				? registeredPackage
				: VDA5050Package.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OSMPackage.eNS_URI);
		OSMPackageImpl theOSMPackage = (OSMPackageImpl) (registeredPackage instanceof OSMPackageImpl ? registeredPackage
				: OSMPackage.eINSTANCE);

		// Create package meta-data objects
		theOFBizPackage.createPackageContents();
		theIPos_DatamodelPackage.createPackageContents();
		theIPosDevKitPackage.createPackageContents();
		theToozPackage.createPackageContents();
		theVDA5050Package.createPackageContents();
		theOSMPackage.createPackageContents();

		// Initialize created meta-data
		theOFBizPackage.initializePackageContents();
		theIPos_DatamodelPackage.initializePackageContents();
		theIPosDevKitPackage.initializePackageContents();
		theToozPackage.initializePackageContents();
		theVDA5050Package.initializePackageContents();
		theOSMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOFBizPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OFBizPackage.eNS_URI, theOFBizPackage);
		return theOFBizPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPicklist() {
		return picklistEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPicklist_PicklistId() {
		return (EAttribute) picklistEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPicklist_PicklistRoles() {
		return (EReference) picklistEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPicklist_PicklistBins() {
		return (EReference) picklistEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPicklistBin() {
		return picklistBinEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPicklistBin_PicklistId() {
		return (EAttribute) picklistBinEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPicklistBin_BinLocationNumber() {
		return (EAttribute) picklistBinEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPicklistBin_PicklistItems() {
		return (EReference) picklistBinEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPicklistItem() {
		return picklistItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPicklistItem_InventoryItem() {
		return (EReference) picklistItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInventoryItem() {
		return inventoryItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInventoryItem_InventoryItemId() {
		return (EAttribute) inventoryItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInventoryItem_ContainerId() {
		return (EAttribute) inventoryItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInventoryItem_ProductId() {
		return (EAttribute) inventoryItemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPicklistRole() {
		return picklistRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPicklistRole_PicklistId() {
		return (EAttribute) picklistRoleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPicklistRole_PartyId() {
		return (EAttribute) picklistRoleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPicklistRole_RoleTypeId() {
		return (EAttribute) picklistRoleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OFBizFactory getOFBizFactory() {
		return (OFBizFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		picklistEClass = createEClass(PICKLIST);
		createEAttribute(picklistEClass, PICKLIST__PICKLIST_ID);
		createEReference(picklistEClass, PICKLIST__PICKLIST_ROLES);
		createEReference(picklistEClass, PICKLIST__PICKLIST_BINS);

		picklistBinEClass = createEClass(PICKLIST_BIN);
		createEAttribute(picklistBinEClass, PICKLIST_BIN__PICKLIST_ID);
		createEAttribute(picklistBinEClass, PICKLIST_BIN__BIN_LOCATION_NUMBER);
		createEReference(picklistBinEClass, PICKLIST_BIN__PICKLIST_ITEMS);

		picklistItemEClass = createEClass(PICKLIST_ITEM);
		createEReference(picklistItemEClass, PICKLIST_ITEM__INVENTORY_ITEM);

		inventoryItemEClass = createEClass(INVENTORY_ITEM);
		createEAttribute(inventoryItemEClass, INVENTORY_ITEM__INVENTORY_ITEM_ID);
		createEAttribute(inventoryItemEClass, INVENTORY_ITEM__CONTAINER_ID);
		createEAttribute(inventoryItemEClass, INVENTORY_ITEM__PRODUCT_ID);

		picklistRoleEClass = createEClass(PICKLIST_ROLE);
		createEAttribute(picklistRoleEClass, PICKLIST_ROLE__PICKLIST_ID);
		createEAttribute(picklistRoleEClass, PICKLIST_ROLE__PARTY_ID);
		createEAttribute(picklistRoleEClass, PICKLIST_ROLE__ROLE_TYPE_ID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE
				.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(picklistEClass, Picklist.class, "Picklist", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPicklist_PicklistId(), theXMLTypePackage.getString(), "picklistId", null, 0, 1,
				Picklist.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getPicklist_PicklistRoles(), this.getPicklistRole(), null, "picklistRoles", null, 0, -1,
				Picklist.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPicklist_PicklistBins(), this.getPicklistBin(), null, "picklistBins", null, 0, -1,
				Picklist.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(picklistBinEClass, PicklistBin.class, "PicklistBin", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPicklistBin_PicklistId(), theXMLTypePackage.getString(), "picklistId", null, 0, 1,
				PicklistBin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPicklistBin_BinLocationNumber(), theXMLTypePackage.getString(), "binLocationNumber", null, 0,
				1, PicklistBin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getPicklistBin_PicklistItems(), this.getPicklistItem(), null, "picklistItems", null, 0, -1,
				PicklistBin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(picklistItemEClass, PicklistItem.class, "PicklistItem", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPicklistItem_InventoryItem(), this.getInventoryItem(), null, "inventoryItem", null, 1, 1,
				PicklistItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inventoryItemEClass, InventoryItem.class, "InventoryItem", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInventoryItem_InventoryItemId(), theXMLTypePackage.getString(), "inventoryItemId", null, 0, 1,
				InventoryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getInventoryItem_ContainerId(), theXMLTypePackage.getString(), "containerId", null, 0, 1,
				InventoryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getInventoryItem_ProductId(), theXMLTypePackage.getString(), "productId", null, 0, 1,
				InventoryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(picklistRoleEClass, PicklistRole.class, "PicklistRole", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPicklistRole_PicklistId(), theXMLTypePackage.getString(), "picklistId", null, 0, 1,
				PicklistRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPicklistRole_PartyId(), theXMLTypePackage.getString(), "partyId", null, 0, 1,
				PicklistRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPicklistRole_RoleTypeId(), theXMLTypePackage.getString(), "roleTypeId", null, 0, 1,
				PicklistRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
	}

} //OFBizPackageImpl
