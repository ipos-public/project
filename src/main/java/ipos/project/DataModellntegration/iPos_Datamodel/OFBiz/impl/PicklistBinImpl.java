/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistItem;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Picklist Bin</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistBinImpl#getPicklistId <em>Picklist Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistBinImpl#getBinLocationNumber <em>Bin Location Number</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistBinImpl#getPicklistItems <em>Picklist Items</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PicklistBinImpl extends MinimalEObjectImpl.Container implements PicklistBin {
	/**
	 * The default value of the '{@link #getPicklistId() <em>Picklist Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPicklistId()
	 * @generated
	 * @ordered
	 */
	protected static final String PICKLIST_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPicklistId() <em>Picklist Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPicklistId()
	 * @generated
	 * @ordered
	 */
	protected String picklistId = PICKLIST_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getBinLocationNumber() <em>Bin Location Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinLocationNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String BIN_LOCATION_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBinLocationNumber() <em>Bin Location Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinLocationNumber()
	 * @generated
	 * @ordered
	 */
	protected String binLocationNumber = BIN_LOCATION_NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPicklistItems() <em>Picklist Items</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPicklistItems()
	 * @generated
	 * @ordered
	 */
	protected EList<PicklistItem> picklistItems;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PicklistBinImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OFBizPackage.Literals.PICKLIST_BIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPicklistId() {
		return picklistId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPicklistId(String newPicklistId) {
		String oldPicklistId = picklistId;
		picklistId = newPicklistId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OFBizPackage.PICKLIST_BIN__PICKLIST_ID, oldPicklistId,
					picklistId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBinLocationNumber() {
		return binLocationNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinLocationNumber(String newBinLocationNumber) {
		String oldBinLocationNumber = binLocationNumber;
		binLocationNumber = newBinLocationNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OFBizPackage.PICKLIST_BIN__BIN_LOCATION_NUMBER,
					oldBinLocationNumber, binLocationNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PicklistItem> getPicklistItems() {
		if (picklistItems == null) {
			picklistItems = new EObjectResolvingEList<PicklistItem>(PicklistItem.class, this,
					OFBizPackage.PICKLIST_BIN__PICKLIST_ITEMS);
		}
		return picklistItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OFBizPackage.PICKLIST_BIN__PICKLIST_ID:
			return getPicklistId();
		case OFBizPackage.PICKLIST_BIN__BIN_LOCATION_NUMBER:
			return getBinLocationNumber();
		case OFBizPackage.PICKLIST_BIN__PICKLIST_ITEMS:
			return getPicklistItems();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OFBizPackage.PICKLIST_BIN__PICKLIST_ID:
			setPicklistId((String) newValue);
			return;
		case OFBizPackage.PICKLIST_BIN__BIN_LOCATION_NUMBER:
			setBinLocationNumber((String) newValue);
			return;
		case OFBizPackage.PICKLIST_BIN__PICKLIST_ITEMS:
			getPicklistItems().clear();
			getPicklistItems().addAll((Collection<? extends PicklistItem>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OFBizPackage.PICKLIST_BIN__PICKLIST_ID:
			setPicklistId(PICKLIST_ID_EDEFAULT);
			return;
		case OFBizPackage.PICKLIST_BIN__BIN_LOCATION_NUMBER:
			setBinLocationNumber(BIN_LOCATION_NUMBER_EDEFAULT);
			return;
		case OFBizPackage.PICKLIST_BIN__PICKLIST_ITEMS:
			getPicklistItems().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OFBizPackage.PICKLIST_BIN__PICKLIST_ID:
			return PICKLIST_ID_EDEFAULT == null ? picklistId != null : !PICKLIST_ID_EDEFAULT.equals(picklistId);
		case OFBizPackage.PICKLIST_BIN__BIN_LOCATION_NUMBER:
			return BIN_LOCATION_NUMBER_EDEFAULT == null ? binLocationNumber != null
					: !BIN_LOCATION_NUMBER_EDEFAULT.equals(binLocationNumber);
		case OFBizPackage.PICKLIST_BIN__PICKLIST_ITEMS:
			return picklistItems != null && !picklistItems.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (picklistId: ");
		result.append(picklistId);
		result.append(", binLocationNumber: ");
		result.append(binLocationNumber);
		result.append(')');
		return result.toString();
	}

} //PicklistBinImpl
