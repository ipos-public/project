/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistBin;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistRole;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Picklist</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistImpl#getPicklistId <em>Picklist Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistImpl#getPicklistRoles <em>Picklist Roles</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistImpl#getPicklistBins <em>Picklist Bins</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PicklistImpl extends MinimalEObjectImpl.Container implements Picklist {
	/**
	 * The default value of the '{@link #getPicklistId() <em>Picklist Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPicklistId()
	 * @generated
	 * @ordered
	 */
	protected static final String PICKLIST_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPicklistId() <em>Picklist Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPicklistId()
	 * @generated
	 * @ordered
	 */
	protected String picklistId = PICKLIST_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPicklistRoles() <em>Picklist Roles</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPicklistRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<PicklistRole> picklistRoles;

	/**
	 * The cached value of the '{@link #getPicklistBins() <em>Picklist Bins</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPicklistBins()
	 * @generated
	 * @ordered
	 */
	protected EList<PicklistBin> picklistBins;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PicklistImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OFBizPackage.Literals.PICKLIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPicklistId() {
		return picklistId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPicklistId(String newPicklistId) {
		String oldPicklistId = picklistId;
		picklistId = newPicklistId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OFBizPackage.PICKLIST__PICKLIST_ID, oldPicklistId,
					picklistId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PicklistRole> getPicklistRoles() {
		if (picklistRoles == null) {
			picklistRoles = new EObjectResolvingEList<PicklistRole>(PicklistRole.class, this,
					OFBizPackage.PICKLIST__PICKLIST_ROLES);
		}
		return picklistRoles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PicklistBin> getPicklistBins() {
		if (picklistBins == null) {
			picklistBins = new EObjectResolvingEList<PicklistBin>(PicklistBin.class, this,
					OFBizPackage.PICKLIST__PICKLIST_BINS);
		}
		return picklistBins;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OFBizPackage.PICKLIST__PICKLIST_ID:
			return getPicklistId();
		case OFBizPackage.PICKLIST__PICKLIST_ROLES:
			return getPicklistRoles();
		case OFBizPackage.PICKLIST__PICKLIST_BINS:
			return getPicklistBins();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OFBizPackage.PICKLIST__PICKLIST_ID:
			setPicklistId((String) newValue);
			return;
		case OFBizPackage.PICKLIST__PICKLIST_ROLES:
			getPicklistRoles().clear();
			getPicklistRoles().addAll((Collection<? extends PicklistRole>) newValue);
			return;
		case OFBizPackage.PICKLIST__PICKLIST_BINS:
			getPicklistBins().clear();
			getPicklistBins().addAll((Collection<? extends PicklistBin>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OFBizPackage.PICKLIST__PICKLIST_ID:
			setPicklistId(PICKLIST_ID_EDEFAULT);
			return;
		case OFBizPackage.PICKLIST__PICKLIST_ROLES:
			getPicklistRoles().clear();
			return;
		case OFBizPackage.PICKLIST__PICKLIST_BINS:
			getPicklistBins().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OFBizPackage.PICKLIST__PICKLIST_ID:
			return PICKLIST_ID_EDEFAULT == null ? picklistId != null : !PICKLIST_ID_EDEFAULT.equals(picklistId);
		case OFBizPackage.PICKLIST__PICKLIST_ROLES:
			return picklistRoles != null && !picklistRoles.isEmpty();
		case OFBizPackage.PICKLIST__PICKLIST_BINS:
			return picklistBins != null && !picklistBins.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (picklistId: ");
		result.append(picklistId);
		result.append(')');
		return result.toString();
	}

} //PicklistImpl
