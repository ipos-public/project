/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.InventoryItem;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.PicklistItem;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Picklist Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.PicklistItemImpl#getInventoryItem <em>Inventory Item</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PicklistItemImpl extends MinimalEObjectImpl.Container implements PicklistItem {
	/**
	 * The cached value of the '{@link #getInventoryItem() <em>Inventory Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInventoryItem()
	 * @generated
	 * @ordered
	 */
	protected InventoryItem inventoryItem;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PicklistItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OFBizPackage.Literals.PICKLIST_ITEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InventoryItem getInventoryItem() {
		if (inventoryItem != null && inventoryItem.eIsProxy()) {
			InternalEObject oldInventoryItem = (InternalEObject) inventoryItem;
			inventoryItem = (InventoryItem) eResolveProxy(oldInventoryItem);
			if (inventoryItem != oldInventoryItem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							OFBizPackage.PICKLIST_ITEM__INVENTORY_ITEM, oldInventoryItem, inventoryItem));
			}
		}
		return inventoryItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InventoryItem basicGetInventoryItem() {
		return inventoryItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInventoryItem(InventoryItem newInventoryItem) {
		InventoryItem oldInventoryItem = inventoryItem;
		inventoryItem = newInventoryItem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OFBizPackage.PICKLIST_ITEM__INVENTORY_ITEM,
					oldInventoryItem, inventoryItem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OFBizPackage.PICKLIST_ITEM__INVENTORY_ITEM:
			if (resolve)
				return getInventoryItem();
			return basicGetInventoryItem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OFBizPackage.PICKLIST_ITEM__INVENTORY_ITEM:
			setInventoryItem((InventoryItem) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OFBizPackage.PICKLIST_ITEM__INVENTORY_ITEM:
			setInventoryItem((InventoryItem) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OFBizPackage.PICKLIST_ITEM__INVENTORY_ITEM:
			return inventoryItem != null;
		}
		return super.eIsSet(featureID);
	}

} //PicklistItemImpl
