/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extracted Attributes</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getBatteryChargeLevel <em>Battery Charge Level</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getLoadedItems <em>Loaded Items</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getErrors <em>Errors</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getTheta <em>Theta</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMExtractedAttributes()
 * @model
 * @generated
 */
public interface OSMExtractedAttributes extends EObject {
	/**
	 * Returns the value of the '<em><b>Battery Charge Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Battery Charge Level</em>' attribute.
	 * @see #setBatteryChargeLevel(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMExtractedAttributes_BatteryChargeLevel()
	 * @model
	 * @generated
	 */
	float getBatteryChargeLevel();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getBatteryChargeLevel <em>Battery Charge Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Battery Charge Level</em>' attribute.
	 * @see #getBatteryChargeLevel()
	 * @generated
	 */
	void setBatteryChargeLevel(float value);

	/**
	 * Returns the value of the '<em><b>Loaded Items</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loaded Items</em>' attribute.
	 * @see #setLoadedItems(int[])
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMExtractedAttributes_LoadedItems()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.IntArray"
	 * @generated
	 */
	int[] getLoadedItems();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getLoadedItems <em>Loaded Items</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loaded Items</em>' attribute.
	 * @see #getLoadedItems()
	 * @generated
	 */
	void setLoadedItems(int[] value);

	/**
	 * Returns the value of the '<em><b>Errors</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Errors</em>' attribute.
	 * @see #setErrors(int[])
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMExtractedAttributes_Errors()
	 * @model dataType="ipos.project.DataModellntegration.iPos_Datamodel.IntArray"
	 * @generated
	 */
	int[] getErrors();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getErrors <em>Errors</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Errors</em>' attribute.
	 * @see #getErrors()
	 * @generated
	 */
	void setErrors(int[] value);

	/**
	 * Returns the value of the '<em><b>Theta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Theta</em>' attribute.
	 * @see #setTheta(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMExtractedAttributes_Theta()
	 * @model
	 * @generated
	 */
	float getTheta();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getTheta <em>Theta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Theta</em>' attribute.
	 * @see #getTheta()
	 * @generated
	 */
	void setTheta(float value);

} // OSMExtractedAttributes
