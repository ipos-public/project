/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage
 * @generated
 */
public interface OSMFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OSMFactory eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object</em>'.
	 * @generated
	 */
	OSMObject createOSMObject();

	/**
	 * Returns a new object of class '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Position</em>'.
	 * @generated
	 */
	OSMPosition createOSMPosition();

	/**
	 * Returns a new object of class '<em>Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Point</em>'.
	 * @generated
	 */
	OSMPoint createOSMPoint();

	/**
	 * Returns a new object of class '<em>Orientation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Orientation</em>'.
	 * @generated
	 */
	OSMOrientation createOSMOrientation();

	/**
	 * Returns a new object of class '<em>Extracted Attributes</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extracted Attributes</em>'.
	 * @generated
	 */
	OSMExtractedAttributes createOSMExtractedAttributes();

	/**
	 * Returns a new object of class '<em>Zone Descriptor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Zone Descriptor</em>'.
	 * @generated
	 */
	OSMZoneDescriptor createOSMZoneDescriptor();

	/**
	 * Returns a new object of class '<em>Monitoring Target</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitoring Target</em>'.
	 * @generated
	 */
	OSMMonitoringTarget createOSMMonitoringTarget();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OSMPackage getOSMPackage();

} //OSMFactory
