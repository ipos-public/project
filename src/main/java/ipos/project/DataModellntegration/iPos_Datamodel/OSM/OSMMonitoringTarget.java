/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitoring Target</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget#getMonitoringTaskId <em>Monitoring Task Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget#getProtocol <em>Protocol</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMMonitoringTarget()
 * @model
 * @generated
 */
public interface OSMMonitoringTarget extends EObject {
	/**
	 * Returns the value of the '<em><b>Monitoring Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitoring Task Id</em>' attribute.
	 * @see #setMonitoringTaskId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMMonitoringTarget_MonitoringTaskId()
	 * @model
	 * @generated
	 */
	String getMonitoringTaskId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget#getMonitoringTaskId <em>Monitoring Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monitoring Task Id</em>' attribute.
	 * @see #getMonitoringTaskId()
	 * @generated
	 */
	void setMonitoringTaskId(String value);

	/**
	 * Returns the value of the '<em><b>Protocol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protocol</em>' attribute.
	 * @see #setProtocol(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMMonitoringTarget_Protocol()
	 * @model
	 * @generated
	 */
	String getProtocol();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget#getProtocol <em>Protocol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protocol</em>' attribute.
	 * @see #getProtocol()
	 * @generated
	 */
	void setProtocol(String value);

} // OSMMonitoringTarget
