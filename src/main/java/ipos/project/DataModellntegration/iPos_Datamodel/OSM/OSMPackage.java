/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMFactory
 * @model kind="package"
 * @generated
 */
public interface OSMPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "OSM";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "osm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "osm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OSMPackage eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl.init();

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl <em>Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMObject()
	 * @generated
	 */
	int OSM_OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__ID = 0;

	/**
	 * The feature id for the '<em><b>Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__SENSOR_ID = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__TYPE = 2;

	/**
	 * The feature id for the '<em><b>Sensor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__SENSOR_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Osmposition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__OSMPOSITION = 4;

	/**
	 * The feature id for the '<em><b>Osmorientation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__OSMORIENTATION = 5;

	/**
	 * The feature id for the '<em><b>Osmextractedattributes</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__OSMEXTRACTEDATTRIBUTES = 6;

	/**
	 * The feature id for the '<em><b>Last Pos Update</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__LAST_POS_UPDATE = 7;

	/**
	 * The feature id for the '<em><b>Osmzonedescriptor</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT__OSMZONEDESCRIPTOR = 8;

	/**
	 * The number of structural features of the '<em>Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_OBJECT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPositionImpl <em>Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPositionImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMPosition()
	 * @generated
	 */
	int OSM_POSITION = 1;

	/**
	 * The feature id for the '<em><b>Ref System Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POSITION__REF_SYSTEM_ID = 0;

	/**
	 * The feature id for the '<em><b>Accuracy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POSITION__ACCURACY = 1;

	/**
	 * The feature id for the '<em><b>Osmpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POSITION__OSMPOINT = 2;

	/**
	 * The number of structural features of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POSITION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POSITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPointImpl <em>Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPointImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMPoint()
	 * @generated
	 */
	int OSM_POINT = 2;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POINT__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POINT__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POINT__Z = 2;

	/**
	 * The number of structural features of the '<em>Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POINT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_POINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMOrientationImpl <em>Orientation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMOrientationImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMOrientation()
	 * @generated
	 */
	int OSM_ORIENTATION = 3;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ORIENTATION__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ORIENTATION__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ORIENTATION__Z = 2;

	/**
	 * The feature id for the '<em><b>W</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ORIENTATION__W = 3;

	/**
	 * The number of structural features of the '<em>Orientation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ORIENTATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Orientation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ORIENTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMExtractedAttributesImpl <em>Extracted Attributes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMExtractedAttributesImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMExtractedAttributes()
	 * @generated
	 */
	int OSM_EXTRACTED_ATTRIBUTES = 4;

	/**
	 * The feature id for the '<em><b>Battery Charge Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_EXTRACTED_ATTRIBUTES__BATTERY_CHARGE_LEVEL = 0;

	/**
	 * The feature id for the '<em><b>Loaded Items</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_EXTRACTED_ATTRIBUTES__LOADED_ITEMS = 1;

	/**
	 * The feature id for the '<em><b>Errors</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_EXTRACTED_ATTRIBUTES__ERRORS = 2;

	/**
	 * The feature id for the '<em><b>Theta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_EXTRACTED_ATTRIBUTES__THETA = 3;

	/**
	 * The number of structural features of the '<em>Extracted Attributes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_EXTRACTED_ATTRIBUTES_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Extracted Attributes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_EXTRACTED_ATTRIBUTES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMZoneDescriptorImpl <em>Zone Descriptor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMZoneDescriptorImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMZoneDescriptor()
	 * @generated
	 */
	int OSM_ZONE_DESCRIPTOR = 5;

	/**
	 * The feature id for the '<em><b>Zone Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ZONE_DESCRIPTOR__ZONE_ID = 0;

	/**
	 * The feature id for the '<em><b>Notification Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ZONE_DESCRIPTOR__NOTIFICATION_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Zone Descriptor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ZONE_DESCRIPTOR_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Zone Descriptor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_ZONE_DESCRIPTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMMonitoringTargetImpl <em>Monitoring Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMMonitoringTargetImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMMonitoringTarget()
	 * @generated
	 */
	int OSM_MONITORING_TARGET = 6;

	/**
	 * The feature id for the '<em><b>Monitoring Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_MONITORING_TARGET__MONITORING_TASK_ID = 0;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_MONITORING_TARGET__PROTOCOL = 1;

	/**
	 * The number of structural features of the '<em>Monitoring Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_MONITORING_TARGET_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Monitoring Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSM_MONITORING_TARGET_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject
	 * @generated
	 */
	EClass getOSMObject();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getId()
	 * @see #getOSMObject()
	 * @generated
	 */
	EAttribute getOSMObject_Id();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getSensorId <em>Sensor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getSensorId()
	 * @see #getOSMObject()
	 * @generated
	 */
	EAttribute getOSMObject_SensorId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getType()
	 * @see #getOSMObject()
	 * @generated
	 */
	EAttribute getOSMObject_Type();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getSensorType <em>Sensor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sensor Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getSensorType()
	 * @see #getOSMObject()
	 * @generated
	 */
	EAttribute getOSMObject_SensorType();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getOsmposition <em>Osmposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Osmposition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getOsmposition()
	 * @see #getOSMObject()
	 * @generated
	 */
	EReference getOSMObject_Osmposition();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getOsmorientation <em>Osmorientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Osmorientation</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getOsmorientation()
	 * @see #getOSMObject()
	 * @generated
	 */
	EReference getOSMObject_Osmorientation();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getOsmextractedattributes <em>Osmextractedattributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Osmextractedattributes</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getOsmextractedattributes()
	 * @see #getOSMObject()
	 * @generated
	 */
	EReference getOSMObject_Osmextractedattributes();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getLastPosUpdate <em>Last Pos Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Pos Update</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getLastPosUpdate()
	 * @see #getOSMObject()
	 * @generated
	 */
	EAttribute getOSMObject_LastPosUpdate();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getOsmzonedescriptor <em>Osmzonedescriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Osmzonedescriptor</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject#getOsmzonedescriptor()
	 * @see #getOSMObject()
	 * @generated
	 */
	EReference getOSMObject_Osmzonedescriptor();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Position</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition
	 * @generated
	 */
	EClass getOSMPosition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getRefSystemId <em>Ref System Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ref System Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getRefSystemId()
	 * @see #getOSMPosition()
	 * @generated
	 */
	EAttribute getOSMPosition_RefSystemId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getAccuracy <em>Accuracy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Accuracy</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getAccuracy()
	 * @see #getOSMPosition()
	 * @generated
	 */
	EAttribute getOSMPosition_Accuracy();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getOsmpoint <em>Osmpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Osmpoint</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getOsmpoint()
	 * @see #getOSMPosition()
	 * @generated
	 */
	EReference getOSMPosition_Osmpoint();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint
	 * @generated
	 */
	EClass getOSMPoint();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getX()
	 * @see #getOSMPoint()
	 * @generated
	 */
	EAttribute getOSMPoint_X();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getY()
	 * @see #getOSMPoint()
	 * @generated
	 */
	EAttribute getOSMPoint_Y();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getZ()
	 * @see #getOSMPoint()
	 * @generated
	 */
	EAttribute getOSMPoint_Z();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Orientation</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation
	 * @generated
	 */
	EClass getOSMOrientation();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation#getX()
	 * @see #getOSMOrientation()
	 * @generated
	 */
	EAttribute getOSMOrientation_X();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation#getY()
	 * @see #getOSMOrientation()
	 * @generated
	 */
	EAttribute getOSMOrientation_Y();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation#getZ()
	 * @see #getOSMOrientation()
	 * @generated
	 */
	EAttribute getOSMOrientation_Z();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation#getW <em>W</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>W</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation#getW()
	 * @see #getOSMOrientation()
	 * @generated
	 */
	EAttribute getOSMOrientation_W();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes <em>Extracted Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extracted Attributes</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes
	 * @generated
	 */
	EClass getOSMExtractedAttributes();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getBatteryChargeLevel <em>Battery Charge Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Battery Charge Level</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getBatteryChargeLevel()
	 * @see #getOSMExtractedAttributes()
	 * @generated
	 */
	EAttribute getOSMExtractedAttributes_BatteryChargeLevel();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getLoadedItems <em>Loaded Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Loaded Items</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getLoadedItems()
	 * @see #getOSMExtractedAttributes()
	 * @generated
	 */
	EAttribute getOSMExtractedAttributes_LoadedItems();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getErrors <em>Errors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Errors</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getErrors()
	 * @see #getOSMExtractedAttributes()
	 * @generated
	 */
	EAttribute getOSMExtractedAttributes_Errors();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getTheta <em>Theta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Theta</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes#getTheta()
	 * @see #getOSMExtractedAttributes()
	 * @generated
	 */
	EAttribute getOSMExtractedAttributes_Theta();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor <em>Zone Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Zone Descriptor</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor
	 * @generated
	 */
	EClass getOSMZoneDescriptor();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor#getZoneId <em>Zone Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Zone Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor#getZoneId()
	 * @see #getOSMZoneDescriptor()
	 * @generated
	 */
	EAttribute getOSMZoneDescriptor_ZoneId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor#getNotificationType <em>Notification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Notification Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor#getNotificationType()
	 * @see #getOSMZoneDescriptor()
	 * @generated
	 */
	EAttribute getOSMZoneDescriptor_NotificationType();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget <em>Monitoring Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitoring Target</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget
	 * @generated
	 */
	EClass getOSMMonitoringTarget();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget#getMonitoringTaskId <em>Monitoring Task Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Monitoring Task Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget#getMonitoringTaskId()
	 * @see #getOSMMonitoringTarget()
	 * @generated
	 */
	EAttribute getOSMMonitoringTarget_MonitoringTaskId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget#getProtocol <em>Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget#getProtocol()
	 * @see #getOSMMonitoringTarget()
	 * @generated
	 */
	EAttribute getOSMMonitoringTarget_Protocol();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OSMFactory getOSMFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl <em>Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMObject()
		 * @generated
		 */
		EClass OSM_OBJECT = eINSTANCE.getOSMObject();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_OBJECT__ID = eINSTANCE.getOSMObject_Id();

		/**
		 * The meta object literal for the '<em><b>Sensor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_OBJECT__SENSOR_ID = eINSTANCE.getOSMObject_SensorId();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_OBJECT__TYPE = eINSTANCE.getOSMObject_Type();

		/**
		 * The meta object literal for the '<em><b>Sensor Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_OBJECT__SENSOR_TYPE = eINSTANCE.getOSMObject_SensorType();

		/**
		 * The meta object literal for the '<em><b>Osmposition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OSM_OBJECT__OSMPOSITION = eINSTANCE.getOSMObject_Osmposition();

		/**
		 * The meta object literal for the '<em><b>Osmorientation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OSM_OBJECT__OSMORIENTATION = eINSTANCE.getOSMObject_Osmorientation();

		/**
		 * The meta object literal for the '<em><b>Osmextractedattributes</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OSM_OBJECT__OSMEXTRACTEDATTRIBUTES = eINSTANCE.getOSMObject_Osmextractedattributes();

		/**
		 * The meta object literal for the '<em><b>Last Pos Update</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_OBJECT__LAST_POS_UPDATE = eINSTANCE.getOSMObject_LastPosUpdate();

		/**
		 * The meta object literal for the '<em><b>Osmzonedescriptor</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OSM_OBJECT__OSMZONEDESCRIPTOR = eINSTANCE.getOSMObject_Osmzonedescriptor();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPositionImpl <em>Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPositionImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMPosition()
		 * @generated
		 */
		EClass OSM_POSITION = eINSTANCE.getOSMPosition();

		/**
		 * The meta object literal for the '<em><b>Ref System Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_POSITION__REF_SYSTEM_ID = eINSTANCE.getOSMPosition_RefSystemId();

		/**
		 * The meta object literal for the '<em><b>Accuracy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_POSITION__ACCURACY = eINSTANCE.getOSMPosition_Accuracy();

		/**
		 * The meta object literal for the '<em><b>Osmpoint</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OSM_POSITION__OSMPOINT = eINSTANCE.getOSMPosition_Osmpoint();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPointImpl <em>Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPointImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMPoint()
		 * @generated
		 */
		EClass OSM_POINT = eINSTANCE.getOSMPoint();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_POINT__X = eINSTANCE.getOSMPoint_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_POINT__Y = eINSTANCE.getOSMPoint_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_POINT__Z = eINSTANCE.getOSMPoint_Z();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMOrientationImpl <em>Orientation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMOrientationImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMOrientation()
		 * @generated
		 */
		EClass OSM_ORIENTATION = eINSTANCE.getOSMOrientation();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_ORIENTATION__X = eINSTANCE.getOSMOrientation_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_ORIENTATION__Y = eINSTANCE.getOSMOrientation_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_ORIENTATION__Z = eINSTANCE.getOSMOrientation_Z();

		/**
		 * The meta object literal for the '<em><b>W</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_ORIENTATION__W = eINSTANCE.getOSMOrientation_W();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMExtractedAttributesImpl <em>Extracted Attributes</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMExtractedAttributesImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMExtractedAttributes()
		 * @generated
		 */
		EClass OSM_EXTRACTED_ATTRIBUTES = eINSTANCE.getOSMExtractedAttributes();

		/**
		 * The meta object literal for the '<em><b>Battery Charge Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_EXTRACTED_ATTRIBUTES__BATTERY_CHARGE_LEVEL = eINSTANCE
				.getOSMExtractedAttributes_BatteryChargeLevel();

		/**
		 * The meta object literal for the '<em><b>Loaded Items</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_EXTRACTED_ATTRIBUTES__LOADED_ITEMS = eINSTANCE.getOSMExtractedAttributes_LoadedItems();

		/**
		 * The meta object literal for the '<em><b>Errors</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_EXTRACTED_ATTRIBUTES__ERRORS = eINSTANCE.getOSMExtractedAttributes_Errors();

		/**
		 * The meta object literal for the '<em><b>Theta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_EXTRACTED_ATTRIBUTES__THETA = eINSTANCE.getOSMExtractedAttributes_Theta();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMZoneDescriptorImpl <em>Zone Descriptor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMZoneDescriptorImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMZoneDescriptor()
		 * @generated
		 */
		EClass OSM_ZONE_DESCRIPTOR = eINSTANCE.getOSMZoneDescriptor();

		/**
		 * The meta object literal for the '<em><b>Zone Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_ZONE_DESCRIPTOR__ZONE_ID = eINSTANCE.getOSMZoneDescriptor_ZoneId();

		/**
		 * The meta object literal for the '<em><b>Notification Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_ZONE_DESCRIPTOR__NOTIFICATION_TYPE = eINSTANCE.getOSMZoneDescriptor_NotificationType();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMMonitoringTargetImpl <em>Monitoring Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMMonitoringTargetImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl#getOSMMonitoringTarget()
		 * @generated
		 */
		EClass OSM_MONITORING_TARGET = eINSTANCE.getOSMMonitoringTarget();

		/**
		 * The meta object literal for the '<em><b>Monitoring Task Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_MONITORING_TARGET__MONITORING_TASK_ID = eINSTANCE.getOSMMonitoringTarget_MonitoringTaskId();

		/**
		 * The meta object literal for the '<em><b>Protocol</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OSM_MONITORING_TARGET__PROTOCOL = eINSTANCE.getOSMMonitoringTarget_Protocol();

	}

} //OSMPackage
