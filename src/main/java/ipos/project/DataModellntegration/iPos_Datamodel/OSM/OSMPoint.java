/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getX <em>X</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getY <em>Y</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getZ <em>Z</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMPoint()
 * @model
 * @generated
 */
public interface OSMPoint extends EObject {
	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMPoint_X()
	 * @model
	 * @generated
	 */
	float getX();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(float value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMPoint_Y()
	 * @model
	 * @generated
	 */
	float getY();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(float value);

	/**
	 * Returns the value of the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Z</em>' attribute.
	 * @see #setZ(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMPoint_Z()
	 * @model
	 * @generated
	 */
	float getZ();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint#getZ <em>Z</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Z</em>' attribute.
	 * @see #getZ()
	 * @generated
	 */
	void setZ(float value);

} // OSMPoint
