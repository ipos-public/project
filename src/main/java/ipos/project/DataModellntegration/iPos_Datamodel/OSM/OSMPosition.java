/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Position</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getRefSystemId <em>Ref System Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getAccuracy <em>Accuracy</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getOsmpoint <em>Osmpoint</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMPosition()
 * @model
 * @generated
 */
public interface OSMPosition extends EObject {
	/**
	 * Returns the value of the '<em><b>Ref System Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref System Id</em>' attribute.
	 * @see #setRefSystemId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMPosition_RefSystemId()
	 * @model
	 * @generated
	 */
	String getRefSystemId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getRefSystemId <em>Ref System Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref System Id</em>' attribute.
	 * @see #getRefSystemId()
	 * @generated
	 */
	void setRefSystemId(String value);

	/**
	 * Returns the value of the '<em><b>Accuracy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accuracy</em>' attribute.
	 * @see #setAccuracy(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMPosition_Accuracy()
	 * @model
	 * @generated
	 */
	float getAccuracy();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getAccuracy <em>Accuracy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accuracy</em>' attribute.
	 * @see #getAccuracy()
	 * @generated
	 */
	void setAccuracy(float value);

	/**
	 * Returns the value of the '<em><b>Osmpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Osmpoint</em>' reference.
	 * @see #setOsmpoint(OSMPoint)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#getOSMPosition_Osmpoint()
	 * @model
	 * @generated
	 */
	OSMPoint getOsmpoint();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition#getOsmpoint <em>Osmpoint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Osmpoint</em>' reference.
	 * @see #getOsmpoint()
	 * @generated
	 */
	void setOsmpoint(OSMPoint value);

} // OSMPosition
