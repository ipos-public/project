/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extracted Attributes</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMExtractedAttributesImpl#getBatteryChargeLevel <em>Battery Charge Level</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMExtractedAttributesImpl#getLoadedItems <em>Loaded Items</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMExtractedAttributesImpl#getErrors <em>Errors</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMExtractedAttributesImpl#getTheta <em>Theta</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OSMExtractedAttributesImpl extends MinimalEObjectImpl.Container implements OSMExtractedAttributes {
	/**
	 * The default value of the '{@link #getBatteryChargeLevel() <em>Battery Charge Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryChargeLevel()
	 * @generated
	 * @ordered
	 */
	protected static final float BATTERY_CHARGE_LEVEL_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getBatteryChargeLevel() <em>Battery Charge Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryChargeLevel()
	 * @generated
	 * @ordered
	 */
	protected float batteryChargeLevel = BATTERY_CHARGE_LEVEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadedItems() <em>Loaded Items</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadedItems()
	 * @generated
	 * @ordered
	 */
	protected static final int[] LOADED_ITEMS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadedItems() <em>Loaded Items</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadedItems()
	 * @generated
	 * @ordered
	 */
	protected int[] loadedItems = LOADED_ITEMS_EDEFAULT;

	/**
	 * The default value of the '{@link #getErrors() <em>Errors</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrors()
	 * @generated
	 * @ordered
	 */
	protected static final int[] ERRORS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getErrors() <em>Errors</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrors()
	 * @generated
	 * @ordered
	 */
	protected int[] errors = ERRORS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTheta() <em>Theta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTheta()
	 * @generated
	 * @ordered
	 */
	protected static final float THETA_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getTheta() <em>Theta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTheta()
	 * @generated
	 * @ordered
	 */
	protected float theta = THETA_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OSMExtractedAttributesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSMPackage.Literals.OSM_EXTRACTED_ATTRIBUTES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getBatteryChargeLevel() {
		return batteryChargeLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBatteryChargeLevel(float newBatteryChargeLevel) {
		float oldBatteryChargeLevel = batteryChargeLevel;
		batteryChargeLevel = newBatteryChargeLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					OSMPackage.OSM_EXTRACTED_ATTRIBUTES__BATTERY_CHARGE_LEVEL, oldBatteryChargeLevel,
					batteryChargeLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int[] getLoadedItems() {
		return loadedItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoadedItems(int[] newLoadedItems) {
		int[] oldLoadedItems = loadedItems;
		loadedItems = newLoadedItems;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_EXTRACTED_ATTRIBUTES__LOADED_ITEMS,
					oldLoadedItems, loadedItems));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int[] getErrors() {
		return errors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setErrors(int[] newErrors) {
		int[] oldErrors = errors;
		errors = newErrors;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_EXTRACTED_ATTRIBUTES__ERRORS,
					oldErrors, errors));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getTheta() {
		return theta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTheta(float newTheta) {
		float oldTheta = theta;
		theta = newTheta;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_EXTRACTED_ATTRIBUTES__THETA, oldTheta,
					theta));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__BATTERY_CHARGE_LEVEL:
			return getBatteryChargeLevel();
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__LOADED_ITEMS:
			return getLoadedItems();
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__ERRORS:
			return getErrors();
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__THETA:
			return getTheta();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__BATTERY_CHARGE_LEVEL:
			setBatteryChargeLevel((Float) newValue);
			return;
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__LOADED_ITEMS:
			setLoadedItems((int[]) newValue);
			return;
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__ERRORS:
			setErrors((int[]) newValue);
			return;
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__THETA:
			setTheta((Float) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__BATTERY_CHARGE_LEVEL:
			setBatteryChargeLevel(BATTERY_CHARGE_LEVEL_EDEFAULT);
			return;
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__LOADED_ITEMS:
			setLoadedItems(LOADED_ITEMS_EDEFAULT);
			return;
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__ERRORS:
			setErrors(ERRORS_EDEFAULT);
			return;
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__THETA:
			setTheta(THETA_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__BATTERY_CHARGE_LEVEL:
			return batteryChargeLevel != BATTERY_CHARGE_LEVEL_EDEFAULT;
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__LOADED_ITEMS:
			return LOADED_ITEMS_EDEFAULT == null ? loadedItems != null : !LOADED_ITEMS_EDEFAULT.equals(loadedItems);
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__ERRORS:
			return ERRORS_EDEFAULT == null ? errors != null : !ERRORS_EDEFAULT.equals(errors);
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES__THETA:
			return theta != THETA_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (batteryChargeLevel: ");
		result.append(batteryChargeLevel);
		result.append(", loadedItems: ");
		result.append(loadedItems);
		result.append(", errors: ");
		result.append(errors);
		result.append(", theta: ");
		result.append(theta);
		result.append(')');
		return result.toString();
	}

} //OSMExtractedAttributesImpl
