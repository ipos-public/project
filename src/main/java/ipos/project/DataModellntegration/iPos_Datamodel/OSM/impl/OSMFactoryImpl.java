/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OSMFactoryImpl extends EFactoryImpl implements OSMFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OSMFactory init() {
		try {
			OSMFactory theOSMFactory = (OSMFactory) EPackage.Registry.INSTANCE.getEFactory(OSMPackage.eNS_URI);
			if (theOSMFactory != null) {
				return theOSMFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OSMFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case OSMPackage.OSM_OBJECT:
			return createOSMObject();
		case OSMPackage.OSM_POSITION:
			return createOSMPosition();
		case OSMPackage.OSM_POINT:
			return createOSMPoint();
		case OSMPackage.OSM_ORIENTATION:
			return createOSMOrientation();
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES:
			return createOSMExtractedAttributes();
		case OSMPackage.OSM_ZONE_DESCRIPTOR:
			return createOSMZoneDescriptor();
		case OSMPackage.OSM_MONITORING_TARGET:
			return createOSMMonitoringTarget();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMObject createOSMObject() {
		OSMObjectImpl osmObject = new OSMObjectImpl();
		return osmObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMPosition createOSMPosition() {
		OSMPositionImpl osmPosition = new OSMPositionImpl();
		return osmPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMPoint createOSMPoint() {
		OSMPointImpl osmPoint = new OSMPointImpl();
		return osmPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMOrientation createOSMOrientation() {
		OSMOrientationImpl osmOrientation = new OSMOrientationImpl();
		return osmOrientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMExtractedAttributes createOSMExtractedAttributes() {
		OSMExtractedAttributesImpl osmExtractedAttributes = new OSMExtractedAttributesImpl();
		return osmExtractedAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMZoneDescriptor createOSMZoneDescriptor() {
		OSMZoneDescriptorImpl osmZoneDescriptor = new OSMZoneDescriptorImpl();
		return osmZoneDescriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMMonitoringTarget createOSMMonitoringTarget() {
		OSMMonitoringTargetImpl osmMonitoringTarget = new OSMMonitoringTargetImpl();
		return osmMonitoringTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMPackage getOSMPackage() {
		return (OSMPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OSMPackage getPackage() {
		return OSMPackage.eINSTANCE;
	}

} //OSMFactoryImpl
