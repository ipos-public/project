/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitoring Target</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMMonitoringTargetImpl#getMonitoringTaskId <em>Monitoring Task Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMMonitoringTargetImpl#getProtocol <em>Protocol</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OSMMonitoringTargetImpl extends MinimalEObjectImpl.Container implements OSMMonitoringTarget {
	/**
	 * The default value of the '{@link #getMonitoringTaskId() <em>Monitoring Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringTaskId()
	 * @generated
	 * @ordered
	 */
	protected static final String MONITORING_TASK_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMonitoringTaskId() <em>Monitoring Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringTaskId()
	 * @generated
	 * @ordered
	 */
	protected String monitoringTaskId = MONITORING_TASK_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getProtocol() <em>Protocol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocol()
	 * @generated
	 * @ordered
	 */
	protected static final String PROTOCOL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProtocol() <em>Protocol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocol()
	 * @generated
	 * @ordered
	 */
	protected String protocol = PROTOCOL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OSMMonitoringTargetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSMPackage.Literals.OSM_MONITORING_TARGET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMonitoringTaskId() {
		return monitoringTaskId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitoringTaskId(String newMonitoringTaskId) {
		String oldMonitoringTaskId = monitoringTaskId;
		monitoringTaskId = newMonitoringTaskId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_MONITORING_TARGET__MONITORING_TASK_ID,
					oldMonitoringTaskId, monitoringTaskId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtocol(String newProtocol) {
		String oldProtocol = protocol;
		protocol = newProtocol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_MONITORING_TARGET__PROTOCOL,
					oldProtocol, protocol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OSMPackage.OSM_MONITORING_TARGET__MONITORING_TASK_ID:
			return getMonitoringTaskId();
		case OSMPackage.OSM_MONITORING_TARGET__PROTOCOL:
			return getProtocol();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OSMPackage.OSM_MONITORING_TARGET__MONITORING_TASK_ID:
			setMonitoringTaskId((String) newValue);
			return;
		case OSMPackage.OSM_MONITORING_TARGET__PROTOCOL:
			setProtocol((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_MONITORING_TARGET__MONITORING_TASK_ID:
			setMonitoringTaskId(MONITORING_TASK_ID_EDEFAULT);
			return;
		case OSMPackage.OSM_MONITORING_TARGET__PROTOCOL:
			setProtocol(PROTOCOL_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_MONITORING_TARGET__MONITORING_TASK_ID:
			return MONITORING_TASK_ID_EDEFAULT == null ? monitoringTaskId != null
					: !MONITORING_TASK_ID_EDEFAULT.equals(monitoringTaskId);
		case OSMPackage.OSM_MONITORING_TARGET__PROTOCOL:
			return PROTOCOL_EDEFAULT == null ? protocol != null : !PROTOCOL_EDEFAULT.equals(protocol);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (monitoringTaskId: ");
		result.append(monitoringTaskId);
		result.append(", protocol: ");
		result.append(protocol);
		result.append(')');
		return result.toString();
	}

} //OSMMonitoringTargetImpl
