/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getId <em>Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getSensorId <em>Sensor Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getType <em>Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getSensorType <em>Sensor Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getOsmposition <em>Osmposition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getOsmorientation <em>Osmorientation</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getOsmextractedattributes <em>Osmextractedattributes</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getLastPosUpdate <em>Last Pos Update</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMObjectImpl#getOsmzonedescriptor <em>Osmzonedescriptor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OSMObjectImpl extends MinimalEObjectImpl.Container implements OSMObject {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSensorId() <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorId()
	 * @generated
	 * @ordered
	 */
	protected static final String SENSOR_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorId() <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorId()
	 * @generated
	 * @ordered
	 */
	protected String sensorId = SENSOR_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSensorType() <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorType()
	 * @generated
	 * @ordered
	 */
	protected static final String SENSOR_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorType() <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorType()
	 * @generated
	 * @ordered
	 */
	protected String sensorType = SENSOR_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOsmposition() <em>Osmposition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOsmposition()
	 * @generated
	 * @ordered
	 */
	protected OSMPosition osmposition;

	/**
	 * The cached value of the '{@link #getOsmorientation() <em>Osmorientation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOsmorientation()
	 * @generated
	 * @ordered
	 */
	protected OSMOrientation osmorientation;

	/**
	 * The cached value of the '{@link #getOsmextractedattributes() <em>Osmextractedattributes</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOsmextractedattributes()
	 * @generated
	 * @ordered
	 */
	protected OSMExtractedAttributes osmextractedattributes;

	/**
	 * The default value of the '{@link #getLastPosUpdate() <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPosUpdate()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_POS_UPDATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastPosUpdate() <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPosUpdate()
	 * @generated
	 * @ordered
	 */
	protected String lastPosUpdate = LAST_POS_UPDATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOsmzonedescriptor() <em>Osmzonedescriptor</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOsmzonedescriptor()
	 * @generated
	 * @ordered
	 */
	protected EList<OSMZoneDescriptor> osmzonedescriptor;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OSMObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSMPackage.Literals.OSM_OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_OBJECT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSensorId() {
		return sensorId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorId(String newSensorId) {
		String oldSensorId = sensorId;
		sensorId = newSensorId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_OBJECT__SENSOR_ID, oldSensorId,
					sensorId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_OBJECT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSensorType() {
		return sensorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorType(String newSensorType) {
		String oldSensorType = sensorType;
		sensorType = newSensorType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_OBJECT__SENSOR_TYPE, oldSensorType,
					sensorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMPosition getOsmposition() {
		if (osmposition != null && osmposition.eIsProxy()) {
			InternalEObject oldOsmposition = (InternalEObject) osmposition;
			osmposition = (OSMPosition) eResolveProxy(oldOsmposition);
			if (osmposition != oldOsmposition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSMPackage.OSM_OBJECT__OSMPOSITION,
							oldOsmposition, osmposition));
			}
		}
		return osmposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMPosition basicGetOsmposition() {
		return osmposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOsmposition(OSMPosition newOsmposition) {
		OSMPosition oldOsmposition = osmposition;
		osmposition = newOsmposition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_OBJECT__OSMPOSITION, oldOsmposition,
					osmposition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMOrientation getOsmorientation() {
		if (osmorientation != null && osmorientation.eIsProxy()) {
			InternalEObject oldOsmorientation = (InternalEObject) osmorientation;
			osmorientation = (OSMOrientation) eResolveProxy(oldOsmorientation);
			if (osmorientation != oldOsmorientation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSMPackage.OSM_OBJECT__OSMORIENTATION,
							oldOsmorientation, osmorientation));
			}
		}
		return osmorientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMOrientation basicGetOsmorientation() {
		return osmorientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOsmorientation(OSMOrientation newOsmorientation) {
		OSMOrientation oldOsmorientation = osmorientation;
		osmorientation = newOsmorientation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_OBJECT__OSMORIENTATION,
					oldOsmorientation, osmorientation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMExtractedAttributes getOsmextractedattributes() {
		if (osmextractedattributes != null && osmextractedattributes.eIsProxy()) {
			InternalEObject oldOsmextractedattributes = (InternalEObject) osmextractedattributes;
			osmextractedattributes = (OSMExtractedAttributes) eResolveProxy(oldOsmextractedattributes);
			if (osmextractedattributes != oldOsmextractedattributes) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							OSMPackage.OSM_OBJECT__OSMEXTRACTEDATTRIBUTES, oldOsmextractedattributes,
							osmextractedattributes));
			}
		}
		return osmextractedattributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMExtractedAttributes basicGetOsmextractedattributes() {
		return osmextractedattributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOsmextractedattributes(OSMExtractedAttributes newOsmextractedattributes) {
		OSMExtractedAttributes oldOsmextractedattributes = osmextractedattributes;
		osmextractedattributes = newOsmextractedattributes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_OBJECT__OSMEXTRACTEDATTRIBUTES,
					oldOsmextractedattributes, osmextractedattributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastPosUpdate() {
		return lastPosUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastPosUpdate(String newLastPosUpdate) {
		String oldLastPosUpdate = lastPosUpdate;
		lastPosUpdate = newLastPosUpdate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_OBJECT__LAST_POS_UPDATE,
					oldLastPosUpdate, lastPosUpdate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OSMZoneDescriptor> getOsmzonedescriptor() {
		if (osmzonedescriptor == null) {
			osmzonedescriptor = new EObjectResolvingEList<OSMZoneDescriptor>(OSMZoneDescriptor.class, this,
					OSMPackage.OSM_OBJECT__OSMZONEDESCRIPTOR);
		}
		return osmzonedescriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OSMPackage.OSM_OBJECT__ID:
			return getId();
		case OSMPackage.OSM_OBJECT__SENSOR_ID:
			return getSensorId();
		case OSMPackage.OSM_OBJECT__TYPE:
			return getType();
		case OSMPackage.OSM_OBJECT__SENSOR_TYPE:
			return getSensorType();
		case OSMPackage.OSM_OBJECT__OSMPOSITION:
			if (resolve)
				return getOsmposition();
			return basicGetOsmposition();
		case OSMPackage.OSM_OBJECT__OSMORIENTATION:
			if (resolve)
				return getOsmorientation();
			return basicGetOsmorientation();
		case OSMPackage.OSM_OBJECT__OSMEXTRACTEDATTRIBUTES:
			if (resolve)
				return getOsmextractedattributes();
			return basicGetOsmextractedattributes();
		case OSMPackage.OSM_OBJECT__LAST_POS_UPDATE:
			return getLastPosUpdate();
		case OSMPackage.OSM_OBJECT__OSMZONEDESCRIPTOR:
			return getOsmzonedescriptor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OSMPackage.OSM_OBJECT__ID:
			setId((String) newValue);
			return;
		case OSMPackage.OSM_OBJECT__SENSOR_ID:
			setSensorId((String) newValue);
			return;
		case OSMPackage.OSM_OBJECT__TYPE:
			setType((String) newValue);
			return;
		case OSMPackage.OSM_OBJECT__SENSOR_TYPE:
			setSensorType((String) newValue);
			return;
		case OSMPackage.OSM_OBJECT__OSMPOSITION:
			setOsmposition((OSMPosition) newValue);
			return;
		case OSMPackage.OSM_OBJECT__OSMORIENTATION:
			setOsmorientation((OSMOrientation) newValue);
			return;
		case OSMPackage.OSM_OBJECT__OSMEXTRACTEDATTRIBUTES:
			setOsmextractedattributes((OSMExtractedAttributes) newValue);
			return;
		case OSMPackage.OSM_OBJECT__LAST_POS_UPDATE:
			setLastPosUpdate((String) newValue);
			return;
		case OSMPackage.OSM_OBJECT__OSMZONEDESCRIPTOR:
			getOsmzonedescriptor().clear();
			getOsmzonedescriptor().addAll((Collection<? extends OSMZoneDescriptor>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_OBJECT__ID:
			setId(ID_EDEFAULT);
			return;
		case OSMPackage.OSM_OBJECT__SENSOR_ID:
			setSensorId(SENSOR_ID_EDEFAULT);
			return;
		case OSMPackage.OSM_OBJECT__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		case OSMPackage.OSM_OBJECT__SENSOR_TYPE:
			setSensorType(SENSOR_TYPE_EDEFAULT);
			return;
		case OSMPackage.OSM_OBJECT__OSMPOSITION:
			setOsmposition((OSMPosition) null);
			return;
		case OSMPackage.OSM_OBJECT__OSMORIENTATION:
			setOsmorientation((OSMOrientation) null);
			return;
		case OSMPackage.OSM_OBJECT__OSMEXTRACTEDATTRIBUTES:
			setOsmextractedattributes((OSMExtractedAttributes) null);
			return;
		case OSMPackage.OSM_OBJECT__LAST_POS_UPDATE:
			setLastPosUpdate(LAST_POS_UPDATE_EDEFAULT);
			return;
		case OSMPackage.OSM_OBJECT__OSMZONEDESCRIPTOR:
			getOsmzonedescriptor().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_OBJECT__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case OSMPackage.OSM_OBJECT__SENSOR_ID:
			return SENSOR_ID_EDEFAULT == null ? sensorId != null : !SENSOR_ID_EDEFAULT.equals(sensorId);
		case OSMPackage.OSM_OBJECT__TYPE:
			return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		case OSMPackage.OSM_OBJECT__SENSOR_TYPE:
			return SENSOR_TYPE_EDEFAULT == null ? sensorType != null : !SENSOR_TYPE_EDEFAULT.equals(sensorType);
		case OSMPackage.OSM_OBJECT__OSMPOSITION:
			return osmposition != null;
		case OSMPackage.OSM_OBJECT__OSMORIENTATION:
			return osmorientation != null;
		case OSMPackage.OSM_OBJECT__OSMEXTRACTEDATTRIBUTES:
			return osmextractedattributes != null;
		case OSMPackage.OSM_OBJECT__LAST_POS_UPDATE:
			return LAST_POS_UPDATE_EDEFAULT == null ? lastPosUpdate != null
					: !LAST_POS_UPDATE_EDEFAULT.equals(lastPosUpdate);
		case OSMPackage.OSM_OBJECT__OSMZONEDESCRIPTOR:
			return osmzonedescriptor != null && !osmzonedescriptor.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", sensorId: ");
		result.append(sensorId);
		result.append(", type: ");
		result.append(type);
		result.append(", sensorType: ");
		result.append(sensorType);
		result.append(", lastPosUpdate: ");
		result.append(lastPosUpdate);
		result.append(')');
		return result.toString();
	}

} //OSMObjectImpl
