/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OSMPackageImpl extends EPackageImpl implements OSMPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass osmObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass osmPositionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass osmPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass osmOrientationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass osmExtractedAttributesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass osmZoneDescriptorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass osmMonitoringTargetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OSMPackageImpl() {
		super(eNS_URI, OSMFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OSMPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OSMPackage init() {
		if (isInited)
			return (OSMPackage) EPackage.Registry.INSTANCE.getEPackage(OSMPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOSMPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OSMPackageImpl theOSMPackage = registeredOSMPackage instanceof OSMPackageImpl
				? (OSMPackageImpl) registeredOSMPackage
				: new OSMPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPos_DatamodelPackage.eNS_URI);
		IPos_DatamodelPackageImpl theIPos_DatamodelPackage = (IPos_DatamodelPackageImpl) (registeredPackage instanceof IPos_DatamodelPackageImpl
				? registeredPackage
				: IPos_DatamodelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OFBizPackage.eNS_URI);
		OFBizPackageImpl theOFBizPackage = (OFBizPackageImpl) (registeredPackage instanceof OFBizPackageImpl
				? registeredPackage
				: OFBizPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPosDevKitPackage.eNS_URI);
		IPosDevKitPackageImpl theIPosDevKitPackage = (IPosDevKitPackageImpl) (registeredPackage instanceof IPosDevKitPackageImpl
				? registeredPackage
				: IPosDevKitPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ToozPackage.eNS_URI);
		ToozPackageImpl theToozPackage = (ToozPackageImpl) (registeredPackage instanceof ToozPackageImpl
				? registeredPackage
				: ToozPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VDA5050Package.eNS_URI);
		VDA5050PackageImpl theVDA5050Package = (VDA5050PackageImpl) (registeredPackage instanceof VDA5050PackageImpl
				? registeredPackage
				: VDA5050Package.eINSTANCE);

		// Create package meta-data objects
		theOSMPackage.createPackageContents();
		theIPos_DatamodelPackage.createPackageContents();
		theOFBizPackage.createPackageContents();
		theIPosDevKitPackage.createPackageContents();
		theToozPackage.createPackageContents();
		theVDA5050Package.createPackageContents();

		// Initialize created meta-data
		theOSMPackage.initializePackageContents();
		theIPos_DatamodelPackage.initializePackageContents();
		theOFBizPackage.initializePackageContents();
		theIPosDevKitPackage.initializePackageContents();
		theToozPackage.initializePackageContents();
		theVDA5050Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOSMPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OSMPackage.eNS_URI, theOSMPackage);
		return theOSMPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOSMObject() {
		return osmObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMObject_Id() {
		return (EAttribute) osmObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMObject_SensorId() {
		return (EAttribute) osmObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMObject_Type() {
		return (EAttribute) osmObjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMObject_SensorType() {
		return (EAttribute) osmObjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOSMObject_Osmposition() {
		return (EReference) osmObjectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOSMObject_Osmorientation() {
		return (EReference) osmObjectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOSMObject_Osmextractedattributes() {
		return (EReference) osmObjectEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMObject_LastPosUpdate() {
		return (EAttribute) osmObjectEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOSMObject_Osmzonedescriptor() {
		return (EReference) osmObjectEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOSMPosition() {
		return osmPositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMPosition_RefSystemId() {
		return (EAttribute) osmPositionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMPosition_Accuracy() {
		return (EAttribute) osmPositionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOSMPosition_Osmpoint() {
		return (EReference) osmPositionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOSMPoint() {
		return osmPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMPoint_X() {
		return (EAttribute) osmPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMPoint_Y() {
		return (EAttribute) osmPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMPoint_Z() {
		return (EAttribute) osmPointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOSMOrientation() {
		return osmOrientationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMOrientation_X() {
		return (EAttribute) osmOrientationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMOrientation_Y() {
		return (EAttribute) osmOrientationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMOrientation_Z() {
		return (EAttribute) osmOrientationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMOrientation_W() {
		return (EAttribute) osmOrientationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOSMExtractedAttributes() {
		return osmExtractedAttributesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMExtractedAttributes_BatteryChargeLevel() {
		return (EAttribute) osmExtractedAttributesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMExtractedAttributes_LoadedItems() {
		return (EAttribute) osmExtractedAttributesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMExtractedAttributes_Errors() {
		return (EAttribute) osmExtractedAttributesEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMExtractedAttributes_Theta() {
		return (EAttribute) osmExtractedAttributesEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOSMZoneDescriptor() {
		return osmZoneDescriptorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMZoneDescriptor_ZoneId() {
		return (EAttribute) osmZoneDescriptorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMZoneDescriptor_NotificationType() {
		return (EAttribute) osmZoneDescriptorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOSMMonitoringTarget() {
		return osmMonitoringTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMMonitoringTarget_MonitoringTaskId() {
		return (EAttribute) osmMonitoringTargetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOSMMonitoringTarget_Protocol() {
		return (EAttribute) osmMonitoringTargetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMFactory getOSMFactory() {
		return (OSMFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		osmObjectEClass = createEClass(OSM_OBJECT);
		createEAttribute(osmObjectEClass, OSM_OBJECT__ID);
		createEAttribute(osmObjectEClass, OSM_OBJECT__SENSOR_ID);
		createEAttribute(osmObjectEClass, OSM_OBJECT__TYPE);
		createEAttribute(osmObjectEClass, OSM_OBJECT__SENSOR_TYPE);
		createEReference(osmObjectEClass, OSM_OBJECT__OSMPOSITION);
		createEReference(osmObjectEClass, OSM_OBJECT__OSMORIENTATION);
		createEReference(osmObjectEClass, OSM_OBJECT__OSMEXTRACTEDATTRIBUTES);
		createEAttribute(osmObjectEClass, OSM_OBJECT__LAST_POS_UPDATE);
		createEReference(osmObjectEClass, OSM_OBJECT__OSMZONEDESCRIPTOR);

		osmPositionEClass = createEClass(OSM_POSITION);
		createEAttribute(osmPositionEClass, OSM_POSITION__REF_SYSTEM_ID);
		createEAttribute(osmPositionEClass, OSM_POSITION__ACCURACY);
		createEReference(osmPositionEClass, OSM_POSITION__OSMPOINT);

		osmPointEClass = createEClass(OSM_POINT);
		createEAttribute(osmPointEClass, OSM_POINT__X);
		createEAttribute(osmPointEClass, OSM_POINT__Y);
		createEAttribute(osmPointEClass, OSM_POINT__Z);

		osmOrientationEClass = createEClass(OSM_ORIENTATION);
		createEAttribute(osmOrientationEClass, OSM_ORIENTATION__X);
		createEAttribute(osmOrientationEClass, OSM_ORIENTATION__Y);
		createEAttribute(osmOrientationEClass, OSM_ORIENTATION__Z);
		createEAttribute(osmOrientationEClass, OSM_ORIENTATION__W);

		osmExtractedAttributesEClass = createEClass(OSM_EXTRACTED_ATTRIBUTES);
		createEAttribute(osmExtractedAttributesEClass, OSM_EXTRACTED_ATTRIBUTES__BATTERY_CHARGE_LEVEL);
		createEAttribute(osmExtractedAttributesEClass, OSM_EXTRACTED_ATTRIBUTES__LOADED_ITEMS);
		createEAttribute(osmExtractedAttributesEClass, OSM_EXTRACTED_ATTRIBUTES__ERRORS);
		createEAttribute(osmExtractedAttributesEClass, OSM_EXTRACTED_ATTRIBUTES__THETA);

		osmZoneDescriptorEClass = createEClass(OSM_ZONE_DESCRIPTOR);
		createEAttribute(osmZoneDescriptorEClass, OSM_ZONE_DESCRIPTOR__ZONE_ID);
		createEAttribute(osmZoneDescriptorEClass, OSM_ZONE_DESCRIPTOR__NOTIFICATION_TYPE);

		osmMonitoringTargetEClass = createEClass(OSM_MONITORING_TARGET);
		createEAttribute(osmMonitoringTargetEClass, OSM_MONITORING_TARGET__MONITORING_TASK_ID);
		createEAttribute(osmMonitoringTargetEClass, OSM_MONITORING_TARGET__PROTOCOL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		IPos_DatamodelPackage theIPos_DatamodelPackage = (IPos_DatamodelPackage) EPackage.Registry.INSTANCE
				.getEPackage(IPos_DatamodelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(osmObjectEClass, OSMObject.class, "OSMObject", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOSMObject_Id(), ecorePackage.getEString(), "id", null, 0, 1, OSMObject.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMObject_SensorId(), ecorePackage.getEString(), "sensorId", null, 0, 1, OSMObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMObject_Type(), ecorePackage.getEString(), "type", null, 0, 1, OSMObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMObject_SensorType(), ecorePackage.getEString(), "sensorType", null, 0, 1, OSMObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOSMObject_Osmposition(), this.getOSMPosition(), null, "osmposition", null, 0, 1,
				OSMObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOSMObject_Osmorientation(), this.getOSMOrientation(), null, "osmorientation", null, 0, 1,
				OSMObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOSMObject_Osmextractedattributes(), this.getOSMExtractedAttributes(), null,
				"osmextractedattributes", null, 0, 1, OSMObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMObject_LastPosUpdate(), ecorePackage.getEString(), "lastPosUpdate", null, 0, 1,
				OSMObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getOSMObject_Osmzonedescriptor(), this.getOSMZoneDescriptor(), null, "osmzonedescriptor", null,
				0, -1, OSMObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(osmPositionEClass, OSMPosition.class, "OSMPosition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOSMPosition_RefSystemId(), ecorePackage.getEString(), "refSystemId", null, 0, 1,
				OSMPosition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMPosition_Accuracy(), ecorePackage.getEFloat(), "accuracy", null, 0, 1, OSMPosition.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOSMPosition_Osmpoint(), this.getOSMPoint(), null, "osmpoint", null, 0, 1, OSMPosition.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(osmPointEClass, OSMPoint.class, "OSMPoint", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOSMPoint_X(), ecorePackage.getEFloat(), "x", null, 0, 1, OSMPoint.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMPoint_Y(), ecorePackage.getEFloat(), "y", null, 0, 1, OSMPoint.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMPoint_Z(), ecorePackage.getEFloat(), "z", null, 0, 1, OSMPoint.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(osmOrientationEClass, OSMOrientation.class, "OSMOrientation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOSMOrientation_X(), ecorePackage.getEFloat(), "x", null, 0, 1, OSMOrientation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMOrientation_Y(), ecorePackage.getEFloat(), "y", null, 0, 1, OSMOrientation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMOrientation_Z(), ecorePackage.getEFloat(), "z", null, 0, 1, OSMOrientation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMOrientation_W(), ecorePackage.getEFloat(), "w", null, 0, 1, OSMOrientation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(osmExtractedAttributesEClass, OSMExtractedAttributes.class, "OSMExtractedAttributes", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOSMExtractedAttributes_BatteryChargeLevel(), ecorePackage.getEFloat(), "batteryChargeLevel",
				null, 0, 1, OSMExtractedAttributes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMExtractedAttributes_LoadedItems(), theIPos_DatamodelPackage.getIntArray(), "loadedItems",
				null, 0, 1, OSMExtractedAttributes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMExtractedAttributes_Errors(), theIPos_DatamodelPackage.getIntArray(), "errors", null, 0, 1,
				OSMExtractedAttributes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMExtractedAttributes_Theta(), ecorePackage.getEFloat(), "theta", null, 0, 1,
				OSMExtractedAttributes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(osmZoneDescriptorEClass, OSMZoneDescriptor.class, "OSMZoneDescriptor", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOSMZoneDescriptor_ZoneId(), ecorePackage.getEString(), "zoneId", null, 0, 1,
				OSMZoneDescriptor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMZoneDescriptor_NotificationType(), ecorePackage.getEString(), "notificationType", null, 0,
				1, OSMZoneDescriptor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(osmMonitoringTargetEClass, OSMMonitoringTarget.class, "OSMMonitoringTarget", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOSMMonitoringTarget_MonitoringTaskId(), ecorePackage.getEString(), "monitoringTaskId", null,
				0, 1, OSMMonitoringTarget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSMMonitoringTarget_Protocol(), ecorePackage.getEString(), "protocol", null, 0, 1,
				OSMMonitoringTarget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //OSMPackageImpl
