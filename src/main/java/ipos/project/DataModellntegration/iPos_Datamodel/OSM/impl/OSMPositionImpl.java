/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Position</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPositionImpl#getRefSystemId <em>Ref System Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPositionImpl#getAccuracy <em>Accuracy</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPositionImpl#getOsmpoint <em>Osmpoint</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OSMPositionImpl extends MinimalEObjectImpl.Container implements OSMPosition {
	/**
	 * The default value of the '{@link #getRefSystemId() <em>Ref System Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefSystemId()
	 * @generated
	 * @ordered
	 */
	protected static final String REF_SYSTEM_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRefSystemId() <em>Ref System Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefSystemId()
	 * @generated
	 * @ordered
	 */
	protected String refSystemId = REF_SYSTEM_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getAccuracy() <em>Accuracy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracy()
	 * @generated
	 * @ordered
	 */
	protected static final float ACCURACY_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getAccuracy() <em>Accuracy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracy()
	 * @generated
	 * @ordered
	 */
	protected float accuracy = ACCURACY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOsmpoint() <em>Osmpoint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOsmpoint()
	 * @generated
	 * @ordered
	 */
	protected OSMPoint osmpoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OSMPositionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSMPackage.Literals.OSM_POSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRefSystemId() {
		return refSystemId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefSystemId(String newRefSystemId) {
		String oldRefSystemId = refSystemId;
		refSystemId = newRefSystemId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_POSITION__REF_SYSTEM_ID,
					oldRefSystemId, refSystemId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getAccuracy() {
		return accuracy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccuracy(float newAccuracy) {
		float oldAccuracy = accuracy;
		accuracy = newAccuracy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_POSITION__ACCURACY, oldAccuracy,
					accuracy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMPoint getOsmpoint() {
		if (osmpoint != null && osmpoint.eIsProxy()) {
			InternalEObject oldOsmpoint = (InternalEObject) osmpoint;
			osmpoint = (OSMPoint) eResolveProxy(oldOsmpoint);
			if (osmpoint != oldOsmpoint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSMPackage.OSM_POSITION__OSMPOINT,
							oldOsmpoint, osmpoint));
			}
		}
		return osmpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMPoint basicGetOsmpoint() {
		return osmpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOsmpoint(OSMPoint newOsmpoint) {
		OSMPoint oldOsmpoint = osmpoint;
		osmpoint = newOsmpoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_POSITION__OSMPOINT, oldOsmpoint,
					osmpoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OSMPackage.OSM_POSITION__REF_SYSTEM_ID:
			return getRefSystemId();
		case OSMPackage.OSM_POSITION__ACCURACY:
			return getAccuracy();
		case OSMPackage.OSM_POSITION__OSMPOINT:
			if (resolve)
				return getOsmpoint();
			return basicGetOsmpoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OSMPackage.OSM_POSITION__REF_SYSTEM_ID:
			setRefSystemId((String) newValue);
			return;
		case OSMPackage.OSM_POSITION__ACCURACY:
			setAccuracy((Float) newValue);
			return;
		case OSMPackage.OSM_POSITION__OSMPOINT:
			setOsmpoint((OSMPoint) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_POSITION__REF_SYSTEM_ID:
			setRefSystemId(REF_SYSTEM_ID_EDEFAULT);
			return;
		case OSMPackage.OSM_POSITION__ACCURACY:
			setAccuracy(ACCURACY_EDEFAULT);
			return;
		case OSMPackage.OSM_POSITION__OSMPOINT:
			setOsmpoint((OSMPoint) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_POSITION__REF_SYSTEM_ID:
			return REF_SYSTEM_ID_EDEFAULT == null ? refSystemId != null : !REF_SYSTEM_ID_EDEFAULT.equals(refSystemId);
		case OSMPackage.OSM_POSITION__ACCURACY:
			return accuracy != ACCURACY_EDEFAULT;
		case OSMPackage.OSM_POSITION__OSMPOINT:
			return osmpoint != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (refSystemId: ");
		result.append(refSystemId);
		result.append(", accuracy: ");
		result.append(accuracy);
		result.append(')');
		return result.toString();
	}

} //OSMPositionImpl
