/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Zone Descriptor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMZoneDescriptorImpl#getZoneId <em>Zone Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMZoneDescriptorImpl#getNotificationType <em>Notification Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OSMZoneDescriptorImpl extends MinimalEObjectImpl.Container implements OSMZoneDescriptor {
	/**
	 * The default value of the '{@link #getZoneId() <em>Zone Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZoneId()
	 * @generated
	 * @ordered
	 */
	protected static final String ZONE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getZoneId() <em>Zone Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZoneId()
	 * @generated
	 * @ordered
	 */
	protected String zoneId = ZONE_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getNotificationType() <em>Notification Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotificationType()
	 * @generated
	 * @ordered
	 */
	protected static final String NOTIFICATION_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNotificationType() <em>Notification Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotificationType()
	 * @generated
	 * @ordered
	 */
	protected String notificationType = NOTIFICATION_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OSMZoneDescriptorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSMPackage.Literals.OSM_ZONE_DESCRIPTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getZoneId() {
		return zoneId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setZoneId(String newZoneId) {
		String oldZoneId = zoneId;
		zoneId = newZoneId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_ZONE_DESCRIPTOR__ZONE_ID, oldZoneId,
					zoneId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNotificationType() {
		return notificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotificationType(String newNotificationType) {
		String oldNotificationType = notificationType;
		notificationType = newNotificationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSMPackage.OSM_ZONE_DESCRIPTOR__NOTIFICATION_TYPE,
					oldNotificationType, notificationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case OSMPackage.OSM_ZONE_DESCRIPTOR__ZONE_ID:
			return getZoneId();
		case OSMPackage.OSM_ZONE_DESCRIPTOR__NOTIFICATION_TYPE:
			return getNotificationType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case OSMPackage.OSM_ZONE_DESCRIPTOR__ZONE_ID:
			setZoneId((String) newValue);
			return;
		case OSMPackage.OSM_ZONE_DESCRIPTOR__NOTIFICATION_TYPE:
			setNotificationType((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_ZONE_DESCRIPTOR__ZONE_ID:
			setZoneId(ZONE_ID_EDEFAULT);
			return;
		case OSMPackage.OSM_ZONE_DESCRIPTOR__NOTIFICATION_TYPE:
			setNotificationType(NOTIFICATION_TYPE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case OSMPackage.OSM_ZONE_DESCRIPTOR__ZONE_ID:
			return ZONE_ID_EDEFAULT == null ? zoneId != null : !ZONE_ID_EDEFAULT.equals(zoneId);
		case OSMPackage.OSM_ZONE_DESCRIPTOR__NOTIFICATION_TYPE:
			return NOTIFICATION_TYPE_EDEFAULT == null ? notificationType != null
					: !NOTIFICATION_TYPE_EDEFAULT.equals(notificationType);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (zoneId: ");
		result.append(zoneId);
		result.append(", notificationType: ");
		result.append(notificationType);
		result.append(')');
		return result.toString();
	}

} //OSMZoneDescriptorImpl
