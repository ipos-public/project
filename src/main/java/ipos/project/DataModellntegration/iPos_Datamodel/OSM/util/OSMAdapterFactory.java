/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.util;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage
 * @generated
 */
public class OSMAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OSMPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OSMPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OSMSwitch<Adapter> modelSwitch = new OSMSwitch<Adapter>() {
		@Override
		public Adapter caseOSMObject(OSMObject object) {
			return createOSMObjectAdapter();
		}

		@Override
		public Adapter caseOSMPosition(OSMPosition object) {
			return createOSMPositionAdapter();
		}

		@Override
		public Adapter caseOSMPoint(OSMPoint object) {
			return createOSMPointAdapter();
		}

		@Override
		public Adapter caseOSMOrientation(OSMOrientation object) {
			return createOSMOrientationAdapter();
		}

		@Override
		public Adapter caseOSMExtractedAttributes(OSMExtractedAttributes object) {
			return createOSMExtractedAttributesAdapter();
		}

		@Override
		public Adapter caseOSMZoneDescriptor(OSMZoneDescriptor object) {
			return createOSMZoneDescriptorAdapter();
		}

		@Override
		public Adapter caseOSMMonitoringTarget(OSMMonitoringTarget object) {
			return createOSMMonitoringTargetAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject
	 * @generated
	 */
	public Adapter createOSMObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPosition
	 * @generated
	 */
	public Adapter createOSMPositionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPoint
	 * @generated
	 */
	public Adapter createOSMPointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMOrientation
	 * @generated
	 */
	public Adapter createOSMOrientationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes <em>Extracted Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMExtractedAttributes
	 * @generated
	 */
	public Adapter createOSMExtractedAttributesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor <em>Zone Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMZoneDescriptor
	 * @generated
	 */
	public Adapter createOSMZoneDescriptorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget <em>Monitoring Target</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget
	 * @generated
	 */
	public Adapter createOSMMonitoringTargetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OSMAdapterFactory
