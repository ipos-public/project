/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.OSM.util;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage
 * @generated
 */
public class OSMSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OSMPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSMSwitch() {
		if (modelPackage == null) {
			modelPackage = OSMPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case OSMPackage.OSM_OBJECT: {
			OSMObject osmObject = (OSMObject) theEObject;
			T result = caseOSMObject(osmObject);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case OSMPackage.OSM_POSITION: {
			OSMPosition osmPosition = (OSMPosition) theEObject;
			T result = caseOSMPosition(osmPosition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case OSMPackage.OSM_POINT: {
			OSMPoint osmPoint = (OSMPoint) theEObject;
			T result = caseOSMPoint(osmPoint);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case OSMPackage.OSM_ORIENTATION: {
			OSMOrientation osmOrientation = (OSMOrientation) theEObject;
			T result = caseOSMOrientation(osmOrientation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case OSMPackage.OSM_EXTRACTED_ATTRIBUTES: {
			OSMExtractedAttributes osmExtractedAttributes = (OSMExtractedAttributes) theEObject;
			T result = caseOSMExtractedAttributes(osmExtractedAttributes);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case OSMPackage.OSM_ZONE_DESCRIPTOR: {
			OSMZoneDescriptor osmZoneDescriptor = (OSMZoneDescriptor) theEObject;
			T result = caseOSMZoneDescriptor(osmZoneDescriptor);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case OSMPackage.OSM_MONITORING_TARGET: {
			OSMMonitoringTarget osmMonitoringTarget = (OSMMonitoringTarget) theEObject;
			T result = caseOSMMonitoringTarget(osmMonitoringTarget);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOSMObject(OSMObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Position</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOSMPosition(OSMPosition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOSMPoint(OSMPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Orientation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Orientation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOSMOrientation(OSMOrientation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extracted Attributes</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extracted Attributes</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOSMExtractedAttributes(OSMExtractedAttributes object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Zone Descriptor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Zone Descriptor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOSMZoneDescriptor(OSMZoneDescriptor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Monitoring Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Monitoring Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOSMMonitoringTarget(OSMMonitoringTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OSMSwitch
