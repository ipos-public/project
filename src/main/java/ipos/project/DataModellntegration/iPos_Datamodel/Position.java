/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Position</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getAccuracy <em>Accuracy</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getPoint <em>Point</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getReferenceSystem <em>Reference System</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPosition()
 * @model
 * @generated
 */
public interface Position extends EObject {
	/**
	 * Returns the value of the '<em><b>Accuracy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accuracy</em>' reference.
	 * @see #setAccuracy(Accuracy)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPosition_Accuracy()
	 * @model
	 * @generated
	 */
	Accuracy getAccuracy();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getAccuracy <em>Accuracy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accuracy</em>' reference.
	 * @see #getAccuracy()
	 * @generated
	 */
	void setAccuracy(Accuracy value);

	/**
	 * Returns the value of the '<em><b>Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Point</em>' reference.
	 * @see #setPoint(Point)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPosition_Point()
	 * @model required="true"
	 * @generated
	 */
	Point getPoint();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getPoint <em>Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Point</em>' reference.
	 * @see #getPoint()
	 * @generated
	 */
	void setPoint(Point value);

	/**
	 * Returns the value of the '<em><b>Reference System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference System</em>' reference.
	 * @see #setReferenceSystem(ReferenceSystem)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPosition_ReferenceSystem()
	 * @model required="true"
	 * @generated
	 */
	ReferenceSystem getReferenceSystem();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Position#getReferenceSystem <em>Reference System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference System</em>' reference.
	 * @see #getReferenceSystem()
	 * @generated
	 */
	void setReferenceSystem(ReferenceSystem value);

} // Position
