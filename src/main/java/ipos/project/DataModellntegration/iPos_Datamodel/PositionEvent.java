/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Position Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getTimeStamp <em>Time Stamp</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getLObjectId <em>LObject Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getZonedescriptors <em>Zonedescriptors</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getPlacing <em>Placing</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPositionEvent()
 * @model
 * @generated
 */
public interface PositionEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Placing</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Placing</em>' reference.
	 * @see #setPlacing(Placing)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPositionEvent_Placing()
	 * @model required="true"
	 * @generated
	 */
	Placing getPlacing();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getPlacing <em>Placing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Placing</em>' reference.
	 * @see #getPlacing()
	 * @generated
	 */
	void setPlacing(Placing value);

	/**
	 * Returns the value of the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Stamp</em>' attribute.
	 * @see #setTimeStamp(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPositionEvent_TimeStamp()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" transient="true"
	 * @generated
	 */
	String getTimeStamp();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getTimeStamp <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Stamp</em>' attribute.
	 * @see #getTimeStamp()
	 * @generated
	 */
	void setTimeStamp(String value);

	/**
	 * Returns the value of the '<em><b>LObject Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LObject Id</em>' attribute.
	 * @see #setLObjectId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPositionEvent_LObjectId()
	 * @model
	 * @generated
	 */
	String getLObjectId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent#getLObjectId <em>LObject Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LObject Id</em>' attribute.
	 * @see #getLObjectId()
	 * @generated
	 */
	void setLObjectId(String value);

	/**
	 * Returns the value of the '<em><b>Zonedescriptors</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zonedescriptors</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getPositionEvent_Zonedescriptors()
	 * @model
	 * @generated
	 */
	EList<ZoneDescriptor> getZonedescriptors();

} // PositionEvent
