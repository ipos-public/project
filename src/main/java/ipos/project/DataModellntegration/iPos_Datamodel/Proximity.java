/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Proximity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getTagId <em>Tag Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getType <em>Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getScannerId <em>Scanner Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getProximity()
 * @model
 * @generated
 */
public interface Proximity extends RawdataEvent {
	/**
	 * Returns the value of the '<em><b>Tag Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Id</em>' attribute.
	 * @see #setTagId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getProximity_TagId()
	 * @model
	 * @generated
	 */
	String getTagId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getTagId <em>Tag Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag Id</em>' attribute.
	 * @see #getTagId()
	 * @generated
	 */
	void setTagId(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getProximity_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Scanner Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scanner Id</em>' attribute.
	 * @see #setScannerId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getProximity_ScannerId()
	 * @model
	 * @generated
	 */
	String getScannerId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity#getScannerId <em>Scanner Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scanner Id</em>' attribute.
	 * @see #getScannerId()
	 * @generated
	 */
	void setScannerId(String value);

} // Proximity
