/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quaternion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getX <em>X</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getY <em>Y</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getZ <em>Z</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getW <em>W</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getQuaternion()
 * @model
 * @generated
 */
public interface Quaternion extends Orientation {
	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getQuaternion_X()
	 * @model
	 * @generated
	 */
	float getX();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(float value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getQuaternion_Y()
	 * @model
	 * @generated
	 */
	float getY();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(float value);

	/**
	 * Returns the value of the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Z</em>' attribute.
	 * @see #setZ(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getQuaternion_Z()
	 * @model
	 * @generated
	 */
	float getZ();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getZ <em>Z</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Z</em>' attribute.
	 * @see #getZ()
	 * @generated
	 */
	void setZ(float value);

	/**
	 * Returns the value of the '<em><b>W</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>W</em>' attribute.
	 * @see #setW(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getQuaternion_W()
	 * @model
	 * @generated
	 */
	float getW();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion#getW <em>W</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>W</em>' attribute.
	 * @see #getW()
	 * @generated
	 */
	void setW(float value);

} // Quaternion
