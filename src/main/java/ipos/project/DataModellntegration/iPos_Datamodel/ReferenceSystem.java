/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getOrigin <em>Origin</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getName <em>Name</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getReferenceSystem()
 * @model
 * @generated
 */
public interface ReferenceSystem extends EObject {
	/**
	 * Returns the value of the '<em><b>Origin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origin</em>' reference.
	 * @see #setOrigin(Placing)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getReferenceSystem_Origin()
	 * @model
	 * @generated
	 */
	Placing getOrigin();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getOrigin <em>Origin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origin</em>' reference.
	 * @see #getOrigin()
	 * @generated
	 */
	void setOrigin(Placing value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getReferenceSystem_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getReferenceSystem_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

} // ReferenceSystem
