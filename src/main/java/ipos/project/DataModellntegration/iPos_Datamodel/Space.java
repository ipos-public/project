/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Space</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getX <em>X</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getY <em>Y</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getZ <em>Z</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getCentrePoint <em>Centre Point</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getSpace()
 * @model
 * @generated
 */
public interface Space extends EObject {
	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getSpace_X()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float"
	 * @generated
	 */
	float getX();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(float value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getSpace_Y()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float"
	 * @generated
	 */
	float getY();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(float value);

	/**
	 * Returns the value of the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Z</em>' attribute.
	 * @see #setZ(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getSpace_Z()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float"
	 * @generated
	 */
	float getZ();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getZ <em>Z</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Z</em>' attribute.
	 * @see #getZ()
	 * @generated
	 */
	void setZ(float value);

	/**
	 * Returns the value of the '<em><b>Centre Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Centre Point</em>' reference.
	 * @see #setCentrePoint(Placing)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getSpace_CentrePoint()
	 * @model required="true"
	 * @generated
	 */
	Placing getCentrePoint();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space#getCentrePoint <em>Centre Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Centre Point</em>' reference.
	 * @see #getCentrePoint()
	 * @generated
	 */
	void setCentrePoint(Placing value);

} // Space
