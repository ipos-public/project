/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.Tooz;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Distant Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getLocalSensorId <em>Local Sensor Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getLocalAgentId <em>Local Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getDistantEntityId <em>Distant Entity Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getDistance <em>Distance</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getProximityIndex <em>Proximity Index</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getEntityData <em>Entity Data</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getTimeStamp <em>Time Stamp</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getDistantEntity()
 * @model
 * @generated
 */
public interface DistantEntity extends EObject {
	/**
	 * Returns the value of the '<em><b>Local Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Sensor Id</em>' attribute.
	 * @see #setLocalSensorId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getDistantEntity_LocalSensorId()
	 * @model
	 * @generated
	 */
	String getLocalSensorId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getLocalSensorId <em>Local Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Sensor Id</em>' attribute.
	 * @see #getLocalSensorId()
	 * @generated
	 */
	void setLocalSensorId(String value);

	/**
	 * Returns the value of the '<em><b>Local Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Agent Id</em>' attribute.
	 * @see #setLocalAgentId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getDistantEntity_LocalAgentId()
	 * @model
	 * @generated
	 */
	String getLocalAgentId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getLocalAgentId <em>Local Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Agent Id</em>' attribute.
	 * @see #getLocalAgentId()
	 * @generated
	 */
	void setLocalAgentId(String value);

	/**
	 * Returns the value of the '<em><b>Distant Entity Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distant Entity Id</em>' attribute.
	 * @see #setDistantEntityId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getDistantEntity_DistantEntityId()
	 * @model
	 * @generated
	 */
	String getDistantEntityId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getDistantEntityId <em>Distant Entity Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distant Entity Id</em>' attribute.
	 * @see #getDistantEntityId()
	 * @generated
	 */
	void setDistantEntityId(String value);

	/**
	 * Returns the value of the '<em><b>Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distance</em>' attribute.
	 * @see #setDistance(double)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getDistantEntity_Distance()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Double"
	 * @generated
	 */
	double getDistance();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getDistance <em>Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distance</em>' attribute.
	 * @see #getDistance()
	 * @generated
	 */
	void setDistance(double value);

	/**
	 * Returns the value of the '<em><b>Proximity Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Proximity Index</em>' attribute.
	 * @see #setProximityIndex(int)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getDistantEntity_ProximityIndex()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 * @generated
	 */
	int getProximityIndex();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getProximityIndex <em>Proximity Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Proximity Index</em>' attribute.
	 * @see #getProximityIndex()
	 * @generated
	 */
	void setProximityIndex(int value);

	/**
	 * Returns the value of the '<em><b>Entity Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Data</em>' attribute.
	 * @see #setEntityData(Map)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getDistantEntity_EntityData()
	 * @model transient="true"
	 * @generated
	 */
	Map<String, String> getEntityData();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getEntityData <em>Entity Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Data</em>' attribute.
	 * @see #getEntityData()
	 * @generated
	 */
	void setEntityData(Map<String, String> value);

	/**
	 * Returns the value of the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Stamp</em>' attribute.
	 * @see #setTimeStamp(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getDistantEntity_TimeStamp()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getTimeStamp();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getTimeStamp <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Stamp</em>' attribute.
	 * @see #getTimeStamp()
	 * @generated
	 */
	void setTimeStamp(String value);

} // DistantEntity
