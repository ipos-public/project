/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.Tooz;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitoring Target</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget#getTargetSensorId <em>Target Sensor Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getMonitoringTarget()
 * @model
 * @generated
 */
public interface MonitoringTarget extends EObject {
	/**
	 * Returns the value of the '<em><b>Target Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Sensor Id</em>' attribute.
	 * @see #setTargetSensorId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#getMonitoringTarget_TargetSensorId()
	 * @model
	 * @generated
	 */
	String getTargetSensorId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget#getTargetSensorId <em>Target Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Sensor Id</em>' attribute.
	 * @see #getTargetSensorId()
	 * @generated
	 */
	void setTargetSensorId(String value);

} // MonitoringTarget
