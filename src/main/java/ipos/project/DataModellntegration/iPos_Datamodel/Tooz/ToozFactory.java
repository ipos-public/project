/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.Tooz;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage
 * @generated
 */
public interface ToozFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ToozFactory eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Monitoring Target</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitoring Target</em>'.
	 * @generated
	 */
	MonitoringTarget createMonitoringTarget();

	/**
	 * Returns a new object of class '<em>Distant Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Distant Entity</em>'.
	 * @generated
	 */
	DistantEntity createDistantEntity();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ToozPackage getToozPackage();

} //ToozFactory
