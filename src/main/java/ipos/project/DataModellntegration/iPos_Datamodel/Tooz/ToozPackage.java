/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.Tooz;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozFactory
 * @model kind="package"
 * @generated
 */
public interface ToozPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Tooz";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "tooz";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tooz";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ToozPackage eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl.init();

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.MonitoringTargetImpl <em>Monitoring Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.MonitoringTargetImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl#getMonitoringTarget()
	 * @generated
	 */
	int MONITORING_TARGET = 0;

	/**
	 * The feature id for the '<em><b>Target Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_TARGET__TARGET_SENSOR_ID = 0;

	/**
	 * The number of structural features of the '<em>Monitoring Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_TARGET_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Monitoring Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORING_TARGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl <em>Distant Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl#getDistantEntity()
	 * @generated
	 */
	int DISTANT_ENTITY = 1;

	/**
	 * The feature id for the '<em><b>Local Sensor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY__LOCAL_SENSOR_ID = 0;

	/**
	 * The feature id for the '<em><b>Local Agent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY__LOCAL_AGENT_ID = 1;

	/**
	 * The feature id for the '<em><b>Distant Entity Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY__DISTANT_ENTITY_ID = 2;

	/**
	 * The feature id for the '<em><b>Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY__DISTANCE = 3;

	/**
	 * The feature id for the '<em><b>Proximity Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY__PROXIMITY_INDEX = 4;

	/**
	 * The feature id for the '<em><b>Entity Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY__ENTITY_DATA = 5;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY__TIME_STAMP = 6;

	/**
	 * The number of structural features of the '<em>Distant Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Distant Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANT_ENTITY_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget <em>Monitoring Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitoring Target</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget
	 * @generated
	 */
	EClass getMonitoringTarget();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget#getTargetSensorId <em>Target Sensor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Sensor Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget#getTargetSensorId()
	 * @see #getMonitoringTarget()
	 * @generated
	 */
	EAttribute getMonitoringTarget_TargetSensorId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity <em>Distant Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Distant Entity</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity
	 * @generated
	 */
	EClass getDistantEntity();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getLocalSensorId <em>Local Sensor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Sensor Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getLocalSensorId()
	 * @see #getDistantEntity()
	 * @generated
	 */
	EAttribute getDistantEntity_LocalSensorId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getLocalAgentId <em>Local Agent Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Agent Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getLocalAgentId()
	 * @see #getDistantEntity()
	 * @generated
	 */
	EAttribute getDistantEntity_LocalAgentId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getDistantEntityId <em>Distant Entity Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distant Entity Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getDistantEntityId()
	 * @see #getDistantEntity()
	 * @generated
	 */
	EAttribute getDistantEntity_DistantEntityId();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getDistance <em>Distance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distance</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getDistance()
	 * @see #getDistantEntity()
	 * @generated
	 */
	EAttribute getDistantEntity_Distance();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getProximityIndex <em>Proximity Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Proximity Index</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getProximityIndex()
	 * @see #getDistantEntity()
	 * @generated
	 */
	EAttribute getDistantEntity_ProximityIndex();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getEntityData <em>Entity Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Entity Data</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getEntityData()
	 * @see #getDistantEntity()
	 * @generated
	 */
	EAttribute getDistantEntity_EntityData();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getTimeStamp <em>Time Stamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Stamp</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity#getTimeStamp()
	 * @see #getDistantEntity()
	 * @generated
	 */
	EAttribute getDistantEntity_TimeStamp();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ToozFactory getToozFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.MonitoringTargetImpl <em>Monitoring Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.MonitoringTargetImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl#getMonitoringTarget()
		 * @generated
		 */
		EClass MONITORING_TARGET = eINSTANCE.getMonitoringTarget();

		/**
		 * The meta object literal for the '<em><b>Target Sensor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORING_TARGET__TARGET_SENSOR_ID = eINSTANCE.getMonitoringTarget_TargetSensorId();

		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl <em>Distant Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl#getDistantEntity()
		 * @generated
		 */
		EClass DISTANT_ENTITY = eINSTANCE.getDistantEntity();

		/**
		 * The meta object literal for the '<em><b>Local Sensor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANT_ENTITY__LOCAL_SENSOR_ID = eINSTANCE.getDistantEntity_LocalSensorId();

		/**
		 * The meta object literal for the '<em><b>Local Agent Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANT_ENTITY__LOCAL_AGENT_ID = eINSTANCE.getDistantEntity_LocalAgentId();

		/**
		 * The meta object literal for the '<em><b>Distant Entity Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANT_ENTITY__DISTANT_ENTITY_ID = eINSTANCE.getDistantEntity_DistantEntityId();

		/**
		 * The meta object literal for the '<em><b>Distance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANT_ENTITY__DISTANCE = eINSTANCE.getDistantEntity_Distance();

		/**
		 * The meta object literal for the '<em><b>Proximity Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANT_ENTITY__PROXIMITY_INDEX = eINSTANCE.getDistantEntity_ProximityIndex();

		/**
		 * The meta object literal for the '<em><b>Entity Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANT_ENTITY__ENTITY_DATA = eINSTANCE.getDistantEntity_EntityData();

		/**
		 * The meta object literal for the '<em><b>Time Stamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANT_ENTITY__TIME_STAMP = eINSTANCE.getDistantEntity_TimeStamp();

	}

} //ToozPackage
