/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Distant Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl#getLocalSensorId <em>Local Sensor Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl#getLocalAgentId <em>Local Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl#getDistantEntityId <em>Distant Entity Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl#getDistance <em>Distance</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl#getProximityIndex <em>Proximity Index</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl#getEntityData <em>Entity Data</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.DistantEntityImpl#getTimeStamp <em>Time Stamp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DistantEntityImpl extends MinimalEObjectImpl.Container implements DistantEntity {
	/**
	 * The default value of the '{@link #getLocalSensorId() <em>Local Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSensorId()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCAL_SENSOR_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocalSensorId() <em>Local Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSensorId()
	 * @generated
	 * @ordered
	 */
	protected String localSensorId = LOCAL_SENSOR_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocalAgentId() <em>Local Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalAgentId()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCAL_AGENT_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocalAgentId() <em>Local Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalAgentId()
	 * @generated
	 * @ordered
	 */
	protected String localAgentId = LOCAL_AGENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDistantEntityId() <em>Distant Entity Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistantEntityId()
	 * @generated
	 * @ordered
	 */
	protected static final String DISTANT_ENTITY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDistantEntityId() <em>Distant Entity Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistantEntityId()
	 * @generated
	 * @ordered
	 */
	protected String distantEntityId = DISTANT_ENTITY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDistance() <em>Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistance()
	 * @generated
	 * @ordered
	 */
	protected static final double DISTANCE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getDistance() <em>Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistance()
	 * @generated
	 * @ordered
	 */
	protected double distance = DISTANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getProximityIndex() <em>Proximity Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProximityIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int PROXIMITY_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getProximityIndex() <em>Proximity Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProximityIndex()
	 * @generated
	 * @ordered
	 */
	protected int proximityIndex = PROXIMITY_INDEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEntityData() <em>Entity Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntityData()
	 * @generated
	 * @ordered
	 */
	protected Map<String, String> entityData;

	/**
	 * The default value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected static final String TIME_STAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected String timeStamp = TIME_STAMP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DistantEntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ToozPackage.Literals.DISTANT_ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocalSensorId() {
		return localSensorId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSensorId(String newLocalSensorId) {
		String oldLocalSensorId = localSensorId;
		localSensorId = newLocalSensorId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ToozPackage.DISTANT_ENTITY__LOCAL_SENSOR_ID,
					oldLocalSensorId, localSensorId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocalAgentId() {
		return localAgentId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalAgentId(String newLocalAgentId) {
		String oldLocalAgentId = localAgentId;
		localAgentId = newLocalAgentId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ToozPackage.DISTANT_ENTITY__LOCAL_AGENT_ID,
					oldLocalAgentId, localAgentId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDistantEntityId() {
		return distantEntityId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDistantEntityId(String newDistantEntityId) {
		String oldDistantEntityId = distantEntityId;
		distantEntityId = newDistantEntityId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ToozPackage.DISTANT_ENTITY__DISTANT_ENTITY_ID,
					oldDistantEntityId, distantEntityId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getDistance() {
		return distance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDistance(double newDistance) {
		double oldDistance = distance;
		distance = newDistance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ToozPackage.DISTANT_ENTITY__DISTANCE, oldDistance,
					distance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getProximityIndex() {
		return proximityIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProximityIndex(int newProximityIndex) {
		int oldProximityIndex = proximityIndex;
		proximityIndex = newProximityIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ToozPackage.DISTANT_ENTITY__PROXIMITY_INDEX,
					oldProximityIndex, proximityIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<String, String> getEntityData() {
		return entityData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntityData(Map<String, String> newEntityData) {
		Map<String, String> oldEntityData = entityData;
		entityData = newEntityData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ToozPackage.DISTANT_ENTITY__ENTITY_DATA,
					oldEntityData, entityData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeStamp(String newTimeStamp) {
		String oldTimeStamp = timeStamp;
		timeStamp = newTimeStamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ToozPackage.DISTANT_ENTITY__TIME_STAMP, oldTimeStamp,
					timeStamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ToozPackage.DISTANT_ENTITY__LOCAL_SENSOR_ID:
			return getLocalSensorId();
		case ToozPackage.DISTANT_ENTITY__LOCAL_AGENT_ID:
			return getLocalAgentId();
		case ToozPackage.DISTANT_ENTITY__DISTANT_ENTITY_ID:
			return getDistantEntityId();
		case ToozPackage.DISTANT_ENTITY__DISTANCE:
			return getDistance();
		case ToozPackage.DISTANT_ENTITY__PROXIMITY_INDEX:
			return getProximityIndex();
		case ToozPackage.DISTANT_ENTITY__ENTITY_DATA:
			return getEntityData();
		case ToozPackage.DISTANT_ENTITY__TIME_STAMP:
			return getTimeStamp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ToozPackage.DISTANT_ENTITY__LOCAL_SENSOR_ID:
			setLocalSensorId((String) newValue);
			return;
		case ToozPackage.DISTANT_ENTITY__LOCAL_AGENT_ID:
			setLocalAgentId((String) newValue);
			return;
		case ToozPackage.DISTANT_ENTITY__DISTANT_ENTITY_ID:
			setDistantEntityId((String) newValue);
			return;
		case ToozPackage.DISTANT_ENTITY__DISTANCE:
			setDistance((Double) newValue);
			return;
		case ToozPackage.DISTANT_ENTITY__PROXIMITY_INDEX:
			setProximityIndex((Integer) newValue);
			return;
		case ToozPackage.DISTANT_ENTITY__ENTITY_DATA:
			setEntityData((Map<String, String>) newValue);
			return;
		case ToozPackage.DISTANT_ENTITY__TIME_STAMP:
			setTimeStamp((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ToozPackage.DISTANT_ENTITY__LOCAL_SENSOR_ID:
			setLocalSensorId(LOCAL_SENSOR_ID_EDEFAULT);
			return;
		case ToozPackage.DISTANT_ENTITY__LOCAL_AGENT_ID:
			setLocalAgentId(LOCAL_AGENT_ID_EDEFAULT);
			return;
		case ToozPackage.DISTANT_ENTITY__DISTANT_ENTITY_ID:
			setDistantEntityId(DISTANT_ENTITY_ID_EDEFAULT);
			return;
		case ToozPackage.DISTANT_ENTITY__DISTANCE:
			setDistance(DISTANCE_EDEFAULT);
			return;
		case ToozPackage.DISTANT_ENTITY__PROXIMITY_INDEX:
			setProximityIndex(PROXIMITY_INDEX_EDEFAULT);
			return;
		case ToozPackage.DISTANT_ENTITY__ENTITY_DATA:
			setEntityData((Map<String, String>) null);
			return;
		case ToozPackage.DISTANT_ENTITY__TIME_STAMP:
			setTimeStamp(TIME_STAMP_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ToozPackage.DISTANT_ENTITY__LOCAL_SENSOR_ID:
			return LOCAL_SENSOR_ID_EDEFAULT == null ? localSensorId != null
					: !LOCAL_SENSOR_ID_EDEFAULT.equals(localSensorId);
		case ToozPackage.DISTANT_ENTITY__LOCAL_AGENT_ID:
			return LOCAL_AGENT_ID_EDEFAULT == null ? localAgentId != null
					: !LOCAL_AGENT_ID_EDEFAULT.equals(localAgentId);
		case ToozPackage.DISTANT_ENTITY__DISTANT_ENTITY_ID:
			return DISTANT_ENTITY_ID_EDEFAULT == null ? distantEntityId != null
					: !DISTANT_ENTITY_ID_EDEFAULT.equals(distantEntityId);
		case ToozPackage.DISTANT_ENTITY__DISTANCE:
			return distance != DISTANCE_EDEFAULT;
		case ToozPackage.DISTANT_ENTITY__PROXIMITY_INDEX:
			return proximityIndex != PROXIMITY_INDEX_EDEFAULT;
		case ToozPackage.DISTANT_ENTITY__ENTITY_DATA:
			return entityData != null;
		case ToozPackage.DISTANT_ENTITY__TIME_STAMP:
			return TIME_STAMP_EDEFAULT == null ? timeStamp != null : !TIME_STAMP_EDEFAULT.equals(timeStamp);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (localSensorId: ");
		result.append(localSensorId);
		result.append(", localAgentId: ");
		result.append(localAgentId);
		result.append(", distantEntityId: ");
		result.append(distantEntityId);
		result.append(", distance: ");
		result.append(distance);
		result.append(", proximityIndex: ");
		result.append(proximityIndex);
		result.append(", entityData: ");
		result.append(entityData);
		result.append(", timeStamp: ");
		result.append(timeStamp);
		result.append(')');
		return result.toString();
	}

} //DistantEntityImpl
