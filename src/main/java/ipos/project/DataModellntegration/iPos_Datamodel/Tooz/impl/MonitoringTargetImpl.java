/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitoring Target</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.MonitoringTargetImpl#getTargetSensorId <em>Target Sensor Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MonitoringTargetImpl extends MinimalEObjectImpl.Container implements MonitoringTarget {
	/**
	 * The default value of the '{@link #getTargetSensorId() <em>Target Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSensorId()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_SENSOR_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetSensorId() <em>Target Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSensorId()
	 * @generated
	 * @ordered
	 */
	protected String targetSensorId = TARGET_SENSOR_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoringTargetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ToozPackage.Literals.MONITORING_TARGET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTargetSensorId() {
		return targetSensorId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetSensorId(String newTargetSensorId) {
		String oldTargetSensorId = targetSensorId;
		targetSensorId = newTargetSensorId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ToozPackage.MONITORING_TARGET__TARGET_SENSOR_ID,
					oldTargetSensorId, targetSensorId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ToozPackage.MONITORING_TARGET__TARGET_SENSOR_ID:
			return getTargetSensorId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ToozPackage.MONITORING_TARGET__TARGET_SENSOR_ID:
			setTargetSensorId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ToozPackage.MONITORING_TARGET__TARGET_SENSOR_ID:
			setTargetSensorId(TARGET_SENSOR_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ToozPackage.MONITORING_TARGET__TARGET_SENSOR_ID:
			return TARGET_SENSOR_ID_EDEFAULT == null ? targetSensorId != null
					: !TARGET_SENSOR_ID_EDEFAULT.equals(targetSensorId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (targetSensorId: ");
		result.append(targetSensorId);
		result.append(')');
		return result.toString();
	}

} //MonitoringTargetImpl
