/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ToozFactoryImpl extends EFactoryImpl implements ToozFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ToozFactory init() {
		try {
			ToozFactory theToozFactory = (ToozFactory) EPackage.Registry.INSTANCE.getEFactory(ToozPackage.eNS_URI);
			if (theToozFactory != null) {
				return theToozFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ToozFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ToozFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ToozPackage.MONITORING_TARGET:
			return createMonitoringTarget();
		case ToozPackage.DISTANT_ENTITY:
			return createDistantEntity();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoringTarget createMonitoringTarget() {
		MonitoringTargetImpl monitoringTarget = new MonitoringTargetImpl();
		return monitoringTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DistantEntity createDistantEntity() {
		DistantEntityImpl distantEntity = new DistantEntityImpl();
		return distantEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ToozPackage getToozPackage() {
		return (ToozPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ToozPackage getPackage() {
		return ToozPackage.eINSTANCE;
	}

} //ToozFactoryImpl
