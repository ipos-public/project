/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ToozPackageImpl extends EPackageImpl implements ToozPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitoringTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass distantEntityEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ToozPackageImpl() {
		super(eNS_URI, ToozFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ToozPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ToozPackage init() {
		if (isInited)
			return (ToozPackage) EPackage.Registry.INSTANCE.getEPackage(ToozPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredToozPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ToozPackageImpl theToozPackage = registeredToozPackage instanceof ToozPackageImpl
				? (ToozPackageImpl) registeredToozPackage
				: new ToozPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPos_DatamodelPackage.eNS_URI);
		IPos_DatamodelPackageImpl theIPos_DatamodelPackage = (IPos_DatamodelPackageImpl) (registeredPackage instanceof IPos_DatamodelPackageImpl
				? registeredPackage
				: IPos_DatamodelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OFBizPackage.eNS_URI);
		OFBizPackageImpl theOFBizPackage = (OFBizPackageImpl) (registeredPackage instanceof OFBizPackageImpl
				? registeredPackage
				: OFBizPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPosDevKitPackage.eNS_URI);
		IPosDevKitPackageImpl theIPosDevKitPackage = (IPosDevKitPackageImpl) (registeredPackage instanceof IPosDevKitPackageImpl
				? registeredPackage
				: IPosDevKitPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VDA5050Package.eNS_URI);
		VDA5050PackageImpl theVDA5050Package = (VDA5050PackageImpl) (registeredPackage instanceof VDA5050PackageImpl
				? registeredPackage
				: VDA5050Package.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OSMPackage.eNS_URI);
		OSMPackageImpl theOSMPackage = (OSMPackageImpl) (registeredPackage instanceof OSMPackageImpl ? registeredPackage
				: OSMPackage.eINSTANCE);

		// Create package meta-data objects
		theToozPackage.createPackageContents();
		theIPos_DatamodelPackage.createPackageContents();
		theOFBizPackage.createPackageContents();
		theIPosDevKitPackage.createPackageContents();
		theVDA5050Package.createPackageContents();
		theOSMPackage.createPackageContents();

		// Initialize created meta-data
		theToozPackage.initializePackageContents();
		theIPos_DatamodelPackage.initializePackageContents();
		theOFBizPackage.initializePackageContents();
		theIPosDevKitPackage.initializePackageContents();
		theVDA5050Package.initializePackageContents();
		theOSMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theToozPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ToozPackage.eNS_URI, theToozPackage);
		return theToozPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitoringTarget() {
		return monitoringTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoringTarget_TargetSensorId() {
		return (EAttribute) monitoringTargetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDistantEntity() {
		return distantEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDistantEntity_LocalSensorId() {
		return (EAttribute) distantEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDistantEntity_LocalAgentId() {
		return (EAttribute) distantEntityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDistantEntity_DistantEntityId() {
		return (EAttribute) distantEntityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDistantEntity_Distance() {
		return (EAttribute) distantEntityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDistantEntity_ProximityIndex() {
		return (EAttribute) distantEntityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDistantEntity_EntityData() {
		return (EAttribute) distantEntityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDistantEntity_TimeStamp() {
		return (EAttribute) distantEntityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ToozFactory getToozFactory() {
		return (ToozFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		monitoringTargetEClass = createEClass(MONITORING_TARGET);
		createEAttribute(monitoringTargetEClass, MONITORING_TARGET__TARGET_SENSOR_ID);

		distantEntityEClass = createEClass(DISTANT_ENTITY);
		createEAttribute(distantEntityEClass, DISTANT_ENTITY__LOCAL_SENSOR_ID);
		createEAttribute(distantEntityEClass, DISTANT_ENTITY__LOCAL_AGENT_ID);
		createEAttribute(distantEntityEClass, DISTANT_ENTITY__DISTANT_ENTITY_ID);
		createEAttribute(distantEntityEClass, DISTANT_ENTITY__DISTANCE);
		createEAttribute(distantEntityEClass, DISTANT_ENTITY__PROXIMITY_INDEX);
		createEAttribute(distantEntityEClass, DISTANT_ENTITY__ENTITY_DATA);
		createEAttribute(distantEntityEClass, DISTANT_ENTITY__TIME_STAMP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE
				.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(monitoringTargetEClass, MonitoringTarget.class, "MonitoringTarget", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMonitoringTarget_TargetSensorId(), ecorePackage.getEString(), "targetSensorId", null, 0, 1,
				MonitoringTarget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(distantEntityEClass, DistantEntity.class, "DistantEntity", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDistantEntity_LocalSensorId(), ecorePackage.getEString(), "localSensorId", null, 0, 1,
				DistantEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getDistantEntity_LocalAgentId(), ecorePackage.getEString(), "localAgentId", null, 0, 1,
				DistantEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getDistantEntity_DistantEntityId(), ecorePackage.getEString(), "distantEntityId", null, 0, 1,
				DistantEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getDistantEntity_Distance(), theXMLTypePackage.getDouble(), "distance", null, 0, 1,
				DistantEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getDistantEntity_ProximityIndex(), theXMLTypePackage.getInt(), "proximityIndex", null, 0, 1,
				DistantEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getDistantEntity_EntityData(), g1, "entityData", null, 0, 1, DistantEntity.class, IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDistantEntity_TimeStamp(), theXMLTypePackage.getString(), "timeStamp", null, 0, 1,
				DistantEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
	}

} //ToozPackageImpl
