/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tracking Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask#getEventfilterconfiguration <em>Eventfilterconfiguration</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getTrackingTask()
 * @model
 * @generated
 */
public interface TrackingTask extends EObject {
	/**
	 * Returns the value of the '<em><b>Eventfilterconfiguration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eventfilterconfiguration</em>' reference.
	 * @see #setEventfilterconfiguration(EventFilterConfiguration)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getTrackingTask_Eventfilterconfiguration()
	 * @model required="true"
	 * @generated
	 */
	EventFilterConfiguration getEventfilterconfiguration();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask#getEventfilterconfiguration <em>Eventfilterconfiguration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Eventfilterconfiguration</em>' reference.
	 * @see #getEventfilterconfiguration()
	 * @generated
	 */
	void setEventfilterconfiguration(EventFilterConfiguration value);

} // TrackingTask
