/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UWB</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getUWB()
 * @model
 * @generated
 */
public interface UWB extends Beacon {
} // UWB
