/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agv Position</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getX <em>X</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getY <em>Y</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getTheta <em>Theta</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getMapId <em>Map Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvPosition()
 * @model
 * @generated
 */
public interface AgvPosition extends EObject {
	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvPosition_X()
	 * @model
	 * @generated
	 */
	float getX();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(float value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvPosition_Y()
	 * @model
	 * @generated
	 */
	float getY();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(float value);

	/**
	 * Returns the value of the '<em><b>Theta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Theta</em>' attribute.
	 * @see #setTheta(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvPosition_Theta()
	 * @model
	 * @generated
	 */
	float getTheta();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getTheta <em>Theta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Theta</em>' attribute.
	 * @see #getTheta()
	 * @generated
	 */
	void setTheta(float value);

	/**
	 * Returns the value of the '<em><b>Map Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Map Id</em>' attribute.
	 * @see #setMapId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvPosition_MapId()
	 * @model
	 * @generated
	 */
	String getMapId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getMapId <em>Map Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Map Id</em>' attribute.
	 * @see #getMapId()
	 * @generated
	 */
	void setMapId(String value);

} // AgvPosition
