/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agv State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getTimeStamp <em>Time Stamp</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getSerialNumber <em>Serial Number</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getLastNodeId <em>Last Node Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getBatteryState <em>Battery State</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getErrors <em>Errors</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getAgvposition <em>Agvposition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getLoads <em>Loads</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getManufacturer <em>Manufacturer</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState()
 * @model
 * @generated
 */
public interface AgvState extends EObject {
	/**
	 * Returns the value of the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Stamp</em>' attribute.
	 * @see #setTimeStamp(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState_TimeStamp()
	 * @model
	 * @generated
	 */
	String getTimeStamp();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getTimeStamp <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Stamp</em>' attribute.
	 * @see #getTimeStamp()
	 * @generated
	 */
	void setTimeStamp(String value);

	/**
	 * Returns the value of the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serial Number</em>' attribute.
	 * @see #setSerialNumber(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState_SerialNumber()
	 * @model
	 * @generated
	 */
	String getSerialNumber();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getSerialNumber <em>Serial Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Serial Number</em>' attribute.
	 * @see #getSerialNumber()
	 * @generated
	 */
	void setSerialNumber(String value);

	/**
	 * Returns the value of the '<em><b>Last Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Node Id</em>' attribute.
	 * @see #setLastNodeId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState_LastNodeId()
	 * @model
	 * @generated
	 */
	String getLastNodeId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getLastNodeId <em>Last Node Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Node Id</em>' attribute.
	 * @see #getLastNodeId()
	 * @generated
	 */
	void setLastNodeId(String value);

	/**
	 * Returns the value of the '<em><b>Battery State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Battery State</em>' reference.
	 * @see #setBatteryState(BatteryState)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState_BatteryState()
	 * @model
	 * @generated
	 */
	BatteryState getBatteryState();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getBatteryState <em>Battery State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Battery State</em>' reference.
	 * @see #getBatteryState()
	 * @generated
	 */
	void setBatteryState(BatteryState value);

	/**
	 * Returns the value of the '<em><b>Errors</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Errors</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState_Errors()
	 * @model
	 * @generated
	 */
	EList<ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error> getErrors();

	/**
	 * Returns the value of the '<em><b>Agvposition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agvposition</em>' reference.
	 * @see #setAgvposition(AgvPosition)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState_Agvposition()
	 * @model
	 * @generated
	 */
	AgvPosition getAgvposition();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getAgvposition <em>Agvposition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agvposition</em>' reference.
	 * @see #getAgvposition()
	 * @generated
	 */
	void setAgvposition(AgvPosition value);

	/**
	 * Returns the value of the '<em><b>Loads</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loads</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState_Loads()
	 * @model
	 * @generated
	 */
	EList<Load> getLoads();

	/**
	 * Returns the value of the '<em><b>Manufacturer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manufacturer</em>' attribute.
	 * @see #setManufacturer(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getAgvState_Manufacturer()
	 * @model
	 * @generated
	 */
	String getManufacturer();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getManufacturer <em>Manufacturer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Manufacturer</em>' attribute.
	 * @see #getManufacturer()
	 * @generated
	 */
	void setManufacturer(String value);

} // AgvState
