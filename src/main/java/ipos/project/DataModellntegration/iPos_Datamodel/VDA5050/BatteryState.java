/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Battery State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState#getBatteryCharge <em>Battery Charge</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getBatteryState()
 * @model
 * @generated
 */
public interface BatteryState extends EObject {
	/**
	 * Returns the value of the '<em><b>Battery Charge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Battery Charge</em>' attribute.
	 * @see #setBatteryCharge(float)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getBatteryState_BatteryCharge()
	 * @model
	 * @generated
	 */
	float getBatteryCharge();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState#getBatteryCharge <em>Battery Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Battery Charge</em>' attribute.
	 * @see #getBatteryCharge()
	 * @generated
	 */
	void setBatteryCharge(float value);

} // BatteryState
