/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Load</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load#getLoadId <em>Load Id</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getLoad()
 * @model
 * @generated
 */
public interface Load extends EObject {
	/**
	 * Returns the value of the '<em><b>Load Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Load Id</em>' attribute.
	 * @see #setLoadId(String)
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#getLoad_LoadId()
	 * @model
	 * @generated
	 */
	String getLoadId();

	/**
	 * Sets the value of the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load#getLoadId <em>Load Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Id</em>' attribute.
	 * @see #getLoadId()
	 * @generated
	 */
	void setLoadId(String value);

} // Load
