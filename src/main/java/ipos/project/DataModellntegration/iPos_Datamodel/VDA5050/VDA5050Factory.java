/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package
 * @generated
 */
public interface VDA5050Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VDA5050Factory eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Agv State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agv State</em>'.
	 * @generated
	 */
	AgvState createAgvState();

	/**
	 * Returns a new object of class '<em>Battery State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Battery State</em>'.
	 * @generated
	 */
	BatteryState createBatteryState();

	/**
	 * Returns a new object of class '<em>Error</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Error</em>'.
	 * @generated
	 */
	Error createError();

	/**
	 * Returns a new object of class '<em>Agv Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agv Position</em>'.
	 * @generated
	 */
	AgvPosition createAgvPosition();

	/**
	 * Returns a new object of class '<em>Load</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Load</em>'.
	 * @generated
	 */
	Load createLoad();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	VDA5050Package getVDA5050Package();

} //VDA5050Factory
