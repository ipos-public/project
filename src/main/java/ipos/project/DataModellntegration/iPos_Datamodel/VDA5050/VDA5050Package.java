/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Factory
 * @model kind="package"
 * @generated
 */
public interface VDA5050Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "VDA5050";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "vda5050";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "vda5050";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VDA5050Package eINSTANCE = ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl.init();

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl <em>Agv State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getAgvState()
	 * @generated
	 */
	int AGV_STATE = 0;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE__TIME_STAMP = 0;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE__SERIAL_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Last Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE__LAST_NODE_ID = 2;

	/**
	 * The feature id for the '<em><b>Battery State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE__BATTERY_STATE = 3;

	/**
	 * The feature id for the '<em><b>Errors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE__ERRORS = 4;

	/**
	 * The feature id for the '<em><b>Agvposition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE__AGVPOSITION = 5;

	/**
	 * The feature id for the '<em><b>Loads</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE__LOADS = 6;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE__MANUFACTURER = 7;

	/**
	 * The number of structural features of the '<em>Agv State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Agv State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_STATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.BatteryStateImpl <em>Battery State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.BatteryStateImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getBatteryState()
	 * @generated
	 */
	int BATTERY_STATE = 1;

	/**
	 * The feature id for the '<em><b>Battery Charge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BATTERY_STATE__BATTERY_CHARGE = 0;

	/**
	 * The number of structural features of the '<em>Battery State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BATTERY_STATE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Battery State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BATTERY_STATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.ErrorImpl <em>Error</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.ErrorImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getError()
	 * @generated
	 */
	int ERROR = 2;

	/**
	 * The feature id for the '<em><b>Error Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR__ERROR_TYPE = 0;

	/**
	 * The number of structural features of the '<em>Error</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Error</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvPositionImpl <em>Agv Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvPositionImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getAgvPosition()
	 * @generated
	 */
	int AGV_POSITION = 3;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_POSITION__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_POSITION__Y = 1;

	/**
	 * The feature id for the '<em><b>Theta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_POSITION__THETA = 2;

	/**
	 * The feature id for the '<em><b>Map Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_POSITION__MAP_ID = 3;

	/**
	 * The number of structural features of the '<em>Agv Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_POSITION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Agv Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGV_POSITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.LoadImpl <em>Load</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.LoadImpl
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getLoad()
	 * @generated
	 */
	int LOAD = 4;

	/**
	 * The feature id for the '<em><b>Load Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD__LOAD_ID = 0;

	/**
	 * The number of structural features of the '<em>Load</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Load</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState <em>Agv State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agv State</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState
	 * @generated
	 */
	EClass getAgvState();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getTimeStamp <em>Time Stamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Stamp</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getTimeStamp()
	 * @see #getAgvState()
	 * @generated
	 */
	EAttribute getAgvState_TimeStamp();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getSerialNumber <em>Serial Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Serial Number</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getSerialNumber()
	 * @see #getAgvState()
	 * @generated
	 */
	EAttribute getAgvState_SerialNumber();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getLastNodeId <em>Last Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Node Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getLastNodeId()
	 * @see #getAgvState()
	 * @generated
	 */
	EAttribute getAgvState_LastNodeId();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getBatteryState <em>Battery State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Battery State</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getBatteryState()
	 * @see #getAgvState()
	 * @generated
	 */
	EReference getAgvState_BatteryState();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getErrors <em>Errors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Errors</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getErrors()
	 * @see #getAgvState()
	 * @generated
	 */
	EReference getAgvState_Errors();

	/**
	 * Returns the meta object for the reference '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getAgvposition <em>Agvposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Agvposition</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getAgvposition()
	 * @see #getAgvState()
	 * @generated
	 */
	EReference getAgvState_Agvposition();

	/**
	 * Returns the meta object for the reference list '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getLoads <em>Loads</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Loads</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getLoads()
	 * @see #getAgvState()
	 * @generated
	 */
	EReference getAgvState_Loads();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getManufacturer <em>Manufacturer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Manufacturer</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState#getManufacturer()
	 * @see #getAgvState()
	 * @generated
	 */
	EAttribute getAgvState_Manufacturer();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState <em>Battery State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Battery State</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState
	 * @generated
	 */
	EClass getBatteryState();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState#getBatteryCharge <em>Battery Charge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Battery Charge</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState#getBatteryCharge()
	 * @see #getBatteryState()
	 * @generated
	 */
	EAttribute getBatteryState_BatteryCharge();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error <em>Error</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Error</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error
	 * @generated
	 */
	EClass getError();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error#getErrorType <em>Error Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Error Type</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error#getErrorType()
	 * @see #getError()
	 * @generated
	 */
	EAttribute getError_ErrorType();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition <em>Agv Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agv Position</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition
	 * @generated
	 */
	EClass getAgvPosition();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getX()
	 * @see #getAgvPosition()
	 * @generated
	 */
	EAttribute getAgvPosition_X();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getY()
	 * @see #getAgvPosition()
	 * @generated
	 */
	EAttribute getAgvPosition_Y();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getTheta <em>Theta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Theta</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getTheta()
	 * @see #getAgvPosition()
	 * @generated
	 */
	EAttribute getAgvPosition_Theta();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getMapId <em>Map Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Map Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition#getMapId()
	 * @see #getAgvPosition()
	 * @generated
	 */
	EAttribute getAgvPosition_MapId();

	/**
	 * Returns the meta object for class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load <em>Load</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Load</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load
	 * @generated
	 */
	EClass getLoad();

	/**
	 * Returns the meta object for the attribute '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load#getLoadId <em>Load Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Id</em>'.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load#getLoadId()
	 * @see #getLoad()
	 * @generated
	 */
	EAttribute getLoad_LoadId();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VDA5050Factory getVDA5050Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl <em>Agv State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getAgvState()
		 * @generated
		 */
		EClass AGV_STATE = eINSTANCE.getAgvState();
		/**
		 * The meta object literal for the '<em><b>Time Stamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGV_STATE__TIME_STAMP = eINSTANCE.getAgvState_TimeStamp();
		/**
		 * The meta object literal for the '<em><b>Serial Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGV_STATE__SERIAL_NUMBER = eINSTANCE.getAgvState_SerialNumber();
		/**
		 * The meta object literal for the '<em><b>Last Node Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGV_STATE__LAST_NODE_ID = eINSTANCE.getAgvState_LastNodeId();
		/**
		 * The meta object literal for the '<em><b>Battery State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGV_STATE__BATTERY_STATE = eINSTANCE.getAgvState_BatteryState();
		/**
		 * The meta object literal for the '<em><b>Errors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGV_STATE__ERRORS = eINSTANCE.getAgvState_Errors();
		/**
		 * The meta object literal for the '<em><b>Agvposition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGV_STATE__AGVPOSITION = eINSTANCE.getAgvState_Agvposition();
		/**
		 * The meta object literal for the '<em><b>Loads</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGV_STATE__LOADS = eINSTANCE.getAgvState_Loads();
		/**
		 * The meta object literal for the '<em><b>Manufacturer</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGV_STATE__MANUFACTURER = eINSTANCE.getAgvState_Manufacturer();
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.BatteryStateImpl <em>Battery State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.BatteryStateImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getBatteryState()
		 * @generated
		 */
		EClass BATTERY_STATE = eINSTANCE.getBatteryState();
		/**
		 * The meta object literal for the '<em><b>Battery Charge</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BATTERY_STATE__BATTERY_CHARGE = eINSTANCE.getBatteryState_BatteryCharge();
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.ErrorImpl <em>Error</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.ErrorImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getError()
		 * @generated
		 */
		EClass ERROR = eINSTANCE.getError();
		/**
		 * The meta object literal for the '<em><b>Error Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ERROR__ERROR_TYPE = eINSTANCE.getError_ErrorType();
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvPositionImpl <em>Agv Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvPositionImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getAgvPosition()
		 * @generated
		 */
		EClass AGV_POSITION = eINSTANCE.getAgvPosition();
		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGV_POSITION__X = eINSTANCE.getAgvPosition_X();
		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGV_POSITION__Y = eINSTANCE.getAgvPosition_Y();
		/**
		 * The meta object literal for the '<em><b>Theta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGV_POSITION__THETA = eINSTANCE.getAgvPosition_Theta();
		/**
		 * The meta object literal for the '<em><b>Map Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGV_POSITION__MAP_ID = eINSTANCE.getAgvPosition_MapId();
		/**
		 * The meta object literal for the '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.LoadImpl <em>Load</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.LoadImpl
		 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl#getLoad()
		 * @generated
		 */
		EClass LOAD = eINSTANCE.getLoad();
		/**
		 * The meta object literal for the '<em><b>Load Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOAD__LOAD_ID = eINSTANCE.getLoad_LoadId();

	}

} //VDA5050Package
