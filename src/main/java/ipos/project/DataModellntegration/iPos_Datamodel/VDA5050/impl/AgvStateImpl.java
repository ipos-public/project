/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agv State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl#getTimeStamp <em>Time Stamp</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl#getSerialNumber <em>Serial Number</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl#getLastNodeId <em>Last Node Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl#getBatteryState <em>Battery State</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl#getErrors <em>Errors</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl#getAgvposition <em>Agvposition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl#getLoads <em>Loads</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.AgvStateImpl#getManufacturer <em>Manufacturer</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AgvStateImpl extends MinimalEObjectImpl.Container implements AgvState {
	/**
	 * The default value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected static final String TIME_STAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected String timeStamp = TIME_STAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #getSerialNumber() <em>Serial Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerialNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String SERIAL_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSerialNumber() <em>Serial Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerialNumber()
	 * @generated
	 * @ordered
	 */
	protected String serialNumber = SERIAL_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastNodeId() <em>Last Node Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastNodeId()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_NODE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastNodeId() <em>Last Node Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastNodeId()
	 * @generated
	 * @ordered
	 */
	protected String lastNodeId = LAST_NODE_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBatteryState() <em>Battery State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryState()
	 * @generated
	 * @ordered
	 */
	protected BatteryState batteryState;

	/**
	 * The cached value of the '{@link #getErrors() <em>Errors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrors()
	 * @generated
	 * @ordered
	 */
	protected EList<ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error> errors;

	/**
	 * The cached value of the '{@link #getAgvposition() <em>Agvposition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgvposition()
	 * @generated
	 * @ordered
	 */
	protected AgvPosition agvposition;

	/**
	 * The cached value of the '{@link #getLoads() <em>Loads</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoads()
	 * @generated
	 * @ordered
	 */
	protected EList<Load> loads;

	/**
	 * The default value of the '{@link #getManufacturer() <em>Manufacturer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturer()
	 * @generated
	 * @ordered
	 */
	protected static final String MANUFACTURER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getManufacturer() <em>Manufacturer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturer()
	 * @generated
	 * @ordered
	 */
	protected String manufacturer = MANUFACTURER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgvStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VDA5050Package.Literals.AGV_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeStamp(String newTimeStamp) {
		String oldTimeStamp = timeStamp;
		timeStamp = newTimeStamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VDA5050Package.AGV_STATE__TIME_STAMP, oldTimeStamp,
					timeStamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSerialNumber(String newSerialNumber) {
		String oldSerialNumber = serialNumber;
		serialNumber = newSerialNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VDA5050Package.AGV_STATE__SERIAL_NUMBER,
					oldSerialNumber, serialNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastNodeId() {
		return lastNodeId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastNodeId(String newLastNodeId) {
		String oldLastNodeId = lastNodeId;
		lastNodeId = newLastNodeId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VDA5050Package.AGV_STATE__LAST_NODE_ID, oldLastNodeId,
					lastNodeId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BatteryState getBatteryState() {
		if (batteryState != null && batteryState.eIsProxy()) {
			InternalEObject oldBatteryState = (InternalEObject) batteryState;
			batteryState = (BatteryState) eResolveProxy(oldBatteryState);
			if (batteryState != oldBatteryState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VDA5050Package.AGV_STATE__BATTERY_STATE,
							oldBatteryState, batteryState));
			}
		}
		return batteryState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BatteryState basicGetBatteryState() {
		return batteryState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBatteryState(BatteryState newBatteryState) {
		BatteryState oldBatteryState = batteryState;
		batteryState = newBatteryState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VDA5050Package.AGV_STATE__BATTERY_STATE,
					oldBatteryState, batteryState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error> getErrors() {
		if (errors == null) {
			errors = new EObjectResolvingEList<ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error>(
					ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error.class, this,
					VDA5050Package.AGV_STATE__ERRORS);
		}
		return errors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgvPosition getAgvposition() {
		if (agvposition != null && agvposition.eIsProxy()) {
			InternalEObject oldAgvposition = (InternalEObject) agvposition;
			agvposition = (AgvPosition) eResolveProxy(oldAgvposition);
			if (agvposition != oldAgvposition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VDA5050Package.AGV_STATE__AGVPOSITION,
							oldAgvposition, agvposition));
			}
		}
		return agvposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgvPosition basicGetAgvposition() {
		return agvposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgvposition(AgvPosition newAgvposition) {
		AgvPosition oldAgvposition = agvposition;
		agvposition = newAgvposition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VDA5050Package.AGV_STATE__AGVPOSITION, oldAgvposition,
					agvposition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Load> getLoads() {
		if (loads == null) {
			loads = new EObjectResolvingEList<Load>(Load.class, this, VDA5050Package.AGV_STATE__LOADS);
		}
		return loads;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setManufacturer(String newManufacturer) {
		String oldManufacturer = manufacturer;
		manufacturer = newManufacturer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VDA5050Package.AGV_STATE__MANUFACTURER,
					oldManufacturer, manufacturer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case VDA5050Package.AGV_STATE__TIME_STAMP:
			return getTimeStamp();
		case VDA5050Package.AGV_STATE__SERIAL_NUMBER:
			return getSerialNumber();
		case VDA5050Package.AGV_STATE__LAST_NODE_ID:
			return getLastNodeId();
		case VDA5050Package.AGV_STATE__BATTERY_STATE:
			if (resolve)
				return getBatteryState();
			return basicGetBatteryState();
		case VDA5050Package.AGV_STATE__ERRORS:
			return getErrors();
		case VDA5050Package.AGV_STATE__AGVPOSITION:
			if (resolve)
				return getAgvposition();
			return basicGetAgvposition();
		case VDA5050Package.AGV_STATE__LOADS:
			return getLoads();
		case VDA5050Package.AGV_STATE__MANUFACTURER:
			return getManufacturer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case VDA5050Package.AGV_STATE__TIME_STAMP:
			setTimeStamp((String) newValue);
			return;
		case VDA5050Package.AGV_STATE__SERIAL_NUMBER:
			setSerialNumber((String) newValue);
			return;
		case VDA5050Package.AGV_STATE__LAST_NODE_ID:
			setLastNodeId((String) newValue);
			return;
		case VDA5050Package.AGV_STATE__BATTERY_STATE:
			setBatteryState((BatteryState) newValue);
			return;
		case VDA5050Package.AGV_STATE__ERRORS:
			getErrors().clear();
			getErrors().addAll(
					(Collection<? extends ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error>) newValue);
			return;
		case VDA5050Package.AGV_STATE__AGVPOSITION:
			setAgvposition((AgvPosition) newValue);
			return;
		case VDA5050Package.AGV_STATE__LOADS:
			getLoads().clear();
			getLoads().addAll((Collection<? extends Load>) newValue);
			return;
		case VDA5050Package.AGV_STATE__MANUFACTURER:
			setManufacturer((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case VDA5050Package.AGV_STATE__TIME_STAMP:
			setTimeStamp(TIME_STAMP_EDEFAULT);
			return;
		case VDA5050Package.AGV_STATE__SERIAL_NUMBER:
			setSerialNumber(SERIAL_NUMBER_EDEFAULT);
			return;
		case VDA5050Package.AGV_STATE__LAST_NODE_ID:
			setLastNodeId(LAST_NODE_ID_EDEFAULT);
			return;
		case VDA5050Package.AGV_STATE__BATTERY_STATE:
			setBatteryState((BatteryState) null);
			return;
		case VDA5050Package.AGV_STATE__ERRORS:
			getErrors().clear();
			return;
		case VDA5050Package.AGV_STATE__AGVPOSITION:
			setAgvposition((AgvPosition) null);
			return;
		case VDA5050Package.AGV_STATE__LOADS:
			getLoads().clear();
			return;
		case VDA5050Package.AGV_STATE__MANUFACTURER:
			setManufacturer(MANUFACTURER_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case VDA5050Package.AGV_STATE__TIME_STAMP:
			return TIME_STAMP_EDEFAULT == null ? timeStamp != null : !TIME_STAMP_EDEFAULT.equals(timeStamp);
		case VDA5050Package.AGV_STATE__SERIAL_NUMBER:
			return SERIAL_NUMBER_EDEFAULT == null ? serialNumber != null : !SERIAL_NUMBER_EDEFAULT.equals(serialNumber);
		case VDA5050Package.AGV_STATE__LAST_NODE_ID:
			return LAST_NODE_ID_EDEFAULT == null ? lastNodeId != null : !LAST_NODE_ID_EDEFAULT.equals(lastNodeId);
		case VDA5050Package.AGV_STATE__BATTERY_STATE:
			return batteryState != null;
		case VDA5050Package.AGV_STATE__ERRORS:
			return errors != null && !errors.isEmpty();
		case VDA5050Package.AGV_STATE__AGVPOSITION:
			return agvposition != null;
		case VDA5050Package.AGV_STATE__LOADS:
			return loads != null && !loads.isEmpty();
		case VDA5050Package.AGV_STATE__MANUFACTURER:
			return MANUFACTURER_EDEFAULT == null ? manufacturer != null : !MANUFACTURER_EDEFAULT.equals(manufacturer);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (timeStamp: ");
		result.append(timeStamp);
		result.append(", serialNumber: ");
		result.append(serialNumber);
		result.append(", lastNodeId: ");
		result.append(lastNodeId);
		result.append(", manufacturer: ");
		result.append(manufacturer);
		result.append(')');
		return result.toString();
	}

} //AgvStateImpl
