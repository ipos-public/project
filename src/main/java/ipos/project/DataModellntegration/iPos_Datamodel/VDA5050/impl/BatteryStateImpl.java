/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Battery State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.BatteryStateImpl#getBatteryCharge <em>Battery Charge</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BatteryStateImpl extends MinimalEObjectImpl.Container implements BatteryState {
	/**
	 * The default value of the '{@link #getBatteryCharge() <em>Battery Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryCharge()
	 * @generated
	 * @ordered
	 */
	protected static final float BATTERY_CHARGE_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getBatteryCharge() <em>Battery Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryCharge()
	 * @generated
	 * @ordered
	 */
	protected float batteryCharge = BATTERY_CHARGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BatteryStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VDA5050Package.Literals.BATTERY_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getBatteryCharge() {
		return batteryCharge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBatteryCharge(float newBatteryCharge) {
		float oldBatteryCharge = batteryCharge;
		batteryCharge = newBatteryCharge;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VDA5050Package.BATTERY_STATE__BATTERY_CHARGE,
					oldBatteryCharge, batteryCharge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case VDA5050Package.BATTERY_STATE__BATTERY_CHARGE:
			return getBatteryCharge();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case VDA5050Package.BATTERY_STATE__BATTERY_CHARGE:
			setBatteryCharge((Float) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case VDA5050Package.BATTERY_STATE__BATTERY_CHARGE:
			setBatteryCharge(BATTERY_CHARGE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case VDA5050Package.BATTERY_STATE__BATTERY_CHARGE:
			return batteryCharge != BATTERY_CHARGE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (batteryCharge: ");
		result.append(batteryCharge);
		result.append(')');
		return result.toString();
	}

} //BatteryStateImpl
