/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Load</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.LoadImpl#getLoadId <em>Load Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LoadImpl extends MinimalEObjectImpl.Container implements Load {
	/**
	 * The default value of the '{@link #getLoadId() <em>Load Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadId()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadId() <em>Load Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadId()
	 * @generated
	 * @ordered
	 */
	protected String loadId = LOAD_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoadImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VDA5050Package.Literals.LOAD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLoadId() {
		return loadId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoadId(String newLoadId) {
		String oldLoadId = loadId;
		loadId = newLoadId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VDA5050Package.LOAD__LOAD_ID, oldLoadId, loadId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case VDA5050Package.LOAD__LOAD_ID:
			return getLoadId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case VDA5050Package.LOAD__LOAD_ID:
			setLoadId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case VDA5050Package.LOAD__LOAD_ID:
			setLoadId(LOAD_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case VDA5050Package.LOAD__LOAD_ID:
			return LOAD_ID_EDEFAULT == null ? loadId != null : !LOAD_ID_EDEFAULT.equals(loadId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (loadId: ");
		result.append(loadId);
		result.append(')');
		return result.toString();
	}

} //LoadImpl
