/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Factory;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VDA5050FactoryImpl extends EFactoryImpl implements VDA5050Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VDA5050Factory init() {
		try {
			VDA5050Factory theVDA5050Factory = (VDA5050Factory) EPackage.Registry.INSTANCE
					.getEFactory(VDA5050Package.eNS_URI);
			if (theVDA5050Factory != null) {
				return theVDA5050Factory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VDA5050FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VDA5050FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case VDA5050Package.AGV_STATE:
			return createAgvState();
		case VDA5050Package.BATTERY_STATE:
			return createBatteryState();
		case VDA5050Package.ERROR:
			return createError();
		case VDA5050Package.AGV_POSITION:
			return createAgvPosition();
		case VDA5050Package.LOAD:
			return createLoad();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgvState createAgvState() {
		AgvStateImpl agvState = new AgvStateImpl();
		return agvState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BatteryState createBatteryState() {
		BatteryStateImpl batteryState = new BatteryStateImpl();
		return batteryState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error createError() {
		ErrorImpl error = new ErrorImpl();
		return error;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgvPosition createAgvPosition() {
		AgvPositionImpl agvPosition = new AgvPositionImpl();
		return agvPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Load createLoad() {
		LoadImpl load = new LoadImpl();
		return load;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VDA5050Package getVDA5050Package() {
		return (VDA5050Package) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VDA5050Package getPackage() {
		return VDA5050Package.eINSTANCE;
	}

} //VDA5050FactoryImpl
