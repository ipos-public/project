/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Factory;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;

import ipos.project.DataModellntegration.iPos_Datamodel.impl.IPos_DatamodelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VDA5050PackageImpl extends EPackageImpl implements VDA5050Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agvStateEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass batteryStateEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass errorEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agvPositionEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loadEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VDA5050PackageImpl() {
		super(eNS_URI, VDA5050Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link VDA5050Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VDA5050Package init() {
		if (isInited)
			return (VDA5050Package) EPackage.Registry.INSTANCE.getEPackage(VDA5050Package.eNS_URI);

		// Obtain or create and register package
		Object registeredVDA5050Package = EPackage.Registry.INSTANCE.get(eNS_URI);
		VDA5050PackageImpl theVDA5050Package = registeredVDA5050Package instanceof VDA5050PackageImpl
				? (VDA5050PackageImpl) registeredVDA5050Package
				: new VDA5050PackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPos_DatamodelPackage.eNS_URI);
		IPos_DatamodelPackageImpl theIPos_DatamodelPackage = (IPos_DatamodelPackageImpl) (registeredPackage instanceof IPos_DatamodelPackageImpl
				? registeredPackage
				: IPos_DatamodelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OFBizPackage.eNS_URI);
		OFBizPackageImpl theOFBizPackage = (OFBizPackageImpl) (registeredPackage instanceof OFBizPackageImpl
				? registeredPackage
				: OFBizPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPosDevKitPackage.eNS_URI);
		IPosDevKitPackageImpl theIPosDevKitPackage = (IPosDevKitPackageImpl) (registeredPackage instanceof IPosDevKitPackageImpl
				? registeredPackage
				: IPosDevKitPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ToozPackage.eNS_URI);
		ToozPackageImpl theToozPackage = (ToozPackageImpl) (registeredPackage instanceof ToozPackageImpl
				? registeredPackage
				: ToozPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OSMPackage.eNS_URI);
		OSMPackageImpl theOSMPackage = (OSMPackageImpl) (registeredPackage instanceof OSMPackageImpl ? registeredPackage
				: OSMPackage.eINSTANCE);

		// Create package meta-data objects
		theVDA5050Package.createPackageContents();
		theIPos_DatamodelPackage.createPackageContents();
		theOFBizPackage.createPackageContents();
		theIPosDevKitPackage.createPackageContents();
		theToozPackage.createPackageContents();
		theOSMPackage.createPackageContents();

		// Initialize created meta-data
		theVDA5050Package.initializePackageContents();
		theIPos_DatamodelPackage.initializePackageContents();
		theOFBizPackage.initializePackageContents();
		theIPosDevKitPackage.initializePackageContents();
		theToozPackage.initializePackageContents();
		theOSMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVDA5050Package.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VDA5050Package.eNS_URI, theVDA5050Package);
		return theVDA5050Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgvState() {
		return agvStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgvState_TimeStamp() {
		return (EAttribute) agvStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgvState_SerialNumber() {
		return (EAttribute) agvStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgvState_LastNodeId() {
		return (EAttribute) agvStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgvState_BatteryState() {
		return (EReference) agvStateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgvState_Errors() {
		return (EReference) agvStateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgvState_Agvposition() {
		return (EReference) agvStateEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgvState_Loads() {
		return (EReference) agvStateEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgvState_Manufacturer() {
		return (EAttribute) agvStateEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBatteryState() {
		return batteryStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBatteryState_BatteryCharge() {
		return (EAttribute) batteryStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getError() {
		return errorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getError_ErrorType() {
		return (EAttribute) errorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgvPosition() {
		return agvPositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgvPosition_X() {
		return (EAttribute) agvPositionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgvPosition_Y() {
		return (EAttribute) agvPositionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgvPosition_Theta() {
		return (EAttribute) agvPositionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgvPosition_MapId() {
		return (EAttribute) agvPositionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLoad() {
		return loadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoad_LoadId() {
		return (EAttribute) loadEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VDA5050Factory getVDA5050Factory() {
		return (VDA5050Factory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		agvStateEClass = createEClass(AGV_STATE);
		createEAttribute(agvStateEClass, AGV_STATE__TIME_STAMP);
		createEAttribute(agvStateEClass, AGV_STATE__SERIAL_NUMBER);
		createEAttribute(agvStateEClass, AGV_STATE__LAST_NODE_ID);
		createEReference(agvStateEClass, AGV_STATE__BATTERY_STATE);
		createEReference(agvStateEClass, AGV_STATE__ERRORS);
		createEReference(agvStateEClass, AGV_STATE__AGVPOSITION);
		createEReference(agvStateEClass, AGV_STATE__LOADS);
		createEAttribute(agvStateEClass, AGV_STATE__MANUFACTURER);

		batteryStateEClass = createEClass(BATTERY_STATE);
		createEAttribute(batteryStateEClass, BATTERY_STATE__BATTERY_CHARGE);

		errorEClass = createEClass(ERROR);
		createEAttribute(errorEClass, ERROR__ERROR_TYPE);

		agvPositionEClass = createEClass(AGV_POSITION);
		createEAttribute(agvPositionEClass, AGV_POSITION__X);
		createEAttribute(agvPositionEClass, AGV_POSITION__Y);
		createEAttribute(agvPositionEClass, AGV_POSITION__THETA);
		createEAttribute(agvPositionEClass, AGV_POSITION__MAP_ID);

		loadEClass = createEClass(LOAD);
		createEAttribute(loadEClass, LOAD__LOAD_ID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(agvStateEClass, AgvState.class, "AgvState", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAgvState_TimeStamp(), ecorePackage.getEString(), "timeStamp", null, 0, 1, AgvState.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgvState_SerialNumber(), ecorePackage.getEString(), "serialNumber", null, 0, 1,
				AgvState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgvState_LastNodeId(), ecorePackage.getEString(), "lastNodeId", null, 0, 1, AgvState.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgvState_BatteryState(), this.getBatteryState(), null, "batteryState", null, 0, 1,
				AgvState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgvState_Errors(), this.getError(), null, "errors", null, 0, -1, AgvState.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgvState_Agvposition(), this.getAgvPosition(), null, "agvposition", null, 0, 1,
				AgvState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgvState_Loads(), this.getLoad(), null, "loads", null, 0, -1, AgvState.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getAgvState_Manufacturer(), ecorePackage.getEString(), "manufacturer", null, 0, 1,
				AgvState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(batteryStateEClass, BatteryState.class, "BatteryState", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBatteryState_BatteryCharge(), ecorePackage.getEFloat(), "batteryCharge", null, 0, 1,
				BatteryState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(errorEClass, ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error.class, "Error",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getError_ErrorType(), ecorePackage.getEString(), "errorType", null, 0, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agvPositionEClass, AgvPosition.class, "AgvPosition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAgvPosition_X(), ecorePackage.getEFloat(), "x", null, 0, 1, AgvPosition.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgvPosition_Y(), ecorePackage.getEFloat(), "y", null, 0, 1, AgvPosition.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgvPosition_Theta(), ecorePackage.getEFloat(), "theta", null, 0, 1, AgvPosition.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgvPosition_MapId(), ecorePackage.getEString(), "mapId", null, 0, 1, AgvPosition.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(loadEClass, Load.class, "Load", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLoad_LoadId(), ecorePackage.getEString(), "loadId", null, 0, 1, Load.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //VDA5050PackageImpl
