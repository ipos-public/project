/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.util;

import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package
 * @generated
 */
public class VDA5050AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VDA5050Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VDA5050AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = VDA5050Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VDA5050Switch<Adapter> modelSwitch = new VDA5050Switch<Adapter>() {
		@Override
		public Adapter caseAgvState(AgvState object) {
			return createAgvStateAdapter();
		}

		@Override
		public Adapter caseBatteryState(BatteryState object) {
			return createBatteryStateAdapter();
		}

		@Override
		public Adapter caseError(ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error object) {
			return createErrorAdapter();
		}

		@Override
		public Adapter caseAgvPosition(AgvPosition object) {
			return createAgvPositionAdapter();
		}

		@Override
		public Adapter caseLoad(Load object) {
			return createLoadAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState <em>Agv State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvState
	 * @generated
	 */
	public Adapter createAgvStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState <em>Battery State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.BatteryState
	 * @generated
	 */
	public Adapter createBatteryStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error <em>Error</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Error
	 * @generated
	 */
	public Adapter createErrorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition <em>Agv Position</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.AgvPosition
	 * @generated
	 */
	public Adapter createAgvPositionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load <em>Load</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.Load
	 * @generated
	 */
	public Adapter createLoadAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //VDA5050AdapterFactory
