/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>World Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getAgent <em>Agent</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getZoneMap <em>Zone Map</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getPois <em>Pois</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel#getReferenceSystem <em>Reference System</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getWorldModel()
 * @model
 * @generated
 */
public interface WorldModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Agent</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.Agent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getWorldModel_Agent()
	 * @model
	 * @generated
	 */
	EList<Agent> getAgent();

	/**
	 * Returns the value of the '<em><b>Zone Map</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zone Map</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getWorldModel_ZoneMap()
	 * @model
	 * @generated
	 */
	EList<ZoneMap> getZoneMap();

	/**
	 * Returns the value of the '<em><b>Pois</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.POI}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pois</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getWorldModel_Pois()
	 * @model
	 * @generated
	 */
	EList<POI> getPois();

	/**
	 * Returns the value of the '<em><b>Reference System</b></em>' reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference System</em>' reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getWorldModel_ReferenceSystem()
	 * @model
	 * @generated
	 */
	EList<ReferenceSystem> getReferenceSystem();

} // WorldModel
