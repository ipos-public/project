/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Zone Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap#getZone <em>Zone</em>}</li>
 * </ul>
 *
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getZoneMap()
 * @model
 * @generated
 */
public interface ZoneMap extends MapType {
	/**
	 * Returns the value of the '<em><b>Zone</b></em>' containment reference list.
	 * The list contents are of type {@link ipos.project.DataModellntegration.iPos_Datamodel.Zone}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zone</em>' containment reference list.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#getZoneMap_Zone()
	 * @model containment="true"
	 * @generated
	 */
	EList<Zone> getZone();

} // ZoneMap
