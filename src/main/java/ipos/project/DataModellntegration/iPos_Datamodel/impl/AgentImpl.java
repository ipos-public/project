/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Agent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.AgentImpl#getLObject <em>LObject</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.AgentImpl#getAgentType <em>Agent Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AgentImpl extends EntityImpl implements Agent {
	/**
	 * The cached value of the '{@link #getLObject() <em>LObject</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLObject()
	 * @generated
	 * @ordered
	 */
	protected EList<LocalizableObject> lObject;

	/**
	 * The default value of the '{@link #getAgentType() <em>Agent Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentType()
	 * @generated
	 * @ordered
	 */
	protected static final String AGENT_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAgentType() <em>Agent Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentType()
	 * @generated
	 * @ordered
	 */
	protected String agentType = AGENT_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LocalizableObject> getLObject() {
		if (lObject == null) {
			lObject = new EObjectResolvingEList<LocalizableObject>(LocalizableObject.class, this,
					IPos_DatamodelPackage.AGENT__LOBJECT);
		}
		return lObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAgentType() {
		return agentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentType(String newAgentType) {
		String oldAgentType = agentType;
		agentType = newAgentType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.AGENT__AGENT_TYPE, oldAgentType,
					agentType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.AGENT__LOBJECT:
			return getLObject();
		case IPos_DatamodelPackage.AGENT__AGENT_TYPE:
			return getAgentType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.AGENT__LOBJECT:
			getLObject().clear();
			getLObject().addAll((Collection<? extends LocalizableObject>) newValue);
			return;
		case IPos_DatamodelPackage.AGENT__AGENT_TYPE:
			setAgentType((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.AGENT__LOBJECT:
			getLObject().clear();
			return;
		case IPos_DatamodelPackage.AGENT__AGENT_TYPE:
			setAgentType(AGENT_TYPE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.AGENT__LOBJECT:
			return lObject != null && !lObject.isEmpty();
		case IPos_DatamodelPackage.AGENT__AGENT_TYPE:
			return AGENT_TYPE_EDEFAULT == null ? agentType != null : !AGENT_TYPE_EDEFAULT.equals(agentType);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (agentType: ");
		result.append(agentType);
		result.append(')');
		return result.toString();
	}

} //AgentImpl
