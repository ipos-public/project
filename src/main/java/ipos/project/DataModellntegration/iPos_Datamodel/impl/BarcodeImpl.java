/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Barcode;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Barcode</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BarcodeImpl extends ProximityImpl implements Barcode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BarcodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.BARCODE;
	}

} //BarcodeImpl
