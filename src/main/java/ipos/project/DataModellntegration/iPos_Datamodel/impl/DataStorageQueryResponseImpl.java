/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Storage Query Response</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.DataStorageQueryResponseImpl#getPositionEvents <em>Position Events</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.DataStorageQueryResponseImpl#getTrackingTaskId <em>Tracking Task Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataStorageQueryResponseImpl extends MinimalEObjectImpl.Container implements DataStorageQueryResponse {
	/**
	 * The cached value of the '{@link #getPositionEvents() <em>Position Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<PositionEvent> positionEvents;

	/**
	 * The default value of the '{@link #getTrackingTaskId() <em>Tracking Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackingTaskId()
	 * @generated
	 * @ordered
	 */
	protected static final String TRACKING_TASK_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTrackingTaskId() <em>Tracking Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackingTaskId()
	 * @generated
	 * @ordered
	 */
	protected String trackingTaskId = TRACKING_TASK_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataStorageQueryResponseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.DATA_STORAGE_QUERY_RESPONSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PositionEvent> getPositionEvents() {
		if (positionEvents == null) {
			positionEvents = new EObjectResolvingEList<PositionEvent>(PositionEvent.class, this,
					IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__POSITION_EVENTS);
		}
		return positionEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTrackingTaskId() {
		return trackingTaskId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrackingTaskId(String newTrackingTaskId) {
		String oldTrackingTaskId = trackingTaskId;
		trackingTaskId = newTrackingTaskId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__TRACKING_TASK_ID, oldTrackingTaskId,
					trackingTaskId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__POSITION_EVENTS:
			return getPositionEvents();
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__TRACKING_TASK_ID:
			return getTrackingTaskId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__POSITION_EVENTS:
			getPositionEvents().clear();
			getPositionEvents().addAll((Collection<? extends PositionEvent>) newValue);
			return;
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__TRACKING_TASK_ID:
			setTrackingTaskId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__POSITION_EVENTS:
			getPositionEvents().clear();
			return;
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__TRACKING_TASK_ID:
			setTrackingTaskId(TRACKING_TASK_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__POSITION_EVENTS:
			return positionEvents != null && !positionEvents.isEmpty();
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE__TRACKING_TASK_ID:
			return TRACKING_TASK_ID_EDEFAULT == null ? trackingTaskId != null
					: !TRACKING_TASK_ID_EDEFAULT.equals(trackingTaskId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (trackingTaskId: ");
		result.append(trackingTaskId);
		result.append(')');
		return result.toString();
	}

} //DataStorageQueryResponseImpl
