/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Filter Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getTimeCondition <em>Time Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getAccuracyCondition <em>Accuracy Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getPositionCondition <em>Position Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getTimeMinInterval <em>Time Min Interval</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getPositionDelta <em>Position Delta</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getSensorIdCondition <em>Sensor Id Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getFilterStructure <em>Filter Structure</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getPositionConditionCells <em>Position Condition Cells</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getIdCondition <em>Id Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getCategoryCondition <em>Category Condition</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConditionImpl#getPropertyCondition <em>Property Condition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventFilterConditionImpl extends MinimalEObjectImpl.Container implements EventFilterCondition {
	/**
	 * The default value of the '{@link #getTimeCondition() <em>Time Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeCondition()
	 * @generated
	 * @ordered
	 */
	protected static final ArrayList<String[]> TIME_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeCondition() <em>Time Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeCondition()
	 * @generated
	 * @ordered
	 */
	protected ArrayList<String[]> timeCondition = TIME_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getAccuracyCondition() <em>Accuracy Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracyCondition()
	 * @generated
	 * @ordered
	 */
	protected static final float ACCURACY_CONDITION_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getAccuracyCondition() <em>Accuracy Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracyCondition()
	 * @generated
	 * @ordered
	 */
	protected float accuracyCondition = ACCURACY_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPositionCondition() <em>Position Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionCondition()
	 * @generated
	 * @ordered
	 */
	protected static final ArrayList<Float[]> POSITION_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPositionCondition() <em>Position Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionCondition()
	 * @generated
	 * @ordered
	 */
	protected ArrayList<Float[]> positionCondition = POSITION_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeMinInterval() <em>Time Min Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeMinInterval()
	 * @generated
	 * @ordered
	 */
	protected static final int TIME_MIN_INTERVAL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTimeMinInterval() <em>Time Min Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeMinInterval()
	 * @generated
	 * @ordered
	 */
	protected int timeMinInterval = TIME_MIN_INTERVAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getPositionDelta() <em>Position Delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionDelta()
	 * @generated
	 * @ordered
	 */
	protected static final float POSITION_DELTA_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getPositionDelta() <em>Position Delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionDelta()
	 * @generated
	 * @ordered
	 */
	protected float positionDelta = POSITION_DELTA_EDEFAULT;

	/**
	 * The default value of the '{@link #getSensorIdCondition() <em>Sensor Id Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorIdCondition()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> SENSOR_ID_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorIdCondition() <em>Sensor Id Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorIdCondition()
	 * @generated
	 * @ordered
	 */
	protected List<String> sensorIdCondition = SENSOR_ID_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getFilterStructure() <em>Filter Structure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterStructure()
	 * @generated
	 * @ordered
	 */
	protected static final boolean[] FILTER_STRUCTURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilterStructure() <em>Filter Structure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterStructure()
	 * @generated
	 * @ordered
	 */
	protected boolean[] filterStructure = FILTER_STRUCTURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPositionConditionCells() <em>Position Condition Cells</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionConditionCells()
	 * @generated
	 * @ordered
	 */
	protected Map<String, ArrayList<Float[][]>> positionConditionCells;

	/**
	 * The default value of the '{@link #getIdCondition() <em>Id Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdCondition()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> ID_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIdCondition() <em>Id Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdCondition()
	 * @generated
	 * @ordered
	 */
	protected List<String> idCondition = ID_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCategoryCondition() <em>Category Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategoryCondition()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> CATEGORY_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCategoryCondition() <em>Category Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategoryCondition()
	 * @generated
	 * @ordered
	 */
	protected List<String> categoryCondition = CATEGORY_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPropertyCondition() <em>Property Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyCondition()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> PROPERTY_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPropertyCondition() <em>Property Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyCondition()
	 * @generated
	 * @ordered
	 */
	protected List<String> propertyCondition = PROPERTY_CONDITION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventFilterConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.EVENT_FILTER_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayList<String[]> getTimeCondition() {
		return timeCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeCondition(ArrayList<String[]> newTimeCondition) {
		ArrayList<String[]> oldTimeCondition = timeCondition;
		timeCondition = newTimeCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_CONDITION, oldTimeCondition, timeCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getAccuracyCondition() {
		return accuracyCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccuracyCondition(float newAccuracyCondition) {
		float oldAccuracyCondition = accuracyCondition;
		accuracyCondition = newAccuracyCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ACCURACY_CONDITION, oldAccuracyCondition,
					accuracyCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayList<Float[]> getPositionCondition() {
		return positionCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionCondition(ArrayList<Float[]> newPositionCondition) {
		ArrayList<Float[]> oldPositionCondition = positionCondition;
		positionCondition = newPositionCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION, oldPositionCondition,
					positionCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTimeMinInterval() {
		return timeMinInterval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeMinInterval(int newTimeMinInterval) {
		int oldTimeMinInterval = timeMinInterval;
		timeMinInterval = newTimeMinInterval;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_MIN_INTERVAL, oldTimeMinInterval,
					timeMinInterval));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getPositionDelta() {
		return positionDelta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionDelta(float newPositionDelta) {
		float oldPositionDelta = positionDelta;
		positionDelta = newPositionDelta;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_DELTA, oldPositionDelta, positionDelta));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getSensorIdCondition() {
		return sensorIdCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorIdCondition(List<String> newSensorIdCondition) {
		List<String> oldSensorIdCondition = sensorIdCondition;
		sensorIdCondition = newSensorIdCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__SENSOR_ID_CONDITION, oldSensorIdCondition,
					sensorIdCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean[] getFilterStructure() {
		return filterStructure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterStructure(boolean[] newFilterStructure) {
		boolean[] oldFilterStructure = filterStructure;
		filterStructure = newFilterStructure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__FILTER_STRUCTURE, oldFilterStructure,
					filterStructure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<String, ArrayList<Float[][]>> getPositionConditionCells() {
		return positionConditionCells;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionConditionCells(Map<String, ArrayList<Float[][]>> newPositionConditionCells) {
		Map<String, ArrayList<Float[][]>> oldPositionConditionCells = positionConditionCells;
		positionConditionCells = newPositionConditionCells;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION_CELLS, oldPositionConditionCells,
					positionConditionCells));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getIdCondition() {
		return idCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdCondition(List<String> newIdCondition) {
		List<String> oldIdCondition = idCondition;
		idCondition = newIdCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ID_CONDITION, oldIdCondition, idCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getCategoryCondition() {
		return categoryCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategoryCondition(List<String> newCategoryCondition) {
		List<String> oldCategoryCondition = categoryCondition;
		categoryCondition = newCategoryCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__CATEGORY_CONDITION, oldCategoryCondition,
					categoryCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getPropertyCondition() {
		return propertyCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyCondition(List<String> newPropertyCondition) {
		List<String> oldPropertyCondition = propertyCondition;
		propertyCondition = newPropertyCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONDITION__PROPERTY_CONDITION, oldPropertyCondition,
					propertyCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_CONDITION:
			return getTimeCondition();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ACCURACY_CONDITION:
			return getAccuracyCondition();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION:
			return getPositionCondition();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_MIN_INTERVAL:
			return getTimeMinInterval();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_DELTA:
			return getPositionDelta();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__SENSOR_ID_CONDITION:
			return getSensorIdCondition();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__FILTER_STRUCTURE:
			return getFilterStructure();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION_CELLS:
			return getPositionConditionCells();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ID_CONDITION:
			return getIdCondition();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__CATEGORY_CONDITION:
			return getCategoryCondition();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__PROPERTY_CONDITION:
			return getPropertyCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_CONDITION:
			setTimeCondition((ArrayList<String[]>) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ACCURACY_CONDITION:
			setAccuracyCondition((Float) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION:
			setPositionCondition((ArrayList<Float[]>) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_MIN_INTERVAL:
			setTimeMinInterval((Integer) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_DELTA:
			setPositionDelta((Float) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__SENSOR_ID_CONDITION:
			setSensorIdCondition((List<String>) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__FILTER_STRUCTURE:
			setFilterStructure((boolean[]) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION_CELLS:
			setPositionConditionCells((Map<String, ArrayList<Float[][]>>) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ID_CONDITION:
			setIdCondition((List<String>) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__CATEGORY_CONDITION:
			setCategoryCondition((List<String>) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__PROPERTY_CONDITION:
			setPropertyCondition((List<String>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_CONDITION:
			setTimeCondition(TIME_CONDITION_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ACCURACY_CONDITION:
			setAccuracyCondition(ACCURACY_CONDITION_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION:
			setPositionCondition(POSITION_CONDITION_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_MIN_INTERVAL:
			setTimeMinInterval(TIME_MIN_INTERVAL_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_DELTA:
			setPositionDelta(POSITION_DELTA_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__SENSOR_ID_CONDITION:
			setSensorIdCondition(SENSOR_ID_CONDITION_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__FILTER_STRUCTURE:
			setFilterStructure(FILTER_STRUCTURE_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION_CELLS:
			setPositionConditionCells((Map<String, ArrayList<Float[][]>>) null);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ID_CONDITION:
			setIdCondition(ID_CONDITION_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__CATEGORY_CONDITION:
			setCategoryCondition(CATEGORY_CONDITION_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__PROPERTY_CONDITION:
			setPropertyCondition(PROPERTY_CONDITION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_CONDITION:
			return TIME_CONDITION_EDEFAULT == null ? timeCondition != null
					: !TIME_CONDITION_EDEFAULT.equals(timeCondition);
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ACCURACY_CONDITION:
			return accuracyCondition != ACCURACY_CONDITION_EDEFAULT;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION:
			return POSITION_CONDITION_EDEFAULT == null ? positionCondition != null
					: !POSITION_CONDITION_EDEFAULT.equals(positionCondition);
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__TIME_MIN_INTERVAL:
			return timeMinInterval != TIME_MIN_INTERVAL_EDEFAULT;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_DELTA:
			return positionDelta != POSITION_DELTA_EDEFAULT;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__SENSOR_ID_CONDITION:
			return SENSOR_ID_CONDITION_EDEFAULT == null ? sensorIdCondition != null
					: !SENSOR_ID_CONDITION_EDEFAULT.equals(sensorIdCondition);
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__FILTER_STRUCTURE:
			return FILTER_STRUCTURE_EDEFAULT == null ? filterStructure != null
					: !FILTER_STRUCTURE_EDEFAULT.equals(filterStructure);
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__POSITION_CONDITION_CELLS:
			return positionConditionCells != null;
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__ID_CONDITION:
			return ID_CONDITION_EDEFAULT == null ? idCondition != null : !ID_CONDITION_EDEFAULT.equals(idCondition);
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__CATEGORY_CONDITION:
			return CATEGORY_CONDITION_EDEFAULT == null ? categoryCondition != null
					: !CATEGORY_CONDITION_EDEFAULT.equals(categoryCondition);
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION__PROPERTY_CONDITION:
			return PROPERTY_CONDITION_EDEFAULT == null ? propertyCondition != null
					: !PROPERTY_CONDITION_EDEFAULT.equals(propertyCondition);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (timeCondition: ");
		result.append(timeCondition);
		result.append(", accuracyCondition: ");
		result.append(accuracyCondition);
		result.append(", positionCondition: ");
		result.append(positionCondition);
		result.append(", timeMinInterval: ");
		result.append(timeMinInterval);
		result.append(", positionDelta: ");
		result.append(positionDelta);
		result.append(", sensorIdCondition: ");
		result.append(sensorIdCondition);
		result.append(", filterStructure: ");
		result.append(filterStructure);
		result.append(", positionConditionCells: ");
		result.append(positionConditionCells);
		result.append(", idCondition: ");
		result.append(idCondition);
		result.append(", categoryCondition: ");
		result.append(categoryCondition);
		result.append(", propertyCondition: ");
		result.append(propertyCondition);
		result.append(')');
		return result.toString();
	}

} //EventFilterConditionImpl
