/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask;
import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Filter Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConfigurationImpl#getPositionAmbiguityStrategy <em>Position Ambiguity Strategy</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConfigurationImpl#getPositionAmbiguityParameters <em>Position Ambiguity Parameters</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConfigurationImpl#getFilterCriteria <em>Filter Criteria</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.EventFilterConfigurationImpl#getEventfiltercondition <em>Eventfiltercondition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventFilterConfigurationImpl extends MinimalEObjectImpl.Container implements EventFilterConfiguration {
	/**
	 * The default value of the '{@link #getPositionAmbiguityStrategy() <em>Position Ambiguity Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionAmbiguityStrategy()
	 * @generated
	 * @ordered
	 */
	protected static final String POSITION_AMBIGUITY_STRATEGY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPositionAmbiguityStrategy() <em>Position Ambiguity Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionAmbiguityStrategy()
	 * @generated
	 * @ordered
	 */
	protected String positionAmbiguityStrategy = POSITION_AMBIGUITY_STRATEGY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPositionAmbiguityParameters() <em>Position Ambiguity Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionAmbiguityParameters()
	 * @generated
	 * @ordered
	 */
	protected Map<String, String> positionAmbiguityParameters;

	/**
	 * The cached value of the '{@link #getFilterCriteria() <em>Filter Criteria</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterCriteria()
	 * @generated
	 * @ordered
	 */
	protected Map<String, EObject> filterCriteria;

	/**
	 * The cached value of the '{@link #getEventfiltercondition() <em>Eventfiltercondition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventfiltercondition()
	 * @generated
	 * @ordered
	 */
	protected EList<TrackingTask> eventfiltercondition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventFilterConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.EVENT_FILTER_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPositionAmbiguityStrategy() {
		return positionAmbiguityStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionAmbiguityStrategy(String newPositionAmbiguityStrategy) {
		String oldPositionAmbiguityStrategy = positionAmbiguityStrategy;
		positionAmbiguityStrategy = newPositionAmbiguityStrategy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_STRATEGY,
					oldPositionAmbiguityStrategy, positionAmbiguityStrategy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<String, String> getPositionAmbiguityParameters() {
		return positionAmbiguityParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionAmbiguityParameters(Map<String, String> newPositionAmbiguityParameters) {
		Map<String, String> oldPositionAmbiguityParameters = positionAmbiguityParameters;
		positionAmbiguityParameters = newPositionAmbiguityParameters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_PARAMETERS,
					oldPositionAmbiguityParameters, positionAmbiguityParameters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<String, EObject> getFilterCriteria() {
		return filterCriteria;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterCriteria(Map<String, EObject> newFilterCriteria) {
		Map<String, EObject> oldFilterCriteria = filterCriteria;
		filterCriteria = newFilterCriteria;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__FILTER_CRITERIA, oldFilterCriteria,
					filterCriteria));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TrackingTask> getEventfiltercondition() {
		if (eventfiltercondition == null) {
			eventfiltercondition = new EObjectResolvingEList<TrackingTask>(TrackingTask.class, this,
					IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__EVENTFILTERCONDITION);
		}
		return eventfiltercondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_STRATEGY:
			return getPositionAmbiguityStrategy();
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_PARAMETERS:
			return getPositionAmbiguityParameters();
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__FILTER_CRITERIA:
			return getFilterCriteria();
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__EVENTFILTERCONDITION:
			return getEventfiltercondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_STRATEGY:
			setPositionAmbiguityStrategy((String) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_PARAMETERS:
			setPositionAmbiguityParameters((Map<String, String>) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__FILTER_CRITERIA:
			setFilterCriteria((Map<String, EObject>) newValue);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__EVENTFILTERCONDITION:
			getEventfiltercondition().clear();
			getEventfiltercondition().addAll((Collection<? extends TrackingTask>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_STRATEGY:
			setPositionAmbiguityStrategy(POSITION_AMBIGUITY_STRATEGY_EDEFAULT);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_PARAMETERS:
			setPositionAmbiguityParameters((Map<String, String>) null);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__FILTER_CRITERIA:
			setFilterCriteria((Map<String, EObject>) null);
			return;
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__EVENTFILTERCONDITION:
			getEventfiltercondition().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_STRATEGY:
			return POSITION_AMBIGUITY_STRATEGY_EDEFAULT == null ? positionAmbiguityStrategy != null
					: !POSITION_AMBIGUITY_STRATEGY_EDEFAULT.equals(positionAmbiguityStrategy);
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_PARAMETERS:
			return positionAmbiguityParameters != null;
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__FILTER_CRITERIA:
			return filterCriteria != null;
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION__EVENTFILTERCONDITION:
			return eventfiltercondition != null && !eventfiltercondition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (positionAmbiguityStrategy: ");
		result.append(positionAmbiguityStrategy);
		result.append(", positionAmbiguityParameters: ");
		result.append(positionAmbiguityParameters);
		result.append(", filterCriteria: ");
		result.append(filterCriteria);
		result.append(')');
		return result.toString();
	}

} //EventFilterConfigurationImpl
