/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Acceleration;
import ipos.project.DataModellntegration.iPos_Datamodel.IMU;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.Quaternion;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IMU</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.IMUImpl#getAngularrate <em>Angularrate</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.IMUImpl#getAcceleration <em>Acceleration</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.IMUImpl#getSensorId <em>Sensor Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IMUImpl extends RawdataEventImpl implements IMU {
	/**
	 * The cached value of the '{@link #getAngularrate() <em>Angularrate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAngularrate()
	 * @generated
	 * @ordered
	 */
	protected Quaternion angularrate;

	/**
	 * The cached value of the '{@link #getAcceleration() <em>Acceleration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcceleration()
	 * @generated
	 * @ordered
	 */
	protected Acceleration acceleration;

	/**
	 * The default value of the '{@link #getSensorId() <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorId()
	 * @generated
	 * @ordered
	 */
	protected static final String SENSOR_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorId() <em>Sensor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorId()
	 * @generated
	 * @ordered
	 */
	protected String sensorId = SENSOR_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IMUImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.IMU;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Quaternion getAngularrate() {
		if (angularrate != null && angularrate.eIsProxy()) {
			InternalEObject oldAngularrate = (InternalEObject) angularrate;
			angularrate = (Quaternion) eResolveProxy(oldAngularrate);
			if (angularrate != oldAngularrate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IPos_DatamodelPackage.IMU__ANGULARRATE,
							oldAngularrate, angularrate));
			}
		}
		return angularrate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Quaternion basicGetAngularrate() {
		return angularrate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAngularrate(Quaternion newAngularrate) {
		Quaternion oldAngularrate = angularrate;
		angularrate = newAngularrate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.IMU__ANGULARRATE,
					oldAngularrate, angularrate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Acceleration getAcceleration() {
		if (acceleration != null && acceleration.eIsProxy()) {
			InternalEObject oldAcceleration = (InternalEObject) acceleration;
			acceleration = (Acceleration) eResolveProxy(oldAcceleration);
			if (acceleration != oldAcceleration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IPos_DatamodelPackage.IMU__ACCELERATION,
							oldAcceleration, acceleration));
			}
		}
		return acceleration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Acceleration basicGetAcceleration() {
		return acceleration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceleration(Acceleration newAcceleration) {
		Acceleration oldAcceleration = acceleration;
		acceleration = newAcceleration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.IMU__ACCELERATION,
					oldAcceleration, acceleration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSensorId() {
		return sensorId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorId(String newSensorId) {
		String oldSensorId = sensorId;
		sensorId = newSensorId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.IMU__SENSOR_ID, oldSensorId,
					sensorId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.IMU__ANGULARRATE:
			if (resolve)
				return getAngularrate();
			return basicGetAngularrate();
		case IPos_DatamodelPackage.IMU__ACCELERATION:
			if (resolve)
				return getAcceleration();
			return basicGetAcceleration();
		case IPos_DatamodelPackage.IMU__SENSOR_ID:
			return getSensorId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.IMU__ANGULARRATE:
			setAngularrate((Quaternion) newValue);
			return;
		case IPos_DatamodelPackage.IMU__ACCELERATION:
			setAcceleration((Acceleration) newValue);
			return;
		case IPos_DatamodelPackage.IMU__SENSOR_ID:
			setSensorId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.IMU__ANGULARRATE:
			setAngularrate((Quaternion) null);
			return;
		case IPos_DatamodelPackage.IMU__ACCELERATION:
			setAcceleration((Acceleration) null);
			return;
		case IPos_DatamodelPackage.IMU__SENSOR_ID:
			setSensorId(SENSOR_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.IMU__ANGULARRATE:
			return angularrate != null;
		case IPos_DatamodelPackage.IMU__ACCELERATION:
			return acceleration != null;
		case IPos_DatamodelPackage.IMU__SENSOR_ID:
			return SENSOR_ID_EDEFAULT == null ? sensorId != null : !SENSOR_ID_EDEFAULT.equals(sensorId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sensorId: ");
		result.append(sensorId);
		result.append(')');
		return result.toString();
	}

} //IMUImpl
