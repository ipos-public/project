/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IPos_DatamodelFactoryImpl extends EFactoryImpl implements IPos_DatamodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static IPos_DatamodelFactory init() {
		try {
			IPos_DatamodelFactory theIPos_DatamodelFactory = (IPos_DatamodelFactory) EPackage.Registry.INSTANCE
					.getEFactory(IPos_DatamodelPackage.eNS_URI);
			if (theIPos_DatamodelFactory != null) {
				return theIPos_DatamodelFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new IPos_DatamodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPos_DatamodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case IPos_DatamodelPackage.AGENT:
			return createAgent();
		case IPos_DatamodelPackage.ENTITY:
			return createEntity();
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT:
			return createLocalizableObject();
		case IPos_DatamodelPackage.PLACING:
			return createPlacing();
		case IPos_DatamodelPackage.POSITION:
			return createPosition();
		case IPos_DatamodelPackage.ORIENTATION:
			return createOrientation();
		case IPos_DatamodelPackage.WGS84_POINT:
			return createWGS84Point();
		case IPos_DatamodelPackage.ACCURACY:
			return createAccuracy();
		case IPos_DatamodelPackage.POINT2_D:
			return createPoint2D();
		case IPos_DatamodelPackage.POINT3_D:
			return createPoint3D();
		case IPos_DatamodelPackage.REFERENCE_SYSTEM:
			return createReferenceSystem();
		case IPos_DatamodelPackage.ZONE:
			return createZone();
		case IPos_DatamodelPackage.ZONE_MAP:
			return createZoneMap();
		case IPos_DatamodelPackage.SPACE:
			return createSpace();
		case IPos_DatamodelPackage.MAP_TYPE:
			return createMapType();
		case IPos_DatamodelPackage.QUATERNION:
			return createQuaternion();
		case IPos_DatamodelPackage.GAUSSIAN:
			return createGaussian();
		case IPos_DatamodelPackage.POINT:
			return createPoint();
		case IPos_DatamodelPackage.RAWDATA_EVENT:
			return createRawdataEvent();
		case IPos_DatamodelPackage.PROXIMITY:
			return createProximity();
		case IPos_DatamodelPackage.RFID:
			return createRFID();
		case IPos_DatamodelPackage.NFC:
			return createNFC();
		case IPos_DatamodelPackage.IMU:
			return createIMU();
		case IPos_DatamodelPackage.ACCELERATION:
			return createAcceleration();
		case IPos_DatamodelPackage.BEACON:
			return createBeacon();
		case IPos_DatamodelPackage.POSITION_EVENT:
			return createPositionEvent();
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION:
			return createEventFilterConfiguration();
		case IPos_DatamodelPackage.MONITORING_TASK:
			return createMonitoringTask();
		case IPos_DatamodelPackage.TRACKING_TASK:
			return createTrackingTask();
		case IPos_DatamodelPackage.WORLD_MODEL:
			return createWorldModel();
		case IPos_DatamodelPackage.BARCODE:
			return createBarcode();
		case IPos_DatamodelPackage.OTHER_PROX:
			return createOtherProx();
		case IPos_DatamodelPackage.BLUETOOTH:
			return createBluetooth();
		case IPos_DatamodelPackage.UWB:
			return createUWB();
		case IPos_DatamodelPackage.OTHER_BEACON:
			return createOtherBeacon();
		case IPos_DatamodelPackage.POI:
			return createPOI();
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION:
			return createEventFilterCondition();
		case IPos_DatamodelPackage.ZONE_DESCRIPTOR:
			return createZoneDescriptor();
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE:
			return createDataStorageQueryResponse();
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT:
			return createMessageReceivedEvent();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case IPos_DatamodelPackage.STRING_LIST:
			return createStringListFromString(eDataType, initialValue);
		case IPos_DatamodelPackage.STRING_ARRAY:
			return createStringArrayFromString(eDataType, initialValue);
		case IPos_DatamodelPackage.FLOAT_ARRAY3D:
			return createFloatArray3dFromString(eDataType, initialValue);
		case IPos_DatamodelPackage.BOOLEAN_LIST:
			return createBooleanListFromString(eDataType, initialValue);
		case IPos_DatamodelPackage.FLOAT_ARRAY:
			return createFloatArrayFromString(eDataType, initialValue);
		case IPos_DatamodelPackage.BYTE_ARRAY:
			return createByteArrayFromString(eDataType, initialValue);
		case IPos_DatamodelPackage.LIST_OF_STRING_MAPS:
			return createListOfStringMapsFromString(eDataType, initialValue);
		case IPos_DatamodelPackage.INT_ARRAY:
			return createIntArrayFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case IPos_DatamodelPackage.STRING_LIST:
			return convertStringListToString(eDataType, instanceValue);
		case IPos_DatamodelPackage.STRING_ARRAY:
			return convertStringArrayToString(eDataType, instanceValue);
		case IPos_DatamodelPackage.FLOAT_ARRAY3D:
			return convertFloatArray3dToString(eDataType, instanceValue);
		case IPos_DatamodelPackage.BOOLEAN_LIST:
			return convertBooleanListToString(eDataType, instanceValue);
		case IPos_DatamodelPackage.FLOAT_ARRAY:
			return convertFloatArrayToString(eDataType, instanceValue);
		case IPos_DatamodelPackage.BYTE_ARRAY:
			return convertByteArrayToString(eDataType, instanceValue);
		case IPos_DatamodelPackage.LIST_OF_STRING_MAPS:
			return convertListOfStringMapsToString(eDataType, instanceValue);
		case IPos_DatamodelPackage.INT_ARRAY:
			return convertIntArrayToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent createAgent() {
		AgentImpl agent = new AgentImpl();
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Entity createEntity() {
		EntityImpl entity = new EntityImpl();
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizableObject createLocalizableObject() {
		LocalizableObjectImpl localizableObject = new LocalizableObjectImpl();
		return localizableObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Placing createPlacing() {
		PlacingImpl placing = new PlacingImpl();
		return placing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Position createPosition() {
		PositionImpl position = new PositionImpl();
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Orientation createOrientation() {
		OrientationImpl orientation = new OrientationImpl();
		return orientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WGS84Point createWGS84Point() {
		WGS84PointImpl wgs84Point = new WGS84PointImpl();
		return wgs84Point;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Accuracy createAccuracy() {
		AccuracyImpl accuracy = new AccuracyImpl();
		return accuracy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Point2D createPoint2D() {
		Point2DImpl point2D = new Point2DImpl();
		return point2D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Point3D createPoint3D() {
		Point3DImpl point3D = new Point3DImpl();
		return point3D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceSystem createReferenceSystem() {
		ReferenceSystemImpl referenceSystem = new ReferenceSystemImpl();
		return referenceSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Zone createZone() {
		ZoneImpl zone = new ZoneImpl();
		return zone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZoneMap createZoneMap() {
		ZoneMapImpl zoneMap = new ZoneMapImpl();
		return zoneMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Space createSpace() {
		SpaceImpl space = new SpaceImpl();
		return space;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapType createMapType() {
		MapTypeImpl mapType = new MapTypeImpl();
		return mapType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Quaternion createQuaternion() {
		QuaternionImpl quaternion = new QuaternionImpl();
		return quaternion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Gaussian createGaussian() {
		GaussianImpl gaussian = new GaussianImpl();
		return gaussian;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Point createPoint() {
		PointImpl point = new PointImpl();
		return point;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RawdataEvent createRawdataEvent() {
		RawdataEventImpl rawdataEvent = new RawdataEventImpl();
		return rawdataEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Proximity createProximity() {
		ProximityImpl proximity = new ProximityImpl();
		return proximity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RFID createRFID() {
		RFIDImpl rfid = new RFIDImpl();
		return rfid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NFC createNFC() {
		NFCImpl nfc = new NFCImpl();
		return nfc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IMU createIMU() {
		IMUImpl imu = new IMUImpl();
		return imu;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Acceleration createAcceleration() {
		AccelerationImpl acceleration = new AccelerationImpl();
		return acceleration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Beacon createBeacon() {
		BeaconImpl beacon = new BeaconImpl();
		return beacon;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PositionEvent createPositionEvent() {
		PositionEventImpl positionEvent = new PositionEventImpl();
		return positionEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventFilterConfiguration createEventFilterConfiguration() {
		EventFilterConfigurationImpl eventFilterConfiguration = new EventFilterConfigurationImpl();
		return eventFilterConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoringTask createMonitoringTask() {
		MonitoringTaskImpl monitoringTask = new MonitoringTaskImpl();
		return monitoringTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrackingTask createTrackingTask() {
		TrackingTaskImpl trackingTask = new TrackingTaskImpl();
		return trackingTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldModel createWorldModel() {
		WorldModelImpl worldModel = new WorldModelImpl();
		return worldModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Barcode createBarcode() {
		BarcodeImpl barcode = new BarcodeImpl();
		return barcode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OtherProx createOtherProx() {
		OtherProxImpl otherProx = new OtherProxImpl();
		return otherProx;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Bluetooth createBluetooth() {
		BluetoothImpl bluetooth = new BluetoothImpl();
		return bluetooth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UWB createUWB() {
		UWBImpl uwb = new UWBImpl();
		return uwb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OtherBeacon createOtherBeacon() {
		OtherBeaconImpl otherBeacon = new OtherBeaconImpl();
		return otherBeacon;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public POI createPOI() {
		POIImpl poi = new POIImpl();
		return poi;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventFilterCondition createEventFilterCondition() {
		EventFilterConditionImpl eventFilterCondition = new EventFilterConditionImpl();
		return eventFilterCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZoneDescriptor createZoneDescriptor() {
		ZoneDescriptorImpl zoneDescriptor = new ZoneDescriptorImpl();
		return zoneDescriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataStorageQueryResponse createDataStorageQueryResponse() {
		DataStorageQueryResponseImpl dataStorageQueryResponse = new DataStorageQueryResponseImpl();
		return dataStorageQueryResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageReceivedEvent createMessageReceivedEvent() {
		MessageReceivedEventImpl messageReceivedEvent = new MessageReceivedEventImpl();
		return messageReceivedEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<String> createStringListFromString(EDataType eDataType, String initialValue) {
		return (List<String>) super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<String[]> createStringArrayFromString(EDataType eDataType, String initialValue) {
		return (ArrayList<String[]>) super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringArrayToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Float[][]> createFloatArray3dFromString(EDataType eDataType, String initialValue) {
		return (ArrayList<Float[][]>) super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFloatArray3dToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean[] createBooleanListFromString(EDataType eDataType, String initialValue) {
		return (boolean[]) super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBooleanListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Float[]> createFloatArrayFromString(EDataType eDataType, String initialValue) {
		return (ArrayList<Float[]>) super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFloatArrayToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte[] createByteArrayFromString(EDataType eDataType, String initialValue) {
		return (byte[]) super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertByteArrayToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public LinkedList<HashMap<String, String>> createListOfStringMapsFromString(EDataType eDataType,
			String initialValue) {
		return (LinkedList<HashMap<String, String>>) super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertListOfStringMapsToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int[] createIntArrayFromString(EDataType eDataType, String initialValue) {
		return (int[]) super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIntArrayToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPos_DatamodelPackage getIPos_DatamodelPackage() {
		return (IPos_DatamodelPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static IPos_DatamodelPackage getPackage() {
		return IPos_DatamodelPackage.eINSTANCE;
	}

} //IPos_DatamodelFactoryImpl
