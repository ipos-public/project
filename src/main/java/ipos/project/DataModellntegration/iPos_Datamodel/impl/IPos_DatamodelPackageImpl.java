/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Acceleration;
import ipos.project.DataModellntegration.iPos_Datamodel.Accuracy;
import ipos.project.DataModellntegration.iPos_Datamodel.Agent;
import ipos.project.DataModellntegration.iPos_Datamodel.Barcode;
import ipos.project.DataModellntegration.iPos_Datamodel.Beacon;
import ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth;
import ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse;
import ipos.project.DataModellntegration.iPos_Datamodel.Entity;
import ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition;
import ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration;
import ipos.project.DataModellntegration.iPos_Datamodel.Gaussian;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.impl.IPosDevKitPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject;
import ipos.project.DataModellntegration.iPos_Datamodel.MapType;
import ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.OFBizPackage;

import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.impl.OFBizPackageImpl;

import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.impl.OSMPackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.Orientation;
import ipos.project.DataModellntegration.iPos_Datamodel.OtherBeacon;
import ipos.project.DataModellntegration.iPos_Datamodel.OtherProx;
import ipos.project.DataModellntegration.iPos_Datamodel.Placing;
import ipos.project.DataModellntegration.iPos_Datamodel.Point;
import ipos.project.DataModellntegration.iPos_Datamodel.Point2D;
import ipos.project.DataModellntegration.iPos_Datamodel.Point3D;
import ipos.project.DataModellntegration.iPos_Datamodel.Position;
import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.Proximity;
import ipos.project.DataModellntegration.iPos_Datamodel.Quaternion;
import ipos.project.DataModellntegration.iPos_Datamodel.RawdataEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem;
import ipos.project.DataModellntegration.iPos_Datamodel.Space;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.impl.ToozPackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.VDA5050Package;
import ipos.project.DataModellntegration.iPos_Datamodel.VDA5050.impl.VDA5050PackageImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point;
import ipos.project.DataModellntegration.iPos_Datamodel.WorldModel;
import ipos.project.DataModellntegration.iPos_Datamodel.Zone;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IPos_DatamodelPackageImpl extends EPackageImpl implements IPos_DatamodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass entityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass localizableObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass placingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass positionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orientationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass wgs84PointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accuracyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass point2DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass point3DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass zoneEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass zoneMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass spaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mapTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass quaternionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gaussianEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rawdataEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass proximityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rfidEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nfcEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imuEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accelerationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass beaconEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass positionEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventFilterConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitoringTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass trackingTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass worldModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass barcodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass otherProxEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bluetoothEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uwbEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass otherBeaconEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass poiEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventFilterConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass zoneDescriptorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataStorageQueryResponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageReceivedEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringArrayEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType floatArray3dEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType booleanListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType floatArrayEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType byteArrayEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType listOfStringMapsEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intArrayEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private IPos_DatamodelPackageImpl() {
		super(eNS_URI, IPos_DatamodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link IPos_DatamodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static IPos_DatamodelPackage init() {
		if (isInited)
			return (IPos_DatamodelPackage) EPackage.Registry.INSTANCE.getEPackage(IPos_DatamodelPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredIPos_DatamodelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		IPos_DatamodelPackageImpl theIPos_DatamodelPackage = registeredIPos_DatamodelPackage instanceof IPos_DatamodelPackageImpl
				? (IPos_DatamodelPackageImpl) registeredIPos_DatamodelPackage
				: new IPos_DatamodelPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OFBizPackage.eNS_URI);
		OFBizPackageImpl theOFBizPackage = (OFBizPackageImpl) (registeredPackage instanceof OFBizPackageImpl
				? registeredPackage
				: OFBizPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(IPosDevKitPackage.eNS_URI);
		IPosDevKitPackageImpl theIPosDevKitPackage = (IPosDevKitPackageImpl) (registeredPackage instanceof IPosDevKitPackageImpl
				? registeredPackage
				: IPosDevKitPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ToozPackage.eNS_URI);
		ToozPackageImpl theToozPackage = (ToozPackageImpl) (registeredPackage instanceof ToozPackageImpl
				? registeredPackage
				: ToozPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VDA5050Package.eNS_URI);
		VDA5050PackageImpl theVDA5050Package = (VDA5050PackageImpl) (registeredPackage instanceof VDA5050PackageImpl
				? registeredPackage
				: VDA5050Package.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OSMPackage.eNS_URI);
		OSMPackageImpl theOSMPackage = (OSMPackageImpl) (registeredPackage instanceof OSMPackageImpl ? registeredPackage
				: OSMPackage.eINSTANCE);

		// Create package meta-data objects
		theIPos_DatamodelPackage.createPackageContents();
		theOFBizPackage.createPackageContents();
		theIPosDevKitPackage.createPackageContents();
		theToozPackage.createPackageContents();
		theVDA5050Package.createPackageContents();
		theOSMPackage.createPackageContents();

		// Initialize created meta-data
		theIPos_DatamodelPackage.initializePackageContents();
		theOFBizPackage.initializePackageContents();
		theIPosDevKitPackage.initializePackageContents();
		theToozPackage.initializePackageContents();
		theVDA5050Package.initializePackageContents();
		theOSMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theIPos_DatamodelPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(IPos_DatamodelPackage.eNS_URI, theIPos_DatamodelPackage);
		return theIPos_DatamodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgent() {
		return agentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgent_LObject() {
		return (EReference) agentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgent_AgentType() {
		return (EAttribute) agentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEntity() {
		return entityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntity_Id() {
		return (EAttribute) entityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocalizableObject() {
		return localizableObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocalizableObject_LastPosUpdate() {
		return (EAttribute) localizableObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocalizableObject_SensorType() {
		return (EAttribute) localizableObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocalizableObject_Agent() {
		return (EReference) localizableObjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocalizableObject_CurrentPlacing() {
		return (EReference) localizableObjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPlacing() {
		return placingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPlacing_Position() {
		return (EReference) placingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPlacing_Orientation() {
		return (EReference) placingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPosition() {
		return positionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPosition_Accuracy() {
		return (EReference) positionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPosition_Point() {
		return (EReference) positionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPosition_ReferenceSystem() {
		return (EReference) positionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrientation() {
		return orientationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWGS84Point() {
		return wgs84PointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWGS84Point_Longitude() {
		return (EAttribute) wgs84PointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWGS84Point_Latitude() {
		return (EAttribute) wgs84PointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWGS84Point_Altitude() {
		return (EAttribute) wgs84PointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccuracy() {
		return accuracyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPoint2D() {
		return point2DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPoint2D_X() {
		return (EAttribute) point2DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPoint2D_Y() {
		return (EAttribute) point2DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPoint3D() {
		return point3DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPoint3D_X() {
		return (EAttribute) point3DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPoint3D_Y() {
		return (EAttribute) point3DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPoint3D_Z() {
		return (EAttribute) point3DEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceSystem() {
		return referenceSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceSystem_Origin() {
		return (EReference) referenceSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReferenceSystem_Name() {
		return (EAttribute) referenceSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReferenceSystem_Id() {
		return (EAttribute) referenceSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getZone() {
		return zoneEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getZone_Space() {
		return (EReference) zoneEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getZone_Name() {
		return (EAttribute) zoneEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getZone_Id() {
		return (EAttribute) zoneEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getZoneMap() {
		return zoneMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getZoneMap_Zone() {
		return (EReference) zoneMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSpace() {
		return spaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSpace_X() {
		return (EAttribute) spaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSpace_Y() {
		return (EAttribute) spaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSpace_Z() {
		return (EAttribute) spaceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSpace_CentrePoint() {
		return (EReference) spaceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMapType() {
		return mapTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuaternion() {
		return quaternionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuaternion_X() {
		return (EAttribute) quaternionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuaternion_Y() {
		return (EAttribute) quaternionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuaternion_Z() {
		return (EAttribute) quaternionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuaternion_W() {
		return (EAttribute) quaternionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGaussian() {
		return gaussianEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGaussian_ConfidenceInterval() {
		return (EAttribute) gaussianEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPoint() {
		return pointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRawdataEvent() {
		return rawdataEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRawdataEvent_TimeStamp() {
		return (EAttribute) rawdataEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProximity() {
		return proximityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProximity_TagId() {
		return (EAttribute) proximityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProximity_Type() {
		return (EAttribute) proximityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProximity_ScannerId() {
		return (EAttribute) proximityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRFID() {
		return rfidEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFID_Location() {
		return (EAttribute) rfidEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNFC() {
		return nfcEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNFC_TagData() {
		return (EAttribute) nfcEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIMU() {
		return imuEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIMU_Angularrate() {
		return (EReference) imuEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIMU_Acceleration() {
		return (EReference) imuEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIMU_SensorId() {
		return (EAttribute) imuEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcceleration() {
		return accelerationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcceleration_X() {
		return (EAttribute) accelerationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcceleration_Y() {
		return (EAttribute) accelerationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcceleration_Z() {
		return (EAttribute) accelerationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBeacon() {
		return beaconEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBeacon_Distances() {
		return (EAttribute) beaconEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBeacon_Type() {
		return (EAttribute) beaconEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBeacon_SensorId() {
		return (EAttribute) beaconEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPositionEvent() {
		return positionEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPositionEvent_Placing() {
		return (EReference) positionEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPositionEvent_TimeStamp() {
		return (EAttribute) positionEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPositionEvent_LObjectId() {
		return (EAttribute) positionEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPositionEvent_Zonedescriptors() {
		return (EReference) positionEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventFilterConfiguration() {
		return eventFilterConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterConfiguration_PositionAmbiguityStrategy() {
		return (EAttribute) eventFilterConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterConfiguration_PositionAmbiguityParameters() {
		return (EAttribute) eventFilterConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterConfiguration_FilterCriteria() {
		return (EAttribute) eventFilterConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventFilterConfiguration_Eventfiltercondition() {
		return (EReference) eventFilterConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitoringTask() {
		return monitoringTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMonitoringTask_Eventfilterconfiguration() {
		return (EReference) monitoringTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTrackingTask() {
		return trackingTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTrackingTask_Eventfilterconfiguration() {
		return (EReference) trackingTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorldModel() {
		return worldModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWorldModel_Agent() {
		return (EReference) worldModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWorldModel_ZoneMap() {
		return (EReference) worldModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWorldModel_Pois() {
		return (EReference) worldModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWorldModel_ReferenceSystem() {
		return (EReference) worldModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBarcode() {
		return barcodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOtherProx() {
		return otherProxEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOtherProx_Data() {
		return (EAttribute) otherProxEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBluetooth() {
		return bluetoothEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBluetooth_Rss() {
		return (EAttribute) bluetoothEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUWB() {
		return uwbEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOtherBeacon() {
		return otherBeaconEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOtherBeacon_Data() {
		return (EAttribute) otherBeaconEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPOI() {
		return poiEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPOI_Placing() {
		return (EReference) poiEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPOI_Description() {
		return (EAttribute) poiEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPOI_Data() {
		return (EAttribute) poiEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPOI_Id() {
		return (EAttribute) poiEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventFilterCondition() {
		return eventFilterConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_TimeCondition() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_AccuracyCondition() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_PositionCondition() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_TimeMinInterval() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_PositionDelta() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_SensorIdCondition() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_FilterStructure() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_PositionConditionCells() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_IdCondition() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_CategoryCondition() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventFilterCondition_PropertyCondition() {
		return (EAttribute) eventFilterConditionEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getZoneDescriptor() {
		return zoneDescriptorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getZoneDescriptor_ZoneId() {
		return (EAttribute) zoneDescriptorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getZoneDescriptor_NotificationType() {
		return (EAttribute) zoneDescriptorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataStorageQueryResponse() {
		return dataStorageQueryResponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataStorageQueryResponse_PositionEvents() {
		return (EReference) dataStorageQueryResponseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataStorageQueryResponse_TrackingTaskId() {
		return (EAttribute) dataStorageQueryResponseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageReceivedEvent() {
		return messageReceivedEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageReceivedEvent_ProtocolName() {
		return (EAttribute) messageReceivedEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageReceivedEvent_SerializedMsg() {
		return (EAttribute) messageReceivedEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageReceivedEvent_AgentId() {
		return (EAttribute) messageReceivedEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageReceivedEvent_ExtractedAttributes() {
		return (EAttribute) messageReceivedEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageReceivedEvent_Timestamp() {
		return (EAttribute) messageReceivedEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getStringList() {
		return stringListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getStringArray() {
		return stringArrayEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFloatArray3d() {
		return floatArray3dEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getBooleanList() {
		return booleanListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFloatArray() {
		return floatArrayEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getByteArray() {
		return byteArrayEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getListOfStringMaps() {
		return listOfStringMapsEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIntArray() {
		return intArrayEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPos_DatamodelFactory getIPos_DatamodelFactory() {
		return (IPos_DatamodelFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		agentEClass = createEClass(AGENT);
		createEReference(agentEClass, AGENT__LOBJECT);
		createEAttribute(agentEClass, AGENT__AGENT_TYPE);

		entityEClass = createEClass(ENTITY);
		createEAttribute(entityEClass, ENTITY__ID);

		localizableObjectEClass = createEClass(LOCALIZABLE_OBJECT);
		createEAttribute(localizableObjectEClass, LOCALIZABLE_OBJECT__LAST_POS_UPDATE);
		createEAttribute(localizableObjectEClass, LOCALIZABLE_OBJECT__SENSOR_TYPE);
		createEReference(localizableObjectEClass, LOCALIZABLE_OBJECT__AGENT);
		createEReference(localizableObjectEClass, LOCALIZABLE_OBJECT__CURRENT_PLACING);

		placingEClass = createEClass(PLACING);
		createEReference(placingEClass, PLACING__POSITION);
		createEReference(placingEClass, PLACING__ORIENTATION);

		positionEClass = createEClass(POSITION);
		createEReference(positionEClass, POSITION__ACCURACY);
		createEReference(positionEClass, POSITION__POINT);
		createEReference(positionEClass, POSITION__REFERENCE_SYSTEM);

		orientationEClass = createEClass(ORIENTATION);

		wgs84PointEClass = createEClass(WGS84_POINT);
		createEAttribute(wgs84PointEClass, WGS84_POINT__LONGITUDE);
		createEAttribute(wgs84PointEClass, WGS84_POINT__LATITUDE);
		createEAttribute(wgs84PointEClass, WGS84_POINT__ALTITUDE);

		accuracyEClass = createEClass(ACCURACY);

		point2DEClass = createEClass(POINT2_D);
		createEAttribute(point2DEClass, POINT2_D__X);
		createEAttribute(point2DEClass, POINT2_D__Y);

		point3DEClass = createEClass(POINT3_D);
		createEAttribute(point3DEClass, POINT3_D__X);
		createEAttribute(point3DEClass, POINT3_D__Y);
		createEAttribute(point3DEClass, POINT3_D__Z);

		referenceSystemEClass = createEClass(REFERENCE_SYSTEM);
		createEReference(referenceSystemEClass, REFERENCE_SYSTEM__ORIGIN);
		createEAttribute(referenceSystemEClass, REFERENCE_SYSTEM__NAME);
		createEAttribute(referenceSystemEClass, REFERENCE_SYSTEM__ID);

		zoneEClass = createEClass(ZONE);
		createEReference(zoneEClass, ZONE__SPACE);
		createEAttribute(zoneEClass, ZONE__NAME);
		createEAttribute(zoneEClass, ZONE__ID);

		zoneMapEClass = createEClass(ZONE_MAP);
		createEReference(zoneMapEClass, ZONE_MAP__ZONE);

		spaceEClass = createEClass(SPACE);
		createEAttribute(spaceEClass, SPACE__X);
		createEAttribute(spaceEClass, SPACE__Y);
		createEAttribute(spaceEClass, SPACE__Z);
		createEReference(spaceEClass, SPACE__CENTRE_POINT);

		mapTypeEClass = createEClass(MAP_TYPE);

		quaternionEClass = createEClass(QUATERNION);
		createEAttribute(quaternionEClass, QUATERNION__X);
		createEAttribute(quaternionEClass, QUATERNION__Y);
		createEAttribute(quaternionEClass, QUATERNION__Z);
		createEAttribute(quaternionEClass, QUATERNION__W);

		gaussianEClass = createEClass(GAUSSIAN);
		createEAttribute(gaussianEClass, GAUSSIAN__CONFIDENCE_INTERVAL);

		pointEClass = createEClass(POINT);

		rawdataEventEClass = createEClass(RAWDATA_EVENT);
		createEAttribute(rawdataEventEClass, RAWDATA_EVENT__TIME_STAMP);

		proximityEClass = createEClass(PROXIMITY);
		createEAttribute(proximityEClass, PROXIMITY__TAG_ID);
		createEAttribute(proximityEClass, PROXIMITY__TYPE);
		createEAttribute(proximityEClass, PROXIMITY__SCANNER_ID);

		rfidEClass = createEClass(RFID);
		createEAttribute(rfidEClass, RFID__LOCATION);

		nfcEClass = createEClass(NFC);
		createEAttribute(nfcEClass, NFC__TAG_DATA);

		imuEClass = createEClass(IMU);
		createEReference(imuEClass, IMU__ANGULARRATE);
		createEReference(imuEClass, IMU__ACCELERATION);
		createEAttribute(imuEClass, IMU__SENSOR_ID);

		accelerationEClass = createEClass(ACCELERATION);
		createEAttribute(accelerationEClass, ACCELERATION__X);
		createEAttribute(accelerationEClass, ACCELERATION__Y);
		createEAttribute(accelerationEClass, ACCELERATION__Z);

		beaconEClass = createEClass(BEACON);
		createEAttribute(beaconEClass, BEACON__DISTANCES);
		createEAttribute(beaconEClass, BEACON__TYPE);
		createEAttribute(beaconEClass, BEACON__SENSOR_ID);

		positionEventEClass = createEClass(POSITION_EVENT);
		createEAttribute(positionEventEClass, POSITION_EVENT__TIME_STAMP);
		createEAttribute(positionEventEClass, POSITION_EVENT__LOBJECT_ID);
		createEReference(positionEventEClass, POSITION_EVENT__ZONEDESCRIPTORS);
		createEReference(positionEventEClass, POSITION_EVENT__PLACING);

		eventFilterConfigurationEClass = createEClass(EVENT_FILTER_CONFIGURATION);
		createEAttribute(eventFilterConfigurationEClass, EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_STRATEGY);
		createEAttribute(eventFilterConfigurationEClass, EVENT_FILTER_CONFIGURATION__POSITION_AMBIGUITY_PARAMETERS);
		createEAttribute(eventFilterConfigurationEClass, EVENT_FILTER_CONFIGURATION__FILTER_CRITERIA);
		createEReference(eventFilterConfigurationEClass, EVENT_FILTER_CONFIGURATION__EVENTFILTERCONDITION);

		monitoringTaskEClass = createEClass(MONITORING_TASK);
		createEReference(monitoringTaskEClass, MONITORING_TASK__EVENTFILTERCONFIGURATION);

		trackingTaskEClass = createEClass(TRACKING_TASK);
		createEReference(trackingTaskEClass, TRACKING_TASK__EVENTFILTERCONFIGURATION);

		worldModelEClass = createEClass(WORLD_MODEL);
		createEReference(worldModelEClass, WORLD_MODEL__AGENT);
		createEReference(worldModelEClass, WORLD_MODEL__ZONE_MAP);
		createEReference(worldModelEClass, WORLD_MODEL__POIS);
		createEReference(worldModelEClass, WORLD_MODEL__REFERENCE_SYSTEM);

		barcodeEClass = createEClass(BARCODE);

		otherProxEClass = createEClass(OTHER_PROX);
		createEAttribute(otherProxEClass, OTHER_PROX__DATA);

		bluetoothEClass = createEClass(BLUETOOTH);
		createEAttribute(bluetoothEClass, BLUETOOTH__RSS);

		uwbEClass = createEClass(UWB);

		otherBeaconEClass = createEClass(OTHER_BEACON);
		createEAttribute(otherBeaconEClass, OTHER_BEACON__DATA);

		poiEClass = createEClass(POI);
		createEReference(poiEClass, POI__PLACING);
		createEAttribute(poiEClass, POI__DESCRIPTION);
		createEAttribute(poiEClass, POI__DATA);
		createEAttribute(poiEClass, POI__ID);

		eventFilterConditionEClass = createEClass(EVENT_FILTER_CONDITION);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__TIME_CONDITION);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__ACCURACY_CONDITION);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__POSITION_CONDITION);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__TIME_MIN_INTERVAL);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__POSITION_DELTA);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__SENSOR_ID_CONDITION);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__FILTER_STRUCTURE);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__POSITION_CONDITION_CELLS);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__ID_CONDITION);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__CATEGORY_CONDITION);
		createEAttribute(eventFilterConditionEClass, EVENT_FILTER_CONDITION__PROPERTY_CONDITION);

		zoneDescriptorEClass = createEClass(ZONE_DESCRIPTOR);
		createEAttribute(zoneDescriptorEClass, ZONE_DESCRIPTOR__ZONE_ID);
		createEAttribute(zoneDescriptorEClass, ZONE_DESCRIPTOR__NOTIFICATION_TYPE);

		dataStorageQueryResponseEClass = createEClass(DATA_STORAGE_QUERY_RESPONSE);
		createEReference(dataStorageQueryResponseEClass, DATA_STORAGE_QUERY_RESPONSE__POSITION_EVENTS);
		createEAttribute(dataStorageQueryResponseEClass, DATA_STORAGE_QUERY_RESPONSE__TRACKING_TASK_ID);

		messageReceivedEventEClass = createEClass(MESSAGE_RECEIVED_EVENT);
		createEAttribute(messageReceivedEventEClass, MESSAGE_RECEIVED_EVENT__PROTOCOL_NAME);
		createEAttribute(messageReceivedEventEClass, MESSAGE_RECEIVED_EVENT__SERIALIZED_MSG);
		createEAttribute(messageReceivedEventEClass, MESSAGE_RECEIVED_EVENT__AGENT_ID);
		createEAttribute(messageReceivedEventEClass, MESSAGE_RECEIVED_EVENT__EXTRACTED_ATTRIBUTES);
		createEAttribute(messageReceivedEventEClass, MESSAGE_RECEIVED_EVENT__TIMESTAMP);

		// Create data types
		stringListEDataType = createEDataType(STRING_LIST);
		stringArrayEDataType = createEDataType(STRING_ARRAY);
		floatArray3dEDataType = createEDataType(FLOAT_ARRAY3D);
		booleanListEDataType = createEDataType(BOOLEAN_LIST);
		floatArrayEDataType = createEDataType(FLOAT_ARRAY);
		byteArrayEDataType = createEDataType(BYTE_ARRAY);
		listOfStringMapsEDataType = createEDataType(LIST_OF_STRING_MAPS);
		intArrayEDataType = createEDataType(INT_ARRAY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		OFBizPackage theOFBizPackage = (OFBizPackage) EPackage.Registry.INSTANCE.getEPackage(OFBizPackage.eNS_URI);
		IPosDevKitPackage theIPosDevKitPackage = (IPosDevKitPackage) EPackage.Registry.INSTANCE
				.getEPackage(IPosDevKitPackage.eNS_URI);
		ToozPackage theToozPackage = (ToozPackage) EPackage.Registry.INSTANCE.getEPackage(ToozPackage.eNS_URI);
		VDA5050Package theVDA5050Package = (VDA5050Package) EPackage.Registry.INSTANCE
				.getEPackage(VDA5050Package.eNS_URI);
		OSMPackage theOSMPackage = (OSMPackage) EPackage.Registry.INSTANCE.getEPackage(OSMPackage.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE
				.getEPackage(XMLTypePackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theOFBizPackage);
		getESubpackages().add(theIPosDevKitPackage);
		getESubpackages().add(theToozPackage);
		getESubpackages().add(theVDA5050Package);
		getESubpackages().add(theOSMPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		agentEClass.getESuperTypes().add(this.getEntity());
		localizableObjectEClass.getESuperTypes().add(this.getEntity());
		wgs84PointEClass.getESuperTypes().add(this.getPoint());
		point2DEClass.getESuperTypes().add(this.getPoint());
		point3DEClass.getESuperTypes().add(this.getPoint());
		zoneMapEClass.getESuperTypes().add(this.getMapType());
		quaternionEClass.getESuperTypes().add(this.getOrientation());
		gaussianEClass.getESuperTypes().add(this.getAccuracy());
		proximityEClass.getESuperTypes().add(this.getRawdataEvent());
		rfidEClass.getESuperTypes().add(this.getProximity());
		nfcEClass.getESuperTypes().add(this.getProximity());
		imuEClass.getESuperTypes().add(this.getRawdataEvent());
		beaconEClass.getESuperTypes().add(this.getRawdataEvent());
		barcodeEClass.getESuperTypes().add(this.getProximity());
		otherProxEClass.getESuperTypes().add(this.getProximity());
		bluetoothEClass.getESuperTypes().add(this.getBeacon());
		uwbEClass.getESuperTypes().add(this.getBeacon());
		otherBeaconEClass.getESuperTypes().add(this.getBeacon());

		// Initialize classes, features, and operations; add parameters
		initEClass(agentEClass, Agent.class, "Agent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAgent_LObject(), this.getLocalizableObject(), null, "lObject", null, 0, -1, Agent.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgent_AgentType(), ecorePackage.getEString(), "agentType", null, 0, 1, Agent.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(entityEClass, Entity.class, "Entity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEntity_Id(), ecorePackage.getEString(), "id", null, 0, 1, Entity.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(localizableObjectEClass, LocalizableObject.class, "LocalizableObject", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLocalizableObject_LastPosUpdate(), ecorePackage.getEString(), "lastPosUpdate", null, 0, 1,
				LocalizableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getLocalizableObject_SensorType(), ecorePackage.getEString(), "sensorType", null, 0, 1,
				LocalizableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getLocalizableObject_Agent(), this.getAgent(), null, "agent", null, 0, 1,
				LocalizableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLocalizableObject_CurrentPlacing(), this.getPlacing(), null, "currentPlacing", null, 0, 1,
				LocalizableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(placingEClass, Placing.class, "Placing", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPlacing_Position(), this.getPosition(), null, "position", null, 1, 1, Placing.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPlacing_Orientation(), this.getOrientation(), null, "orientation", null, 1, 1, Placing.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(positionEClass, Position.class, "Position", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPosition_Accuracy(), this.getAccuracy(), null, "accuracy", null, 0, 1, Position.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPosition_Point(), this.getPoint(), null, "point", null, 1, 1, Position.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getPosition_ReferenceSystem(), this.getReferenceSystem(), null, "referenceSystem", null, 1, 1,
				Position.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(orientationEClass, Orientation.class, "Orientation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(wgs84PointEClass, WGS84Point.class, "WGS84Point", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWGS84Point_Longitude(), theXMLTypePackage.getDouble(), "longitude", null, 0, 1,
				WGS84Point.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getWGS84Point_Latitude(), theXMLTypePackage.getDouble(), "latitude", null, 0, 1,
				WGS84Point.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getWGS84Point_Altitude(), theXMLTypePackage.getDouble(), "altitude", null, 0, 1,
				WGS84Point.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(accuracyEClass, Accuracy.class, "Accuracy", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(point2DEClass, Point2D.class, "Point2D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPoint2D_X(), theXMLTypePackage.getFloat(), "x", null, 0, 1, Point2D.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPoint2D_Y(), theXMLTypePackage.getFloat(), "y", null, 0, 1, Point2D.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(point3DEClass, Point3D.class, "Point3D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPoint3D_X(), theXMLTypePackage.getFloat(), "x", null, 0, 1, Point3D.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPoint3D_Y(), ecorePackage.getEFloat(), "y", null, 0, 1, Point3D.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPoint3D_Z(), ecorePackage.getEFloat(), "z", null, 0, 1, Point3D.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referenceSystemEClass, ReferenceSystem.class, "ReferenceSystem", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferenceSystem_Origin(), this.getPlacing(), null, "origin", null, 0, 1,
				ReferenceSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReferenceSystem_Name(), ecorePackage.getEString(), "name", null, 0, 1, ReferenceSystem.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReferenceSystem_Id(), ecorePackage.getEString(), "id", null, 0, 1, ReferenceSystem.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(zoneEClass, Zone.class, "Zone", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getZone_Space(), this.getSpace(), null, "space", null, 0, -1, Zone.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getZone_Name(), ecorePackage.getEString(), "name", null, 0, 1, Zone.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getZone_Id(), ecorePackage.getEString(), "id", null, 0, 1, Zone.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(zoneMapEClass, ZoneMap.class, "ZoneMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getZoneMap_Zone(), this.getZone(), null, "zone", null, 0, -1, ZoneMap.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(spaceEClass, Space.class, "Space", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSpace_X(), theXMLTypePackage.getFloat(), "x", null, 0, 1, Space.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSpace_Y(), theXMLTypePackage.getFloat(), "y", null, 0, 1, Space.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSpace_Z(), theXMLTypePackage.getFloat(), "z", null, 0, 1, Space.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSpace_CentrePoint(), this.getPlacing(), null, "centrePoint", null, 1, 1, Space.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mapTypeEClass, MapType.class, "MapType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(quaternionEClass, Quaternion.class, "Quaternion", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getQuaternion_X(), ecorePackage.getEFloat(), "x", null, 0, 1, Quaternion.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQuaternion_Y(), ecorePackage.getEFloat(), "y", null, 0, 1, Quaternion.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQuaternion_Z(), ecorePackage.getEFloat(), "z", null, 0, 1, Quaternion.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQuaternion_W(), ecorePackage.getEFloat(), "w", null, 0, 1, Quaternion.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gaussianEClass, Gaussian.class, "Gaussian", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGaussian_ConfidenceInterval(), ecorePackage.getEFloat(), "confidenceInterval", "0.0", 0, 1,
				Gaussian.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(pointEClass, Point.class, "Point", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rawdataEventEClass, RawdataEvent.class, "RawdataEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRawdataEvent_TimeStamp(), theXMLTypePackage.getString(), "timeStamp", null, 0, 1,
				RawdataEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(proximityEClass, Proximity.class, "Proximity", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProximity_TagId(), ecorePackage.getEString(), "tagId", null, 0, 1, Proximity.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProximity_Type(), ecorePackage.getEString(), "type", null, 0, 1, Proximity.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProximity_ScannerId(), ecorePackage.getEString(), "scannerId", null, 0, 1, Proximity.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rfidEClass, ipos.project.DataModellntegration.iPos_Datamodel.RFID.class, "RFID", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRFID_Location(), ecorePackage.getEString(), "location", null, 0, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.RFID.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nfcEClass, ipos.project.DataModellntegration.iPos_Datamodel.NFC.class, "NFC", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getNFC_TagData(), g1, "tagData", null, 0, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.NFC.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(imuEClass, ipos.project.DataModellntegration.iPos_Datamodel.IMU.class, "IMU", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIMU_Angularrate(), this.getQuaternion(), null, "angularrate", null, 1, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.IMU.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIMU_Acceleration(), this.getAcceleration(), null, "acceleration", null, 1, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.IMU.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIMU_SensorId(), theXMLTypePackage.getString(), "sensorId", null, 0, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.IMU.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(accelerationEClass, Acceleration.class, "Acceleration", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAcceleration_X(), theXMLTypePackage.getFloat(), "x", null, 0, 1, Acceleration.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAcceleration_Y(), theXMLTypePackage.getFloat(), "y", null, 0, 1, Acceleration.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAcceleration_Z(), theXMLTypePackage.getFloat(), "z", null, 0, 1, Acceleration.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(beaconEClass, Beacon.class, "Beacon", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEDoubleObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getBeacon_Distances(), g1, "distances", null, 0, 1, Beacon.class, IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBeacon_Type(), ecorePackage.getEString(), "type", null, 0, 1, Beacon.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBeacon_SensorId(), theXMLTypePackage.getString(), "sensorId", null, 0, 1, Beacon.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(positionEventEClass, PositionEvent.class, "PositionEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPositionEvent_TimeStamp(), theXMLTypePackage.getString(), "timeStamp", null, 0, 1,
				PositionEvent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPositionEvent_LObjectId(), ecorePackage.getEString(), "lObjectId", null, 0, 1,
				PositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getPositionEvent_Zonedescriptors(), this.getZoneDescriptor(), null, "zonedescriptors", null, 0,
				-1, PositionEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPositionEvent_Placing(), this.getPlacing(), null, "placing", null, 1, 1, PositionEvent.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventFilterConfigurationEClass, EventFilterConfiguration.class, "EventFilterConfiguration",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEventFilterConfiguration_PositionAmbiguityStrategy(), ecorePackage.getEString(),
				"positionAmbiguityStrategy", null, 0, 1, EventFilterConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getEventFilterConfiguration_PositionAmbiguityParameters(), g1, "positionAmbiguityParameters",
				null, 0, 1, EventFilterConfiguration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getEventFilterConfiguration_FilterCriteria(), g1, "filterCriteria", null, 0, 1,
				EventFilterConfiguration.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventFilterConfiguration_Eventfiltercondition(), this.getTrackingTask(), null,
				"eventfiltercondition", null, 0, -1, EventFilterConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(monitoringTaskEClass, MonitoringTask.class, "MonitoringTask", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMonitoringTask_Eventfilterconfiguration(), this.getEventFilterConfiguration(), null,
				"eventfilterconfiguration", null, 1, 1, MonitoringTask.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(trackingTaskEClass, TrackingTask.class, "TrackingTask", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTrackingTask_Eventfilterconfiguration(), this.getEventFilterConfiguration(), null,
				"eventfilterconfiguration", null, 1, 1, TrackingTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(worldModelEClass, WorldModel.class, "WorldModel", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWorldModel_Agent(), this.getAgent(), null, "agent", null, 0, -1, WorldModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWorldModel_ZoneMap(), this.getZoneMap(), null, "zoneMap", null, 0, -1, WorldModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWorldModel_Pois(), this.getPOI(), null, "pois", null, 0, -1, WorldModel.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getWorldModel_ReferenceSystem(), this.getReferenceSystem(), null, "referenceSystem", null, 0, -1,
				WorldModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(barcodeEClass, Barcode.class, "Barcode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(otherProxEClass, OtherProx.class, "OtherProx", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getOtherProx_Data(), g1, "data", null, 0, 1, OtherProx.class, IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bluetoothEClass, Bluetooth.class, "Bluetooth", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theXMLTypePackage.getDoubleObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getBluetooth_Rss(), g1, "rss", null, 0, 1, Bluetooth.class, IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uwbEClass, ipos.project.DataModellntegration.iPos_Datamodel.UWB.class, "UWB", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(otherBeaconEClass, OtherBeacon.class, "OtherBeacon", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getOtherBeacon_Data(), g1, "data", null, 0, 1, OtherBeacon.class, IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(poiEClass, ipos.project.DataModellntegration.iPos_Datamodel.POI.class, "POI", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPOI_Placing(), this.getPlacing(), null, "placing", null, 1, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.POI.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPOI_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.POI.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getPOI_Data(), g1, "data", null, 0, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.POI.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPOI_Id(), ecorePackage.getEString(), "id", null, 0, 1,
				ipos.project.DataModellntegration.iPos_Datamodel.POI.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventFilterConditionEClass, EventFilterCondition.class, "EventFilterCondition", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEventFilterCondition_TimeCondition(), this.getStringArray(), "timeCondition", null, 0, 1,
				EventFilterCondition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_AccuracyCondition(), theXMLTypePackage.getFloat(), "accuracyCondition",
				null, 0, 1, EventFilterCondition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_PositionCondition(), this.getFloatArray(), "positionCondition", null, 0,
				1, EventFilterCondition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_TimeMinInterval(), theXMLTypePackage.getInt(), "timeMinInterval", null,
				0, 1, EventFilterCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_PositionDelta(), theXMLTypePackage.getFloat(), "positionDelta", null, 0,
				1, EventFilterCondition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_SensorIdCondition(), this.getStringList(), "sensorIdCondition", null, 0,
				1, EventFilterCondition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_FilterStructure(), this.getBooleanList(), "filterStructure", null, 0, 1,
				EventFilterCondition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(theXMLTypePackage.getString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getFloatArray3d());
		g1.getETypeArguments().add(g2);
		initEAttribute(getEventFilterCondition_PositionConditionCells(), g1, "positionConditionCells", null, 0, 1,
				EventFilterCondition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_IdCondition(), this.getStringList(), "idCondition", null, 0, 1,
				EventFilterCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_CategoryCondition(), this.getStringList(), "categoryCondition", null, 0,
				1, EventFilterCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventFilterCondition_PropertyCondition(), this.getStringList(), "propertyCondition", null, 0,
				1, EventFilterCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(zoneDescriptorEClass, ZoneDescriptor.class, "ZoneDescriptor", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getZoneDescriptor_ZoneId(), theXMLTypePackage.getString(), "zoneId", null, 0, 1,
				ZoneDescriptor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getZoneDescriptor_NotificationType(), theXMLTypePackage.getString(), "notificationType", null, 0,
				1, ZoneDescriptor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(dataStorageQueryResponseEClass, DataStorageQueryResponse.class, "DataStorageQueryResponse",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataStorageQueryResponse_PositionEvents(), this.getPositionEvent(), null, "positionEvents",
				null, 0, -1, DataStorageQueryResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataStorageQueryResponse_TrackingTaskId(), theXMLTypePackage.getString(), "trackingTaskId",
				null, 0, 1, DataStorageQueryResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageReceivedEventEClass, MessageReceivedEvent.class, "MessageReceivedEvent", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMessageReceivedEvent_ProtocolName(), ecorePackage.getEString(), "protocolName", null, 0, 1,
				MessageReceivedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageReceivedEvent_SerializedMsg(), this.getByteArray(), "serializedMsg", null, 0, 1,
				MessageReceivedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageReceivedEvent_AgentId(), ecorePackage.getEString(), "agentId", null, 0, 1,
				MessageReceivedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageReceivedEvent_ExtractedAttributes(), this.getListOfStringMaps(), "extractedAttributes",
				null, 0, 1, MessageReceivedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageReceivedEvent_Timestamp(), ecorePackage.getEString(), "timestamp", null, 0, 1,
				MessageReceivedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(stringListEDataType, List.class, "StringList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS,
				"java.util.List<java.lang.String>");
		initEDataType(stringArrayEDataType, ArrayList.class, "StringArray", IS_SERIALIZABLE,
				!IS_GENERATED_INSTANCE_CLASS, "java.util.ArrayList<java.lang.String[]>");
		initEDataType(floatArray3dEDataType, ArrayList.class, "FloatArray3d", IS_SERIALIZABLE,
				!IS_GENERATED_INSTANCE_CLASS, "java.util.ArrayList<java.lang.Float[][]>");
		initEDataType(booleanListEDataType, boolean[].class, "BooleanList", IS_SERIALIZABLE,
				!IS_GENERATED_INSTANCE_CLASS);
		initEDataType(floatArrayEDataType, ArrayList.class, "FloatArray", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS,
				"java.util.ArrayList<java.lang.Float[]>");
		initEDataType(byteArrayEDataType, byte[].class, "ByteArray", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listOfStringMapsEDataType, LinkedList.class, "ListOfStringMaps", IS_SERIALIZABLE,
				!IS_GENERATED_INSTANCE_CLASS,
				"java.util.LinkedList<java.util.HashMap<java.lang.String, java.lang.String>>");
		initEDataType(intArrayEDataType, int[].class, "IntArray", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //IPos_DatamodelPackageImpl
