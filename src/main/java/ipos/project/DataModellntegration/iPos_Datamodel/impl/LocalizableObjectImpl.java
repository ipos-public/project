/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Agent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject;
import ipos.project.DataModellntegration.iPos_Datamodel.Placing;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Localizable Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.LocalizableObjectImpl#getLastPosUpdate <em>Last Pos Update</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.LocalizableObjectImpl#getSensorType <em>Sensor Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.LocalizableObjectImpl#getAgent <em>Agent</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.LocalizableObjectImpl#getCurrentPlacing <em>Current Placing</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocalizableObjectImpl extends EntityImpl implements LocalizableObject {
	/**
	 * The default value of the '{@link #getLastPosUpdate() <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPosUpdate()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_POS_UPDATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastPosUpdate() <em>Last Pos Update</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPosUpdate()
	 * @generated
	 * @ordered
	 */
	protected String lastPosUpdate = LAST_POS_UPDATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSensorType() <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorType()
	 * @generated
	 * @ordered
	 */
	protected static final String SENSOR_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSensorType() <em>Sensor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSensorType()
	 * @generated
	 * @ordered
	 */
	protected String sensorType = SENSOR_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAgent() <em>Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgent()
	 * @generated
	 * @ordered
	 */
	protected Agent agent;

	/**
	 * The cached value of the '{@link #getCurrentPlacing() <em>Current Placing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentPlacing()
	 * @generated
	 * @ordered
	 */
	protected Placing currentPlacing;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocalizableObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.LOCALIZABLE_OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastPosUpdate() {
		return lastPosUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastPosUpdate(String newLastPosUpdate) {
		String oldLastPosUpdate = lastPosUpdate;
		lastPosUpdate = newLastPosUpdate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.LOCALIZABLE_OBJECT__LAST_POS_UPDATE, oldLastPosUpdate, lastPosUpdate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSensorType() {
		return sensorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensorType(String newSensorType) {
		String oldSensorType = sensorType;
		sensorType = newSensorType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.LOCALIZABLE_OBJECT__SENSOR_TYPE,
					oldSensorType, sensorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent getAgent() {
		if (agent != null && agent.eIsProxy()) {
			InternalEObject oldAgent = (InternalEObject) agent;
			agent = (Agent) eResolveProxy(oldAgent);
			if (agent != oldAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPos_DatamodelPackage.LOCALIZABLE_OBJECT__AGENT, oldAgent, agent));
			}
		}
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent basicGetAgent() {
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgent(Agent newAgent) {
		Agent oldAgent = agent;
		agent = newAgent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.LOCALIZABLE_OBJECT__AGENT,
					oldAgent, agent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Placing getCurrentPlacing() {
		if (currentPlacing != null && currentPlacing.eIsProxy()) {
			InternalEObject oldCurrentPlacing = (InternalEObject) currentPlacing;
			currentPlacing = (Placing) eResolveProxy(oldCurrentPlacing);
			if (currentPlacing != oldCurrentPlacing) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPos_DatamodelPackage.LOCALIZABLE_OBJECT__CURRENT_PLACING, oldCurrentPlacing,
							currentPlacing));
			}
		}
		return currentPlacing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Placing basicGetCurrentPlacing() {
		return currentPlacing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentPlacing(Placing newCurrentPlacing) {
		Placing oldCurrentPlacing = currentPlacing;
		currentPlacing = newCurrentPlacing;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.LOCALIZABLE_OBJECT__CURRENT_PLACING, oldCurrentPlacing, currentPlacing));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__LAST_POS_UPDATE:
			return getLastPosUpdate();
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__SENSOR_TYPE:
			return getSensorType();
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__AGENT:
			if (resolve)
				return getAgent();
			return basicGetAgent();
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__CURRENT_PLACING:
			if (resolve)
				return getCurrentPlacing();
			return basicGetCurrentPlacing();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__LAST_POS_UPDATE:
			setLastPosUpdate((String) newValue);
			return;
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__SENSOR_TYPE:
			setSensorType((String) newValue);
			return;
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__AGENT:
			setAgent((Agent) newValue);
			return;
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__CURRENT_PLACING:
			setCurrentPlacing((Placing) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__LAST_POS_UPDATE:
			setLastPosUpdate(LAST_POS_UPDATE_EDEFAULT);
			return;
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__SENSOR_TYPE:
			setSensorType(SENSOR_TYPE_EDEFAULT);
			return;
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__AGENT:
			setAgent((Agent) null);
			return;
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__CURRENT_PLACING:
			setCurrentPlacing((Placing) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__LAST_POS_UPDATE:
			return LAST_POS_UPDATE_EDEFAULT == null ? lastPosUpdate != null
					: !LAST_POS_UPDATE_EDEFAULT.equals(lastPosUpdate);
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__SENSOR_TYPE:
			return SENSOR_TYPE_EDEFAULT == null ? sensorType != null : !SENSOR_TYPE_EDEFAULT.equals(sensorType);
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__AGENT:
			return agent != null;
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT__CURRENT_PLACING:
			return currentPlacing != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lastPosUpdate: ");
		result.append(lastPosUpdate);
		result.append(", sensorType: ");
		result.append(sensorType);
		result.append(')');
		return result.toString();
	}

} //LocalizableObjectImpl
