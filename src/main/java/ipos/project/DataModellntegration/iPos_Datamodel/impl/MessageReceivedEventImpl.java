/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent;

import java.util.HashMap;
import java.util.LinkedList;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Received Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl#getProtocolName <em>Protocol Name</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl#getSerializedMsg <em>Serialized Msg</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl#getAgentId <em>Agent Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl#getExtractedAttributes <em>Extracted Attributes</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.MessageReceivedEventImpl#getTimestamp <em>Timestamp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MessageReceivedEventImpl extends MinimalEObjectImpl.Container implements MessageReceivedEvent {
	/**
	 * The default value of the '{@link #getProtocolName() <em>Protocol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolName()
	 * @generated
	 * @ordered
	 */
	protected static final String PROTOCOL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProtocolName() <em>Protocol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolName()
	 * @generated
	 * @ordered
	 */
	protected String protocolName = PROTOCOL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSerializedMsg() <em>Serialized Msg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerializedMsg()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] SERIALIZED_MSG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSerializedMsg() <em>Serialized Msg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerializedMsg()
	 * @generated
	 * @ordered
	 */
	protected byte[] serializedMsg = SERIALIZED_MSG_EDEFAULT;

	/**
	 * The default value of the '{@link #getAgentId() <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentId()
	 * @generated
	 * @ordered
	 */
	protected static final String AGENT_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAgentId() <em>Agent Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentId()
	 * @generated
	 * @ordered
	 */
	protected String agentId = AGENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtractedAttributes() <em>Extracted Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtractedAttributes()
	 * @generated
	 * @ordered
	 */
	protected static final LinkedList<HashMap<String, String>> EXTRACTED_ATTRIBUTES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtractedAttributes() <em>Extracted Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtractedAttributes()
	 * @generated
	 * @ordered
	 */
	protected LinkedList<HashMap<String, String>> extractedAttributes = EXTRACTED_ATTRIBUTES_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final String TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected String timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageReceivedEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.MESSAGE_RECEIVED_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProtocolName() {
		return protocolName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtocolName(String newProtocolName) {
		String oldProtocolName = protocolName;
		protocolName = newProtocolName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__PROTOCOL_NAME, oldProtocolName, protocolName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte[] getSerializedMsg() {
		return serializedMsg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSerializedMsg(byte[] newSerializedMsg) {
		byte[] oldSerializedMsg = serializedMsg;
		serializedMsg = newSerializedMsg;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__SERIALIZED_MSG, oldSerializedMsg, serializedMsg));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentId(String newAgentId) {
		String oldAgentId = agentId;
		agentId = newAgentId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__AGENT_ID, oldAgentId, agentId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkedList<HashMap<String, String>> getExtractedAttributes() {
		return extractedAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtractedAttributes(LinkedList<HashMap<String, String>> newExtractedAttributes) {
		LinkedList<HashMap<String, String>> oldExtractedAttributes = extractedAttributes;
		extractedAttributes = newExtractedAttributes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__EXTRACTED_ATTRIBUTES, oldExtractedAttributes,
					extractedAttributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimestamp(String newTimestamp) {
		String oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__TIMESTAMP, oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__PROTOCOL_NAME:
			return getProtocolName();
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__SERIALIZED_MSG:
			return getSerializedMsg();
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__AGENT_ID:
			return getAgentId();
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__EXTRACTED_ATTRIBUTES:
			return getExtractedAttributes();
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__TIMESTAMP:
			return getTimestamp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__PROTOCOL_NAME:
			setProtocolName((String) newValue);
			return;
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__SERIALIZED_MSG:
			setSerializedMsg((byte[]) newValue);
			return;
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__AGENT_ID:
			setAgentId((String) newValue);
			return;
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__EXTRACTED_ATTRIBUTES:
			setExtractedAttributes((LinkedList<HashMap<String, String>>) newValue);
			return;
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__TIMESTAMP:
			setTimestamp((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__PROTOCOL_NAME:
			setProtocolName(PROTOCOL_NAME_EDEFAULT);
			return;
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__SERIALIZED_MSG:
			setSerializedMsg(SERIALIZED_MSG_EDEFAULT);
			return;
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__AGENT_ID:
			setAgentId(AGENT_ID_EDEFAULT);
			return;
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__EXTRACTED_ATTRIBUTES:
			setExtractedAttributes(EXTRACTED_ATTRIBUTES_EDEFAULT);
			return;
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__TIMESTAMP:
			setTimestamp(TIMESTAMP_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__PROTOCOL_NAME:
			return PROTOCOL_NAME_EDEFAULT == null ? protocolName != null : !PROTOCOL_NAME_EDEFAULT.equals(protocolName);
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__SERIALIZED_MSG:
			return SERIALIZED_MSG_EDEFAULT == null ? serializedMsg != null
					: !SERIALIZED_MSG_EDEFAULT.equals(serializedMsg);
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__AGENT_ID:
			return AGENT_ID_EDEFAULT == null ? agentId != null : !AGENT_ID_EDEFAULT.equals(agentId);
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__EXTRACTED_ATTRIBUTES:
			return EXTRACTED_ATTRIBUTES_EDEFAULT == null ? extractedAttributes != null
					: !EXTRACTED_ATTRIBUTES_EDEFAULT.equals(extractedAttributes);
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT__TIMESTAMP:
			return TIMESTAMP_EDEFAULT == null ? timestamp != null : !TIMESTAMP_EDEFAULT.equals(timestamp);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (protocolName: ");
		result.append(protocolName);
		result.append(", serializedMsg: ");
		result.append(serializedMsg);
		result.append(", agentId: ");
		result.append(agentId);
		result.append(", extractedAttributes: ");
		result.append(extractedAttributes);
		result.append(", timestamp: ");
		result.append(timestamp);
		result.append(')');
		return result.toString();
	}

} //MessageReceivedEventImpl
