/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.POI;
import ipos.project.DataModellntegration.iPos_Datamodel.Placing;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>POI</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.POIImpl#getPlacing <em>Placing</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.POIImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.POIImpl#getData <em>Data</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.POIImpl#getId <em>Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class POIImpl extends MinimalEObjectImpl.Container implements POI {
	/**
	 * The cached value of the '{@link #getPlacing() <em>Placing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlacing()
	 * @generated
	 * @ordered
	 */
	protected Placing placing;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected Map<String, String> data;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected POIImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.POI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Placing getPlacing() {
		if (placing != null && placing.eIsProxy()) {
			InternalEObject oldPlacing = (InternalEObject) placing;
			placing = (Placing) eResolveProxy(oldPlacing);
			if (placing != oldPlacing) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IPos_DatamodelPackage.POI__PLACING,
							oldPlacing, placing));
			}
		}
		return placing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Placing basicGetPlacing() {
		return placing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlacing(Placing newPlacing) {
		Placing oldPlacing = placing;
		placing = newPlacing;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POI__PLACING, oldPlacing,
					placing));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POI__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map<String, String> getData() {
		return data;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setData(Map<String, String> newData) {
		Map<String, String> oldData = data;
		data = newData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POI__DATA, oldData, data));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POI__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.POI__PLACING:
			if (resolve)
				return getPlacing();
			return basicGetPlacing();
		case IPos_DatamodelPackage.POI__DESCRIPTION:
			return getDescription();
		case IPos_DatamodelPackage.POI__DATA:
			return getData();
		case IPos_DatamodelPackage.POI__ID:
			return getId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.POI__PLACING:
			setPlacing((Placing) newValue);
			return;
		case IPos_DatamodelPackage.POI__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case IPos_DatamodelPackage.POI__DATA:
			setData((Map<String, String>) newValue);
			return;
		case IPos_DatamodelPackage.POI__ID:
			setId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.POI__PLACING:
			setPlacing((Placing) null);
			return;
		case IPos_DatamodelPackage.POI__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case IPos_DatamodelPackage.POI__DATA:
			setData((Map<String, String>) null);
			return;
		case IPos_DatamodelPackage.POI__ID:
			setId(ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.POI__PLACING:
			return placing != null;
		case IPos_DatamodelPackage.POI__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case IPos_DatamodelPackage.POI__DATA:
			return data != null;
		case IPos_DatamodelPackage.POI__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", data: ");
		result.append(data);
		result.append(", id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //POIImpl
