/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.Placing;
import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Position Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionEventImpl#getTimeStamp <em>Time Stamp</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionEventImpl#getLObjectId <em>LObject Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionEventImpl#getZonedescriptors <em>Zonedescriptors</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionEventImpl#getPlacing <em>Placing</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PositionEventImpl extends MinimalEObjectImpl.Container implements PositionEvent {
	/**
	 * The default value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected static final String TIME_STAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected String timeStamp = TIME_STAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #getLObjectId() <em>LObject Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLObjectId()
	 * @generated
	 * @ordered
	 */
	protected static final String LOBJECT_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLObjectId() <em>LObject Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLObjectId()
	 * @generated
	 * @ordered
	 */
	protected String lObjectId = LOBJECT_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getZonedescriptors() <em>Zonedescriptors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZonedescriptors()
	 * @generated
	 * @ordered
	 */
	protected EList<ZoneDescriptor> zonedescriptors;

	/**
	 * The cached value of the '{@link #getPlacing() <em>Placing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlacing()
	 * @generated
	 * @ordered
	 */
	protected Placing placing;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PositionEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.POSITION_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Placing getPlacing() {
		if (placing != null && placing.eIsProxy()) {
			InternalEObject oldPlacing = (InternalEObject) placing;
			placing = (Placing) eResolveProxy(oldPlacing);
			if (placing != oldPlacing) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPos_DatamodelPackage.POSITION_EVENT__PLACING, oldPlacing, placing));
			}
		}
		return placing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Placing basicGetPlacing() {
		return placing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlacing(Placing newPlacing) {
		Placing oldPlacing = placing;
		placing = newPlacing;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POSITION_EVENT__PLACING,
					oldPlacing, placing));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeStamp(String newTimeStamp) {
		String oldTimeStamp = timeStamp;
		timeStamp = newTimeStamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POSITION_EVENT__TIME_STAMP,
					oldTimeStamp, timeStamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLObjectId() {
		return lObjectId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLObjectId(String newLObjectId) {
		String oldLObjectId = lObjectId;
		lObjectId = newLObjectId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POSITION_EVENT__LOBJECT_ID,
					oldLObjectId, lObjectId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ZoneDescriptor> getZonedescriptors() {
		if (zonedescriptors == null) {
			zonedescriptors = new EObjectResolvingEList<ZoneDescriptor>(ZoneDescriptor.class, this,
					IPos_DatamodelPackage.POSITION_EVENT__ZONEDESCRIPTORS);
		}
		return zonedescriptors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.POSITION_EVENT__TIME_STAMP:
			return getTimeStamp();
		case IPos_DatamodelPackage.POSITION_EVENT__LOBJECT_ID:
			return getLObjectId();
		case IPos_DatamodelPackage.POSITION_EVENT__ZONEDESCRIPTORS:
			return getZonedescriptors();
		case IPos_DatamodelPackage.POSITION_EVENT__PLACING:
			if (resolve)
				return getPlacing();
			return basicGetPlacing();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.POSITION_EVENT__TIME_STAMP:
			setTimeStamp((String) newValue);
			return;
		case IPos_DatamodelPackage.POSITION_EVENT__LOBJECT_ID:
			setLObjectId((String) newValue);
			return;
		case IPos_DatamodelPackage.POSITION_EVENT__ZONEDESCRIPTORS:
			getZonedescriptors().clear();
			getZonedescriptors().addAll((Collection<? extends ZoneDescriptor>) newValue);
			return;
		case IPos_DatamodelPackage.POSITION_EVENT__PLACING:
			setPlacing((Placing) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.POSITION_EVENT__TIME_STAMP:
			setTimeStamp(TIME_STAMP_EDEFAULT);
			return;
		case IPos_DatamodelPackage.POSITION_EVENT__LOBJECT_ID:
			setLObjectId(LOBJECT_ID_EDEFAULT);
			return;
		case IPos_DatamodelPackage.POSITION_EVENT__ZONEDESCRIPTORS:
			getZonedescriptors().clear();
			return;
		case IPos_DatamodelPackage.POSITION_EVENT__PLACING:
			setPlacing((Placing) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.POSITION_EVENT__TIME_STAMP:
			return TIME_STAMP_EDEFAULT == null ? timeStamp != null : !TIME_STAMP_EDEFAULT.equals(timeStamp);
		case IPos_DatamodelPackage.POSITION_EVENT__LOBJECT_ID:
			return LOBJECT_ID_EDEFAULT == null ? lObjectId != null : !LOBJECT_ID_EDEFAULT.equals(lObjectId);
		case IPos_DatamodelPackage.POSITION_EVENT__ZONEDESCRIPTORS:
			return zonedescriptors != null && !zonedescriptors.isEmpty();
		case IPos_DatamodelPackage.POSITION_EVENT__PLACING:
			return placing != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (timeStamp: ");
		result.append(timeStamp);
		result.append(", lObjectId: ");
		result.append(lObjectId);
		result.append(')');
		return result.toString();
	}

} //PositionEventImpl
