/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Accuracy;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.Point;
import ipos.project.DataModellntegration.iPos_Datamodel.Position;
import ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Position</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionImpl#getAccuracy <em>Accuracy</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionImpl#getPoint <em>Point</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.PositionImpl#getReferenceSystem <em>Reference System</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PositionImpl extends MinimalEObjectImpl.Container implements Position {
	/**
	 * The cached value of the '{@link #getAccuracy() <em>Accuracy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracy()
	 * @generated
	 * @ordered
	 */
	protected Accuracy accuracy;

	/**
	 * The cached value of the '{@link #getPoint() <em>Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPoint()
	 * @generated
	 * @ordered
	 */
	protected Point point;

	/**
	 * The cached value of the '{@link #getReferenceSystem() <em>Reference System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceSystem()
	 * @generated
	 * @ordered
	 */
	protected ReferenceSystem referenceSystem;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PositionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.POSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Accuracy getAccuracy() {
		if (accuracy != null && accuracy.eIsProxy()) {
			InternalEObject oldAccuracy = (InternalEObject) accuracy;
			accuracy = (Accuracy) eResolveProxy(oldAccuracy);
			if (accuracy != oldAccuracy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IPos_DatamodelPackage.POSITION__ACCURACY,
							oldAccuracy, accuracy));
			}
		}
		return accuracy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Accuracy basicGetAccuracy() {
		return accuracy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccuracy(Accuracy newAccuracy) {
		Accuracy oldAccuracy = accuracy;
		accuracy = newAccuracy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POSITION__ACCURACY, oldAccuracy,
					accuracy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Point getPoint() {
		if (point != null && point.eIsProxy()) {
			InternalEObject oldPoint = (InternalEObject) point;
			point = (Point) eResolveProxy(oldPoint);
			if (point != oldPoint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IPos_DatamodelPackage.POSITION__POINT,
							oldPoint, point));
			}
		}
		return point;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Point basicGetPoint() {
		return point;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPoint(Point newPoint) {
		Point oldPoint = point;
		point = newPoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POSITION__POINT, oldPoint,
					point));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceSystem getReferenceSystem() {
		if (referenceSystem != null && referenceSystem.eIsProxy()) {
			InternalEObject oldReferenceSystem = (InternalEObject) referenceSystem;
			referenceSystem = (ReferenceSystem) eResolveProxy(oldReferenceSystem);
			if (referenceSystem != oldReferenceSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPos_DatamodelPackage.POSITION__REFERENCE_SYSTEM, oldReferenceSystem, referenceSystem));
			}
		}
		return referenceSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceSystem basicGetReferenceSystem() {
		return referenceSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceSystem(ReferenceSystem newReferenceSystem) {
		ReferenceSystem oldReferenceSystem = referenceSystem;
		referenceSystem = newReferenceSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.POSITION__REFERENCE_SYSTEM,
					oldReferenceSystem, referenceSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.POSITION__ACCURACY:
			if (resolve)
				return getAccuracy();
			return basicGetAccuracy();
		case IPos_DatamodelPackage.POSITION__POINT:
			if (resolve)
				return getPoint();
			return basicGetPoint();
		case IPos_DatamodelPackage.POSITION__REFERENCE_SYSTEM:
			if (resolve)
				return getReferenceSystem();
			return basicGetReferenceSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.POSITION__ACCURACY:
			setAccuracy((Accuracy) newValue);
			return;
		case IPos_DatamodelPackage.POSITION__POINT:
			setPoint((Point) newValue);
			return;
		case IPos_DatamodelPackage.POSITION__REFERENCE_SYSTEM:
			setReferenceSystem((ReferenceSystem) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.POSITION__ACCURACY:
			setAccuracy((Accuracy) null);
			return;
		case IPos_DatamodelPackage.POSITION__POINT:
			setPoint((Point) null);
			return;
		case IPos_DatamodelPackage.POSITION__REFERENCE_SYSTEM:
			setReferenceSystem((ReferenceSystem) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.POSITION__ACCURACY:
			return accuracy != null;
		case IPos_DatamodelPackage.POSITION__POINT:
			return point != null;
		case IPos_DatamodelPackage.POSITION__REFERENCE_SYSTEM:
			return referenceSystem != null;
		}
		return super.eIsSet(featureID);
	}

} //PositionImpl
