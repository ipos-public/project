/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.Proximity;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Proximity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ProximityImpl#getTagId <em>Tag Id</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ProximityImpl#getType <em>Type</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.ProximityImpl#getScannerId <em>Scanner Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProximityImpl extends RawdataEventImpl implements Proximity {
	/**
	 * The default value of the '{@link #getTagId() <em>Tag Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTagId()
	 * @generated
	 * @ordered
	 */
	protected static final String TAG_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTagId() <em>Tag Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTagId()
	 * @generated
	 * @ordered
	 */
	protected String tagId = TAG_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getScannerId() <em>Scanner Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScannerId()
	 * @generated
	 * @ordered
	 */
	protected static final String SCANNER_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getScannerId() <em>Scanner Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScannerId()
	 * @generated
	 * @ordered
	 */
	protected String scannerId = SCANNER_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProximityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.PROXIMITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTagId() {
		return tagId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTagId(String newTagId) {
		String oldTagId = tagId;
		tagId = newTagId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.PROXIMITY__TAG_ID, oldTagId,
					tagId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.PROXIMITY__TYPE, oldType,
					type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScannerId() {
		return scannerId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScannerId(String newScannerId) {
		String oldScannerId = scannerId;
		scannerId = newScannerId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IPos_DatamodelPackage.PROXIMITY__SCANNER_ID,
					oldScannerId, scannerId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.PROXIMITY__TAG_ID:
			return getTagId();
		case IPos_DatamodelPackage.PROXIMITY__TYPE:
			return getType();
		case IPos_DatamodelPackage.PROXIMITY__SCANNER_ID:
			return getScannerId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.PROXIMITY__TAG_ID:
			setTagId((String) newValue);
			return;
		case IPos_DatamodelPackage.PROXIMITY__TYPE:
			setType((String) newValue);
			return;
		case IPos_DatamodelPackage.PROXIMITY__SCANNER_ID:
			setScannerId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.PROXIMITY__TAG_ID:
			setTagId(TAG_ID_EDEFAULT);
			return;
		case IPos_DatamodelPackage.PROXIMITY__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		case IPos_DatamodelPackage.PROXIMITY__SCANNER_ID:
			setScannerId(SCANNER_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.PROXIMITY__TAG_ID:
			return TAG_ID_EDEFAULT == null ? tagId != null : !TAG_ID_EDEFAULT.equals(tagId);
		case IPos_DatamodelPackage.PROXIMITY__TYPE:
			return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		case IPos_DatamodelPackage.PROXIMITY__SCANNER_ID:
			return SCANNER_ID_EDEFAULT == null ? scannerId != null : !SCANNER_ID_EDEFAULT.equals(scannerId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (tagId: ");
		result.append(tagId);
		result.append(", type: ");
		result.append(type);
		result.append(", scannerId: ");
		result.append(scannerId);
		result.append(')');
		return result.toString();
	}

} //ProximityImpl
