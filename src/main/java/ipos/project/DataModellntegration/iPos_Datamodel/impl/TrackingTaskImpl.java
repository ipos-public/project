/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tracking Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.TrackingTaskImpl#getEventfilterconfiguration <em>Eventfilterconfiguration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TrackingTaskImpl extends MinimalEObjectImpl.Container implements TrackingTask {
	/**
	 * The cached value of the '{@link #getEventfilterconfiguration() <em>Eventfilterconfiguration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventfilterconfiguration()
	 * @generated
	 * @ordered
	 */
	protected EventFilterConfiguration eventfilterconfiguration;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TrackingTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.TRACKING_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventFilterConfiguration getEventfilterconfiguration() {
		if (eventfilterconfiguration != null && eventfilterconfiguration.eIsProxy()) {
			InternalEObject oldEventfilterconfiguration = (InternalEObject) eventfilterconfiguration;
			eventfilterconfiguration = (EventFilterConfiguration) eResolveProxy(oldEventfilterconfiguration);
			if (eventfilterconfiguration != oldEventfilterconfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IPos_DatamodelPackage.TRACKING_TASK__EVENTFILTERCONFIGURATION, oldEventfilterconfiguration,
							eventfilterconfiguration));
			}
		}
		return eventfilterconfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventFilterConfiguration basicGetEventfilterconfiguration() {
		return eventfilterconfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventfilterconfiguration(EventFilterConfiguration newEventfilterconfiguration) {
		EventFilterConfiguration oldEventfilterconfiguration = eventfilterconfiguration;
		eventfilterconfiguration = newEventfilterconfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					IPos_DatamodelPackage.TRACKING_TASK__EVENTFILTERCONFIGURATION, oldEventfilterconfiguration,
					eventfilterconfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.TRACKING_TASK__EVENTFILTERCONFIGURATION:
			if (resolve)
				return getEventfilterconfiguration();
			return basicGetEventfilterconfiguration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.TRACKING_TASK__EVENTFILTERCONFIGURATION:
			setEventfilterconfiguration((EventFilterConfiguration) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.TRACKING_TASK__EVENTFILTERCONFIGURATION:
			setEventfilterconfiguration((EventFilterConfiguration) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.TRACKING_TASK__EVENTFILTERCONFIGURATION:
			return eventfilterconfiguration != null;
		}
		return super.eIsSet(featureID);
	}

} //TrackingTaskImpl
