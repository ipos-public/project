/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.UWB;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UWB</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UWBImpl extends BeaconImpl implements UWB {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UWBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.UWB;
	}

} //UWBImpl
