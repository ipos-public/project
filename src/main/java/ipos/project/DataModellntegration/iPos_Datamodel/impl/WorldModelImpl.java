/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.impl;

import ipos.project.DataModellntegration.iPos_Datamodel.Agent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage;
import ipos.project.DataModellntegration.iPos_Datamodel.POI;
import ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem;
import ipos.project.DataModellntegration.iPos_Datamodel.WorldModel;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>World Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.WorldModelImpl#getAgent <em>Agent</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.WorldModelImpl#getZoneMap <em>Zone Map</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.WorldModelImpl#getPois <em>Pois</em>}</li>
 *   <li>{@link ipos.project.DataModellntegration.iPos_Datamodel.impl.WorldModelImpl#getReferenceSystem <em>Reference System</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WorldModelImpl extends MinimalEObjectImpl.Container implements WorldModel {
	/**
	 * The cached value of the '{@link #getAgent() <em>Agent</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgent()
	 * @generated
	 * @ordered
	 */
	protected EList<Agent> agent;

	/**
	 * The cached value of the '{@link #getZoneMap() <em>Zone Map</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZoneMap()
	 * @generated
	 * @ordered
	 */
	protected EList<ZoneMap> zoneMap;

	/**
	 * The cached value of the '{@link #getPois() <em>Pois</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPois()
	 * @generated
	 * @ordered
	 */
	protected EList<POI> pois;

	/**
	 * The cached value of the '{@link #getReferenceSystem() <em>Reference System</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceSystem()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceSystem> referenceSystem;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorldModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IPos_DatamodelPackage.Literals.WORLD_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Agent> getAgent() {
		if (agent == null) {
			agent = new EObjectResolvingEList<Agent>(Agent.class, this, IPos_DatamodelPackage.WORLD_MODEL__AGENT);
		}
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ZoneMap> getZoneMap() {
		if (zoneMap == null) {
			zoneMap = new EObjectResolvingEList<ZoneMap>(ZoneMap.class, this,
					IPos_DatamodelPackage.WORLD_MODEL__ZONE_MAP);
		}
		return zoneMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<POI> getPois() {
		if (pois == null) {
			pois = new EObjectResolvingEList<POI>(POI.class, this, IPos_DatamodelPackage.WORLD_MODEL__POIS);
		}
		return pois;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceSystem> getReferenceSystem() {
		if (referenceSystem == null) {
			referenceSystem = new EObjectResolvingEList<ReferenceSystem>(ReferenceSystem.class, this,
					IPos_DatamodelPackage.WORLD_MODEL__REFERENCE_SYSTEM);
		}
		return referenceSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IPos_DatamodelPackage.WORLD_MODEL__AGENT:
			return getAgent();
		case IPos_DatamodelPackage.WORLD_MODEL__ZONE_MAP:
			return getZoneMap();
		case IPos_DatamodelPackage.WORLD_MODEL__POIS:
			return getPois();
		case IPos_DatamodelPackage.WORLD_MODEL__REFERENCE_SYSTEM:
			return getReferenceSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IPos_DatamodelPackage.WORLD_MODEL__AGENT:
			getAgent().clear();
			getAgent().addAll((Collection<? extends Agent>) newValue);
			return;
		case IPos_DatamodelPackage.WORLD_MODEL__ZONE_MAP:
			getZoneMap().clear();
			getZoneMap().addAll((Collection<? extends ZoneMap>) newValue);
			return;
		case IPos_DatamodelPackage.WORLD_MODEL__POIS:
			getPois().clear();
			getPois().addAll((Collection<? extends POI>) newValue);
			return;
		case IPos_DatamodelPackage.WORLD_MODEL__REFERENCE_SYSTEM:
			getReferenceSystem().clear();
			getReferenceSystem().addAll((Collection<? extends ReferenceSystem>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.WORLD_MODEL__AGENT:
			getAgent().clear();
			return;
		case IPos_DatamodelPackage.WORLD_MODEL__ZONE_MAP:
			getZoneMap().clear();
			return;
		case IPos_DatamodelPackage.WORLD_MODEL__POIS:
			getPois().clear();
			return;
		case IPos_DatamodelPackage.WORLD_MODEL__REFERENCE_SYSTEM:
			getReferenceSystem().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IPos_DatamodelPackage.WORLD_MODEL__AGENT:
			return agent != null && !agent.isEmpty();
		case IPos_DatamodelPackage.WORLD_MODEL__ZONE_MAP:
			return zoneMap != null && !zoneMap.isEmpty();
		case IPos_DatamodelPackage.WORLD_MODEL__POIS:
			return pois != null && !pois.isEmpty();
		case IPos_DatamodelPackage.WORLD_MODEL__REFERENCE_SYSTEM:
			return referenceSystem != null && !referenceSystem.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //WorldModelImpl
