/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.util;

import ipos.project.DataModellntegration.iPos_Datamodel.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage
 * @generated
 */
public class IPos_DatamodelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static IPos_DatamodelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPos_DatamodelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = IPos_DatamodelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPos_DatamodelSwitch<Adapter> modelSwitch = new IPos_DatamodelSwitch<Adapter>() {
		@Override
		public Adapter caseAgent(Agent object) {
			return createAgentAdapter();
		}

		@Override
		public Adapter caseEntity(Entity object) {
			return createEntityAdapter();
		}

		@Override
		public Adapter caseLocalizableObject(LocalizableObject object) {
			return createLocalizableObjectAdapter();
		}

		@Override
		public Adapter casePlacing(Placing object) {
			return createPlacingAdapter();
		}

		@Override
		public Adapter casePosition(Position object) {
			return createPositionAdapter();
		}

		@Override
		public Adapter caseOrientation(Orientation object) {
			return createOrientationAdapter();
		}

		@Override
		public Adapter caseWGS84Point(WGS84Point object) {
			return createWGS84PointAdapter();
		}

		@Override
		public Adapter caseAccuracy(Accuracy object) {
			return createAccuracyAdapter();
		}

		@Override
		public Adapter casePoint2D(Point2D object) {
			return createPoint2DAdapter();
		}

		@Override
		public Adapter casePoint3D(Point3D object) {
			return createPoint3DAdapter();
		}

		@Override
		public Adapter caseReferenceSystem(ReferenceSystem object) {
			return createReferenceSystemAdapter();
		}

		@Override
		public Adapter caseZone(Zone object) {
			return createZoneAdapter();
		}

		@Override
		public Adapter caseZoneMap(ZoneMap object) {
			return createZoneMapAdapter();
		}

		@Override
		public Adapter caseSpace(Space object) {
			return createSpaceAdapter();
		}

		@Override
		public Adapter caseMapType(MapType object) {
			return createMapTypeAdapter();
		}

		@Override
		public Adapter caseQuaternion(Quaternion object) {
			return createQuaternionAdapter();
		}

		@Override
		public Adapter caseGaussian(Gaussian object) {
			return createGaussianAdapter();
		}

		@Override
		public Adapter casePoint(Point object) {
			return createPointAdapter();
		}

		@Override
		public Adapter caseRawdataEvent(RawdataEvent object) {
			return createRawdataEventAdapter();
		}

		@Override
		public Adapter caseProximity(Proximity object) {
			return createProximityAdapter();
		}

		@Override
		public Adapter caseRFID(RFID object) {
			return createRFIDAdapter();
		}

		@Override
		public Adapter caseNFC(NFC object) {
			return createNFCAdapter();
		}

		@Override
		public Adapter caseIMU(IMU object) {
			return createIMUAdapter();
		}

		@Override
		public Adapter caseAcceleration(Acceleration object) {
			return createAccelerationAdapter();
		}

		@Override
		public Adapter caseBeacon(Beacon object) {
			return createBeaconAdapter();
		}

		@Override
		public Adapter casePositionEvent(PositionEvent object) {
			return createPositionEventAdapter();
		}

		@Override
		public Adapter caseEventFilterConfiguration(EventFilterConfiguration object) {
			return createEventFilterConfigurationAdapter();
		}

		@Override
		public Adapter caseMonitoringTask(MonitoringTask object) {
			return createMonitoringTaskAdapter();
		}

		@Override
		public Adapter caseTrackingTask(TrackingTask object) {
			return createTrackingTaskAdapter();
		}

		@Override
		public Adapter caseWorldModel(WorldModel object) {
			return createWorldModelAdapter();
		}

		@Override
		public Adapter caseBarcode(Barcode object) {
			return createBarcodeAdapter();
		}

		@Override
		public Adapter caseOtherProx(OtherProx object) {
			return createOtherProxAdapter();
		}

		@Override
		public Adapter caseBluetooth(Bluetooth object) {
			return createBluetoothAdapter();
		}

		@Override
		public Adapter caseUWB(UWB object) {
			return createUWBAdapter();
		}

		@Override
		public Adapter caseOtherBeacon(OtherBeacon object) {
			return createOtherBeaconAdapter();
		}

		@Override
		public Adapter casePOI(POI object) {
			return createPOIAdapter();
		}

		@Override
		public Adapter caseEventFilterCondition(EventFilterCondition object) {
			return createEventFilterConditionAdapter();
		}

		@Override
		public Adapter caseZoneDescriptor(ZoneDescriptor object) {
			return createZoneDescriptorAdapter();
		}

		@Override
		public Adapter caseDataStorageQueryResponse(DataStorageQueryResponse object) {
			return createDataStorageQueryResponseAdapter();
		}

		@Override
		public Adapter caseMessageReceivedEvent(MessageReceivedEvent object) {
			return createMessageReceivedEventAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Agent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Agent
	 * @generated
	 */
	public Adapter createAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Entity
	 * @generated
	 */
	public Adapter createEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject <em>Localizable Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.LocalizableObject
	 * @generated
	 */
	public Adapter createLocalizableObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Placing <em>Placing</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Placing
	 * @generated
	 */
	public Adapter createPlacingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Position <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Position
	 * @generated
	 */
	public Adapter createPositionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Orientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Orientation
	 * @generated
	 */
	public Adapter createOrientationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point <em>WGS84 Point</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WGS84Point
	 * @generated
	 */
	public Adapter createWGS84PointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Accuracy <em>Accuracy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Accuracy
	 * @generated
	 */
	public Adapter createAccuracyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point2D <em>Point2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point2D
	 * @generated
	 */
	public Adapter createPoint2DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point3D <em>Point3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point3D
	 * @generated
	 */
	public Adapter createPoint3DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem <em>Reference System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ReferenceSystem
	 * @generated
	 */
	public Adapter createReferenceSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Zone <em>Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Zone
	 * @generated
	 */
	public Adapter createZoneAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap <em>Zone Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ZoneMap
	 * @generated
	 */
	public Adapter createZoneMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Space <em>Space</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Space
	 * @generated
	 */
	public Adapter createSpaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.MapType <em>Map Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MapType
	 * @generated
	 */
	public Adapter createMapTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Quaternion <em>Quaternion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Quaternion
	 * @generated
	 */
	public Adapter createQuaternionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Gaussian <em>Gaussian</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Gaussian
	 * @generated
	 */
	public Adapter createGaussianAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Point <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Point
	 * @generated
	 */
	public Adapter createPointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.RawdataEvent <em>Rawdata Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.RawdataEvent
	 * @generated
	 */
	public Adapter createRawdataEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Proximity <em>Proximity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Proximity
	 * @generated
	 */
	public Adapter createProximityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.RFID <em>RFID</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.RFID
	 * @generated
	 */
	public Adapter createRFIDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.NFC <em>NFC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.NFC
	 * @generated
	 */
	public Adapter createNFCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.IMU <em>IMU</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.IMU
	 * @generated
	 */
	public Adapter createIMUAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Acceleration <em>Acceleration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Acceleration
	 * @generated
	 */
	public Adapter createAccelerationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Beacon <em>Beacon</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Beacon
	 * @generated
	 */
	public Adapter createBeaconAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent <em>Position Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent
	 * @generated
	 */
	public Adapter createPositionEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration <em>Event Filter Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterConfiguration
	 * @generated
	 */
	public Adapter createEventFilterConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask <em>Monitoring Task</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MonitoringTask
	 * @generated
	 */
	public Adapter createMonitoringTaskAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask <em>Tracking Task</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.TrackingTask
	 * @generated
	 */
	public Adapter createTrackingTaskAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.WorldModel <em>World Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.WorldModel
	 * @generated
	 */
	public Adapter createWorldModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Barcode <em>Barcode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Barcode
	 * @generated
	 */
	public Adapter createBarcodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OtherProx <em>Other Prox</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OtherProx
	 * @generated
	 */
	public Adapter createOtherProxAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth <em>Bluetooth</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth
	 * @generated
	 */
	public Adapter createBluetoothAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.UWB <em>UWB</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.UWB
	 * @generated
	 */
	public Adapter createUWBAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.OtherBeacon <em>Other Beacon</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.OtherBeacon
	 * @generated
	 */
	public Adapter createOtherBeaconAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.POI <em>POI</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.POI
	 * @generated
	 */
	public Adapter createPOIAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition <em>Event Filter Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.EventFilterCondition
	 * @generated
	 */
	public Adapter createEventFilterConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor <em>Zone Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor
	 * @generated
	 */
	public Adapter createZoneDescriptorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse <em>Data Storage Query Response</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.DataStorageQueryResponse
	 * @generated
	 */
	public Adapter createDataStorageQueryResponseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent <em>Message Received Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent
	 * @generated
	 */
	public Adapter createMessageReceivedEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //IPos_DatamodelAdapterFactory
