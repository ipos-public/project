/**
 */
package ipos.project.DataModellntegration.iPos_Datamodel.util;

import ipos.project.DataModellntegration.iPos_Datamodel.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelPackage
 * @generated
 */
public class IPos_DatamodelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static IPos_DatamodelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPos_DatamodelSwitch() {
		if (modelPackage == null) {
			modelPackage = IPos_DatamodelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case IPos_DatamodelPackage.AGENT: {
			Agent agent = (Agent) theEObject;
			T result = caseAgent(agent);
			if (result == null)
				result = caseEntity(agent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.ENTITY: {
			Entity entity = (Entity) theEObject;
			T result = caseEntity(entity);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.LOCALIZABLE_OBJECT: {
			LocalizableObject localizableObject = (LocalizableObject) theEObject;
			T result = caseLocalizableObject(localizableObject);
			if (result == null)
				result = caseEntity(localizableObject);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.PLACING: {
			Placing placing = (Placing) theEObject;
			T result = casePlacing(placing);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.POSITION: {
			Position position = (Position) theEObject;
			T result = casePosition(position);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.ORIENTATION: {
			Orientation orientation = (Orientation) theEObject;
			T result = caseOrientation(orientation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.WGS84_POINT: {
			WGS84Point wgs84Point = (WGS84Point) theEObject;
			T result = caseWGS84Point(wgs84Point);
			if (result == null)
				result = casePoint(wgs84Point);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.ACCURACY: {
			Accuracy accuracy = (Accuracy) theEObject;
			T result = caseAccuracy(accuracy);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.POINT2_D: {
			Point2D point2D = (Point2D) theEObject;
			T result = casePoint2D(point2D);
			if (result == null)
				result = casePoint(point2D);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.POINT3_D: {
			Point3D point3D = (Point3D) theEObject;
			T result = casePoint3D(point3D);
			if (result == null)
				result = casePoint(point3D);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.REFERENCE_SYSTEM: {
			ReferenceSystem referenceSystem = (ReferenceSystem) theEObject;
			T result = caseReferenceSystem(referenceSystem);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.ZONE: {
			Zone zone = (Zone) theEObject;
			T result = caseZone(zone);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.ZONE_MAP: {
			ZoneMap zoneMap = (ZoneMap) theEObject;
			T result = caseZoneMap(zoneMap);
			if (result == null)
				result = caseMapType(zoneMap);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.SPACE: {
			Space space = (Space) theEObject;
			T result = caseSpace(space);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.MAP_TYPE: {
			MapType mapType = (MapType) theEObject;
			T result = caseMapType(mapType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.QUATERNION: {
			Quaternion quaternion = (Quaternion) theEObject;
			T result = caseQuaternion(quaternion);
			if (result == null)
				result = caseOrientation(quaternion);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.GAUSSIAN: {
			Gaussian gaussian = (Gaussian) theEObject;
			T result = caseGaussian(gaussian);
			if (result == null)
				result = caseAccuracy(gaussian);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.POINT: {
			Point point = (Point) theEObject;
			T result = casePoint(point);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.RAWDATA_EVENT: {
			RawdataEvent rawdataEvent = (RawdataEvent) theEObject;
			T result = caseRawdataEvent(rawdataEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.PROXIMITY: {
			Proximity proximity = (Proximity) theEObject;
			T result = caseProximity(proximity);
			if (result == null)
				result = caseRawdataEvent(proximity);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.RFID: {
			RFID rfid = (RFID) theEObject;
			T result = caseRFID(rfid);
			if (result == null)
				result = caseProximity(rfid);
			if (result == null)
				result = caseRawdataEvent(rfid);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.NFC: {
			NFC nfc = (NFC) theEObject;
			T result = caseNFC(nfc);
			if (result == null)
				result = caseProximity(nfc);
			if (result == null)
				result = caseRawdataEvent(nfc);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.IMU: {
			IMU imu = (IMU) theEObject;
			T result = caseIMU(imu);
			if (result == null)
				result = caseRawdataEvent(imu);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.ACCELERATION: {
			Acceleration acceleration = (Acceleration) theEObject;
			T result = caseAcceleration(acceleration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.BEACON: {
			Beacon beacon = (Beacon) theEObject;
			T result = caseBeacon(beacon);
			if (result == null)
				result = caseRawdataEvent(beacon);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.POSITION_EVENT: {
			PositionEvent positionEvent = (PositionEvent) theEObject;
			T result = casePositionEvent(positionEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.EVENT_FILTER_CONFIGURATION: {
			EventFilterConfiguration eventFilterConfiguration = (EventFilterConfiguration) theEObject;
			T result = caseEventFilterConfiguration(eventFilterConfiguration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.MONITORING_TASK: {
			MonitoringTask monitoringTask = (MonitoringTask) theEObject;
			T result = caseMonitoringTask(monitoringTask);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.TRACKING_TASK: {
			TrackingTask trackingTask = (TrackingTask) theEObject;
			T result = caseTrackingTask(trackingTask);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.WORLD_MODEL: {
			WorldModel worldModel = (WorldModel) theEObject;
			T result = caseWorldModel(worldModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.BARCODE: {
			Barcode barcode = (Barcode) theEObject;
			T result = caseBarcode(barcode);
			if (result == null)
				result = caseProximity(barcode);
			if (result == null)
				result = caseRawdataEvent(barcode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.OTHER_PROX: {
			OtherProx otherProx = (OtherProx) theEObject;
			T result = caseOtherProx(otherProx);
			if (result == null)
				result = caseProximity(otherProx);
			if (result == null)
				result = caseRawdataEvent(otherProx);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.BLUETOOTH: {
			Bluetooth bluetooth = (Bluetooth) theEObject;
			T result = caseBluetooth(bluetooth);
			if (result == null)
				result = caseBeacon(bluetooth);
			if (result == null)
				result = caseRawdataEvent(bluetooth);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.UWB: {
			UWB uwb = (UWB) theEObject;
			T result = caseUWB(uwb);
			if (result == null)
				result = caseBeacon(uwb);
			if (result == null)
				result = caseRawdataEvent(uwb);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.OTHER_BEACON: {
			OtherBeacon otherBeacon = (OtherBeacon) theEObject;
			T result = caseOtherBeacon(otherBeacon);
			if (result == null)
				result = caseBeacon(otherBeacon);
			if (result == null)
				result = caseRawdataEvent(otherBeacon);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.POI: {
			POI poi = (POI) theEObject;
			T result = casePOI(poi);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.EVENT_FILTER_CONDITION: {
			EventFilterCondition eventFilterCondition = (EventFilterCondition) theEObject;
			T result = caseEventFilterCondition(eventFilterCondition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.ZONE_DESCRIPTOR: {
			ZoneDescriptor zoneDescriptor = (ZoneDescriptor) theEObject;
			T result = caseZoneDescriptor(zoneDescriptor);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.DATA_STORAGE_QUERY_RESPONSE: {
			DataStorageQueryResponse dataStorageQueryResponse = (DataStorageQueryResponse) theEObject;
			T result = caseDataStorageQueryResponse(dataStorageQueryResponse);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IPos_DatamodelPackage.MESSAGE_RECEIVED_EVENT: {
			MessageReceivedEvent messageReceivedEvent = (MessageReceivedEvent) theEObject;
			T result = caseMessageReceivedEvent(messageReceivedEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAgent(Agent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntity(Entity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Localizable Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Localizable Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalizableObject(LocalizableObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Placing</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Placing</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePlacing(Placing object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Position</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePosition(Position object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Orientation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Orientation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrientation(Orientation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>WGS84 Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>WGS84 Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWGS84Point(WGS84Point object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Accuracy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Accuracy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccuracy(Accuracy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point2 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point2 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePoint2D(Point2D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point3 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point3 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePoint3D(Point3D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceSystem(ReferenceSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Zone</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Zone</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseZone(Zone object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Zone Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Zone Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseZoneMap(ZoneMap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Space</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Space</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpace(Space object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Map Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Map Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMapType(MapType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quaternion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quaternion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuaternion(Quaternion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gaussian</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gaussian</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGaussian(Gaussian object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePoint(Point object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rawdata Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rawdata Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRawdataEvent(RawdataEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Proximity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Proximity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProximity(Proximity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RFID</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RFID</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRFID(RFID object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NFC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NFC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNFC(NFC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IMU</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IMU</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIMU(IMU object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acceleration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acceleration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcceleration(Acceleration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Beacon</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Beacon</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBeacon(Beacon object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Position Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Position Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePositionEvent(PositionEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Filter Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Filter Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventFilterConfiguration(EventFilterConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Monitoring Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Monitoring Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonitoringTask(MonitoringTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tracking Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tracking Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrackingTask(TrackingTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>World Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>World Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorldModel(WorldModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Barcode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Barcode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBarcode(Barcode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Other Prox</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Other Prox</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOtherProx(OtherProx object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bluetooth</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bluetooth</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBluetooth(Bluetooth object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UWB</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UWB</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUWB(UWB object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Other Beacon</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Other Beacon</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOtherBeacon(OtherBeacon object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>POI</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>POI</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePOI(POI object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Filter Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Filter Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventFilterCondition(EventFilterCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Zone Descriptor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Zone Descriptor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseZoneDescriptor(ZoneDescriptor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Storage Query Response</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Storage Query Response</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataStorageQueryResponse(DataStorageQueryResponse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Received Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Received Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageReceivedEvent(MessageReceivedEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //IPos_DatamodelSwitch
