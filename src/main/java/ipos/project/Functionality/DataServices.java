package ipos.project.Functionality;

import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.entity.IWorldModelAccess;
import ipos.project.entity.LocalObjectWorldModel;
import org.apache.logging.log4j.LogManager;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DataServices {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;

    private static IWorldModelAccess wma;

    private static Map<String, List<PositionEvent>> trackingStorage = new HashMap<>();

    public static void clearWorldModel(){
        wma = new LocalObjectWorldModel();
    }

    public static ReferenceSystem getReferenceSystemByIdOrNull(String refSystemId) {
        for (ReferenceSystem refSystem : wma.getAllRefSystems()){
            if(refSystem.getId().equals(refSystemId)){
                return refSystem;
            }
        }
        return null;
    }

    public static LocalizableObject getLObjectByIdOrNull(String lObjectId) {
        if(wma == null){
            LOG.info("wma is null. lObjectId: " + lObjectId);
            return null;
        }
        for (Agent agent : wma.getAllAgents()){
            for (LocalizableObject lObject : agent.getLObject()){
                if (lObject.getId().equals(lObjectId)){
                    return lObject;
                }
            }
        }
        return null;
    }

    public static Zone getZoneByIdOrNull(String zoneId){
        for (var zone : wma.getAllZones()){
            if(zone.getId().equals(zoneId)){
                return zone;
            }
        }
        return null;
    }

    public static Agent getAgentForLocalizableObject(String lObjectId) throws IllegalArgumentException {
        LocalizableObject lObject = DataServices.getLObjectByIdOrNull(lObjectId);
        if (lObject == null || lObject.getAgent() == null) throw new IllegalArgumentException("localizable object unknown or has no agent associated");
        return lObject.getAgent();
    }

    public static void addZone(Zone zone_input) {
        for (Zone zone_loaded : wma.getAllZones()){
            if (zone_loaded.getId().equals(zone_input.getId())){
                LOG.warn("Zone " + zone_input.getId() + " could not be added to world model, as it already contains" +
                        "a zone with the same id");
                return;
            }
        }
        wma.addZone(zone_input);
    }

    public static void persist(PositionEvent positionEvent, String trackingTaskId){
        List<PositionEvent> trackedPositions = trackingStorage.get(trackingTaskId);
        if(trackedPositions == null){
            throw new IllegalArgumentException("unknown trackingTaskId");
        }
        trackedPositions.add(positionEvent);
    }

    public static List<PositionEvent> query(String trackingTaskId){
        List<PositionEvent> trackedPositions = trackingStorage.get(trackingTaskId);
        if(trackedPositions == null){
            throw new IllegalArgumentException("unknown trackingTaskId");
        }
        if (trackedPositions.equals(new LinkedList<PositionEvent>())){
            LOG.warn("No PositionEvents have been tracked for trackingTaskId " + trackingTaskId);
        }
        return trackedPositions;
    }

    public static void initTrackingTask(String trackingTaskId){
        if (trackingStorage.get(trackingTaskId)!= null){
            LOG.warn("Cleaning tracking task " + trackingTaskId);
        }
        trackingStorage.put(trackingTaskId, new LinkedList<PositionEvent>());
    }

    public static void addAgent(Agent agent_input) {
        for (Agent agent_loaded : wma.getAllAgents()){
            if (agent_loaded.getId().equals(agent_input.getId())){
                mergeInputAgentIntoLoadedAgent(agent_loaded, agent_input);
                return;
                // TODO: careful: What about those LObjects that point on the currently existing agent-object?
                // wma.removeAgent(agent_loaded.getId());
                // wma.addAgent(agent_merged);
            }
        }
        wma.addAgent(agent_input);
    }

    /**
     * Keeps those LocalizableObjects that are currently connected to the loaded agent
     * and adds those LocalizableObjects that are additionally connected to the input-agent.
     * @param agent_loaded
     * @param agent_input
     * @return
     */
    private static Agent mergeInputAgentIntoLoadedAgent(Agent agent_loaded, Agent agent_input) {
        List<LocalizableObject> lObjects_loaded = agent_loaded.getLObject();
        List<LocalizableObject> lObjects_input = agent_input.getLObject();
        for (LocalizableObject lObject_input : lObjects_input){
            if (!containsLObjectId(lObjects_loaded, lObject_input.getId())){
                lObjects_loaded.add(lObject_input);
                LOG.info("Added lObject with id " + lObject_input.getId() + " to agent " + agent_loaded.getId());
            }
        }
        return agent_loaded;
    }

    private static boolean containsLObjectId(List<LocalizableObject> lObjects, String id) {
        for (LocalizableObject lObject : lObjects){
            if(lObject.getId().equals(id)){
                return true;
            }
        }
        return false;
    }

    public static void addRefSystem(ReferenceSystem refSystem_input) {
        for (ReferenceSystem refSystem_loaded : wma.getAllRefSystems()){
            if (refSystem_loaded.getId().equals(refSystem_input.getId())){
                LOG.warn("Reference system " + refSystem_input.getId() + " could not be added to world model, as it already contains" +
                        "a reference system with the same id");
                return;
            }
        }
        wma.addRefSystem(refSystem_input);
    }

    public static void addPoi(POI poi_input) {
        for (POI poi_loaded : wma.getAllPois()){
            if (poi_loaded.getId().equals(poi_input.getId())){
                LOG.warn("POI " + poi_input.getId() + " could not be added to world model, as it already contains" +
                        "a poi with the same id");
                return;
            }
        }
        wma.addPoi(poi_input);
    }

    public static POI createPOI(Placing placing, String description, Map<String, String> data){
        POI poi = modelFactory.createPOI();
        poi.setPlacing(placing);
        poi.setDescription(description);
        poi.setData(data);
        return poi;
    }

    public static Agent createAgent(String id, String type){
        Agent agent = modelFactory.createAgent();
        agent.setId(id);
        agent.setAgentType(type);
        return agent;
    }

    public static LocalizableObject createLobject(String id, String sensorType, Agent agent, Placing currentPlacing){
        LocalizableObject lobject = modelFactory.createLocalizableObject();
        lobject.setAgent(agent);
        lobject.setId(id);
        lobject.setSensorType(sensorType);
        lobject.setCurrentPlacing(currentPlacing);
        return lobject;
    }

    public static Placing createPlacing(Position position, Orientation orientation){
        Placing placing = modelFactory.createPlacing();
        placing.setPosition(position);
        placing.setOrientation(orientation);
        return placing;
    }

    public static Position createPosition(Point3D point3D, Float accuracy, ReferenceSystem refSystem){
        Position position = modelFactory.createPosition();
        position.setPoint(point3D);
        Gaussian accrcy = modelFactory.createGaussian();
        accrcy.setConfidenceInterval(accuracy);
        position.setAccuracy(accrcy);
        position.setReferenceSystem(refSystem);
        return position;
    }

    public static Point3D createPoint3D(float x, float y, float z){
        Point3D point3D = modelFactory.createPoint3D();
        point3D.setX(x);
        point3D.setY(y);
        point3D.setZ(z);
        return point3D;
    }

    public static ReferenceSystem createReferenceSystem(String name, String id, Placing origin){
        ReferenceSystem refSystem = modelFactory.createReferenceSystem();
        refSystem.setName(name);
        refSystem.setId(id);
        refSystem.setOrigin(origin);
        return refSystem;
    }

    public static Zone createZone(String name, String id, List<Space> spaces){
        Zone zone = modelFactory.createZone();
        zone.setName(name);
        zone.setId(id);
        for (var space : spaces) {
            zone.getSpace().add(space);
        }
        return zone;
    }

    public static Space createSpace(float x, float y, float z, Placing centrePoint){
        Space space = modelFactory.createSpace();
        space.setX(x);
        space.setY(y);
        space.setZ(z);
        space.setCentrePoint(centrePoint);
        return space;
    }

    public static Quaternion createOrientation(float x, float y, float z, float w){
        Quaternion quaternion = modelFactory.createQuaternion();
        quaternion.setX(x);
        quaternion.setY(y);
        quaternion.setZ(z);
        quaternion.setW(w);
        return quaternion;
    }

    public static POI getPoiByIdOrNull(String poiId) {
        for (POI poi : wma.getAllPois()){
            if(poi.getId().equals(poiId)){
                return poi;
            }
        }
        return null;
    }
}
