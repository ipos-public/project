package ipos.project.Functionality;

import ipos.project.DataModellntegration.iPos_Datamodel.*;
import org.apache.logging.log4j.LogManager;

public class PositionCalculation {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;

    public static PositionEvent calcPositionFromNfcOrNull(NFC nfcRawDataEvent) {
        if (isMobileScanner(nfcRawDataEvent)){
            return createPosEvent(nfcRawDataEvent.getScannerId(), nfcRawDataEvent.getTagId(), nfcRawDataEvent.getTimeStamp());
        }
        if(isMobileTag(nfcRawDataEvent)){
            return createPosEvent(nfcRawDataEvent.getTagId(), nfcRawDataEvent.getScannerId(), nfcRawDataEvent.getTimeStamp());
        }
        LOG.warn("NFC-RawdataEvent could not be processed, as the provided scanner " +
                "and/or tag-id could not be associated to an registered object and/or " +
                "an registered poi");
        return null;
    }

    private static PositionEvent createPosEvent(String lObjectId, String poiId, String timeStamp) {
        PositionEvent positionEvent = modelFactory.createPositionEvent();
        positionEvent.setLObjectId(lObjectId);
        positionEvent.setTimeStamp(timeStamp);
        positionEvent.setPlacing(DataServices.getPoiByIdOrNull(poiId).getPlacing());
        return positionEvent;
    }

    private static boolean isMobileScanner(NFC nfcRawDataEvent) {
        Boolean scannerIsMobile = DataServices.getLObjectByIdOrNull(nfcRawDataEvent.getScannerId()) != null;
        Boolean tagIsFixed = DataServices.getPoiByIdOrNull(nfcRawDataEvent.getTagId()) != null;
        return scannerIsMobile && tagIsFixed;
    }

    private static boolean isMobileTag(NFC nfcRawDataEvent) {
        Boolean tagIsMobile = DataServices.getLObjectByIdOrNull(nfcRawDataEvent.getTagId()) != null;
        Boolean scannerIsFixed = DataServices.getPoiByIdOrNull(nfcRawDataEvent.getScannerId()) != null;
        return tagIsMobile && scannerIsFixed;
    }
}
