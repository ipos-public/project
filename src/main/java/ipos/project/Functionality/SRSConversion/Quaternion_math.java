package ipos.project.Functionality.SRSConversion;

public class Quaternion_math {
    private float x;
    private float y;
    private float z;
    private float w;
    //private float[] matrixs;

    public Quaternion_math(final Quaternion_math q) {
        this(q.x, q.y, q.z, q.w);
    }

    public Quaternion_math(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public void set(final Quaternion_math q) {
        //matrixs = null;
        this.x = q.x;
        this.y = q.y;
        this.z = q.z;
        this.w = q.w;
    }

    public Quaternion_math(Vector_math axis, float angle) {
        set(axis, angle);
    }

    public float norm() {
        return  (float) Math.sqrt(dot(this));
    }

    public float getW() {
        return w;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    /**
     * @param axis
     *            rotation axis, unit vector
     * @param angle
     *            the rotation angle
     * @return this
     */
    public Quaternion_math set(Vector_math axis, float angle) {
        //matrixs = null;
        angle = (float) Math.toRadians(angle);
        float s = (float) Math.sin(angle);
        w = (float) Math.cos(angle);
        x = axis.getX() * s;
        y = axis.getY() * s;
        z = axis.getZ() * s;
        //System.out.println("angle set: " + angle);
        return this;
    }

    public Quaternion_math mulThis(Quaternion_math q) {
        //matrixs = null;
        float nw = w * q.w - x * q.x - y * q.y - z * q.z;
        float nx = w * q.x + x * q.w + y * q.z - z * q.y;
        float ny = w * q.y + y * q.w + z * q.x - x * q.z;
        z = w * q.z + z * q.w + x * q.y - y * q.x;
        w = nw;
        x = nx;
        y = ny;
        return this;
    }

    public Quaternion_math scaleThis(float scale) {
        if (scale != 1) {
            //matrixs = null;
            w *= scale;
            x *= scale;
            y *= scale;
            z *= scale;
        }
        return this;
    }

    public Quaternion_math divThis(float scale) {
        if (scale != 1) {
            //matrixs = null;
            w /= scale;
            x /= scale;
            y /= scale;
            z /= scale;
        }
        return this;
    }

    public float dot(Quaternion_math q) {
        return x * q.x + y * q.y + z * q.z + w * q.w;
    }

    public boolean equals(Quaternion_math q) {
        return x == q.x && y == q.y && z == q.z && w == q.w;
    }

    public Quaternion_math interpolateThis(Quaternion_math q, float t) {
        if (!equals(q)) {
            float d = dot(q);
            float qx, qy, qz, qw;

            if (d < 0f) {
                qx = -q.x;
                qy = -q.y;
                qz = -q.z;
                qw = -q.w;
                d = -d;
            } else {
                qx = q.x;
                qy = q.y;
                qz = q.z;
                qw = q.w;
            }

            float f0, f1;

            if ((1 - d) > 0.1f) {
                float angle = (float) Math.acos(d);
                float s = (float) Math.sin(angle);
                float tAngle = t * angle;
                f0 = (float) Math.sin(angle - tAngle) / s;
                f1 = (float) Math.sin(tAngle) / s;
            } else {
                f0 = 1 - t;
                f1 = t;
            }

            x = f0 * x + f1 * qx;
            y = f0 * y + f1 * qy;
            z = f0 * z + f1 * qz;
            w = f0 * w + f1 * qw;
        }

        return this;
    }

    public Quaternion_math inverse() {
        float norm = norm();
        if (norm > 0.0) {
            float invNorm = 1.0f / norm;
            return new Quaternion_math(-x * invNorm, -y * invNorm, -z * invNorm, w
                    * invNorm);
        }
        // return an invalid result to flag the error
        return new Quaternion_math(0,0,0,0);
    }
    public Quaternion_math normalizeThis() {
        return divThis(norm());
    }

    public Quaternion_math interpolate(Quaternion_math q, float t) {
        return new Quaternion_math(this).interpolateThis(q, t);
    }




    /**
     * Converts this Quaternion_math into a matrix, returning it as a float array.
     */
    public float[] toMatrix() {
        float[] matrixs = new float[16];
        toMatrix(matrixs);
        return matrixs;
    }
    /** Conjugate the Quaternion_math.
     *
     * @return This Quaternion_math for chaining */
    public Quaternion_math conjugate () {
        x = -x;
        y = -y;
        z = -z;
        return this;
    }

    /** Multiplies this Quaternion_math with another one
     *
     * @param q Quaternion_math to multiply with
     * @return This Quaternion_math for chaining */
    public Quaternion_math mul (Quaternion_math q) {
        float newX = w * q.x + x * q.w + y * q.z - z * q.y;
        float newY = w * q.y + y * q.w + z * q.x - x * q.z;
        float newZ = w * q.z + z * q.w + x * q.y - y * q.x;
        float newW = w * q.w - x * q.x - y * q.y - z * q.z;
        x = newX;
        y = newY;
        z = newZ;
        w = newW;
        return this;
    }

    /** Multiplies this Quaternion_math with another one in the form of q * this
     *
     * @param q Quaternion_math to multiply with
     * @return This Quaternion_math for chaining */
    public Quaternion_math mulLeft (Quaternion_math q) {
        float newX = q.w * x + q.x * w + q.y * z - q.z * y;
        float newY = q.w * y + q.y * w + q.z * x - q.x * z;
        float newZ = q.w * z + q.z * w + q.x * y - q.y * x;
        float newW = q.w * w - q.x * x - q.y * y - q.z * z;
        x = newX;
        y = newY;
        z = newZ;
        w = newW;
        return this;
    }

        /**
         * Converts this Quaternion_math into a matrix, placing the values into the given array.
         * @param matrixs 16-length float array.
         */
    public final void toMatrix(float[] matrixs) {
        matrixs[3] = 0.0f;
        matrixs[7] = 0.0f;
        matrixs[11] = 0.0f;
        matrixs[12] = 0.0f;
        matrixs[13] = 0.0f;
        matrixs[14] = 0.0f;
        matrixs[15] = 1.0f;

        matrixs[0] = (float) (1.0f - (2.0f * ((y * y) + (z * z))));
        matrixs[1] = (float) (2.0f * ((x * y) - (z * w)));
        matrixs[2] = (float) (2.0f * ((x * z) + (y * w)));

        matrixs[4] = (float) (2.0f * ((x * y) + (z * w)));
        matrixs[5] = (float) (1.0f - (2.0f * ((x * x) + (z * z))));
        matrixs[6] = (float) (2.0f * ((y * z) - (x * w)));

        matrixs[8] = (float) (2.0f * ((x * z) - (y * w)));
        matrixs[9] = (float) (2.0f * ((y * z) + (x * w)));
        matrixs[10] = (float) (1.0f - (2.0f * ((x * x) + (y * y))));
    }
}
