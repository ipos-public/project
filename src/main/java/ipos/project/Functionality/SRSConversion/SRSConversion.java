package ipos.project.Functionality.SRSConversion;
import org.apache.logging.log4j.LogManager;
import org.eclipse.emf.common.util.EList;

import ipos.project.DataModellntegration.iPos_Datamodel.*;

/*
This class contains methods to convert the coordinate system of positions.
All reference Systems should be defined in the ROOT system.
*/
public class SRSConversion {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    /*
    This function converts the coordinate system of a position to a desired coordinate system.
    p1 is the coordinate of the position in the original coordinate system
    ref_1 is the origin point of the original coordinate system (A Point3D in ROOT)
    ref_2 is the origin point of the desired coordinate system (A Point3D in ROOT)
    quat_1 is the rotation of the original coordinate system (against ROOT)
    quat_2 is the rotation of the desired coordinate system (against ROOT)
    */
    public static void switchSRS(Placing pla, ReferenceSystem ref){
        IPos_DatamodelFactory datamodelFactory = IPos_DatamodelFactory.eINSTANCE;
        Position pos = pla.getPosition();
        Point3D P = (Point3D) pos.getPoint();

        Point3D R = (Point3D) ref.getOrigin().getPosition().getPoint();
        Quaternion Q1 = (Quaternion) pla.getOrientation();
        Quaternion Q2 = (Quaternion) ref.getOrigin().getOrientation();
        String ref_id = ref.getId();
        String pla_ref_id = pla.getPosition().getReferenceSystem().getId();

        Vector_math p1 = new Vector_math(P.getX(), P.getY(), P.getZ());
        Vector_math ref_org = new Vector_math(R.getX(), R.getY(), R.getZ());
        Quaternion_math quat_1 = new Quaternion_math(Q1.getX(), Q1.getY(), Q1.getZ(), Q1.getW());
        Quaternion_math quat_2 = new Quaternion_math(Q2.getX(), Q2.getY(), Q2.getZ(), Q2.getW());
        //System.out.println("Quaterion_1: " + quat_1.getX() + ", "+ quat_1.getY() + ", "+ quat_1.getZ() + ", " + quat_1.getW());
        //System.out.println("Quaterion_2: " + quat_2.getX() + ", "+ quat_2.getY() + ", "+ quat_2.getZ() + ", " + quat_2.getW());


        if (pla_ref_id.equals(ref_id)) {
            //System.out.println("inversed Quaterion_1: " + quat_1.inverse().getX() + ", "+ quat_1.inverse().getY() + ", "+ quat_1.inverse().getZ() + ", " + quat_1.inverse().getW());
            LOG.info("INDFRO-DEBUG: Transforming placing-SRS " + pla_ref_id + " to SRS " + ref.getOrigin().getPosition().getReferenceSystem().getId());
            quat_1.mulLeft(quat_2.inverse());

            //System.out.println("phase 1 transfrom: " + p1.getX() + ", "+ p1.getY() + ", "+ p1.getZ());
            p1.rotate_quaternion(quat_2);
            //System.out.println("phase 1 transfrom: " + p1.getX() + ", "+ p1.getY() + ", "+ p1.getZ());
            p1.add(ref_org);
            pos.setReferenceSystem(ref.getOrigin().getPosition().getReferenceSystem());

        }
        else if(pla_ref_id.equals(ref.getOrigin().getPosition().getReferenceSystem().getId())) {
            LOG.info("INDFRO-DEBUG: Transforming placing-SRS " + pla_ref_id + " to SRS " + ref_id);
            //System.out.println("inversed Quaterion_1: " + quat_1.inverse().getX() + ", "+ quat_1.inverse().getY() + ", "+ quat_1.inverse().getZ() + ", " + quat_1.inverse().getW());
            quat_1.mulLeft(quat_2);
            p1.substract(ref_org);
            p1.rotate_quaternion(quat_2.inverse());
            pos.setReferenceSystem(ref);
        }
        else {
            //error message
        }
        Q1.setX(quat_1.getX());
        Q1.setY(quat_1.getY());
        Q1.setZ(quat_1.getZ());
        Q1.setW(quat_1.getW());
        P.setX(p1.getX());
        P.setY(p1.getY());
        P.setZ(p1.getZ());
        pla.setOrientation(Q1);
        pos.setPoint(P);
        pla.setPosition(pos);
    }
    /*public static void switchSRSZone(Zone zone, ReferenceSystem ref){
        IPos_DatamodelFactory datamodelFactory = IPos_DatamodelFactory.eINSTANCE;
        EList spaceList = zone.getSpace();
        for (int i = 0; i < spaceList.size(); i++) {
            Space S = (Space)spaceList.get(i);
            Placing P = S.getCentrePoint();
            Position Pos = P.getPosition();
            positionInOtherSystem(Pos, ref);
            Quaternion Q1 = (Quaternion) P.getOrientation();
            Quaternion Q2 = (Quaternion) ref.getOrigin().getOrientation();

            Quaternion_math quat_1 = new Quaternion_math(Q1.getX(), Q1.getY(), Q1.getZ(), Q1.getW());
            Quaternion_math quat_2 = new Quaternion_math(Q2.getX(), Q2.getY(), Q2.getZ(), Q2.getW());
            quat_1.mulLeft(quat_2);
            Q1.setX(quat_1.getX());
            Q1.setY(quat_1.getY());
            Q1.setZ(quat_1.getZ());
            Q1.setW(quat_1.getW());
            P.setOrientation(Q1);
            P.setPosition(Pos);
            S.setCentrePoint(P);
            spaceList.set(i, S);
        }
    }
    public float[] getCordInOtherSystem (float[] cod, float[] ref, float[] quaternion){
        float[] new_cod = new float[3];
        //float[] conv_vec = new float[16];
        Vector_math cod_vec = new Vector_math(cod[0], cod[1], cod[2]);
        Vector_math ref_new = new Vector_math(ref[0], ref[1], ref[2]);
        Quaternion_math quaternion_new  = new Quaternion_math(quaternion[0], quaternion[1], quaternion[2], quaternion[3]);
        //conv_vec = quaternion_new.toMatrix();
        //float[][] conv_matrix = new float[][]{{conv_vec[0], conv_vec[1], conv_vec[2]}, {conv_vec[3], conv_vec[4], conv_vec[5]}, {conv_vec[6], conv_vec[7], conv_vec[8]}};
        cod_vec.transform(quaternion_new);
        cod_vec.add(ref_new);
        new_cod[0] = cod_vec.getX();
        new_cod[1] = cod_vec.getY();
        new_cod[2] = cod_vec.getZ();
        return new_cod;
    }*/
}
