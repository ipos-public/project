package ipos.project.Functionality.SRSConversion;

import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.Functionality.SRSConversion.SRSConversion;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestSRSConverter {
    public static void test() throws ParseException {
        IPos_DatamodelFactory datamodelFactory = IPos_DatamodelFactory.eINSTANCE;
        Vector_math axis = new Vector_math(1, 0,0);
        Point3D testPoint3D = datamodelFactory.createPoint3D();
        Position testPosition = datamodelFactory.createPosition();
        Placing testPlacing = datamodelFactory.createPlacing();

        testPoint3D.setX((float) 0);
        testPoint3D.setY((float) 1);
        testPoint3D.setZ((float) 0);

        ReferenceSystem testRef_1 = datamodelFactory.createReferenceSystem();
        Placing refPlacing_1 = datamodelFactory.createPlacing();
        Position refPosition_1 = datamodelFactory.createPosition();
        Point3D ref_1 = datamodelFactory.createPoint3D();
        Quaternion rot_1 = datamodelFactory.createQuaternion();
        Quaternion_math rot_1_math = new Quaternion_math(axis, 90);
        rot_1.setX(rot_1_math.getX());
        rot_1.setY(rot_1_math.getY());
        rot_1.setZ(rot_1_math.getZ());
        rot_1.setW(rot_1_math.getW());
        ref_1.setX(1);
        ref_1.setY(1);
        ref_1.setZ(1);
        refPosition_1.setPoint(ref_1);
        refPlacing_1.setPosition(refPosition_1);
        refPlacing_1.setOrientation(rot_1);
        testRef_1.setId("CITI");
        testRef_1.setOrigin(refPlacing_1);
        testPosition.setPoint(testPoint3D);
        testPosition.setReferenceSystem(testRef_1);
        testPlacing.setPosition(testPosition);


        ReferenceSystem testRef_2 = datamodelFactory.createReferenceSystem();
        Placing refPlacing_2 = datamodelFactory.createPlacing();
        Position refPosition_2 = datamodelFactory.createPosition();
        Point3D ref_2 = datamodelFactory.createPoint3D();
        Quaternion rot_2 = datamodelFactory.createQuaternion();
        Quaternion_math rot_2_math = new Quaternion_math(axis, 0);
        rot_2.setX(rot_2_math.getX());
        rot_2.setY(rot_2_math.getY());
        rot_2.setZ(rot_2_math.getZ());
        rot_2.setW(rot_2_math.getW());
        ref_2.setX(0);
        ref_2.setY(0);
        ref_2.setZ(0);
        refPosition_2.setPoint(ref_2);
        refPlacing_2.setPosition(refPosition_2);
        refPlacing_2.setOrientation(rot_2);
        testRef_2.setId("ROOT");
        testRef_2.setOrigin(refPlacing_2);


        System.out.println("Position: refID " + ": " + testPosition.getReferenceSystem().getId());
        System.out.println("Position: coord " + ": " + testPosition.getPoint());

        SRSConversion.switchSRS(testPlacing, testRef_2);
        System.out.println("Position: refID " + ": " + testPosition.getReferenceSystem().getId());
        System.out.println("Position: coord " + ": " + testPosition.getPoint());



    }
}
