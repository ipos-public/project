package ipos.project.Functionality.SRSConversion;

public final class Vector_math {
    private static Quaternion_math tmp1 = new Quaternion_math(0, 0, 0, 0);
    private static Quaternion_math tmp2 = new Quaternion_math(0, 0, 0, 0);
    private float x,y,z;

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public Vector_math(float ix, float iy, float iz) {
        x = ix;
        y = iy;
        z = iz;
    }

    public void set(float ix, float iy, float iz) {
        x = ix;
        y = iy;
        z = iz;
    }

    public double magnitude() {
        return Math.sqrt(x*x+y*y+z*z);
    }

    public void multiply(float f) {
        x *= f;
        y *= f;
        z *= f;
    }

    public void normalise() {
        double mag = magnitude();
        x /= mag;
        y /= mag;
        z /= mag;
    }
    public void add(Vector_math v) {
        x += v.x;
        y += v.y;
        z += v.z;
    }
    public void substract(Vector_math v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }
    public float dot (Vector_math v){
        float dotProduct;
        dotProduct = x * v.x + y * v.y  +z * v.z;
        return dotProduct;
    }
    public void cross (Vector_math v){
        float xt = x;
        float yt = y;
        float zt = z;
        x = yt * v.z - zt * v.y;
        y = zt * v.x - xt * v.z;
        z = xt * v.y - yt * v.x;
    }
    public void rotate_quaternion (Quaternion_math q) {
        // Extract the scalar part of the quaternion
        Vector_math u = new Vector_math(q.getX(),q.getY(),q.getZ());

        Vector_math temp1 = new Vector_math(x,y,z);
        Vector_math temp2 = new Vector_math(q.getX(),q.getY(),q.getZ());

        float s = q.getW();
        //System.out.println("s: " + s);
        //System.out.println("Quaternion Vector: " + u.getX() + ", "+ u.getY() + ", "+ u.getZ());

        // Do the math
        temp2.multiply(2.0f * this.dot(u));
        temp1.cross(u);
        temp1.multiply(2.0f * s);
        //System.out.println("temp1: " + temp1.getX() + ", "+ temp1.getY() + ", "+ temp1.getZ());
        //System.out.println("temp2: " + temp2.getX() + ", "+ temp2.getY() + ", "+ temp2.getZ());
        //System.out.println("phase 1 transfrom: " + this.getX() + ", "+ this.getY() + ", "+ this.getZ());
        this.multiply(s * s - u.dot(u));
        //System.out.println("phase 2 transfrom: " + this.getX() + ", "+ this.getY() + ", "+ this.getZ());
        this.add(temp1);
        //System.out.println("phase 3 transfrom: " + this.getX() + ", "+ this.getY() + ", "+ this.getZ());
        this.add(temp2);
        //System.out.println("phase 4 transfrom: " + this.getX() + ", "+ this.getY() + ", "+ this.getZ());
    }
}
