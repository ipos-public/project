package ipos.project.Functionality;

import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.UseCaseController.PositionMonitoring;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.eclipse.emf.common.util.EMap;

import java.sql.Timestamp;
import java.text.ParseException;
import java.lang.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.Optional;

public class Triangulation {
    private static final int MIN_NUMBER_OF_BEACONS = 3;
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;

    public static Optional<PositionEvent> update(UWB uwbRawDataEvent) throws ParseException {

        if(eventNotApplicable(uwbRawDataEvent)){
            return Optional.empty();
        }

        double[][] positions = new double[][]{{0, 0}, {0, 0}, {0, 0}, {0, 0}};
        double[] distances = new double[]{0,0,0,0};
        HashMap uwbData = (HashMap) uwbRawDataEvent.getDistances();
        int index = 0;
        for (Object key : uwbData.keySet()) {
            Point3D beaconPoint3D = modelFactory.createPoint3D();
            beaconPoint3D = (Point3D) DataServices.getPoiByIdOrNull((String) key).getPlacing().getPosition().getPoint();
            positions[index] = new double[]{beaconPoint3D.getX(), beaconPoint3D.getY()};
            distances[index] = (Double) uwbData.get(key);
            index ++;
        }


        Point3D tagPoint3D = modelFactory.createPoint3D();
        Position tagPosition = modelFactory.createPosition();
        Placing tagPlacing = modelFactory.createPlacing();
        Gaussian tagGaussian = modelFactory.createGaussian();

        NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(new TrilaterationFunction(positions, distances), new LevenbergMarquardtOptimizer());
        LeastSquaresOptimizer.Optimum optimum = solver.solve();

        // the answer
        double[] centroid = optimum.getPoint().toArray();

        // error and geometry information; may throw SingularMatrixException depending the threshold argument provided
        RealVector standardDeviation = optimum.getSigma(0);
        RealMatrix covarianceMatrix = optimum.getCovariances(0);

        tagPoint3D.setX((float) centroid[0]);
        tagPoint3D.setY((float) centroid[1]);
        // tagPoint3D.setZ((float) PositionMonitoring.TRIANGULATION_HEIGHT);
        tagGaussian.setConfidenceInterval((float) standardDeviation.getLInfNorm());
        tagPosition.setPoint(tagPoint3D);
        tagPosition.setAccuracy(tagGaussian);
        tagPosition.setReferenceSystem(DataServices.getReferenceSystemByIdOrNull("ROOT"));
        tagPlacing.setPosition(tagPosition);
        tagPlacing.setOrientation(modelFactory.createQuaternion());
        return Optional.of(createPosEvent(uwbRawDataEvent.getSensorId(), uwbRawDataEvent.getTimeStamp(), tagPlacing));

    }

    private static boolean eventNotApplicable(UWB uwbRawDataEvent) {
        return notEnoughBeaconDistances(uwbRawDataEvent) || includesUnknownBeacon(uwbRawDataEvent);
    }

    private static boolean includesUnknownBeacon(UWB uwbRawDataEvent) {
        HashMap<String, Double> uwbData = (HashMap<String, Double>) uwbRawDataEvent.getDistances();
        for (String beaconId : uwbData.keySet()){
            if (null == DataServices.getPoiByIdOrNull(beaconId)){
                LOG.info("UWBRawDataEvent contains unknown beacon: " + beaconId);
                return true;
            }
        }
        return false;
    }

    private static boolean notEnoughBeaconDistances(UWB uwbRawDataEvent) {
        HashMap uwbData = (HashMap) uwbRawDataEvent.getDistances();
        if(uwbData.keySet().size() < Triangulation.MIN_NUMBER_OF_BEACONS){
            LOG.info("UWBRawDataEvent only contains " + uwbData.keySet().size() + " beacon-distances. Can not calculate a position");
            return true;
        }else {
            return false;
        }
    }

    private static double[] getPositionFromBeaconId(String beaconId) {
        Point3D beaconPos = modelFactory.createPoint3D();
        beaconPos = (Point3D) DataServices.getPoiByIdOrNull(beaconId).getPlacing().getPosition().getPoint();
        double[] position = new double[]{beaconPos.getX(), beaconPos.getY(), beaconPos.getZ()};
        return position;
    }
    private static PositionEvent createPosEvent(String lObjectId, String timeStamp, Placing placing) {
        PositionEvent positionEvent = modelFactory.createPositionEvent();
        positionEvent.setLObjectId(lObjectId);
        positionEvent.setTimeStamp(timeStamp);
        positionEvent.setPlacing(placing);
        return positionEvent;
    }
}
