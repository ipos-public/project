package ipos.project.Functionality.eventfilter;

import ipos.project.DataModellntegration.iPos_Datamodel.Beacon;

public class BeaconFilteringResult extends FilteringResult {

    private Beacon beaconRawdataEvent;

    public BeaconFilteringResult(String monitoringTaskId, Beacon beaconRawdataEvent, Boolean isBlocking) {
        super(monitoringTaskId, isBlocking);
        this.beaconRawdataEvent = beaconRawdataEvent;
     }

    public Beacon getBeaconRawdataEvent() {
        return beaconRawdataEvent;
    }

}
