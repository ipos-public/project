package ipos.project.Functionality.eventfilter;

import ipos.project.DataModellntegration.iPos_Datamodel.Beacon;

public class FilteringResult {

    private String monitoringTaskId;
    private boolean isBlocking;

    public FilteringResult(String monitoringTaskId, Boolean isBlocking) {
        this.monitoringTaskId = monitoringTaskId;
        this.isBlocking = isBlocking;
    }

    public String getMonitoringTaskId() {
        return monitoringTaskId;
    }

    public boolean hasBlockedEvent(){
        return this.isBlocking;
    }


}
