package ipos.project.Functionality.eventfilter;

import ipos.project.DataModellntegration.iPos_Datamodel.MessageReceivedEvent;

public class MsgRcvFilteringResult extends FilteringResult {

    private MessageReceivedEvent messageReceivedEvent;

    public MsgRcvFilteringResult(String monitoringTaskId, MessageReceivedEvent messageReceivedEvent, Boolean isBlocking) {
        super(monitoringTaskId, isBlocking);
        this.messageReceivedEvent = messageReceivedEvent;
    }

    public MessageReceivedEvent getMessageReceivedEvent() {
        return messageReceivedEvent;
    }

}
