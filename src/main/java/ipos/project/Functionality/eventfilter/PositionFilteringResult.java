package ipos.project.Functionality.eventfilter;

import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;

import java.util.List;

public class PositionFilteringResult extends FilteringResult{

    public PositionFilteringResult(String monitoringTaskId, PositionEvent posEvent, boolean isBlocking, List<String> matchingPositionConditionCells, List<String> allPositionConditionCells){
        super(monitoringTaskId, isBlocking);
        this.posEvent = posEvent;
        this.matchingPositionConditionCells = matchingPositionConditionCells;
        this.nonMatchingPositionConditionCells = allPositionConditionCells;
        this.nonMatchingPositionConditionCells.removeAll(matchingPositionConditionCells);
    }

    private List<String> matchingPositionConditionCells;
    private List<String> nonMatchingPositionConditionCells;


    public PositionEvent getPosEvent() {
        return posEvent;
    }

    private PositionEvent posEvent;

    public List<String> getMatchingPositionConditionCellIds() {
        return this.matchingPositionConditionCells;
    }

    public List<String> getNonMatchingPositionConditionCellIds(){
        return this.nonMatchingPositionConditionCells;
    }
}
