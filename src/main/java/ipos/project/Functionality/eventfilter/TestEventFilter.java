package ipos.project.Functionality.eventfilter;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.Functionality.eventfilter.*;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

// --- Internal data model

@Component
public class TestEventFilter {
    public static void test() throws ParseException {
        IPos_DatamodelFactory datamodelFactory = IPos_DatamodelFactory.eINSTANCE;
        IPosDevKitFactory devKitFactory = IPosDevKitFactory.eINSTANCE;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        //HashMap events = new HashMap<String, String>();
        PositionEvent testPositionEvent = datamodelFactory.createPositionEvent();
        Placing testPlacing = datamodelFactory.createPlacing();
        LocalizableObject testLObject = datamodelFactory.createLocalizableObject();
        testLObject.setSensorType("UWB");
        testLObject.setId("AGV001");
        Agent testAgent = datamodelFactory.createAgent();
        testAgent.setAgentType("AGV");
        testAgent.getLObject().add(testLObject);
        testAgent.setId("testAgent-Id");
        testLObject.setAgent(testAgent);
        Position testPosition = datamodelFactory.createPosition();
        Point3D testPoint3D = datamodelFactory.createPoint3D();

        MonitoringRequest monReq = devKitFactory.createMonitoringRequest();
        monReq.setMonitoringTaskId("testMonitoringTaskId");
        eventFilter filter = new eventFilter(monReq);
        EventFilterCondition config = datamodelFactory.createEventFilterCondition();
        String jsonPath = System.getProperty("user.dir") + "\\src\\main\\java\\ipos\\project\\Functionality\\eventfilter\\configExample.json"; //assign your JSON file here
		
		try {
            readConfig.readFilterConfigFromJson(jsonPath, config);
        } catch (Exception e) {
            e.printStackTrace();
        }
        readConfig.printFilterConfig(config);
        filter.init(config);

        String message = "";
        /* events.put(tagNames.tagTime,"2021-07-05 14:09:03.591");
        events.put(tagNames.tagDriverID,"driverid");
        events.put(tagNames.tagObjectID,"AGV001");
        events.put(tagNames.category, "AGV");
        events.put(tagNames.tagSensorID, "UWB");
        events.put(tagNames.tagPositionTechnology,"positiontech");
        events.put(tagNames.tagCoordinateSystem,"coordsys");
        events.put(tagNames.tagX,"1.34");
        events.put(tagNames.tagY,"2.456");
        events.put(tagNames.tagZ, "1");
        events.put(tagNames.tagAccuracy,"0.15"); */
        testPoint3D.setX((float) 3.34);
        testPoint3D.setY((float) 2.656);
        testPoint3D.setZ((float) 2);
        testPosition.setPoint(testPoint3D);
        testPlacing.setPosition(testPosition);
        testLObject.setId("AGV001");
        testPositionEvent.setTimeStamp("2021-07-05 14:09:03.591");
        testPositionEvent.setPlacing(testPlacing);
        System.out.println("Filter result " + ": " + filter.process(testPositionEvent));
    }
}
