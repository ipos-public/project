package ipos.project.Functionality.eventfilter;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.Functionality.DataServices;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.UseCaseController.PositionMonitoring;
import org.apache.logging.log4j.LogManager;

import java.sql.Timestamp;
import java.lang.Math;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class eventFilter {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    public static final String MONREQ_DISTANCE_PROPERTY = "distance";
    public static final String MONREQ_VDA5050_PROPERTY = "VDA5050";
    public static final List<String> MONREQ_ALLOWED_PROPERTIES = Arrays.asList("position", "id", "type", MONREQ_DISTANCE_PROPERTY, MONREQ_VDA5050_PROPERTY);

    private boolean[] filterStructure;
    //
    private PositionEvent last_position_event;
    private MonitoringRequest monitoringRequest;

    public eventFilter(MonitoringRequest monitoringRequest){
        this.monitoringRequest = monitoringRequest;
        filterStructure = new boolean[9];
    }

    public String getMonitoringTaskId(){
        return this.monitoringRequest.getMonitoringTaskId();
    }

    public String getSerializationType(){
        return this.monitoringRequest.getSerializationType();
    }

    public String getRefSystemId(){
        return this.monitoringRequest.getRefSystemId();
    }

    private String getFusionStrategy(){
        return this.monitoringRequest.getFusionStrategy();
    }

    //Conditions: unique conditions are defined as single values, Parallel conditions are defined in lists.
    //initial value: 0 (for float and int) and empty (for arrays), it's not always possible to determine the status of the filter with just the condition value, so a filterStructure is defined to register which conditions are active.

    public EventFilterCondition getFilterConditionConfig() {
        return filterConditionConfig;
    }

    private ArrayList<String[]> timeCondition;
    private List<String> categoryCondition;
    private List<String> sensorIdCondition;
    private List<String> idCondition;
    private float accuracyCondition;
    private ArrayList<Float[]> positionCondition;
    private int timeMinInterval;
    private float positionDelta;
    private Map<String, ArrayList<Float[][]>> positionConditionCells; // zoneId -> PositionConditionCell
    private EventFilterCondition filterConditionConfig;
    private List<String> propertyCondition;

    public void init(EventFilterCondition conf) {
        filterStructure = conf.getFilterStructure();
        timeCondition = conf.getTimeCondition();
        categoryCondition = conf.getCategoryCondition();
        sensorIdCondition = conf.getSensorIdCondition(); //regular expression of match condition
        idCondition = conf.getIdCondition();
        accuracyCondition = conf.getAccuracyCondition();
        positionCondition = conf.getPositionCondition();
        timeMinInterval = conf.getTimeMinInterval();
        positionDelta = conf.getPositionDelta();
        positionConditionCells = conf.getPositionConditionCells();
        propertyCondition = conf.getPropertyCondition();
        this.filterConditionConfig = conf;
    }

    private static String createRegexFromGlob(String glob) {
        StringBuilder out = new StringBuilder("^");
        for(int i = 0; i < glob.length(); ++i) {
            final char c = glob.charAt(i);
            switch(c) {
                case '*': out.append(".*"); break;
                case '?': out.append('.'); break;
                case '.': out.append("\\."); break;
                case '\\': out.append("\\\\"); break;
                default: out.append(c);
            }
        }
        out.append('$');
        return out.toString();
    }
    
    private static float cal_distance(Point3D cod1, Point3D cod2){
        float dx = Math.abs(cod1.getX() - cod2.getX());
        float dy = Math.abs(cod1.getY() - cod2.getY());
        float dz = Math.abs(cod1.getZ() - cod2.getZ());
        return (float) Math.hypot(Math.hypot(dx, dy), dz);
    }

    private static boolean is_in_cell(Point3D cod, Float[] ref, Float[] size, Float[] rot){
        if ((Math.abs(cod.getX()-ref[0])<= size[0]/2) && (Math.abs(cod.getY()-ref[1])<= size[1]/2) && (Math.abs(cod.getZ()-ref[2])<= size[2]/2)) {
            return true;
        }
        else {
            return false;
        }
    }

    public BeaconFilteringResult processRawdata(Beacon beaconRawdataEvent){
        LocalizableObject lObject = DataServices.getLObjectByIdOrNull(beaconRawdataEvent.getSensorId());
        if (null == lObject){
            LOG.info("EventFilter: Warning: Received beaconRawdataEvent from sensor with unknown sensor-id: " + beaconRawdataEvent.getSensorId());
            return new BeaconFilteringResult(getMonitoringTaskId(), beaconRawdataEvent, true);
        }

        if (null == propertyCondition){
            // assumption: default value of property is 'position' and not 'distance' or any other rawdata.
            // so we will not publish rawdata if property-field is unset
            return new BeaconFilteringResult(getMonitoringTaskId(), beaconRawdataEvent, true);
        }

        boolean isBlocking = false; //false for pass, true for block
        if (sensorIdCondition != null){
            String rawdataSensorId = beaconRawdataEvent.getSensorId();
            if (!sensorIdCondition.contains(rawdataSensorId)){
                isBlocking = true; // important: once true, isBlocking must not be switched to false again, i.e., once blocked dont unblock anymore
            }
        }

        // rawdata-filter should block, if something other than rawdata is requested.
        // for case propertyCondition == null: see above
        if (propertyCondition != null){
            // currently we only accept 'distance' as a keyword to indicate the request for publishing rawdata
            if (! propertyCondition.contains(MONREQ_DISTANCE_PROPERTY)){
                return new BeaconFilteringResult(getMonitoringTaskId(), beaconRawdataEvent, true);
            }
        }
        return new BeaconFilteringResult(getMonitoringTaskId(), beaconRawdataEvent, isBlocking);
    }

    public PositionFilteringResult process(PositionEvent event) throws ParseException {
        IPos_DatamodelFactory dataModelFactory = IPos_DatamodelFactory.eINSTANCE;
        LocalizableObject lObject = DataServices.getLObjectByIdOrNull(event.getLObjectId());
        boolean flag = false; //false for pass, true for block
        List<String> matchingCells = new LinkedList<>();
        if (null == event.getPlacing() || null == event.getPlacing().getPosition() || null == event.getPlacing().getPosition().getPoint()){
            LOG.info("EventFilter: Warning: Received event with unknown Position. Sensor-id: " + event.getLObjectId());
            return new PositionFilteringResult(getMonitoringTaskId(), event,true, matchingCells, new ArrayList<>(this.positionConditionCells.keySet()));
        }
        if (null == lObject){
            LOG.info("EventFilter: Warning: Received event from sensor with unknown sensor-id: " + event.getLObjectId());
            return new PositionFilteringResult(getMonitoringTaskId(), event,true, matchingCells, new ArrayList<>(this.positionConditionCells.keySet()));
        }
        Agent agent = lObject.getAgent();
        if (null == agent){
            LOG.info("EventFilter: Warning: Received event for sensor-id with unknown agent. Sensor-id: " + lObject.getId());
            return new PositionFilteringResult(getMonitoringTaskId(), event,true, matchingCells, new ArrayList<>(this.positionConditionCells.keySet()));
        }

        LOG.info("Eventfilter: Filter structure: " + filterStructure[0] + ", " + filterStructure[1] + ", " + filterStructure[2] + ", " + filterStructure[3] + ", " + filterStructure[4] + ", " + filterStructure[5] + ", " + filterStructure[6] + ", " + filterStructure[7]);
        //filter time conditions
        if (filterStructure[0]) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            LOG.info("Eventfilter: Time condition: " + timeCondition);
            for (int i = 0; i < timeCondition.size(); i++) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                Date date = formatter.parse(event.getTimeStamp());
                Date lower_date = formatter.parse(timeCondition.get(i)[0]);
                Date upper_date = formatter.parse(timeCondition.get(i)[1]);
                Timestamp ts = new Timestamp(date.getTime());
                Timestamp lower_ts = new Timestamp(lower_date.getTime());
                Timestamp upper_ts = new Timestamp(upper_date.getTime());

                LOG.info("Eventfilter: Time in event" + ": " + date);

                if (lower_ts.equals(upper_ts) || ts.equals(lower_ts)) {
                    flag = false;
                    break;
                }
                else if (ts.after(lower_ts) && ts.before(upper_ts)) {
                    flag = false;
                    break;
                }
            }
        }
        LOG.info("Eventfilter: Filter status (Time) " + ": " + flag);
        //filter category conditions
        if (filterStructure[1] && !flag) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            for (int i = 0; i < categoryCondition.size(); i++) {
                LOG.info("Eventfilter: Type in event: " + agent.getAgentType());

                if (agent.getAgentType().equals(categoryCondition.get(i))) {
                    flag = false;
                    break;
                }
            }
        }
        LOG.info("Eventfilter: Filter status (category) " + ": " + flag);
        //filter id conditions
        if (filterStructure[2] && !flag) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            for (int i = 0; i < idCondition.size(); i++) {
                if (lObject.getId().matches(idCondition.get(i))) {
                    flag = false;
                    break;
                }
            }
        }
        LOG.info("Eventfilter: Filter status (id) " + ": " + flag);
        //filter sensor_id conditions
        if (filterStructure[3] && !flag) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            for (int i = 0; i < sensorIdCondition.size(); i++) {
                String patternString = createRegexFromGlob(sensorIdCondition.get(i));
                // TODO: Does this really work?
                if (lObject.getSensorType().matches(patternString)) {
                    flag = false;
                    break;
                }
            }
        }
        LOG.info("Eventfilter: Filter status (sensor_id) " + ": " + flag);

        //filter accuracy condition
        if (filterStructure[4] && !flag) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            //LOG.info("Eventfilter: Accuracy condition: " + accuracyCondition);
            //LOG.info("Eventfilter: Accuracy event: " + event.getPositionInfo().getPosition().getAccuracy());
            if (event.getPlacing().getPosition().getAccuracy() instanceof Gaussian) {
                Gaussian gAccuracy = (Gaussian) event.getPlacing().getPosition().getAccuracy();
                if (gAccuracy.getConfidenceInterval() <= accuracyCondition) {
                    flag = false;
                }
            }
        }
        LOG.info("Eventfilter: Filter status (accuracy) " + ": " + flag);


        //filter position conditions
        if (filterStructure[5] && !flag) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            Position position = event.getPlacing().getPosition();
            if (position.getPoint() instanceof Point3D) {
                Point3D cod2 = dataModelFactory.createPoint3D();
                for(int    i=0;    i<sensorIdCondition.size();    i++){
                    Point3D cod1 = dataModelFactory.createPoint3D();
                    cod1.setX(positionCondition.get(i)[0]);
                    cod1.setY(positionCondition.get(i)[1]);
                    cod1.setZ(positionCondition.get(i)[2]);
                    if (cal_distance(cod1, cod2)<=positionCondition.get(i)[3]){
                        flag = false;
                        break;
                    }
                }
            }
        }
        LOG.info("Eventfilter: Filter status (position) " + ": " + flag);
        //filter time_min_interval condition
        if (filterStructure[6] && !flag && last_position_event !=null) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = formatter.parse(event.getTimeStamp());
            Timestamp ts = new Timestamp(date.getTime());
            Date last_date = formatter.parse(last_position_event.getTimeStamp());
            Timestamp last_ts = new Timestamp(last_date.getTime());
            long interval = date.getTime() - last_date.getTime();
            //LOG.info("Eventfilter: Time stamp" + ": " + ts);
            //LOG.info("Eventfilter: Interval" + ": " + interval);
            if (interval >= timeMinInterval) {
                flag = false;
            }
        }
        LOG.info("Eventfilter: Filter status (time_min_interval) " + ": " + flag);

        //filter position_delta condition
        if (filterStructure[7] && !flag && last_position_event !=null) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            Position position1 = event.getPlacing().getPosition();
            Position position2 = last_position_event.getPlacing().getPosition();
            if (position1.getPoint() instanceof Point3D) {
                Point3D cod1 = (Point3D) position1.getPoint();
                Point3D cod2 = (Point3D) position2.getPoint();
                float distance = cal_distance(cod1, cod2);

                LOG.info("Eventfilter: Distance: " + distance);
                if (distance >= positionDelta) {
                    flag = false;
                }
            }

        }
        LOG.info("Eventfilter: Filter status (position_delta) " + ": " + flag);

        //filter positionCondition_cell condition
        if (filterStructure[8] && !flag) {
            flag = true; //switch the filter status to block, if the event meet the filter condition, switch to pass
            Position position = event.getPlacing().getPosition();
            for (Map.Entry<String, ArrayList<Float[][]>> identifiedCell : positionConditionCells.entrySet()){
                boolean evalResult = evaluatePositionConditionCell(position, identifiedCell.getValue());
                if (!evalResult){ // let the event pass if filter is not blocking for at least one cell
                    flag = false;
                    matchingCells.add(identifiedCell.getKey());
                }
            }
        }
        LOG.info("Eventfilter: Filter status (positionCondition_cell) " + ": " + flag);
        LOG.info("Eventfilter: EventFilter " + getMonitoringTaskId() + " accepted zones " + matchingCells.toString());
        LOG.info("INDFRO-DEBUG: EventFilter " + getMonitoringTaskId() + " accepted zones " + matchingCells.toString());

        if (!flag) {
            last_position_event = event;
        }
        PositionFilteringResult positionFilteringResult = new PositionFilteringResult(getMonitoringTaskId(), event, flag, matchingCells, new ArrayList<>(this.positionConditionCells.keySet()));
        return positionFilteringResult;
    }

    private boolean evaluatePositionConditionCell(Position position, ArrayList<Float[][]> positionConditionCell) {
        boolean result = true;
        for (int i = 0; i < positionConditionCell.size(); i++) {
            if (position.getPoint() instanceof Point3D) {
                Point3D cod = (Point3D) position.getPoint();
                if (is_in_cell(cod, positionConditionCell.get(i)[0], positionConditionCell.get(i)[1], positionConditionCell.get(i)[2])) {
                    result = false;
                }
            }
        }
        return result;
    }

    public boolean respectsAccuracySdfForPosEvent(PositionEvent posEvent) {
        return (true || PositionMonitoring.ACCURACY_FUSION_STRATEGY.equals(this.getFusionStrategy())); // sensor data fusion is currently switched on by default, but later it may depend on agent-id
    }

    public MsgRcvFilteringResult processMessageReceivedEvent(MessageReceivedEvent messageReceivedEvent) {
        boolean isVDA5050Message = MONREQ_VDA5050_PROPERTY.equals(messageReceivedEvent.getProtocolName());
        boolean eFilterIsAcceptingVDA5050Message = propertyCondition.contains(MONREQ_VDA5050_PROPERTY);
        boolean isAccepting = isVDA5050Message && eFilterIsAcceptingVDA5050Message;
        boolean isBlocking =  !isAccepting;
        MsgRcvFilteringResult msgRcvFilteringResult = new MsgRcvFilteringResult(this.getMonitoringTaskId(), messageReceivedEvent, isBlocking);
        return msgRcvFilteringResult;
    }
}
