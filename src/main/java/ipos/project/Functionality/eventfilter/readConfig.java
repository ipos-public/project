package ipos.project.Functionality.eventfilter;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.Functionality.DataServices;
import org.apache.logging.log4j.LogManager;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class readConfig {

    // kommt später raus, Zugriff auf Weltmodell erfolgt eigentlich über Komponente DataRetrieval
    // private static WorldModel wm;
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    /**
     * Returns false if MonitoringRequest could not be read successfully.
     * @param monReq
     * @param config
     * @return
     */
    public static boolean readFilterConfigFromMonitoringRequest (MonitoringRequest monReq, EventFilterCondition config){
        // TODO: support other criteria than "position_condition_cell"
        config.setFilterStructure(new boolean[] {false, false, false, false, false, false, false, false, true});
        for (String frameId : monReq.getFrameIds()){
            Zone zone_monReq = DataServices.getZoneByIdOrNull(frameId);
            if (null == zone_monReq){
                LOG.warn("MonitoringRequest contained an invalid Zone-id: " + frameId);
                return false;
            }
            ArrayList<Float[][]> positionConditionCell = transformZone2PositionConditionCell(zone_monReq);
            config.getPositionConditionCells().put(frameId, positionConditionCell);
        }

        try {
            monReq.getId().forEach(sensorId -> addSensorIdToFilterConfig(sensorId, config));
        }catch (RuntimeException e){
            LOG.warn(e.getMessage());
            return false;
        }

        try {
            monReq.getProperties().forEach(property -> addPropertyToFilterConfig(property, config));
        }catch (RuntimeException e){
            LOG.warn(e.getMessage());
            return false;
        }

        return true;
    }

    private static void addPropertyToFilterConfig(String property, EventFilterCondition config) {
        if (eventFilter.MONREQ_ALLOWED_PROPERTIES.contains(property)){
            config.getPropertyCondition().add(property);
        }else{
            throw new RuntimeException("MonitoringRequest contained an invalid property: " + property);
        }
    }

    private static void addSensorIdToFilterConfig(String sensorId, EventFilterCondition config) {
        LocalizableObject lObject = DataServices.getLObjectByIdOrNull(sensorId);
        if (null == lObject){
            throw new RuntimeException("MonitoringRequest contained an invalid sensor-id: " + sensorId);
        }
        config.getSensorIdCondition().add(sensorId);
    }

    public static ArrayList<Float[][]> transformZone2PositionConditionCell(Zone zone) {
        // TODO: support Point-Types other than Point3D
        ArrayList<Float[][]> position_condition_cell = new ArrayList<>();
        for (var space : zone.getSpace()) {
            Float[][] cell_c = new Float[3][4]; // first dimension selects: Position, Space, Quaternion; second dimension selects: x,y,z,w
            Point3D centrePoint = (Point3D) space.getCentrePoint().getPosition().getPoint();
            Quaternion orientation = (Quaternion) space.getCentrePoint().getOrientation();
            cell_c[0] = new Float[]{centrePoint.getX(), centrePoint.getY(), centrePoint.getZ()};
            cell_c[1] = new Float[]{space.getX(), space.getY(), space.getZ()};
            cell_c[2] = new Float[]{orientation.getX(), orientation.getY(), orientation.getZ(), orientation.getW()};
            position_condition_cell.add(cell_c);
        }
        return position_condition_cell;
    }

    public static void readFilterConfigFromJson (String json_file, EventFilterCondition config) throws Exception {

        //Time condition: One time condition consist of two timestamp in format "yyyy-MM-dd HH:mm:ss.SSS", if the timestamp of the position event falls in between, the event passes.
        ArrayList<String[]> time_condition = new ArrayList<>();
        //Category condition: The category condition is a list of string, if the category of the event matches one value in the list, the event passes.
        List<String> category_condition = new ArrayList<>();
        //Sensor_id condition: The sensor id condition is a list of string, if the sensor id of the event matches one value in the list, the event passes. Wild card String match is supported.
        List<String> sensor_id_condition = new ArrayList<>();
        //ID condition: The id condition is a list of string, if the id of the event matches one value in the list, the event passes.
        List<String> id_condition = new ArrayList<>();
        //Accuracy condition: The accuracy condition is a float value, if the accuracy of the position event is better (smaller) than the accuracy condition, the event passes.
        float accuracy_condition;
        //Position condition: One position condition consist of four float values, the first 3 are x-y-z value of a coordinate of a reference point, the 4th float is the distance limit. If the distance between the coordinate of the position event and the reference point is smaller than the distance limit, the event passes.
        ArrayList<Float[]> position_condition = new ArrayList<>();
        //Time min interval: The time min interval is a int value (unit ms), if the time difference between the current event and the last event which passes the filter is larger than the interval, the current event passes.
        int time_min_interval;
        //Position delta: The position delta is a float value (unit m), if the distance between the current event and the last event which passes the filter is larger than the position delta, the current event passes.
        float position_delta;
        //Position condition cell: position condition where the permitted area is defined by a cell. A cell is defined by a reference point, a cell size and a Quaternion vector.
        ArrayList<Float[][]> position_condition_cell = new ArrayList<>();
        boolean[] filter_structure = new boolean[9];

        //Converting jsonData string into JSON object
        JSONObject jsnobject = new JSONObject(Files.readString(Paths.get(json_file)));
        //Printing JSON object
        //System.out.println("JSON Object");
        //System.out.println(jsnobject);

        //------------------read time condition-------------------------------------------------------------
        //Getting time conditions JSON array from the JSON object
        JSONArray jsonArray = jsnobject.getJSONArray("time_condition");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray);
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            //Iterating JSON array
            for (int i=0;i<jsonArray.length();i++) {
                //Adding each element of JSON array into ArrayList
                JSONArray jsa1 = jsonArray.getJSONArray(i);
                String[] time_c = new String[2];
                int count = jsa1.length();
                for (int j = 0; j < count; j++) {
                    time_c[j] = jsa1.getString(j);
                    //System.out.println("Time limit " + j + ": " + time_c[j]);
                }
                //System.out.println("Time lower limit " + 0 + ": " + time_c[0]);
                //System.out.println("Time upper limit " + 1 + ": " + time_c[1]);
                time_condition.add(time_c);
            }
            filter_structure[0] = true;
            config.setTimeCondition(time_condition);
        }
        else {
            filter_structure[0]=false;
        }

        //------------------read category condition---------------------------------------------------------
        //Getting category conditions JSON array from the JSON object
        jsonArray = jsnobject.getJSONArray("category_condition");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray);
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            //Iterating JSON array
            for (int i=0;i<jsonArray.length();i++) {

                category_condition.add(jsonArray.getString(i));

            }
            filter_structure[1] = true;
            config.setCategoryCondition(category_condition);
        }
        else {
            filter_structure[1]=false;
        }

        //------------------read id condition---------------------------------------------------------
        //Getting id conditions JSON array from the JSON object
        jsonArray = jsnobject.getJSONArray("id_condition");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray.length());
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            //Iterating JSON array
            for (int i=0;i<jsonArray.length();i++) {

                id_condition.add(jsonArray.getString(i));

            }
            filter_structure[2] = true;
            config.setIdCondition(id_condition);
        }
        else {
            filter_structure[2]=false;
        }

        //------------------read sensor id condition---------------------------------------------------------
        //Getting sensor id conditions JSON array from the JSON object
        jsonArray = jsnobject.getJSONArray("sensor_id_condition");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray.length());
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            //Iterating JSON array
            for (int i=0;i<jsonArray.length();i++) {

                sensor_id_condition.add(jsonArray.getString(i));

            }
            filter_structure[3] = true;
            config.setSensorIdCondition(sensor_id_condition);
        }
        else {
            filter_structure[3]=false;
        }

        //------------------read accuracy condition---------------------------------------------------------
        //Getting accuracy conditions JSON array from the JSON object
        jsonArray = jsnobject.getJSONArray("accuracy_condition");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray.length());
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            accuracy_condition=(float) jsonArray.getDouble(0);
            filter_structure[4] = true;
            config.setAccuracyCondition(accuracy_condition);
        }
        else {
            filter_structure[4]=false;
        }

        //------------------read position condition-------------------------------------------------------------
        //Getting position conditions JSON array from the JSON object
        jsonArray = jsnobject.getJSONArray("position_condition");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray);
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            //Iterating JSON array
            for (int i=0;i<jsonArray.length();i++) {
                //Adding each element of JSON array into ArrayList
                JSONArray jsa1 = jsonArray.getJSONArray(i);
                Float[] position_c = new Float[4];
                int count = jsa1.length();
                System.out.println(count);
                for (int j = 0; j < count; j++) {
                    position_c[j] = (float)jsa1.getDouble(j);
                }
                //System.out.println("Time lower limit " + 0 + ": " + time_c[0]);
                //System.out.println("Time upper limit " + 1 + ": " + time_c[1]);
                position_condition.add(position_c);

            }
            filter_structure[5] = true;
            config.setPositionCondition(position_condition);
        }
        else {
            filter_structure[5]=false;
        }

        //------------------read time_min_interval condition---------------------------------------------------------
        //Getting accuracy conditions JSON array from the JSON object
        jsonArray = jsnobject.getJSONArray("time_min_interval");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray.length());
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            time_min_interval= jsonArray.getInt(0);
            filter_structure[6] = true;
            config.setTimeMinInterval(time_min_interval);
        }
        else {
            filter_structure[6]=false;
        }

        //------------------read position_delta condition---------------------------------------------------------
        //Getting position_delta conditions JSON array from the JSON object
        jsonArray = jsnobject.getJSONArray("position_delta");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray.length());
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            position_delta= (float)jsonArray.getDouble(0);
            filter_structure[7] = true;
            config.setPositionDelta(position_delta);
        }
        else {
            filter_structure[7]=false;
        }

        //------------------position condition cell-------------------------------------------------------------
        //Getting time conditions JSON array from the JSON object
        jsonArray = jsnobject.getJSONArray("position_condition_cell");
        //Printing JSON array
        //System.out.println("JSON Array");
        //System.out.println(jsonArray);
        //Checking whether the JSON array has some value or not
        if (jsonArray != null && jsonArray.length()>0) {
            //Iterating JSON array
            for (int i=0;i<jsonArray.length();i++) {
                //Adding each element of JSON array into ArrayList
                JSONArray jsa1 = jsonArray.getJSONArray(i);
                Float[][] cell_c = new Float[3][4];
                int count = jsa1.length();
                for (int j = 0; j < count; j++) {
                    JSONArray jsa2 = jsa1.getJSONArray(j);
                    int count_k = jsa2.length();
                    for (int k =0; k < count_k; k++) {
                        cell_c[j][k] = (float) jsa2.getDouble(k);
                        //System.out.println("cell condition " + j + ", " + k + ": " + cell_c[j][k]);
                    }
                }
                position_condition_cell.add(cell_c);
            }
            filter_structure[8] = true;
            config.getPositionConditionCells().put("DefaultZoneId", position_condition_cell);
        }
        else {
            filter_structure[8]=false;
        }
        config.setFilterStructure(filter_structure);

        /*filterConfig config = new filterConfig();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            config = objectMapper.readValue(new File(json_file), filterConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setTime_condition(config.getTime_condition());
        this.setCategory_condition(config.getCategory_condition());
        this.setId_condition(config.getId_condition());
        this.setSensor_id_condition(config.getSensor_id_condition());
        this.setAccuracy_condition(config.getAccuracy_condition());
        this.setPosition_condition(config.getPosition_condition());
        this.setPosition_delta(config.getPosition_delta());
        this.setTime_min_interval(config.getTime_min_interval());*/
    }
    public static void printFilterConfig(EventFilterCondition config) {
        System.out.println("\ttime_condition               : " + config.getTimeCondition().get(0)[0] + ", " + config.getTimeCondition().get(0)[1]);
        System.out.println("\tcategory_condition           : " + config.getCategoryCondition());
        System.out.println("\tsensor_id_condition          : " + config.getSensorIdCondition());
        System.out.println("\tid_condition                 : " + config.getIdCondition());
        System.out.println("\taccuracy_condition           : " + config.getAccuracyCondition());
        //System.out.println("\tposition_condition           : " + config.getPositionCondition().get(0)[0] + ", " + config.getPositionCondition().get(0)[1] + ", " + config.getPositionCondition().get(0)[2] + ", " + config.getPositionCondition().get(0)[3]);
        System.out.println("\ttime_min_interval            : " + config.getTimeMinInterval());
        System.out.println("\tposition_delta               : " + config.getPositionDelta());
        System.out.println("\tfilter_structure             : " + config.getFilterStructure());
    }
}
