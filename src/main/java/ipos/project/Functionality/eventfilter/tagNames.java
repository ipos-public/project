package ipos.project.Functionality.eventfilter;

public class tagNames {
    public static final String tagTime = "time";
    public static final String tagDriverID = "Driver_id";
    public static final String tagObjectID = "Object_id";
    public static final String tagPositionTechnology = "Position_technology";
    public static final String tagPositionInfo = "PositionInfo";
    public static final String tagPosition = "Position";
    public static final String tagCoordinateSystem = "Coordinate_System";
    public static final String tagCoordinate = "Coordinate";
    public static final String tagX = "X";
    public static final String tagY = "Y";
    public static final String tagZ = "Z";
    public static final String tagAccuracy = "Accuracy";
    public static final String tagSender = "Sender";
    public static final String tagReceiver = "Receiver";
    public static final String tagUpdateInterval = "UpdateInterval";
    public static final String category = "Category";
    public static final String tagSensorID = "Sensor_ID";
}
