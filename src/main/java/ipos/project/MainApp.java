package ipos.project;

import ipos.project.DataModellntegration.SimpleSceneIntegration.SimpleSceneIntegration;
import ipos.project.DataModellntegration.VDA5050Processor.VDA5050Processor;
import ipos.project.SensorValueIntegration.GenericSensorValueProcessor;
import ipos.project.iposextension.orderpicker.OFBizOrderPicker;
import ipos.project.iposextension.osm.OSM;
import ipos.project.iposextension.tooz.Tooz;
import org.apache.logging.log4j.LogManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Scanner;

@SpringBootApplication
// @EnableJpaRepositories("ipos.project.models.SimpleScene.*")
@EnableJpaRepositories("ipos.project.DataModellntegration.iPos_Datamodel.impl")
public class MainApp {
    public static boolean READY_TO_READ_NEXT_LINE = true; // reading the first line shall not require user actions
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    // public static final String TESTRAWDATA_FILE = "./testdata_raw.txt";
    // public static final String TESTDATA_EMPTY = "./testdata_raw_empty.txt";
    public static final String INDUSTRIEROBOTIK_FILE = "./init_Industrierobotik.txt";
    public static final String SENSORDATENFUSION_FILE = "./init_Sensordatenfusion.txt";
    public static final String ORDERPICKER_FILE = "./init_Orderpicker.txt";
    public static final String TESTDATA_INDFRO = "./testdata_raw_indfro.txt";
    public static final String TESTDATA_QUERY = "./testdata_raw_query.txt";
    public static final String TESTDATA_OP = "./testdata_raw_orderpicker.txt";
    public static final String TESTDATA_OP_EVTS = "testdata_raw_orderpicker_posEvts.txt";
    public static final String TESTDATA_SDF = "testdata_raw_sdf.txt";
    public static final String TESTDATA_TOOZ = "testdata_raw_tooz.txt";
    public static final String TESTDATA_TOOZ_MONTARGET = "testdata_raw_tooz_init.txt";
    public static final String TESTDATA_OSM_MONTARGET = "testdata_raw_osm_init.txt";
    public static final String TESTDATA_VDA5050 = "testdata_raw_vda5050.txt";
    public static final String COMMAND_INDFRO = "indfro testdata"; // Industrierobotik_Frontend
    public static final String COMMAND_OP = "oppl testdata";
    public static final String COMMAND_OP_EVTS = "opevts testdata";
    public static final String COMMAND_SDF = "sdf testdata";
    public static final String COMMAND_TOOZ_TARGET = "tooz setLocalAgent";
    public static final String COMMAND_TOOZ = "tooz testdata";
    public static final String COMMAND_VDA5050 = "testdata VDA5050";
    public static final String COMMAND_EXIT = "exit";
    public static final String COMMAND_HELP = "help";
    public static final String COMMAND_QUERY = "query";
    public static final String COMMAND_INIT_INDFRO = "init indfro";
    public static final String COMMAND_INIT_SDF = "init sdf";
    public static final String COMMAND_INIT_OP = "init op";
    public static final String COMMAND_INIT_OSM = "init osm";
    public static final String COMMAND_READY_FOR_NEXT_LINE = "next line";
    public static final String UNKNOWN_COMMAND = "SHELL: Unknown command";
    private static boolean proceed = true;

    public static void main(String[] args){
        SpringApplication.run(MainApp.class, args);
        printCommands();
        processCommands();
    }

    private static void processCommands() {
        Scanner scanner = new Scanner(System.in);
        while(proceed) {
             if(scanner.hasNext()) {
                String line = scanner.nextLine();
                switch(line){
                    case COMMAND_INIT_INDFRO: initIndfro(); break;
                    case COMMAND_INIT_SDF: initSdf(); break;
                    case COMMAND_INIT_OP: initOp(); break;
                    case COMMAND_INIT_OSM: initOSM(TESTDATA_OSM_MONTARGET); break;
                    case COMMAND_EXIT : proceed = false; break;
                    case COMMAND_HELP: printCommands(); break;
                    case COMMAND_INDFRO: GenericSensorValueProcessor.processTestData(TESTDATA_INDFRO); break;
                    case COMMAND_TOOZ_TARGET: Tooz.processMonTargetTestdata(TESTDATA_TOOZ_MONTARGET); break;
                    case COMMAND_VDA5050: VDA5050Processor.processTestData(TESTDATA_VDA5050); break;
                    case COMMAND_TOOZ: GenericSensorValueProcessor.processTestData(TESTDATA_TOOZ); break;
                    case COMMAND_SDF: GenericSensorValueProcessor.processTestData(TESTDATA_SDF); break;
                    case COMMAND_OP : OFBizOrderPicker.processPicklistTestData(TESTDATA_OP); break;
                    case COMMAND_OP_EVTS: OFBizOrderPicker.processPosEvtTestData(TESTDATA_OP_EVTS); break;
                    case COMMAND_QUERY : SimpleSceneIntegration.handleMessageFile(TESTDATA_QUERY); break;
                    default: LOG.info(UNKNOWN_COMMAND); break;
                }
            }
        }
    }

    private static void initOSM(String testdataOsmMontarget) {
        OSM.processTestData(testdataOsmMontarget);
    }

    private static void initIndfro() {
        ipos.project.SensorValueIntegration.api.MqttPositionHandler.setInitialized(false);
        SimpleSceneIntegration.init(INDUSTRIEROBOTIK_FILE);
        ipos.project.SensorValueIntegration.api.MqttPositionHandler.setInitialized(true);
    }

    private static void initSdf() {
        ipos.project.SensorValueIntegration.api.MqttPositionHandler.setInitialized(false);
        SimpleSceneIntegration.init(SENSORDATENFUSION_FILE);
        ipos.project.SensorValueIntegration.api.MqttPositionHandler.setInitialized(true);
    }

    /**
     * IPos-Framework is initialized with data that is suitable for testing the "OrderPicker"-usecase
     * This initialization data is contained in configuration file ORDERPICKER_FILE.
     * OrderPicker extension is initialized (OFBizOrderPicker.initialize()).
     * After initialization the "OrderPicker extension" should be updated by the IPos-FW with all relevant positions
     */
    private static void initOp() {
        ipos.project.SensorValueIntegration.api.MqttPositionHandler.setInitialized(false);
        SimpleSceneIntegration.init(ORDERPICKER_FILE);
        ipos.project.SensorValueIntegration.api.MqttPositionHandler.setInitialized(true);
        OFBizOrderPicker.initialize();
    }

    private static void printCommands() {
        LOG.info("SHELL:-------------------------------------");
        LOG.info("SHELL:Available commands: ");
        LOG.info("SHELL:---------");
        LOG.info("SHELL:Initialization: ");
        LOG.info("SHELL:---------");
        LOG.info("SHELL:" + COMMAND_INIT_INDFRO);
        LOG.info("SHELL:" + COMMAND_INIT_SDF);
        LOG.info("SHELL:" + COMMAND_INIT_OP);
        LOG.info("SHELL:---------");
        LOG.info("SHELL:Testdata:");
        LOG.info("SHELL:---------");
        LOG.info("SHELL:" + COMMAND_INDFRO);
        LOG.info("SHELL:" + COMMAND_SDF);
        LOG.info("SHELL:" + COMMAND_OP);
        LOG.info("SHELL:" + COMMAND_OP_EVTS);
        LOG.info("SHELL:" + COMMAND_QUERY);
        LOG.info("SHELL:" + COMMAND_TOOZ);
        LOG.info("SHELL:" + COMMAND_VDA5050);
        LOG.info("SHELL:---------");
        LOG.info("SHELL:Other:");
        LOG.info("SHELL:---------");
        LOG.info("SHELL:" + COMMAND_EXIT);
        LOG.info("SHELL:" + COMMAND_HELP);
        LOG.info("SHELL:-------------------------------------");
    }

}
