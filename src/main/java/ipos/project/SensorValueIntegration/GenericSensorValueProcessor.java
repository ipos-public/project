package ipos.project.SensorValueIntegration;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.GenericSensor;
import ipos.models.SimpleScene.IposPosition;
import ipos.project.DataModellntegration.iPos_Datamodel.Beacon;
import ipos.project.DataModellntegration.iPos_Datamodel.Bluetooth;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.UWB;
import ipos.project.SensorValueIntegration.Service.GenericSensorTransformer;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.SensorValueIntegration.api.MqttPositionHandler;
import ipos.project.devkit.utility.OtherUtility;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Scanner;

@Component
public class GenericSensorValueProcessor {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    @Autowired
    public GenericSensorValueProcessor(ExternalPubServiceImpl mqttService_){
        mqttService = mqttService_;
    }

    // @Autowired
    private static ExternalPubServiceImpl mqttService;

    @JmsListener(destination = "/positions", containerFactory = "jmsListenFactory")
    public void receiveMessage(IposPosition pos) {
        LOG.trace("Data integration get: " + pos);
        // we can translate a class into a string using several methods: 1) `.toString()` 2) `JsonFormat` in `ProtoJsonMap`
        String jsonPos = ProtoJsonMap.toJson(pos);
        if (jsonPos != null) {
            this.mqttService.publish("test51/subscribe/positions", jsonPos, 0, false);
        }
    }

    public static void processTestData(String path_to_test_data_file){
        File testDataFile = new File(path_to_test_data_file);
        Scanner scanner = new Scanner(System.in);
        try {
            for (String line : OtherUtility.readLines(testDataFile)) {
                GenericSensor.SensorEventWrapper sensorEventWrapper = ProtoJsonMap.fromJson(line, GenericSensor.SensorEventWrapper.class);
                OtherUtility.waitUntilUserRequestsReadingNextLine(scanner);
                MqttPositionHandler.handleSensorEventWrapper(sensorEventWrapper);
            }
        }catch (InvalidProtocolBufferException e) {
            LOG.error("Error trying to read JSON into protobuf-objects: ");
            e.printStackTrace();
        }
    }

    public static void receiveMessage(Beacon beaconRawdataEvent, String monitoringTaskId, String serializationType, String agentId, String agentType) {
        if (beaconRawdataEvent instanceof UWB){
            GenericSensorValueProcessor.receiveMessage((UWB) beaconRawdataEvent, monitoringTaskId, serializationType, agentId, agentType);
        } else if(beaconRawdataEvent instanceof Bluetooth){
            GenericSensorValueProcessor.receiveMessage((Bluetooth) beaconRawdataEvent, monitoringTaskId, serializationType, agentId, agentType);
        } else{
            LOG.error("Rawdata could not be forwarded as position sensor technology could not be determined.");
        }
    }

    public static void receiveMessage(Bluetooth btRawdataEvent, String monitoringTaskId, String serializationType, String agentId, String agentType) {
        IPosBTEvent iPosBTEvent_internal = GenericSensorTransformer.bt_internal2IPosRawdata(btRawdataEvent, agentId, agentType);
        GenericSensor.IPosBTEvent iPosBTEvent_proto = GenericSensorTransformer.bt_IPosRawdata2proto(iPosBTEvent_internal);
        GenericSensor.IPosRawdataEventWrapper iPosRawdataEventWrapper_proto = GenericSensorTransformer.bt_wrapIntoRawdataEventWrapper(iPosBTEvent_proto);
        logging_tooz(iPosBTEvent_proto, monitoringTaskId, serializationType);
        OtherUtility.publishRespectingSerializationType(monitoringTaskId, serializationType, iPosRawdataEventWrapper_proto);
    }

    public static void receiveMessage(UWB beaconRawdataEvent, String monitoringTaskId, String serializationType, String agentId, String agentType) {
        IPosUWBEvent iPosUWBEvent_internal = GenericSensorTransformer.uwb_internal2IPosRawdata(beaconRawdataEvent, agentId, agentType);
        GenericSensor.IPosUWBEvent iPosUWBEvent_proto = GenericSensorTransformer.uwb_IPosRawdata2proto(iPosUWBEvent_internal);
        GenericSensor.IPosRawdataEventWrapper iPosRawdataEventWrapper_proto = GenericSensorTransformer.uwb_wrapIntoRawdataEventWrapper(iPosUWBEvent_proto);
        logging_tooz(iPosUWBEvent_proto, monitoringTaskId, serializationType);
        OtherUtility.publishRespectingSerializationType(monitoringTaskId, serializationType, iPosRawdataEventWrapper_proto);
    }

    private static void logging_tooz(com.google.protobuf.AbstractMessage event_proto, String monitoringTaskId, String serializationType) {
        LOG.info("TOOZ: Publishing " + event_proto.getClass().getName() + " on topic " + monitoringTaskId + " with serialization type " + serializationType);
        LOG.info("TOOZ: ");
    }
    /*
    public static void publishRespectingSerializationType(String topic, String serializationType, String jsonString, MqttMessage mqttMessage) {
        // publishes protobuf over MQTT
        if (PositionMonitoring.PROTOBUF_SERIALIZATION_TYPE.equals(serializationType)){
            mqttService.publish(topic, mqttMessage);
            LOG.info("publishing protobuf mqttMessage. JSON-representation:" + jsonString + " on topic: " + topic);
        }

        // publishes JSON over MQTT
        if (PositionMonitoring.JSON_SERIALIZATION_TYPE.equals(serializationType)){
            mqttService.publish(topic, jsonString, 0, false);
            LOG.info("publishing JSON mqttMessage:" + jsonString + " on topic: " + topic);
        }
    }*/

}
