package ipos.project.SensorValueIntegration.Service;

import com.google.protobuf.ProtocolStringList;
import ipos.models.GenericSensor;
import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory;
import ipos.project.Functionality.DataServices;
import ipos.project.devkit.trans.IPos2protoTransformer;


import java.time.LocalDateTime;
import java.util.*;

public class GenericSensorTransformer {

    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;
    private static IPosDevKitFactory devKitFactory = IPosDevKitFactory.eINSTANCE;

   private static List<String> toJavaStringList(ProtocolStringList protoList){
        List<String> javaList = new ArrayList<String>();
        for(int i = 0; i < protoList.size(); i++ ){
            javaList.set(i, protoList.get(i));
        }
        return javaList;
    }

    public static SimpleScene.IposMonitoringRequest monReq_Internal2SScene(){
        return null;
    }

    public static PositionEvent posEvent_GSensor2Internal(GenericSensor.SensorPositionEvent protoSensPosEvent) {
        PositionEvent positionEvent = modelFactory.createPositionEvent();
        positionEvent.setTimeStamp(protoSensPosEvent.getLastPosUpdate());
        positionEvent.setLObjectId(protoSensPosEvent.getSensorId());

        // TODO: Zugriff auf Weltmodell ermöglichen um Bezugssystem-Objekt beschaffen zu können
        positionEvent.setPlacing(
                IPos2protoTransformer.createPlacing(
                        IPos2protoTransformer.createPosition(
                                IPos2protoTransformer.createPoint3D(
                                        protoSensPosEvent.getPosition().getPoint().getX(),
                                        protoSensPosEvent.getPosition().getPoint().getY(),
                                        protoSensPosEvent.getPosition().getPoint().getZ()
                                ),
                                protoSensPosEvent.getPosition().getAccuracy(),
                                DataServices.getReferenceSystemByIdOrNull(protoSensPosEvent.getPosition().getRefSystemId())

                        ),
                        IPos2protoTransformer.createOrientation(
                                protoSensPosEvent.getOrientation().getX(),
                                protoSensPosEvent.getOrientation().getY(),
                                protoSensPosEvent.getOrientation().getZ(),
                                protoSensPosEvent.getOrientation().getW()
                        )
                ));
        return positionEvent;
    }

    public static NFC nfc_GSensor2Internal(GenericSensor.NFCRawDataEvent proto_nfcRawdataEvent) {
        NFC internal_nfcRawdataEvent = modelFactory.createNFC();
        // internal_nfcRawdataEvent.setTimeStamp(proto_nfcRawdataEvent.getTimestamp());

        // fake nfc-timestamp (queube could not provide millisecond-accuracy as it relies on time server)
       internal_nfcRawdataEvent.setTimeStamp(LocalDateTime.now() + "+00:00"); // suffix necessary to be parsable by DateTimeFormatter.ISO_OFFSET_DATE_TIME
       internal_nfcRawdataEvent.setTagId(proto_nfcRawdataEvent.getTagId());
        internal_nfcRawdataEvent.setScannerId(proto_nfcRawdataEvent.getScannerId());
        internal_nfcRawdataEvent.setType(proto_nfcRawdataEvent.getType());
        internal_nfcRawdataEvent.setTagData(convertToStringToObjectMap(proto_nfcRawdataEvent.getTagDataMap()));
        return internal_nfcRawdataEvent;
   }

    private static Map<String, Object> convertToStringToObjectMap(Map<String, String> inputMap) {
        Map<String,Object> outputMap =new HashMap<String,Object>();
        for (Map.Entry<String, String> entry : inputMap.entrySet()) {
            if(entry.getValue() != null){
                outputMap.put(entry.getKey(), (Object) entry.getValue());
            }
        }
        return outputMap;
    }

    public static IMU imu_GSensor2Internal(GenericSensor.IMURawDataEvent proto_imuRawDataEvent) {
       IMU internal_imuRawDataEvent = modelFactory.createIMU();
       internal_imuRawDataEvent.setTimeStamp(proto_imuRawDataEvent.getTimestamp());
       internal_imuRawDataEvent.setSensorId(proto_imuRawDataEvent.getSensorId());
       Acceleration internal_acceleration = modelFactory.createAcceleration();
       internal_acceleration.setX(proto_imuRawDataEvent.getX());
       internal_acceleration.setY(proto_imuRawDataEvent.getY());
       internal_acceleration.setZ(proto_imuRawDataEvent.getZ());
       internal_imuRawDataEvent.setAcceleration(internal_acceleration);
       Quaternion orientation = IPos2protoTransformer.createOrientation(proto_imuRawDataEvent.getOrientation().getX(),
               proto_imuRawDataEvent.getOrientation().getY(),
               proto_imuRawDataEvent.getOrientation().getZ(),
               proto_imuRawDataEvent.getOrientation().getW());
       internal_imuRawDataEvent.setAngularrate(orientation);
       return internal_imuRawDataEvent;
    }

    public static UWB uwb_GSensor2Internal(GenericSensor.UWBRawDataEvent proto_uwbRawDataEvent) {
        UWB internal_btRawDataEvent = modelFactory.createUWB();
        // UWB adds nothing to Beacon, so all we do here is using the function for the transformation of the Beacon-specific properties
        internal_btRawDataEvent = beacon_GSensor2Internal(internal_btRawDataEvent, proto_uwbRawDataEvent.getTimestamp(), proto_uwbRawDataEvent.getSensorId(), proto_uwbRawDataEvent.getDistancesMap(), proto_uwbRawDataEvent.getType());
        return internal_btRawDataEvent;
    }

    private static Map<String, Double> convertToStringToDoubleMap(Map<String, String> distancesMap) throws NumberFormatException {
        Map<String,Double> outputMap =new HashMap<String,Double>();
        for (Map.Entry<String, String> entry : distancesMap.entrySet()) {
            if(entry.getValue() != null){
                outputMap.put(entry.getKey(), Double.parseDouble(entry.getValue()));
            }
        }
        return outputMap;

   }

    public static IPosUWBEvent uwb_internal2IPosRawdata(UWB beaconRawdataEvent, String agentId, String agentType) {
       IPosUWBEvent iPosUWBEvent = devKitFactory.createIPosUWBEvent();
       iPosUWBEvent.setAgentId(agentId);
       iPosUWBEvent.setSensorId(beaconRawdataEvent.getSensorId());
       iPosUWBEvent.setAgentType(agentType);
       iPosUWBEvent.setSensorType(beaconRawdataEvent.getType());
       iPosUWBEvent.setTimeStamp(beaconRawdataEvent.getTimeStamp());
       iPosUWBEvent.setDistances(beaconRawdataEvent.getDistances());
       return iPosUWBEvent;
   }

    public static IPosBTEvent bt_internal2IPosRawdata(Bluetooth btRawdataEvent, String agentId, String agentType) {
        IPosBTEvent iPosBTEvent = devKitFactory.createIPosBTEvent();
        iPosBTEvent.setAgentId(agentId);
        iPosBTEvent.setSensorId(btRawdataEvent.getSensorId());
        iPosBTEvent.setAgentType(agentType);
        iPosBTEvent.setSensorType(btRawdataEvent.getType());
        iPosBTEvent.setTimeStamp(btRawdataEvent.getTimeStamp());
        iPosBTEvent.setDistances(btRawdataEvent.getDistances());
        iPosBTEvent.setRss(btRawdataEvent.getRss());
        return iPosBTEvent;
    }

    private static Map<String, String> beaconRawdataMap_internal2GSensor(Map<String, Double> distances) {
        HashMap<String, String> distancesGSensor = new HashMap<>();
        for (Map.Entry<String, Double> distanceEntry : distances.entrySet()){
            distancesGSensor.put(distanceEntry.getKey(), String.valueOf(distanceEntry.getValue().doubleValue()));
        }
        return distancesGSensor;
    }

    public static GenericSensor.IPosUWBEvent uwb_IPosRawdata2proto(IPosUWBEvent iPosUWBEvent_internal) {
        GenericSensor.IPosUWBEvent.Builder protoIPosUWBEvent = GenericSensor.IPosUWBEvent.newBuilder();
        protoIPosUWBEvent.setAgentId(iPosUWBEvent_internal.getAgentId());
        protoIPosUWBEvent.setSensorId(iPosUWBEvent_internal.getSensorId());
        protoIPosUWBEvent.setAgentType(iPosUWBEvent_internal.getAgentType());
        protoIPosUWBEvent.setSensorType(iPosUWBEvent_internal.getSensorType());
        protoIPosUWBEvent.setTimeStamp(iPosUWBEvent_internal.getTimeStamp());
        protoIPosUWBEvent.putAllDistances(beaconRawdataMap_internal2GSensor(iPosUWBEvent_internal.getDistances()));
        return protoIPosUWBEvent.build();
   }

    public static GenericSensor.IPosBTEvent bt_IPosRawdata2proto(IPosBTEvent iPosBTEvent_internal) {
        GenericSensor.IPosBTEvent.Builder protoIPosBTEvent = GenericSensor.IPosBTEvent.newBuilder();
        protoIPosBTEvent.setAgentId(iPosBTEvent_internal.getAgentId());
        protoIPosBTEvent.setSensorId(iPosBTEvent_internal.getSensorId());
        protoIPosBTEvent.setAgentType(iPosBTEvent_internal.getAgentType());
        protoIPosBTEvent.setSensorType(iPosBTEvent_internal.getSensorType());
        protoIPosBTEvent.setTimeStamp(iPosBTEvent_internal.getTimeStamp());
        protoIPosBTEvent.putAllDistances(beaconRawdataMap_internal2GSensor(iPosBTEvent_internal.getDistances()));
        protoIPosBTEvent.putAllRss(beaconRawdataMap_internal2GSensor(iPosBTEvent_internal.getRss()));
        return protoIPosBTEvent.build();
    }

    public static GenericSensor.IPosRawdataEventWrapper uwb_wrapIntoRawdataEventWrapper(GenericSensor.IPosUWBEvent iPosUWBEvent_proto) {
        GenericSensor.IPosRawdataEventWrapper.Builder iPosRawdataEventWrapper_proto = GenericSensor.IPosRawdataEventWrapper.newBuilder();
        iPosRawdataEventWrapper_proto.addIPosUWBEvent(iPosUWBEvent_proto);
        return iPosRawdataEventWrapper_proto.build();
   }

    public static GenericSensor.IPosRawdataEventWrapper bt_wrapIntoRawdataEventWrapper(GenericSensor.IPosBTEvent iPosBTEvent_proto) {
        GenericSensor.IPosRawdataEventWrapper.Builder iPosRawdataEventWrapper_proto = GenericSensor.IPosRawdataEventWrapper.newBuilder();
        iPosRawdataEventWrapper_proto.addIPosBTEvent(iPosBTEvent_proto);
        return iPosRawdataEventWrapper_proto.build();
    }

    public static Bluetooth bt_GSensor2Internal(GenericSensor.BluetoothRawDataEvent proto_bluetoothRawDataEvent) {
        Bluetooth internal_btRawDataEvent = modelFactory.createBluetooth();
        internal_btRawDataEvent = beacon_GSensor2Internal(internal_btRawDataEvent, proto_bluetoothRawDataEvent.getTimestamp(), proto_bluetoothRawDataEvent.getSensorId(), proto_bluetoothRawDataEvent.getDistancesMap(), proto_bluetoothRawDataEvent.getType());
        internal_btRawDataEvent.setRss(convertToStringToDoubleMap(proto_bluetoothRawDataEvent.getRssMap()));
        return internal_btRawDataEvent;
    }

    private static <T extends Beacon> T beacon_GSensor2Internal(T internal_beaconRawDataEvent, String timestamp, String sensorId, Map<String, String> distances, String type){
        internal_beaconRawDataEvent.setTimeStamp(timestamp);
        internal_beaconRawDataEvent.setDistances(convertToStringToDoubleMap(distances));
        internal_beaconRawDataEvent.setType(type);
        internal_beaconRawDataEvent.setSensorId(sensorId);
        return internal_beaconRawDataEvent;
    }
}


