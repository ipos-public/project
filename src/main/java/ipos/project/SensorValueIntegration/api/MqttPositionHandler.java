package ipos.project.SensorValueIntegration.api;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.GenericSensor;
import ipos.models.GenericSensor.SensorPositionEvent;
import ipos.models.GenericSensor.SensorEventWrapper;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.SensorValueIntegration.Service.GenericSensorTransformer;
import ipos.project.UseCaseController.PositionMonitoring;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import java.util.List;


// subscribe to the topic
@MqttListener("usertopic/SensorEventWrapper")
public class MqttPositionHandler implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    JmsTemplate jmsTemplate;

    @Autowired
    public MqttPositionHandler(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    static boolean iposFrameworkIsInitialized = false;

    public static void setInitialized(boolean b) {
        iposFrameworkIsInitialized = b;
    }

    // @Autowired
   // private ExternalPubServiceImpl mqttService;

    // method that handle new message from the topic
    public void handle(MqttMessage message) {
        try {
            if (! iposFrameworkIsInitialized){
                return;
            }
            SensorEventWrapper protoSensEventWrapper = ProtoJsonMap.fromJson(message.toString(), GenericSensor.SensorEventWrapper.class);
            handleSensorEventWrapper(protoSensEventWrapper);
          } catch (InvalidProtocolBufferException e) {
            LOG.error("Invalid mqtt message:", e);
        }
    }

    public static void handleSensorEventWrapper(GenericSensor.SensorEventWrapper iposSensorEventWrapper) {
        LOG.info("IposSensorEventWrapper received: \n"
                + iposSensorEventWrapper.toString());
        // Annahme: iposSensorEventWrapper enthält für leere Felder leere Listen und nicht null
        processBluetoothEvents(iposSensorEventWrapper.getBluetoothRawDataEventList());
        processPositionEvents(iposSensorEventWrapper.getSensorPositionEventList());
        processNFCRawdataEvents(iposSensorEventWrapper.getNfcRawDataEventList());
        processIMURawdataEvents(iposSensorEventWrapper.getImuRawDataEventList());
        processUWBRawdataEvents(iposSensorEventWrapper.getUwbRawDataEventList());
        // this.jmsTemplate.convertAndSend("/request123", monReqInternal); // submit request to the internal broker
    }

    private static void processBluetoothEvents(List<GenericSensor.BluetoothRawDataEvent> bluetoothRawDataEventList) {
        for ( GenericSensor.BluetoothRawDataEvent proto_bluetoothRawDataEvent : bluetoothRawDataEventList){
            Bluetooth btEvent_beacon;
            try {
                btEvent_beacon = GenericSensorTransformer.bt_GSensor2Internal(proto_bluetoothRawDataEvent);
            }catch(NumberFormatException e){
                LOG.info("Warning: Received BT-rawdata that contained ill-formatted distance or rss information");
                continue;
            }
            PositionMonitoring.receiveMessage((Beacon) btEvent_beacon);
        }
    }

    private static void processUWBRawdataEvents(List<GenericSensor.UWBRawDataEvent> uwbRawDataEventList) {
        for (GenericSensor.UWBRawDataEvent proto_uwbRawDataEvent : uwbRawDataEventList){
            UWB internal_uwbRawDataEvent;
            try {
                internal_uwbRawDataEvent = GenericSensorTransformer.uwb_GSensor2Internal(proto_uwbRawDataEvent);
            }catch (NumberFormatException e){
                LOG.info("Warning: Received UWB-rawdata that contained ill-formatted distance information");
                continue;
            }
            PositionMonitoring.receiveMessage(internal_uwbRawDataEvent);
        }

    }

    private static void processIMURawdataEvents(List<GenericSensor.IMURawDataEvent> imuRawDataEventList) {
        for (GenericSensor.IMURawDataEvent proto_imuRawDataEvent : imuRawDataEventList){
            IMU internal_imuRawDataEvent = GenericSensorTransformer.imu_GSensor2Internal(proto_imuRawDataEvent);


            // JMS-topic: imu_rawdata_event
            PositionMonitoring.receiveMessage(internal_imuRawDataEvent);
        }
    }

    private static void processNFCRawdataEvents(List<GenericSensor.NFCRawDataEvent> nfcRawDataEventList) {
        for (GenericSensor.NFCRawDataEvent proto_nfcRawdataEvent : nfcRawDataEventList){
            // transformer fakes nfc-timestamp...
            NFC internal_nfcRawDataEvent = GenericSensorTransformer.nfc_GSensor2Internal(proto_nfcRawdataEvent);
            PositionMonitoring.receiveMessage(internal_nfcRawDataEvent);
        }
    }

    private static void processPositionEvents(List<SensorPositionEvent> sensorPositionEventList) {
        // assert sensorPositionEventList != null;
        for (GenericSensor.SensorPositionEvent proto_sensPosEvent : sensorPositionEventList){
            PositionEvent positionEvent = GenericSensorTransformer.posEvent_GSensor2Internal(proto_sensPosEvent);
            LOG.info("SensorValueIntegration: Transformed Protobuf-SensorPositionEvent into Internal-PositionEvent: " + positionEvent.toString() + ", " + positionEvent.getPlacing().getPosition().getPoint().toString() + ", " + positionEvent.getPlacing().getOrientation().toString());

            // vorübergehend: anstatt JMS einfach den PositionMonitoringController per Funktionsaufruf aufrufen
            PositionMonitoring.receiveMessage(positionEvent);
            // jmsTemplate.convertAndSend("/PositionEvent", protoSensPosEvent); // submit position to the internal broker
            //TODO: fix message converter for the internal broker
        }
    }

}
