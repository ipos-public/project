package ipos.project.UseCaseController;

import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.Functionality.DataServices;
import ipos.project.Functionality.SRSConversion.SRSConversion;
import org.apache.logging.log4j.LogManager;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class Administration {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    public Administration(){}

    public static void initialize(){
        LOG.info("Application initialized");
        DataServices.clearWorldModel();
    }

    public static void handleZone(Zone zone){
        LOG.info("handling zone-object: " + zone.toString());
        Administration.transformToRootCoordinates(zone); // transformation is necessary, EventFilter should have to evaluate ROOT-Positions, therefore both, the PositionEvents and the Space-CentrePoint, should have RootPositions.
        DataServices.addZone(zone);
    }

    public static void handleAgent(Agent agent) {
        LOG.info("handling agent-object: " + agent.toString());
        DataServices.addAgent(agent);
    }

    public static void handleRefSystem(ReferenceSystem refSystem) {
        // muss checken, ob das RefSystem auf das ggf. verwiesen wird - tatsächlich existiert. Falls nicht: Speicherung ablehnen
        LOG.info("handling refSystem-object: " + refSystem.toString());
        //this would delete the SRS-information of the refSystem-object and transform its origin into ROOT-SRS: Administration.transformToRootCoordinates(refSystem);
        // nur sicherstellen das alle Bezugssysteme wirklich existieren
        DataServices.addRefSystem(refSystem);
    }

    public static void handlePoi(POI poi) {
        // muss checken, ob das RefSystem auf das verwiesen wird - tatsächlich existiert. Falls nicht: Speicherung ablehnen
        LOG.info("handling poi-object: " + poi.toString());
        LOG.info("INDFRO-DEBUG: SRS of poi-object: " + poi.getPlacing().getPosition().getReferenceSystem().getId());
        Administration.transformToRootCoordinates(poi);
        DataServices.addPoi(poi);
    }

    public static void transformToRootCoordinates(PositionEvent posEvent) {
        LOG.info("INDFRO-DEBUG: Handling PositionEvent-object. SRS: " + posEvent.getPlacing().getPosition().getReferenceSystem().getId());
        Administration.transformToRootCoordinates(posEvent.getPlacing());
    }

    public static void transformToRootCoordinates(Zone zone) {
        zone.getSpace().stream()
                .map(Administration::transformToRootCoordinates);
    }

    public static Space transformToRootCoordinates(Space space) {
        LOG.info("INDFRO-DEBUG: Handling space-object. SRS: " + space.getCentrePoint().getPosition().getReferenceSystem().getId());
        Administration.transformToRootCoordinates(space.getCentrePoint());
        return space;
    }

    public static void transformToRootCoordinates(ReferenceSystem refSystem) {
        LOG.info("INDFRO-DEBUG: Transforming refSystem-object " + refSystem.toString() + "to ROOT-SRS");
        Administration.transformToRootCoordinates(refSystem.getOrigin());
    }

    public static void transformToRootCoordinates(POI poi) {
        Administration.transformToRootCoordinates(poi.getPlacing());
    }

    /**
     * Function recursively transforms a placing into a ROOT-placing. In each step the placing
     * is transformed in a placing of the SRS that the origin of its current SRS has been defined
     * in. Recursion is stopped after obtaining a ROOT-placing.
     * @param placingToBeTransformed
     */
    public static void transformToRootCoordinates(Placing placingToBeTransformed) {
        if (isNotRootPosition(placingToBeTransformed.getPosition())) {
            ReferenceSystem nextTransformationGoal_SRS = placingToBeTransformed.getPosition().getReferenceSystem();
            LOG.info("INDFRO-DEBUG: (before) ReferenceSystem: " + placingToBeTransformed.getPosition().getReferenceSystem().getId() + "; Position: " + PositionMonitoring.logPosition(placingToBeTransformed) + "; Orientation: " + PositionMonitoring.logOrientation(placingToBeTransformed));
            SRSConversion.switchSRS(placingToBeTransformed, nextTransformationGoal_SRS); // assumption: not only position is changed but also ReferenceSystem-object of the placingToBeTransformed is updated
            LOG.info("INDFRO-DEBUG: (after) ReferenceSystem: " + placingToBeTransformed.getPosition().getReferenceSystem().getId() + "; Position: " + PositionMonitoring.logPosition(placingToBeTransformed) + "; Orientation: " + PositionMonitoring.logOrientation(placingToBeTransformed));
            LOG.info("INDFRO-DEBUG:");
            transformToRootCoordinates(placingToBeTransformed); // recursion stops when placing.getPosition() has been transformed in a ROOT-position
        }
    }

    public static void transformToRootCoordinates(Position position) {
        LOG.info("INDFRO-DEBUG: Transforming Position-object " + position.toString() + "to ROOT-SRS");
        Orientation dummyOrientation = createDummyOrientation();
        Placing placing = DataServices.createPlacing(position, dummyOrientation);
        transformToRootCoordinates(placing); // placing.getPosition() points to the position-object that was passed to this function. The contents of this object will be changed by this call.
    }

    private static Orientation createDummyOrientation() {
        return DataServices.createOrientation(0, 0, 0, 0);
    }

    private static boolean isRootPosition(Position position) {
        return !isNotRootPosition(position);
    }

    private static boolean isRootPosition(String srsId){
        return !isNotRootPosition(srsId);
    }

    private static boolean isNotRootPosition(Position position) {
        return isNotRootPosition(position.getReferenceSystem().getId());
    }

    private static boolean isNotRootPosition(String srsId){
        return !srsId.equals(PositionMonitoring.SRS_ID_ROOT);
    }

    /**
     * Warning: This function does not check whether SRSConversion is actually necessary.
     * Use function Administration::isNotRootPosition to check whether SRSConversion is necessary,
     * before calling this function
     * @param orientation

    public static void transformToRootCoordinates(Position position, Orientation orientation) {
        // Komponente SRSConversion aufrufen

                        SRSConversion.switchSRS(rootPlacing, DataServices.getReferenceSystemByIdOrNull("CeTI_SRS"));
                        SRSConversion.switchSRS(cetiPlacing, DataServices.getReferenceSystemByIdOrNull("CeTI_SRS"));
    }
     */

    /**
     * This function expects the position posEvent to be defined in reference to the ROOT-SRS
     * @param posEvent
     * @param refSystemId
     */
    public static void transformToRequesterCoordinates(PositionEvent posEvent, String refSystemId) {
        if(! posEvent.getPlacing().getPosition().getReferenceSystem().getId().equals(PositionMonitoring.SRS_ID_ROOT)){
            LOG.warn("INDFRO-DEBUG: Warning: Function transformToRequesterCoordinates was called with a position that is not defined in reference to the ROOT-SRS. " +
                    "All internal data processing should be based on ROOT-positions.");
            return;
        }
        if(isRootPosition(refSystemId)){
            return; // nothing to be done if ROOT is the transformation target, as posEvent is assumed to be a ROOT-position
        }
        ReferenceSystem finalTarget = DataServices.getReferenceSystemByIdOrNull(refSystemId);
        Placing placingToBeTransformed = posEvent.getPlacing();
        LOG.info("INDFRO-DEBUG: transformToRequesterCoordinates: Transforming posEvent (SRS: " + placingToBeTransformed.getPosition().getReferenceSystem().getId() +" ) to requester coordinates (SRS: " + refSystemId + ")");
        List<ReferenceSystem> allTargetsFromRootToFinal = computeTargets(finalTarget);
        LOG.info("INDFRO-DEBUG: Transformation targets (excluding " + PositionMonitoring.SRS_ID_ROOT + "): " + printTargets(allTargetsFromRootToFinal));
        transformPlacingSuccessively(placingToBeTransformed, allTargetsFromRootToFinal);
    }

    private static String printTargets(List<ReferenceSystem> allTargetsFromRootToFinal) {
        return allTargetsFromRootToFinal.stream()
                .map(srs -> srs.getId())
                .collect(Collectors.joining(" -> "));
    }

    /**
     * Assumption: Sooner or later each target depends on ROOT-SRS
     * @param target
     * @return
     */
    private static List<ReferenceSystem> computeTargets(ReferenceSystem target) {
        if (isRootPosition(target.getId())){
            List<ReferenceSystem> targets = new LinkedList<>();
            return targets; // ROOT-SRS itself is not a target, therefore it is not added to the list
        }else {
            ReferenceSystem nextTarget = target.getOrigin().getPosition().getReferenceSystem();
            List<ReferenceSystem> targets = computeTargets(nextTarget);
            targets.add(target); // targets[0]: the SRS that the ROOT-position should be transformed into; targets[targets.size()-1]: final target, i.e., requester-SRS
            return targets;
        }

    }

    private static void transformPlacingSuccessively(Placing placingToBeTransformed, List<ReferenceSystem> transformationTargets) {
        for (ReferenceSystem transTarget : transformationTargets){
            LOG.info("INDFRO-DEBUG: (before) ReferenceSystem: " + placingToBeTransformed.getPosition().getReferenceSystem().getId() + "; Position: " + PositionMonitoring.logPosition(placingToBeTransformed)  + "; Orientation: " + PositionMonitoring.logOrientation(placingToBeTransformed));
            SRSConversion.switchSRS(placingToBeTransformed, transTarget);
            LOG.info("INDFRO-DEBUG: (after) ReferenceSystem: " + placingToBeTransformed.getPosition().getReferenceSystem().getId() + "; Position: " + PositionMonitoring.logPosition(placingToBeTransformed)  + "; Orientation: " + PositionMonitoring.logOrientation(placingToBeTransformed));
        }
    }
}
