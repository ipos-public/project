package ipos.project.UseCaseController;

import ipos.project.DataModellntegration.SimpleSceneIntegration.SimpleSceneIntegration;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.DataStorageQueryRequest;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposMsgRcvEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.Functionality.*;
import ipos.project.Functionality.eventfilter.*;
import ipos.project.Functionality.eventfilter.BeaconFilteringResult;
import ipos.project.SensorValueIntegration.GenericSensorValueProcessor;
import ipos.project.devkit.trans.IPos2protoTransformer;
import org.apache.logging.log4j.LogManager;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Component
public class PositionMonitoring {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static List<eventFilter> eventFilters = new ArrayList<eventFilter>();
    private static Map<String, List<String>> zonePopulation = new HashMap<String, List<String>>(); // maps zone-ids to list of ids of those agents that are currently located within that zone.
    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;
    private static IPosDevKitFactory devKitFactory = IPosDevKitFactory.eINSTANCE;

    //TODO: Constant-definitions shall be migrated to a dedicated component. Otherwise, avoidable dependencies are created among PositionMonitoring and the other components
    public static final String EXIT_NOTIFICATION_TYPE = "ExitNotification";
    public static final String ENTRY_NOTIFICATION_TYPE = "EntryNotification";
    public static final String UNDEFINED_TYPE = "undefined";
    public static final String PROTOBUF_SERIALIZATION_TYPE = "protobuf";
    public static final String JSON_SERIALIZATION_TYPE = "json";
    public static final String ACCURACY_FUSION_STRATEGY = "accuracy";
    public static final String TRACKING_SUFFIX = "_tracking";
    public static final String TOPIC_ConfigWrapper = "usertopic/IposConfigWrapper";
    public static final String TOPIC_DOOR = "Human_at_Door_Side";
    public static final String TOPIC_WINDOW = "Human_at_Window_Side";
    public static final String ZONE_ID_DOOR = "cobot1_door_zone";
    public static final String ZONE_ID_WINDOW = "cobot1_window_zone";
    public static final String SRS_ID_ROOT = "ROOT";

    public static final int SDF_TIME_INTERVAL = 2500; // recent milliseconds to take into account for sensor data fusion

    public PositionMonitoring(){}

    public static void receiveMessage(NFC nfcRawDataEvent) {
        PositionEvent positionEvent = PositionCalculation.calcPositionFromNfcOrNull(nfcRawDataEvent);
        if (positionEvent != null) {
            updateLocalizableObject(positionEvent);
            try {
                Odometry.calibrate(positionEvent);
            } catch (ParseException e) {
                LOG.warn("Odometry-component could not be calibrated");
                e.printStackTrace();
            }
            receiveMessage(positionEvent);
        }
    }

    public static void receiveMessage (Beacon beaconRawDataEvent){
        // currently we only filter rawdata on beacon-level. Position calculation is performed on the level of the specific beacon technology (e.g., UWB)
        // calculateAndProcessPosition(uwbRawDataEvent);
        List<BeaconFilteringResult> beaconFilteringResults = filterRawdata(beaconRawDataEvent);
        processBeaconFilteringResults(beaconFilteringResults);
    }

    public static void receiveMessage (UWB uwbRawDataEvent){
        calculateAndProcessPosition(uwbRawDataEvent);
        List<BeaconFilteringResult> beaconFilteringResults = filterRawdata((Beacon) uwbRawDataEvent);
        processBeaconFilteringResults(beaconFilteringResults);
    }

    private static void calculateAndProcessPosition(UWB uwbRawDataEvent) {
        Optional<PositionEvent> optionalPositionEvent = Optional.empty();
        try {
            optionalPositionEvent = Triangulation.update(uwbRawDataEvent);
            // optionalPositionEvent = PositionCalculation.updateUWBPositionFromDistances(uwbRawDataEvent);
            // = PositionCalculation.updateBTDistancesFromRSS(
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (optionalPositionEvent.isPresent()) {
            PositionEvent positionEvent = optionalPositionEvent.get();
            LOG.info("OP:");
            LOG.info("OP:");
            LOG.info("OP:");
            LOG.info("OP:----PositionMonitoring--------------------------");
            LOG.info("OP: position: " + logPosition(positionEvent.getPlacing()));
            updateLocalizableObject(positionEvent);
            receiveMessage(positionEvent);
        }else {
            LOG.info("OP: No position could be calculated from UWBRawDataEvent");
        }
    }

    private static void processBeaconFilteringResults(List<BeaconFilteringResult> beaconFilteringResults) {
        for (BeaconFilteringResult beaconFilteringResult : beaconFilteringResults){
            if (beaconFilteringResult.hasBlockedEvent()){ // don't publish Beacon-rawdata on behalf of an eventFilter that has blocked the event
                continue;
            }
            eventFilter eFilter = getEventFilterByIdOrNull(beaconFilteringResult.getMonitoringTaskId());

            LocalizableObject lObject = PositionMonitoring.getLObjectByIdOrNull(beaconFilteringResult.getBeaconRawdataEvent().getSensorId());
            if (null == lObject || null == lObject.getAgent()) {
                LOG.error("Internal Beacon-Rawdata Event could not be transformed into IPos Rawdata-Event " +
                        "No LocalizableObject with the provided sensor-id could be found, or no agent " +
                        "is associated to the LocalizableObject that has been found.");
                return;
            }
            GenericSensorValueProcessor.receiveMessage(beaconFilteringResult.getBeaconRawdataEvent(), beaconFilteringResult.getMonitoringTaskId(), eFilter.getSerializationType(), lObject.getAgent().getId(), lObject.getAgent().getAgentType());
        }
    }

    private static List<BeaconFilteringResult> filterRawdata(Beacon beaconRawDataEvent) {
        List<BeaconFilteringResult> beaconFilteringResults = new LinkedList<BeaconFilteringResult>();
        for (eventFilter eFilter : eventFilters){
            beaconFilteringResults.add(eFilter.processRawdata(beaconRawDataEvent));
        }
        return beaconFilteringResults;
    }

    static int imuCount = 0;

    // JMS-topic: imuRawdataEvent
    public static void receiveMessage(IMU imuRawdataEvent){

        PositionEvent positionEvent = null; // if not calibrated: exception
        try {
            positionEvent = Odometry.update(imuRawdataEvent);
            logOdo(positionEvent);
        } catch (ParseException e) {
            LOG.info("Odometry: ParseException: " + e.getMessage());
        } catch (MissingResourceException e){
            LOG.info("Odometry: " + e.getMessage());
        }
        if (positionEvent != null) {
            updateLocalizableObject(positionEvent);
            if ( imuCount < 5) {
                imuCount += 1;
            }else {
                receiveMessage(positionEvent);
                imuCount = 0;
            }
        }


    }

    private static void logOdo(PositionEvent positionEvent) {
        float odoAccuracy = ((Gaussian) positionEvent.getPlacing().getPosition().getAccuracy()).getConfidenceInterval();
        LOG.info("ODO: Odometry calculated position with accuracy: " + odoAccuracy);
    }

    private static eventFilter getEventFilterByIdOrNull(String monitoringTaskId){
        for (eventFilter eFilter : eventFilters){
            if (eFilter.getMonitoringTaskId().equals(monitoringTaskId)){
                return eFilter;
            }
        }
        return null;
    }

    private static void updateLocalizableObject(PositionEvent positionEvent) {
        LocalizableObject lObject = DataServices.getLObjectByIdOrNull(positionEvent.getLObjectId());
        if (lObject == null){
            LOG.info("Warning: lObject not found. id contained in positionEvent: " + positionEvent.getLObjectId());
        }
        // TODO: caution: all changes to the data model should be performed using DataServices.
        // Directly changing this object just works for the current simple case that we only have the object-representation of the world model
        // in general it has to be made sure that both, all links from this object and to this object keep intact
        lObject.setCurrentPlacing(positionEvent.getPlacing());
        lObject.setLastPosUpdate(positionEvent.getTimeStamp());
    }

    private MonitoringRequest createMonitoringRequest(List<String> frameIds, String monitoringTaskId) {
        MonitoringRequest monReq = devKitFactory.createMonitoringRequest();
        monReq.getFrameIds().addAll(frameIds);
        monReq.setMonitoringTaskId(monitoringTaskId);
        return monReq;
    }

    public static LocalizableObject getLObjectByIdOrNull(String lObjectId){
       return DataServices.getLObjectByIdOrNull(lObjectId);
     }

    public static ReferenceSystem getReferenceSystemByIdOrNull(String refSystemId){
        return DataServices.getReferenceSystemByIdOrNull(refSystemId);
     }

    @JmsListener(destination = "/monitoringRequest", containerFactory = "jmsListenFactory")
    public static void receiveMessage(MonitoringRequest monReq) {
        LOG.info("Received MonitoringRequest <" + monReq + ">");
        eventFilter filter = new eventFilter(monReq);
        EventFilterCondition config = createAndInitEventFilterCondition();
        if (! readConfig.readFilterConfigFromMonitoringRequest(monReq, config)){
            return; // monitoringRequest could not be read
        }
        filter.init(config);
        eventFilters.add(filter);
        LOG.info("EventFilter was created: " + filter.toString() + "; monitoringTaskId: " + filter.getMonitoringTaskId() + "; zone-ids:" + filter.getFilterConditionConfig().getPositionConditionCells().keySet() + "; property: " + new HashSet(filter.getFilterConditionConfig().getPropertyCondition()));

        /*
         * trackingTaskId equals MonitoringTaskId + '_tracking' (which equals the name
         * of the topic used for fetching the stored positions) to be able to
         * distinguish the monitoring-topic from the tracking-topic. Therefore, the
         * data should be stored using the altered name. The DataQueryRequest will also
         * contain the suffix, therefore, we change the suffix here automatically.
        */
        String trackingTaskId = monReq.getMonitoringTaskId() + PositionMonitoring.TRACKING_SUFFIX;
        DataServices.initTrackingTask(trackingTaskId);
    }

    private static EventFilterCondition createAndInitEventFilterCondition() {
        EventFilterCondition config = modelFactory.createEventFilterCondition();
        config.setPositionConditionCells(new HashMap<>());
        config.setSensorIdCondition(new LinkedList<>());
        config.setPropertyCondition(new LinkedList<>());
        return config;
    }

    private static boolean isDoor = false;
    private static boolean isCentre = false;
    private static boolean isWindow = false;
    private static boolean firstMessage = true;


    /**
     * It is important to find out which zones should be actually reported to the requester
     * (there may be other zones that overlap the ones that tha requester wanted to monitor or track)
     * I think, we should make sure that using they key trackingTaskId only those positionEvents should be persisted
     * that do actually have been accepted by the associated monitoringRequest (so, whose monitoringRequestId equals
     * the trackingTaskId without "_tracking" at the end)
     * @param dsQueryRequest
     */
    public static void receiveMessage(DataStorageQueryRequest dsQueryRequest){
        if(! dsQueryRequest.getTrackingTaskId().endsWith(PositionMonitoring.TRACKING_SUFFIX)){
            LOG.warn("Can not handle QueryRequest due to unexpected trackingTaskId. " +
                    "TrackingTaskIds shall end with " + PositionMonitoring.TRACKING_SUFFIX);
            return;
        }
        String trackingTaskId = dsQueryRequest.getTrackingTaskId();
        List<PositionEvent> trackedEvents = DataServices.query(dsQueryRequest.getTrackingTaskId());
        DataStorageQueryResponse dsQueryResponse = modelFactory.createDataStorageQueryResponse();
        dsQueryResponse.getPositionEvents().addAll(trackedEvents);
        dsQueryResponse.setTrackingTaskId(trackingTaskId);
        eventFilter eFilter = getEventFilterFromTrackingTaskId(trackingTaskId);
        SimpleSceneIntegration.receiveMessage(dsQueryResponse, dsQueryRequest.getTrackingTaskId(), eFilter.getSerializationType());
    }

    private static eventFilter getEventFilterFromTrackingTaskId(String trackingTaskId) {
        int lengthMonitoringTaskId = trackingTaskId.length() - PositionMonitoring.TRACKING_SUFFIX.length();
        String monitoringTaskId = trackingTaskId.substring(0, lengthMonitoringTaskId);
        eventFilter eFilter = getEventFilterByIdOrNull(monitoringTaskId);
        return eFilter;
    }

    // @JmsListener(destination = "/PositionEvent", containerFactory = "jmsListenFactory")
    public static void receiveMessage(PositionEvent posEvent) {
        // TODO: Update currentPlacing attribute of the affected localizableObject
        LOG.info("Received <" + posEvent + ">");
        logging_sdfindfro(posEvent);
        Administration.transformToRootCoordinates(posEvent);
        LOG.info("Position after transformation to ROOT-coordinates: " + posEvent.getPlacing().getPosition().toString());

        boolean posEventMeetsSdfAccuracyCondition = SensorDataFusion.isMostAccuratePositionAvailable(posEvent);
        List<PositionFilteringResult> positionFilteringResults = new LinkedList<>();
        for (eventFilter eFilter : eventFilters){
           try {
                if (eFilterMayHandlePosEvent(posEvent, posEventMeetsSdfAccuracyCondition, eFilter)){
                    positionFilteringResults.add(applyEfilterToPosEvent(posEvent, eFilter));
                }
            } catch (ParseException e) {
               processFilteringResults(posEvent, positionFilteringResults);
               e.printStackTrace();
                return;
            }
        }
        processFilteringResults(posEvent, positionFilteringResults);
        LOG.info("INDFRO-DEBUG: zonePopulation after processing filtering results: " + zonePopulation.toString());
    }

    public static void receiveMessage(MessageReceivedEvent messageReceivedEvent) {
        try {
            _receiveMessage(messageReceivedEvent);
        }catch (NullPointerException e){
            LOG.info("Could not process received message. " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static void _receiveMessage(MessageReceivedEvent messageReceivedEvent) {
        for (eventFilter eFilter : eventFilters){
            applyEFilterToMessageReceivedEventAndProcessFilteringResult(messageReceivedEvent, eFilter);
        }
    }

    private static void applyEFilterToMessageReceivedEventAndProcessFilteringResult(MessageReceivedEvent messageReceivedEvent, eventFilter eFilter) {
        MsgRcvFilteringResult msgRcvFilteringResult = eFilter.processMessageReceivedEvent(messageReceivedEvent);
        if (msgRcvFilteringResult.hasBlockedEvent()){
            return;
        }else{
            IposMsgRcvEvent iposMsgRcvEvent = createIposMsgRcvEvent(msgRcvFilteringResult);
            LOG.info("VDA5050: Filter accepted MessageReceived event.");
            LOG.info("VDA5050: Corresponding IposMsgRcvEvent: " + iposMsgRcvEvent.toString());
            SimpleSceneIntegration.receiveMessage(iposMsgRcvEvent, msgRcvFilteringResult.getMonitoringTaskId(), eFilter.getSerializationType());
        }
    }

    private static IposMsgRcvEvent createIposMsgRcvEvent(MsgRcvFilteringResult msgRcvFilteringResult) {
        LocalizableObject lObject = getLObjectAssociatedToMessageRcvFilteringEvent(msgRcvFilteringResult);
        IposMsgRcvEvent iposMsgRcvEvent = extendMessageRcvFilteringEventToIposMsgRcvFilteringEvent(msgRcvFilteringResult, lObject);
        return iposMsgRcvEvent;
    }

    private static IposMsgRcvEvent extendMessageRcvFilteringEventToIposMsgRcvFilteringEvent(MsgRcvFilteringResult msgRcvFilteringResult, LocalizableObject lObject) {
        IposMsgRcvEvent iposMsgRcvEvent = devKitFactory.createIposMsgRcvEvent();
        iposMsgRcvEvent.setProtocolName(msgRcvFilteringResult.getMessageReceivedEvent().getProtocolName());
        iposMsgRcvEvent.setSerializedMsg(msgRcvFilteringResult.getMessageReceivedEvent().getSerializedMsg());
        iposMsgRcvEvent.setAgentId(msgRcvFilteringResult.getMessageReceivedEvent().getAgentId());
        iposMsgRcvEvent.setExtractedAttributes(msgRcvFilteringResult.getMessageReceivedEvent().getExtractedAttributes());
        iposMsgRcvEvent.setTimestamp(msgRcvFilteringResult.getMessageReceivedEvent().getTimestamp());
        handleOptionalData(lObject, iposMsgRcvEvent);
        iposMsgRcvEvent.getLastKnownZonedescriptors().addAll(createDescriptorsForZonesThatAgentIsCurrentlyLocatedIn(msgRcvFilteringResult));
        return iposMsgRcvEvent;
    }

    private static void handleOptionalData(LocalizableObject lObject, IposMsgRcvEvent iposMsgRcvEvent) {
        if (null != lObject.getCurrentPlacing() && null != lObject.getCurrentPlacing().getPosition()){
            iposMsgRcvEvent.setLastKnownPosition(lObject.getCurrentPlacing().getPosition());
            iposMsgRcvEvent.setLastPosUpdate(lObject.getLastPosUpdate());
        }
        if (null != lObject.getCurrentPlacing() && null != lObject.getCurrentPlacing().getOrientation()){
            iposMsgRcvEvent.setLastKnownOrientation(lObject.getCurrentPlacing().getOrientation());
        }
    }


    private static List<ZoneDescriptor> createDescriptorsForZonesThatAgentIsCurrentlyLocatedIn(MsgRcvFilteringResult msgRcvFilteringResult) {
        List<ZoneDescriptor> zoneDescriptors = new LinkedList<>();
        eventFilter eFilter = Objects.requireNonNull(getEventFilterByIdOrNull(msgRcvFilteringResult.getMonitoringTaskId()), "unknown monitoring-task-id: " + msgRcvFilteringResult.getMonitoringTaskId());
        Set<String> zoneIdsThatEFilterIsInterestedIn = eFilter.getFilterConditionConfig().getPositionConditionCells().keySet();
        for (String zoneId : zoneIdsThatEFilterIsInterestedIn){
            List<String> idsOfAgentsCurrentlyInZone = Optional.ofNullable(zonePopulation.get(zoneId)).orElse(new LinkedList<>());
            // Is the AGV that the Message originated from known to be currently located in that zone?
            if(idsOfAgentsCurrentlyInZone.contains(msgRcvFilteringResult.getMessageReceivedEvent().getAgentId())){
                ZoneDescriptor zoneDescriptor = modelFactory.createZoneDescriptor();
                zoneDescriptor.setZoneId(zoneId);
                zoneDescriptor.setNotificationType(UNDEFINED_TYPE); // notification type: Do we indicate with this message that a zone was left or entered?
                zoneDescriptors.add(zoneDescriptor);
            }
        }
        return zoneDescriptors;
    }

    private static LocalizableObject getLObjectAssociatedToMessageRcvFilteringEvent(MsgRcvFilteringResult msgRcvFilteringResult) {
        String agentId_whichEquals_sensorId = msgRcvFilteringResult.getMessageReceivedEvent().getAgentId();
        LocalizableObject lObject = getLObjectByIdOrNull(agentId_whichEquals_sensorId);
        return Objects.requireNonNull(lObject, "Received message is associated to an unknown sensor-id: " + agentId_whichEquals_sensorId);
    }

    private static void processFilteringResults(PositionEvent posEvent, List<PositionFilteringResult> positionFilteringResults) {
        Map<String, List<ZoneDescriptor>> eFilterZoneDescriptors = translateResultsIntoZoneDescriptors(positionFilteringResults);
        persistPositionEvent(posEvent, eFilterZoneDescriptors);
        sendPositionEventToRequesters(posEvent, eFilterZoneDescriptors);
        updateZoneAssociations(posEvent, eFilterZoneDescriptors);
        updateLocalizableObject(posEvent);
    }

    /**
     * Assumption: The PositionEvent posEvent passed the eventFilter whose results are
     * communicated by the zoneDescriptors.
     * @param posEvent
     * @param eFilterZoneDescriptors
     */
    private static void sendPositionEventToRequesters(PositionEvent posEvent, Map<String, List<ZoneDescriptor>> eFilterZoneDescriptors) {
        Set<String> sentAlready = sendExitNotifications(posEvent, eFilterZoneDescriptors);
        sendOtherMonitoringMessages(posEvent, eFilterZoneDescriptors, sentAlready);
    }

    private static void sendOtherMonitoringMessages(PositionEvent posEvent, Map<String, List<ZoneDescriptor>> eFilterZoneDescriptors, Set<String> sentAlready) {
        // TODO: This code duplicates sendExitNotifications()-function
        Set<String> toBeSent = eFilterZoneDescriptors.keySet().stream().collect(Collectors.toSet());
        toBeSent.removeAll(sentAlready);
        for (String monitoringTaskId : toBeSent){
            List<ZoneDescriptor> zoneDescriptorsEFilterResult = eFilterZoneDescriptors.get(monitoringTaskId);
            eventFilter eFilter = getEventFilterByIdOrNull(monitoringTaskId);
            if (eFilter == null){
                LOG.warn("A monitoringTaskId was passed that is not associated to any known eventFilter");
                continue;
            }
            PositionEvent posEventCopy = createDeepCopyOfPosEvent(posEvent);
            posEventCopy.getZonedescriptors().addAll(zoneDescriptorsEFilterResult); // ZoneDescriptors-Collection of PositionEvent was empty up until this point of execution
            Administration.transformToRequesterCoordinates(posEventCopy, eFilter.getRefSystemId());
            SimpleSceneIntegration.receiveMessage(posEventCopy, monitoringTaskId, getFirstNotificationTypeOrUndefined(zoneDescriptorsEFilterResult), eFilter.getSerializationType());
        }
    }

    private static Set<String> sendExitNotifications(PositionEvent posEvent, Map<String, List<ZoneDescriptor>> eFilterZoneDescriptors) {
        // TODO: This code duplicates sendOtherNotifications()-function
        Set<String> sentAlready = new HashSet<>();
        for (String monitoringTaskId : eFilterZoneDescriptors.keySet()){
            List<ZoneDescriptor> zoneDescriptorsEFilterResult = eFilterZoneDescriptors.get(monitoringTaskId);
            eventFilter eFilter = getEventFilterByIdOrNull(monitoringTaskId);
            if (eFilter == null){
                LOG.warn("A monitoringTaskId was passed that is not associated to any known eventFilter");
                continue;
            }
            if( containsExitNotification(zoneDescriptorsEFilterResult)){
                PositionEvent posEventCopy = createDeepCopyOfPosEvent(posEvent);
                posEventCopy.getZonedescriptors().addAll(zoneDescriptorsEFilterResult);
                Administration.transformToRequesterCoordinates(posEventCopy, eFilter.getRefSystemId());
                SimpleSceneIntegration.receiveMessage(posEventCopy, monitoringTaskId, getFirstNotificationTypeOrUndefined(zoneDescriptorsEFilterResult), eFilter.getSerializationType());
                sentAlready.add(monitoringTaskId);
            }
        }
        return sentAlready;
    }

    private static String getFirstNotificationTypeOrUndefined(List<ZoneDescriptor> zoneDescriptorsEFilterResult) {
        String firstNotificationType = PositionMonitoring.UNDEFINED_TYPE;
        if (zoneDescriptorsEFilterResult.size() > 0){
            firstNotificationType = zoneDescriptorsEFilterResult.get(0).getNotificationType();
        }
        return firstNotificationType;
    }

    private static boolean containsExitNotification(List<ZoneDescriptor> zoneDescriptorsEFilterResult) {
        for (ZoneDescriptor zoneDescriptor : zoneDescriptorsEFilterResult){
            if (zoneDescriptor.getNotificationType().equals(PositionMonitoring.EXIT_NOTIFICATION_TYPE)){
                return true;
            }
        }
        return false;
    }

    private static void persistPositionEvent(PositionEvent posEvent, Map<String, List<ZoneDescriptor>> eFilterZoneDescriptors) {
        for (String monitoringTaskId : eFilterZoneDescriptors.keySet()){
            List<ZoneDescriptor> zoneDescriptorsEFilterResult = eFilterZoneDescriptors.get(monitoringTaskId);
            PositionEvent posEventCopy = createDeepCopyOfPosEvent(posEvent);
            posEventCopy.getZonedescriptors().addAll(zoneDescriptorsEFilterResult);
            DataServices.persist(posEventCopy, monitoringTaskId + PositionMonitoring.TRACKING_SUFFIX);
        }
    }

    /**
     * Creates a shallow copy of the posEvent
     * @param posEvent
     * @return
     */
    private static PositionEvent createDeepCopyOfPosEvent(PositionEvent posEvent) {
        PositionEvent posEventDeepCopy = modelFactory.createPositionEvent();
        posEventDeepCopy.setPlacing(createDeepCopyOfPlacing(posEvent.getPlacing()));
        posEventDeepCopy.setTimeStamp(posEvent.getTimeStamp());
        posEventDeepCopy.setLObjectId(posEvent.getLObjectId());
        return posEventDeepCopy;
    }

    private static Placing createDeepCopyOfPlacing(Placing placingOriginal) {
        Placing placingCopy = IPos2protoTransformer.createPlacing(
                IPos2protoTransformer.createPosition(
                        IPos2protoTransformer.createPoint3D(
                                ((Point3D) placingOriginal.getPosition().getPoint()).getX(),
                                ((Point3D) placingOriginal.getPosition().getPoint()).getY(),
                                ((Point3D) placingOriginal.getPosition().getPoint()).getZ()
                        ),
                        ((Gaussian) placingOriginal.getPosition().getAccuracy()).getConfidenceInterval(),
                        DataServices.getReferenceSystemByIdOrNull(placingOriginal.getPosition().getReferenceSystem().getId())
                ),
                IPos2protoTransformer.createOrientation(
                        ((Quaternion) placingOriginal.getOrientation()).getX(),
                        ((Quaternion) placingOriginal.getOrientation()).getY(),
                        ((Quaternion) placingOriginal.getOrientation()).getZ(),
                        ((Quaternion) placingOriginal.getOrientation()).getW()
                )
        );
        return placingCopy;
    }

    private static void updateZoneAssociations(PositionEvent posEvent, Map<String, List<ZoneDescriptor>> eFilterZoneDescriptors) {
        String agentId = DataServices.getAgentForLocalizableObject(posEvent.getLObjectId()).getId();
        for (String monitoringTaskId  : eFilterZoneDescriptors.keySet()){
            updateEfilterZoneAssociations(eFilterZoneDescriptors, agentId, monitoringTaskId);
        }

    }

    private static void updateEfilterZoneAssociations(Map<String, List<ZoneDescriptor>> eFilterZoneDescriptors, String agentId, String monitoringTaskId) {
        List<ZoneDescriptor> zoneDescriptorsEFilterResult = eFilterZoneDescriptors.get(monitoringTaskId);
        for (ZoneDescriptor zoneDescriptor : zoneDescriptorsEFilterResult){
            if (zoneDescriptor.getNotificationType().equals(PositionMonitoring.ENTRY_NOTIFICATION_TYPE)){
                LOG.info("INDFRO-DEBUG: Associating agent " + agentId + " with zone " + zoneDescriptor.getZoneId());
                associateAgentWithZone(zoneDescriptor.getZoneId(), agentId);
            }
            if (zoneDescriptor.getNotificationType().equals(PositionMonitoring.EXIT_NOTIFICATION_TYPE)){
                LOG.info("INDFRO-DEBUG: Disassociating agent " + agentId + " from zone " + zoneDescriptor.getZoneId());
                disassociateAgentFromZone(zoneDescriptor.getZoneId(), agentId);
            }
        }
    }

    /**
     * ZoneDescriptors are provided for filteringResults that originated in eventFilters
     * that rejected the event only if those ZoneDescriptors communicate an ExitNotification.
     * @param positionFilteringResults
     * @return
     */
    private static Map<String, List<ZoneDescriptor>> translateResultsIntoZoneDescriptors(List<PositionFilteringResult> positionFilteringResults) {
        Map<String, List<ZoneDescriptor>> eFilterzoneDescriptors = new HashMap<>();
        for (PositionFilteringResult positionFilteringResult : positionFilteringResults){
            String monitoringTaskId = positionFilteringResult.getMonitoringTaskId();
            List<ZoneDescriptor> zoneDescriptors = toZoneDescriptors(positionFilteringResult);
            if (!positionFilteringResult.hasBlockedEvent()) {
                eFilterzoneDescriptors.put(monitoringTaskId, zoneDescriptors);
            }else{
                if (containsExitNotification(zoneDescriptors)){
                    // send monitoring messages and persist even if the filter rejected the event, as it contains an ExitNotification for a monitored zone
                    eFilterzoneDescriptors.put(monitoringTaskId, zoneDescriptors);
                }
            }
        }
        return eFilterzoneDescriptors;
    }

    private static List<ZoneDescriptor> toZoneDescriptors(PositionFilteringResult positionFilteringResult) {
        List<ZoneDescriptor> zoneDescriptors = new LinkedList<>();
        zoneDescriptors.addAll(calcDescriptorsForMatchingZones(positionFilteringResult));
        zoneDescriptors.addAll(calcDescriptorsForNonMatchingZones(positionFilteringResult));
        return zoneDescriptors;
    }

    private static List<ZoneDescriptor> calcDescriptorsForNonMatchingZones(PositionFilteringResult positionFilteringResult) {
        List<ZoneDescriptor> zoneDescriptors = new LinkedList<>();
        for (String zoneId : positionFilteringResult.getNonMatchingPositionConditionCellIds()){
            ZoneDescriptor zoneDescriptor = modelFactory.createZoneDescriptor();
            zoneDescriptor.setZoneId(zoneId);
            if(isExitNotification(positionFilteringResult, zoneId)){
                zoneDescriptor.setNotificationType(PositionMonitoring.EXIT_NOTIFICATION_TYPE);
                zoneDescriptors.add(zoneDescriptor);
            }
        }
        return zoneDescriptors;
    }

    private static List<ZoneDescriptor> calcDescriptorsForMatchingZones(PositionFilteringResult positionFilteringResult) {
        List<ZoneDescriptor> zoneDescriptors = new LinkedList<>();
        for (String zoneId : positionFilteringResult.getMatchingPositionConditionCellIds()){
            ZoneDescriptor zoneDescriptor = modelFactory.createZoneDescriptor();
            //LOG.info("OP: zoneId: " + zoneId);
            zoneDescriptor.setZoneId(zoneId);
            if(isEntryNotification(positionFilteringResult, zoneId)) {
                zoneDescriptor.setNotificationType(PositionMonitoring.ENTRY_NOTIFICATION_TYPE);
            }else {
                zoneDescriptor.setNotificationType(PositionMonitoring.UNDEFINED_TYPE);
            }
            zoneDescriptors.add(zoneDescriptor);
        }
        return zoneDescriptors;
    }

    private static void logging_sdfindfro(PositionEvent posEvent) {
        LocalizableObject lObject = PositionMonitoring.getLObjectByIdOrNull(posEvent.getLObjectId());
        if (lObject == null){
            LOG.info("INDFRO: Warning, unknown sensorId was found. Can not log!");
            return;
        }
        String sensorType = lObject.getSensorType();
        String timestamp = posEvent.getTimeStamp();
        String sensorId = posEvent.getLObjectId();
        String agentId = lObject.getAgent().getId();
        LOG.info("SDF: Received: sensorType: " + sensorType + " timestamp: " + timestamp + " now: " + LocalDateTime.now() + " sensorId: " + sensorId + " agentId: " + agentId);
        LOG.info("INDFRO:");
        LOG.info("INDFRO: Received: sensorType: " + sensorType + "; position: " + logPosition(posEvent.getPlacing()) + "; orientation: " + logOrientation(posEvent.getPlacing()) + "; timestamp: " + timestamp + "; sensorId: " + sensorId + "; agentId: " + agentId);
    }

    public static String logPosition(Placing placing) {
        Point3D point = (Point3D) placing.getPosition().getPoint();
        return "(x: " + point.getX() + ", y: " + point.getY() + ", z: " + point.getZ() + ")";
    }

    public static String logOrientation(Placing placing) {
        Quaternion orientation = (Quaternion) placing.getOrientation();
        return "(x: " + orientation.getX() + ", y: " + orientation.getY() + ", z: " + orientation.getZ() + ", w: " + orientation.getW() + ")";
    }

    /**
     * An event-filter may hot handle a position event if the filter should respect the results of
     * sensor data fusion (e.g., wrt accuracy-criterium) and the result is negative for this event,
     * i.e., the event did not pass sensor data fusion and was rejected. If sensor-data-fusion is not
     * relevant, the eFilter should handle the event regardless of whether it was sorted out by sdf or
     * not. If it is relevant, the eFilter should block the event if it was sorted out by sdf.
     * @param posEvent
     * @param posEventMeetsSdfAccuracyCondition
     * @param eFilter
     * @return
     */
    private static boolean eFilterMayHandlePosEvent(PositionEvent posEvent, boolean posEventMeetsSdfAccuracyCondition, eventFilter eFilter) {
        boolean sdfIsRelevant = eFilter.respectsAccuracySdfForPosEvent(posEvent);
        return !sdfIsRelevant || (sdfIsRelevant && posEventMeetsSdfAccuracyCondition);
    }

    private static PositionFilteringResult applyEfilterToPosEvent(PositionEvent posEvent, eventFilter eFilter) throws ParseException {
        PositionFilteringResult positionFilteringResult = eFilter.process(posEvent);
        boolean posEventMeetsFilterConditions = !positionFilteringResult.hasBlockedEvent();
        if(posEventMeetsFilterConditions) {
            LOG.info(eFilter.getMonitoringTaskId() + "-EventFilter accepted the position");
        }
        return positionFilteringResult;
        /*
            posEvent = createAndAppendZoneDescriptors(posEvent, filteringResult);


            if (allNotificationsAreEntryNotifications(posEvent)){
                // hier feststellen ob alle matchingZones EntryNotifications sind
                entryNotifications.add(Tuples.of(posEvent, eFilter.getMonitoringTaskId(), ENTRY_NOTIFICATION_TYPE, eFilter.getSerializationType())); // note: EntryNotifications are sent after that all ExitNotifications have been sent
                // updateZoneAssociations(eFilter, posEvent);
            }else {
                SimpleSceneIntegration.receiveMessage(posEvent, eFilter.getMonitoringTaskId(), UNDEFINED_TYPE, eFilter.getSerializationType());
            }
        }
        return filteringResult;

        else if (isExitNotification(posEvent, eFilter)){
            disassociateAgentFromZone(eFilter.getFilterConditionConfig().getPositionConditionCellId(), posEvent.getLObjectId());
            SimpleSceneIntegration.receiveMessage(posEvent, eFilter.getMonitoringTaskId(), EXIT_NOTIFICATION_TYPE, eFilter.getSerializationType()); // note: ExitNotifications are sent immediately
        */
        }

    private static void updateZoneAssociation(String zoneId, PositionEvent posEvent) {
        String agentId = DataServices.getAgentForLocalizableObject(posEvent.getLObjectId()).getId();
        associateAgentWithZone(zoneId, agentId);
    }

    /**
     *     assumption: An object has been found to be located outside of a zone whose id is zoneId.
     *     This function determines whether it just exited from that zone.
     * @param positionFilteringResult
     * @param zoneId
     * @return
     */
    private static boolean isExitNotification(PositionFilteringResult positionFilteringResult, String zoneId) {
        PositionEvent posEvent = positionFilteringResult.getPosEvent();
        String agentId = DataServices.getAgentForLocalizableObject(posEvent.getLObjectId()).getId();
        if(null == zoneId) return false;
        return isLocatedInsideZone(zoneId, agentId);
    }

    /**
     * Assumption: zoneId is the id of a zone that the position (contained in filteringResult)
     * is actually located in.
     * @param positionFilteringResult
     * @param zoneId
     * @return
     */
    private static boolean isEntryNotification(PositionFilteringResult positionFilteringResult, String zoneId) {
        PositionEvent posEvent = positionFilteringResult.getPosEvent();
        String agentId = DataServices.getAgentForLocalizableObject(posEvent.getLObjectId()).getId();
        LOG.info("zoneId: " + zoneId + "; agentId: " + agentId);
        if(null == zoneId) return false;
        return !isLocatedInsideZone(zoneId, agentId); // if the agent was not inside this zone before, it has just entered the zone
    }

    private static boolean isLocatedInsideZone(String zoneId, String agentId) {
        List<String> agentsInsideZone = Optional.ofNullable(zonePopulation.get(zoneId)).orElse(new LinkedList<>());
        return agentsInsideZone.contains(agentId);
    }

    private static void associateAgentWithZone(String zoneId, String agentId) {
        if(zonePopulation.keySet().contains(zoneId)){
            if (! zonePopulation.get(zoneId).contains(agentId)){
                zonePopulation.get(zoneId).add(agentId);
            }
        }else{
            zonePopulation.put(zoneId, new LinkedList<String>(Arrays.asList(agentId)));
        }
    }

    private static void disassociateAgentFromZone(String zoneId, String agentId) {
        if(zonePopulation.keySet().contains(zoneId)){
            while(zonePopulation.get(zoneId).remove(agentId)); // remove all occurences
        }else{
            LOG.warn("AgentId cannot be removed as there are currently no AgentIds associated to the zone");
        }
    }
}

