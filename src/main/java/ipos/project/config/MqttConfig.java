package ipos.project.config;

import ipos.project.config.mqtt.MqttHandlerMap;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Timestamp;

@Configuration
public class MqttConfig {

    private final Logger LOG = LoggerFactory.getLogger(getClass());
    private final MqttHandlerMap messageHandler;

    @Autowired
    public MqttConfig(MqttHandlerMap mqttHandler) {
        this.messageHandler = mqttHandler;
    }

    @Bean
    @ConfigurationProperties(prefix = "mqtt")
    public MqttConnectOptions mqttConnectOptions() {
        return new MqttConnectOptions();
    }

    @Bean
    public MqttAsyncClient mqttClient(@Value("${mqtt.clientId}") String clientId,
                                      @Value("${mqtt.hostname}") String hostname,
                                      @Value("${mqtt.port}") int port) throws MqttException {

        LOG.info("MQTT host: tcp://" + hostname + ":" + port);

        MqttAsyncClient mqttClient = new MqttAsyncClient("tcp://" + hostname + ":" + port, clientId);

        mqttClient.setCallback(new MqttCallback() {
            public void messageArrived(String topic, MqttMessage message) {
                String time = new Timestamp(System.currentTimeMillis()).toString();
                LOG.debug("\nReceived a Message: " +
                        "\n\tTime:    " + time +
                        "\n\tTopic:   " + topic +
                        "\n\tMessage: " + new String(message.getPayload()) +
                        "\n\tQoS:     " + message.getQos() + "\n");
                messageHandler.getMessageHandler(topic).handle(message); // classes with @MqttListener annotations
            }

            public void connectionLost(Throwable cause) {
                LOG.warn("Connection to MQTT broker lost: " + cause.getMessage());
                cause.printStackTrace();
            }

            public void deliveryComplete(IMqttDeliveryToken token) {
            }

        });

        mqttClient.connect(mqttConnectOptions()).waitForCompletion();
        messageHandler.addSubscriptions(mqttClient);

        return mqttClient;
    }

}
