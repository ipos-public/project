package ipos.project.config.mqtt;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface Handler {
    void handle(MqttMessage message);
}
