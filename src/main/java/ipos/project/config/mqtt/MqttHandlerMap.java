package ipos.project.config.mqtt;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class MqttHandlerMap {

    private final ApplicationContext ctx;
    private Map<String, Handler> handlerMap;

    @Autowired
    public MqttHandlerMap(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    @PostConstruct
    public void init() {
        this.handlerMap = findMessageHandlers();
    }

    private Map<String, Handler> findMessageHandlers() {
        /**
         * Find beans with @MqttHandler annotation. One topic = One handler (TODO: one-to-many)
         * @return Map(key=Topic name, value=Handler container)
         */

        Map<String, Handler> handlerMap = new HashMap<>();
        //
        Collection<Object> containers = ctx.getBeansWithAnnotation(MqttListener.class).values();
        if (containers.size()>0) {
            for (Object container : containers) {
                String containerTopic = container.getClass().getAnnotation(MqttListener.class).value();
                handlerMap.put(containerTopic, (Handler)container);
            }
        }
        //TODO: add default handler for general messages

        return handlerMap;
    }

    public void addSubscriptions(MqttAsyncClient mqttClient) {
        if (!handlerMap.isEmpty()) {
            for (String keyTopic : handlerMap.keySet()) {
                try {
                    mqttClient.subscribe(keyTopic, 0);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public Handler getMessageHandler(String topic) {
        if (handlerMap.isEmpty()) {
            findMessageHandlers();
        }
        return handlerMap.get(topic);
    }

}
