package ipos.project.data.database;

import ipos.models.SimpleScene.IposPosition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<IposPosition, Long> { }
