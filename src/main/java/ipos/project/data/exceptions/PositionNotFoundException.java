package ipos.project.data.exceptions;

public class PositionNotFoundException extends RuntimeException {
    public PositionNotFoundException(long positionId) {
        super("Position not found: " + positionId);
    }

}
