package ipos.project.data.persistency.impl;

import ipos.project.data.database.PositionRepository;
import ipos.project.data.exceptions.PositionNotFoundException;
import ipos.project.data.persistency.PositionPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ipos.models.SimpleScene.IposPosition;

import java.util.Optional;

@Service
public class PositionPersistenceImpl implements PositionPersistence {

//    private final PositionRepository positionRepo;
//
//    @Autowired
//    public PositionPersistenceImpl(PositionRepository positionRepo) {
//        this.positionRepo = positionRepo;
//    }
//
//    @Override
//    @Transactional(readOnly = true)
//    public Page<IposPosition> findAllPosition(Pageable pageable) {
//        return positionRepo.findAll(pageable);
//    }
//
//    @Override
//    @Transactional
//    public IposPosition addPosition(IposPosition pos) {
//        return positionRepo.save(pos);
//    }
//
//    @Override
//    @Transactional(readOnly = true)
//    public Optional<IposPosition> findPositionsById(long id) {
//        return positionRepo.findById(id);
//    }
//
//    @Override
//    @Transactional
//    public void updatePosition(long id, IposPosition pos) {
//        IposPosition old_pos = positionRepo.findById(id)
//                .orElseThrow(() -> new PositionNotFoundException(id));
//        positionRepo.delete(old_pos);
//        positionRepo.save(pos);
//    }
//
//    @Override
//    @Transactional
//    public void deletePosition(long id) {
//        IposPosition pos = positionRepo.findById(id)
//                .orElseThrow(() -> new PositionNotFoundException(id));
//        positionRepo.delete(pos);
//    }

}
