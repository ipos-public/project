package ipos.project.devkit.eca;

public interface Action {

    public void execute();

}
