package ipos.project.devkit.eca;

import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;

public class CEFResult {

    PositionEvent posEvent;
    boolean posEventIsInOrder;
    boolean posEventTriggeredStateChange;

    public CEFResult(PositionEvent posEvent, boolean posEventIsInOrder, boolean posEventTriggeredStateChange) {
        this.posEvent = posEvent;
        this.posEventIsInOrder = posEventIsInOrder;
        this.posEventTriggeredStateChange = posEventTriggeredStateChange;
    }

    public PositionEvent getPosEvent() {
        return posEvent;
    }

    public boolean isPosEventIsInOrder() {
        return posEventIsInOrder;
    }

    public boolean isPosEventTriggeredStateChange() {
        return posEventTriggeredStateChange;
    }
}
