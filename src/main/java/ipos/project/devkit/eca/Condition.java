package ipos.project.devkit.eca;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;

public interface Condition {

    Action evaluate(IposPositionEvent positionEvent);

}
