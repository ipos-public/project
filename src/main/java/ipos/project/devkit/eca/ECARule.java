package ipos.project.devkit.eca;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;
import ipos.project.devkit.iposext.IPosArguments;

public abstract class ECARule {

    protected Condition condition;

    /**
     * This function implements the overall behaviour of each ECARule:
     * Determine applicability of the PositionEvent ->
     * determine whether the PositionEvent fulfills the current condition and can be accepted ->
     * translate the PositionEvent into an action and execute it.
     * Remark: If the PositionEvent can not be accepted a no-operation action is created and executed.
     * The behaviour of the ECARule at runtime depends on the implementation of the Condition- and
     * Action-interface
     * @param posEvent
     */
    public void apply(IposPositionEvent posEvent){
        if (isApplicable(posEvent)){
            Action action = condition.evaluate(posEvent);
            action.execute();
        }
    }

    public abstract boolean isApplicable(IposPositionEvent posEvent);

    public abstract void configure(IPosArguments.IPosEcaConfig ecaConfig);
}
