package ipos.project.devkit.eca;

import org.apache.logging.log4j.LogManager;

public class NoOpAction implements Action{
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    String message = "";
    public NoOpAction() {
    }

    public NoOpAction(String message){
        this.message = message;
    }

    @Override
    public void execute() {
        LOG.info(this.message);
    }
}
