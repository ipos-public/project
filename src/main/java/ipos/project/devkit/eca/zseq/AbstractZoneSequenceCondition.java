package ipos.project.devkit.eca.zseq;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;
import ipos.project.devkit.eca.Action;
import ipos.project.devkit.eca.Condition;
import ipos.project.devkit.eca.NoOpAction;
import org.apache.logging.log4j.LogManager;

public abstract class AbstractZoneSequenceCondition implements Condition {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    protected ZoneSequenceStage currentStage;

    @Override
    public Action evaluate(IposPositionEvent positionEvent) {
        if (positionEvent.getZoneDescriptors().size() != 1){
            return new NoOpAction("OP: Error: Positionevent does not contain a single zone descriptor.");
        }
        // assumption: positionEvent is located in exactly one zone (checked in ZoneSequenceRule.isApplicable())
        ZoneDescriptor zoneDescriptor = positionEvent.getZoneDescriptors().get(0);
        if (causesStageChange(zoneDescriptor)){
            logStageChange();
            StageChangeAction scAction = evaluateStageChangeIntoAction(zoneDescriptor);
            setCurrentStage(currentStage.getNextStage());
            return scAction;
        }else{
            updateCurrentQuantity();
            return new NoOpAction(createNoStageChangeLogMsg());
        }
    }

    private String createNoStageChangeLogMsg() {
        String currentZoneId = currentStage.getEventDescriptors().get(0).getZoneId();
        int currentQuantity = currentStage.getEventDescriptors().get(0).getQuantity();
        String logMsg = "OP: Event did not cause a stage change. Current stage expects zone " + currentZoneId + ". This was the " + currentQuantity + " event that did not cause a stage change.";
        return logMsg;
    }

    private void logStageChange() {
        String currentZone = currentStage.getEventDescriptors().get(0).getZoneId();
        String nextZone = currentStage.getNextStage().getEventDescriptors().get(0).getZoneId();
        LOG.info("OP: Event causes a stage change. Zone of current stage: " + currentZone + ". Zone of next stage: " + nextZone);
    }

    private void updateCurrentQuantity() {
        int currentQuantity = currentStage.getEventDescriptors().get(0).getQuantity();
        currentStage.getEventDescriptors().get(0).setQuantity(currentQuantity + 1);
    }

    public abstract StageChangeAction evaluateStageChangeIntoAction(ZoneDescriptor zoneDescriptor);

    public abstract boolean causesStageChange(ZoneDescriptor zoneDescriptor);

    public ZoneSequenceStage getCurrentStage() {
        return currentStage;
    }
    public void setCurrentStage(ZoneSequenceStage nextStage) {
        this.currentStage = nextStage;
    }

}
