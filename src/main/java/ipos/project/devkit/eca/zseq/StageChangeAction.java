package ipos.project.devkit.eca.zseq;

import ipos.project.devkit.eca.Action;

public abstract class StageChangeAction implements Action {
    @Override
    public abstract void execute();
}
