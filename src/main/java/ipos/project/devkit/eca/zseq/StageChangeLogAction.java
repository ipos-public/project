package ipos.project.devkit.eca.zseq;

import org.apache.logging.log4j.LogManager;

public class StageChangeLogAction extends StageChangeAction{
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private String currentZoneId;
    private String nextZoneId;

    public StageChangeLogAction(String currentZoneId, String nextZoneId) {
        this.currentZoneId = currentZoneId;
        this.nextZoneId = nextZoneId;
    }

    @Override
    public void execute() {
        LOG.info("Stage change occured: " + currentZoneId + " -> " + nextZoneId);
    }

    public String getCurrentZoneId() {
        return currentZoneId;
    }

    public String getNextZoneId() {
        return nextZoneId;
    }
}
