package ipos.project.devkit.eca.zseq;

import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;
import ipos.project.devkit.eca.Condition;
import ipos.project.devkit.iposext.IPosArguments;

import java.util.List;

public class ZoneSequenceArgs {

    public static class ZoneSequenceConfigure implements IPosArguments.IPosEcaConfig{
        private List<ZoneDescriptor> zoneDescriptors;
        private Condition condition;

        public ZoneSequenceConfigure(List<ZoneDescriptor> zoneDescriptors, Condition condition) {
            this.zoneDescriptors = zoneDescriptors;
            this.condition = condition;
        }

        public List<ZoneDescriptor> getZoneDescriptors() {
            return zoneDescriptors;
        }
        public Condition getCondition() {
            return condition;
        }

    }

}
