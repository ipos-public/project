package ipos.project.devkit.eca.zseq;

import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;
import ipos.project.devkit.eca.NoOpAction;
import org.apache.logging.log4j.LogManager;

public class ZoneSequenceCondition extends AbstractZoneSequenceCondition{
    String recentZoneId = "";

    @Override
    public String toString() {
        String strRepresentation = "";
        ZoneSequenceStage firstStage = this.getCurrentStage();
        ZoneSequenceStage currentStage = firstStage;
        while(currentStage != null) {
            String currentZoneId = currentStage.getEventDescriptors().get(0).getZoneId();
            strRepresentation += " -> " + currentZoneId;
            currentStage = currentStage.getNextStage();
        }
        return "expected zone sequence: " + strRepresentation.substring(4);
    }

    @Override
    public StageChangeAction evaluateStageChangeIntoAction(ZoneDescriptor zoneDescriptor) {
        String currentZoneId = getCurrentStage().getEventDescriptors().get(0).getZoneId();
        String nextZoneId = getCurrentStage().getNextStage().getEventDescriptors().get(0).getZoneId();
        return new StageChangeLogAction(currentZoneId, nextZoneId);
    }

    @Override
    public boolean causesStageChange(ZoneDescriptor zoneDescriptor) {
        boolean zoneChange = eventsZoneHasChanged(zoneDescriptor); // there is no stage-change if an out-of-order event is repeated
        if (zoneChange){
            this.recentZoneId = zoneDescriptor.getZoneId();
        }
        return zoneChange;
/*
        String eventZoneId = zoneDescriptor.getZoneId();
        String currentStageZoneId = currentStage.getEventDescriptors().get(0).getZoneId(); // Function ZoneSequenceRule::transformIntoStage ensures that there is at most one EventDescriptor contained in a stage
        return !eventZoneId.equals(currentStageZoneId);

 */
    }

    private boolean eventsZoneHasChanged(ZoneDescriptor zoneDescriptor) {
        String eventZoneId = zoneDescriptor.getZoneId();
        return !eventZoneId.equals(this.recentZoneId);
    }
}
