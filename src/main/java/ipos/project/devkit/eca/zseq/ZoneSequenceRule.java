package ipos.project.devkit.eca.zseq;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;
import ipos.project.devkit.eca.ECARule;
import ipos.project.devkit.iposext.IPosArguments;

import java.util.List;
import java.util.stream.Collectors;

public abstract class ZoneSequenceRule extends ECARule {

    protected ZoneSequenceCondition zsCondition;

    @Override
    public boolean isApplicable(IposPositionEvent posEvent) {
        List<ZoneDescriptor> zones = posEvent.getZoneDescriptors();
        return zones.size() == 1;
    }

    @Override
    public void configure(IPosArguments.IPosEcaConfig iposEcaConfig) {
        ZoneSequenceArgs.ZoneSequenceConfigure zsConfigure = (ZoneSequenceArgs.ZoneSequenceConfigure) iposEcaConfig;
        configure(zsConfigure);
    }

        /**
         * Creates a sequence of ZoneSequenceStages for a list of zoneDescriptors.
         * Each ZoneSequenceStage encapsulates a single zone with a quantity. The sequence
         * encodes a pattern for a sequence of positionEvents. The pattern is met if the
         * sequence of positionEvents and their zoneAssociations matches the sequence of
         * ZoneSequenceStages and their respective zone associations.
         * @param zsConfigure
         */
    public void configure(ZoneSequenceArgs.ZoneSequenceConfigure zsConfigure){
        List<ZoneDescriptor> zoneDescriptors = zsConfigure.getZoneDescriptors();
        // this.condition = new ZoneSequenceCondition();
        zsCondition = (ZoneSequenceCondition) zsConfigure.getCondition();
        List<ZoneSequenceStage> zsStages = mapZoneDescriptorsToStages(zoneDescriptors);
        linkSuccessiveStages(zsStages);
        initializeCondition(zsStages);
        /*
        ZoneSequenceStage initialStage = new ZoneSequenceStage();
        ZoneSequenceStage loopStage = initialStage;
        for (ZoneDescriptor zoneDescriptor : zoneDescriptors){
            loopStage.addEventDescriptor(zoneDescriptor.getZoneId(), 0);
            ZoneSequenceStage nextLoopStage = new ZoneSequenceStage();
            loopStage.setNextStage(nextLoopStage);
            loopStage = nextLoopStage;
        }
        zsCondition.setCurrentStage(initialStage);
*/
    }

    private void initializeCondition(List<ZoneSequenceStage> zsStages) {
        ZoneSequenceStage initialStage = new ZoneSequenceStage();
        initialStage.addEventDescriptor("Start", 0);
        initialStage.setNextStage(zsStages.get(0));
        zsCondition.setCurrentStage(initialStage);
        this.condition = zsCondition;
    }

    private void linkSuccessiveStages(List<ZoneSequenceStage> zsStages) {
        for (int i = 0; i < zsStages.size()-1 ; i++){
            ZoneSequenceStage stage = zsStages.get(i);
            ZoneSequenceStage successor = zsStages.get(i+1);
            stage.setNextStage(successor);
        }
    }

    private List<ZoneSequenceStage> mapZoneDescriptorsToStages(List<ZoneDescriptor> zoneDescriptors) {
        List<ZoneSequenceStage> zsStages = zoneDescriptors.stream()
                .map(ZoneSequenceRule::transformIntoStage)
                .collect(Collectors.toList());
        return zsStages;
    }

    private static ZoneSequenceStage transformIntoStage(ZoneDescriptor zoneDescriptor){
        ZoneSequenceStage zsStage = new ZoneSequenceStage();
        zsStage.addEventDescriptor(zoneDescriptor.getZoneId(), 0);
        return zsStage;
    }

}
