package ipos.project.devkit.eca.zseq;

import ipos.project.DataModellntegration.iPos_Datamodel.PositionEvent;

import java.util.LinkedList;
import java.util.List;

public class ZoneSequenceStage {

    ZoneSequenceStage nextStage;
    List<EventDescriptor> eventDescriptors = new LinkedList<>();

    public List<EventDescriptor> getEventDescriptors() {
        return eventDescriptors;
    }

    public void addEventDescriptor(String zoneId, int quantity) {
        this.eventDescriptors.add(new EventDescriptor(zoneId, quantity));
    }

    public ZoneSequenceStage getNextStage() {
        return nextStage;
    }

    public void setNextStage(ZoneSequenceStage nextStage) {
        this.nextStage = nextStage;
    }


    public class EventDescriptor {
        String zoneId;
        int quantity;

        public EventDescriptor(String zoneId, int quantity) {
            this.zoneId = zoneId;
            this.quantity = quantity;
        }


        public String getZoneId() {
            return zoneId;
        }

        public void setZoneId(String zoneId) {
            this.zoneId = zoneId;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

    }

}
