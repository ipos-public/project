package ipos.project.devkit.iposext;

import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent;
import ipos.project.UseCaseController.PositionMonitoring;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;

import java.util.Objects;

abstract public class IPosExtension {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    public ExternalPubServiceImpl mqttService;

    public void setMqttService(ExternalPubServiceImpl mqttService_){
        mqttService = mqttService_;
    }

    public void configureIpos(IPosArguments.IPosConfigData iposConfigData) {
        try{
            SimpleScene.IposConfigWrapper configWrapper = Objects.requireNonNull(prepareConfigWrapper(iposConfigData), "Warning: An attempt to configure the IPos-IPS has failed");
            sendConfigWrapperToIpos(configWrapper);
        } catch(NullPointerException e){
            LOG.info(e.getMessage());
            return;
        }
    }

    private void sendConfigWrapperToIpos(SimpleScene.IposConfigWrapper proto_configWrapper) {
        String json_configWrapper = ProtoJsonMap.toJson(proto_configWrapper);
        if (mqttService == null){
            throw new RuntimeException("Could not send configuration data to IPos-Framework as MQTT-service was not properly initialized.");
        }
        mqttService.publish(PositionMonitoring.TOPIC_ConfigWrapper, json_configWrapper, 0, false);
    }

    public abstract void handlePositionEvent(IposPositionEvent posEvent);
    public abstract void handlePositionEvent(IposPositionEvent posEvent, IPosArguments.HandlePosEventConf posEventConf);

    public abstract void handleRawdataEvent(IPosRawdataEvent iPosRawdataEvent);

    public abstract SimpleScene.IposConfigWrapper prepareConfigWrapper(IPosArguments.IPosConfigData iposConfigData);

    /*
    private void useTransformer(){
        ...
        IPosTransformer.MonReqTransformer<SimpleScene.IposMonitoringRequest> monReq2protoTransformer = new IPos2protoTransformer.MonReq2ProtoTransformer();
        //monReq2protoTransformer.transformMonReq();
    }
    */
}
