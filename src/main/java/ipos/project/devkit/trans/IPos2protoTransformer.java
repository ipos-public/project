package ipos.project.devkit.trans;

import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.*;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;
import ipos.project.SensorValueIntegration.Service.GenericSensorTransformer;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class IPos2protoTransformer implements IPosTransformer {

    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;
    private static IPosDevKitFactory devKitFactory = IPosDevKitFactory.eINSTANCE;

    public static ReferenceSystem createReferenceSystem(String name, String id, Placing origin){
        ReferenceSystem refSystem = modelFactory.createReferenceSystem();
        refSystem.setName(name);
        refSystem.setId(id);
        refSystem.setOrigin(origin);
        return refSystem;
    }

    public static Position createPosition(Point3D point3D, Float accuracy, ReferenceSystem refSystem){
        Position position = modelFactory.createPosition();
        position.setPoint(point3D);
        Gaussian accrcy = modelFactory.createGaussian();
        accrcy.setConfidenceInterval(accuracy);
        position.setAccuracy(accrcy);
        position.setReferenceSystem(refSystem);
        return position;
    }

    public static Point3D createPoint3D(float x, float y, float z){
        Point3D point3D = modelFactory.createPoint3D();
        point3D.setX(x);
        point3D.setY(y);
        point3D.setZ(z);
        return point3D;
    }

    public static Quaternion createOrientation(float x, float y, float z, float w){
        Quaternion quaternion = modelFactory.createQuaternion();
        quaternion.setX(x);
        quaternion.setY(y);
        quaternion.setZ(z);
        quaternion.setW(w);
        return quaternion;
    }

    public static Placing createPlacing(Position position, Orientation orientation){
        Placing placing = modelFactory.createPlacing();
        placing.setPosition(position);
        placing.setOrientation(orientation);
        return placing;
    }

    public static class MonReq2ProtoTransformer implements IPosTransformer.MonReqTransformer<SimpleScene.IposMonitoringRequest>{

        @Override
        public SimpleScene.IposMonitoringRequest transformMonReq(MonitoringRequest monitoringRequest) {
            return null;
        }
    }

    public static List<IposPositionEvent> transformToInternal(SimpleScene.IposPositionEvent proto_iposPositionEvent){
        List<IposPositionEvent> iposPositionEvents = proto_iposPositionEvent.getObjectsList().stream()
                .map(IPos2protoTransformer::transformProtoObjectToInternalPosEvent)
                .collect(Collectors.toList());
        return iposPositionEvents;
    }

    public static ZoneDescriptor transformToInternal(SimpleScene.IposZoneDescriptor proto_zoneDescriptor){
        ZoneDescriptor zoneDescriptor = modelFactory.createZoneDescriptor();
        zoneDescriptor.setZoneId(proto_zoneDescriptor.getZoneId());
        zoneDescriptor.setNotificationType(proto_zoneDescriptor.getNotificationType());
        return zoneDescriptor;
    }

    private static IposPositionEvent transformProtoObjectToInternalPosEvent(SimpleScene.IposObject proto_object) {
        IposPositionEvent iposPositionEvent = devKitFactory.createIposPositionEvent();
        iposPositionEvent.setAgentId(proto_object.getId());
        iposPositionEvent.setSensorId(proto_object.getSensorId());
        iposPositionEvent.setType(proto_object.getType());
        iposPositionEvent.setSensorType(proto_object.getSensorType());
        iposPositionEvent.setPosition(
                createPosition(
                        IPos2protoTransformer.createPoint3D(
                                proto_object.getPosition().getPoint().getX(),
                                proto_object.getPosition().getPoint().getY(),
                                proto_object.getPosition().getPoint().getZ()
                        ),
                        proto_object.getPosition().getAccuracy(),
                        IPos2protoTransformer.createReferenceSystem("ROOT", "ROOT", null)

                )
        );
        iposPositionEvent.setOrientation(
                IPos2protoTransformer.createOrientation(
                        proto_object.getOrientation().getX(),
                        proto_object.getOrientation().getY(),
                        proto_object.getOrientation().getZ(),
                        proto_object.getOrientation().getW()
                )
        );
        iposPositionEvent.setLastPosUpdate(proto_object.getLastPosUpdate());
        List<ZoneDescriptor> zoneDescriptors = proto_object.getZoneDescriptorsList().stream()
                .map(IPos2protoTransformer::transformToInternal)
                .collect(Collectors.toList());
        iposPositionEvent.getZoneDescriptors().addAll(zoneDescriptors);
        return iposPositionEvent;
    }

}
