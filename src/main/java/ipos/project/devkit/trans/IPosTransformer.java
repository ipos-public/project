package ipos.project.devkit.trans;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.MonitoringRequest;

public interface IPosTransformer {

    interface MonReqTransformer<K> {
        K transformMonReq(MonitoringRequest monitoringRequest);
    }

}
