package ipos.project.devkit.utility;

import com.google.protobuf.AbstractMessageLite;
import ipos.project.DataModellntegration.SimpleSceneIntegration.service.ExternalPubService;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class ExternalPubServiceImpl implements ExternalPubService {

    private final Logger LOG = LoggerFactory.getLogger(getClass());
    private final MqttAsyncClient mqttClient;

    @Autowired
    public ExternalPubServiceImpl(MqttAsyncClient mqttClient) {
        this.mqttClient = mqttClient;
    }

    public void publish(final String topic, final String msg , int qos, boolean retained) {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setPayload(msg.getBytes());
        mqttMessage.setQos(qos);
        mqttMessage.setRetained(retained);
        publish(topic, mqttMessage);
    }

    public void publish(final String topic, final MqttMessage mqttMessage){
        try {
            mqttClient.publish(topic, mqttMessage);
        } catch (MqttException e) {
            LOG.error("MQTT-message could not be published");
            e.printStackTrace();
        }
    }

    @Override
    public MqttMessage createMqttMsg(AbstractMessageLite protoMessage, int qos, boolean retained) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            protoMessage.writeTo(baos);
        } catch (IOException e) {
            LOG.error("Protobuf-message could not be serialized");
            e.printStackTrace();
        }
        MqttMessage mqttMessage = new MqttMessage(baos.toByteArray());
        mqttMessage.setQos(qos);
        mqttMessage.setRetained(retained);
        return mqttMessage;
    }
}
