package ipos.project.devkit.utility;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.MessageOrBuilder;
import ipos.project.DataModellntegration.SimpleSceneIntegration.SimpleSceneIntegration;
import ipos.project.MainApp;
import ipos.project.SensorValueIntegration.GenericSensorValueProcessor;
import ipos.project.UseCaseController.PositionMonitoring;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OtherUtility {
    public static final String KEY_EXTRACTED_ATTRIBUTE_NAME = "name";
    public static final String KEY_EXTRACTED_ATTRIBUTE_TYPE = "type";
    public static final String KEY_EXTRACTED_ATTRIBUTE_VALUE = "data";
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    public static List<String> readLines(File initFile) {
        List<String> lines = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(initFile))) { // br is automatically closed at the end of try-scope
            for (String line; (line = br.readLine()) != null; ){
                lines.add(line);
            }
        } catch (IOException e) {
            LOG.error("Error while reading the text file: ");
            e.printStackTrace();
        }
        return lines;
    }

    public static void publishRespectingSerializationType(String topic, String serializationType, AbstractMessageLite protoMsg){
        MqttMessage mqttMessage = SimpleSceneIntegration.mqttService.createMqttMsg(protoMsg, 0, false);
        String jsonString = ProtoJsonMap.toJson((MessageOrBuilder) protoMsg);

        // publishes protobuf over MQTT
        if (PositionMonitoring.PROTOBUF_SERIALIZATION_TYPE.equals(serializationType)){
            SimpleSceneIntegration.mqttService.publish(topic, mqttMessage);
            LOG.info("publishing protobuf mqttMessage of type " + protoMsg.getClass().getName() + ". JSON-representation:" + jsonString + " on topic: " + topic);
        }

        // publishes JSON over MQTT
        if (PositionMonitoring.JSON_SERIALIZATION_TYPE.equals(serializationType)){
            SimpleSceneIntegration.mqttService.publish(topic, jsonString, 0, false);
            LOG.info("publishing JSON mqttMessage of type " + protoMsg.getClass().getName() + ":" + jsonString + " on topic: " + topic);
        }
    }

    public static void waitUntilUserRequestsReadingNextLine(Scanner scanner) {
        while (!MainApp.READY_TO_READ_NEXT_LINE){
            try {
                Thread.sleep(500);
                if(scanner.hasNext()) {
                    String str = scanner.nextLine();
                    switch(str){
                        case MainApp.COMMAND_READY_FOR_NEXT_LINE: MainApp.READY_TO_READ_NEXT_LINE = true; break;
                        default: LOG.info("Waiting for permission to read next line");
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        MainApp.READY_TO_READ_NEXT_LINE = false; // make sure that we will also wait before reading the line that comes after the current one
    }
}
