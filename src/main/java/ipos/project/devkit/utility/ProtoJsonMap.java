package ipos.project.devkit.utility;

import java.lang.reflect.InvocationTargetException;
import com.google.protobuf.AbstractMessage.Builder;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.util.JsonFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * https://stackoverflow.com/a/57216530/5126658
 */
public class ProtoJsonMap {

    private static final Logger LOG = LoggerFactory.getLogger(ProtoJsonMap.class);

    public static String toJson(MessageOrBuilder messageOrBuilder) {
        try {
            return JsonFormat.printer().print(messageOrBuilder);
        } catch (InvalidProtocolBufferException e) {
            LOG.error("JSON converting error: ", e);
            return null;
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <T extends Message> T fromJson(String json, Class<T> clazz) throws InvalidProtocolBufferException {
        try {
            Builder builder = (Builder) clazz.getMethod("newBuilder").invoke(null);
            JsonFormat.parser().ignoringUnknownFields().merge(json, builder);
            return (T) builder.build();
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            LOG.error("JSON converting error: ", e);
            return null;
        }
    }
}
