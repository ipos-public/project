package ipos.project.entity;

import ipos.project.DataModellntegration.iPos_Datamodel.*;

import java.util.List;

public interface IWorldModelAccess {

    /**
     * Shall return empty list there are currently no objects of the respective kind stored in the world model
     * @return
     */
    public List<Agent> getAllAgents();

    public List<ReferenceSystem> getAllRefSystems();

    void removeAgent(String id);

    void addAgent(Agent agent_merged);

    List<Zone> getAllZones();

    void addZone(Zone zone_input);

    void addRefSystem(ReferenceSystem refSystem_input);

    List<POI> getAllPois();

    void addPoi(POI poi_input);
}
