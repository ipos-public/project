package ipos.project.entity;

import ipos.project.DataModellntegration.iPos_Datamodel.*;

import java.util.List;

public class LocalObjectWorldModel implements IWorldModelAccess {

    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;
    WorldModel wm;

    public LocalObjectWorldModel(){
        wm = modelFactory.createWorldModel();
        wm.getZoneMap().add(modelFactory.createZoneMap()); // zonemaps are currently not supported
    }

    @Override
    public List<Agent> getAllAgents() {
        return wm.getAgent();
    }

    @Override
    public List<ReferenceSystem> getAllRefSystems() {
        return wm.getReferenceSystem();
    }

    @Override
    public void removeAgent(String id) {
        for (Agent agent : wm.getAgent()){
            if(agent.getId().equals(id)){
                wm.getAgent().remove(agent);
                return;
            }
        }
    }

    /**
     * Assumption: The calling code made sure that there are currently no agents
     * with the same id contained in the world model.
     * @param agent
     */
    @Override
    public void addAgent(Agent agent) {
        wm.getAgent().add(agent);
    }

    @Override
    public List<Zone> getAllZones() {
        return wm.getZoneMap().get(0).getZone();
    }

    /**
     * Assumption: The calling code made sure that there are currently no zones
     * with the same id contained in the world model.
     * @param zone
     */
    @Override
    public void addZone(Zone zone) {
        wm.getZoneMap().get(0).getZone().add(zone);
    }

    /**
     * Assumption: The calling code made sure that there are currently no refSystems
     * with the same id contained in the world model.
     * @param refSystem
     */
    @Override
    public void addRefSystem(ReferenceSystem refSystem) {
        wm.getReferenceSystem().add(refSystem);
    }

    @Override
    public List<POI> getAllPois() {
        return wm.getPois();
    }

    /**
     * Assumption: The calling code made sure that there are currently no pois
     * with the same id contained in the world model.
     * @param poi
     */
    @Override
    public void addPoi(POI poi) {
        wm.getPois().add(poi);
    }
}
