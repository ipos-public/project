package ipos.project.iposextension.orderpicker;

import ipos.models.OFBizOrderpicker;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.*;
import ipos.project.devkit.iposext.IPosArguments;
import ipos.project.devkit.iposext.IPosExtension;
import ipos.project.devkit.utility.OtherUtility;
import ipos.project.devkit.utility.ProtoJsonMap;
import ipos.project.iposextension.orderpicker.frontend.FETable;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * This class coordinates all activities required for verifying that the picker
 * picks all items in the correct order, i.e., in the order defined in the picklist.
 * An extension of the IPos-FW, that was specifically created for this usecase, is employed.
 * For each received picklist a ZoneSequenceRecognition-ComplexEventFilter is created.
 * This filter is part of this IPos-FW and is used for the verification. Also, the
 * frontend application is initialized and updated to allow the operator to track the
 * progress of the picker and detect errors.
 *
 * The picklist conforms to the datamodel defined in the open-source ERP-system OFBiz.
 */
@Component
public class OFBizOrderPicker {
    public static final String OP_MONITORINGREQUEST = "{\"monitoringRequests\": [{\"frameIds\": [\"box_1\", \"box_2\", \"box_3\", \"shipmentBin_1\", \"shipmentBin_2\"], \"monitoringTaskId\": \"" + OFBizOrderPicker.TOPIC_POSITION_EVENT + "\", \"serializationType\": \"json\"}]}";
    public static final String OP_PICKER_ROLE_TYPE = "PICKER";
    public static final String TOPIC_POSITION_EVENT = "RobolabMonitoringOPExtension";
    public static final String TOPIC_FRONTEND = "ipos/client/tableWrapper";
    public static final String INVENTORYITEM_TYPE_NAME = "inventory";
    public static final String SHIPMENTBIN_TYPE_NAME = "shipment";
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    static IPosExtension ofbizOpIposExt = new OFBizOrderPickerExt();

    public static ExternalPubServiceImpl mqttService;

    @Autowired
    public OFBizOrderPicker(ExternalPubServiceImpl mqttService_){
        mqttService = mqttService_;
        ofbizOpIposExt.setMqttService(mqttService);
    }

    /**
     * The OFBizOrderPicker application is an extension of the IPos-FW. Therefore, initialization relies
     * on the function "configureIpos", which is provided by the abstract class IPosExtension. Each application-specific
     * extension of the IPos-FW, i.e., also the OrderPicker extension, has to derive a concrete class from that class.
     * The OrderPicker extension derives the class ipos.project.iposextension.orderpicker.OFBizOrderPickerExt
     */
    public static void initialize(){
        try {
            ofbizOpIposExt.configureIpos(new IPosArguments.IPosConfigData() {
            }); // orderpicker-extension has a prepareConfigWrapper-function that does not need any arguments. We are interested in PositionEvents for all available vessels
        }catch(RuntimeException e){
            LOG.info("OP: OFBizOrderPicker-Extension of the IPos-FW could not be initialized. Exception-msg: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /** Function transforms the received picklist into a data format that can be handled
     * by the frontend-application (FETable) and publishes the picklist to the
     * frontend-application. Moreover, a filter (ECARule) is set up for accepting the
     * PositionEvents that are sent from the IPos-FW. The information that is relevant
     * for the filter is the zone that the position belongs to.
     *
     * @param picklist
     */
   public static void handlePicklist(Picklist picklist){
        try {
            FETable feTable = OFBizOrderPickerTrans.transformPicklistIntoFeTable(picklist);
            publishFeTableToFeApp(feTable);
            ((OFBizOrderPickerExt) ofbizOpIposExt).setupECARule(picklist);
        }catch (RuntimeException e){
            LOG.info("OP: Picklist could not be handled: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static void publishFeTableToFeApp(FETable feTable) {
        OFBizOrderpicker.FEndWrapper proto_fendWrapper = OFBizOrderPickerTrans.transform2proto(feTable);
        String jsonString = ProtoJsonMap.toJson(proto_fendWrapper);
        mqttService.publish(OFBizOrderPicker.TOPIC_FRONTEND, jsonString, 0, false);
        LOG.info("OP: publishing JSON mqttMessage:" + jsonString + " on topic: " + OFBizOrderPicker.TOPIC_FRONTEND);
    }

    public static void processPicklistTestData(String path_to_test_data_file){
        File testDataFile = new File(path_to_test_data_file);
        for (String line : OtherUtility.readLines(testDataFile)) {
            OFBizOrderPickerMqtt.handleJsonOpWrapper(line);
          }
    }

    public static void processPosEvtTestData(String path_to_test_data_file){
        File testDataFile = new File(path_to_test_data_file);
        for (String line : OtherUtility.readLines(testDataFile)) {
            OFBizOrderPickerMqtt.handleIPosMonitoringWrapper(line);
        }
    }
}
