package ipos.project.iposextension.orderpicker;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist;
import ipos.project.DataModellntegration.iPos_Datamodel.Point3D;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;
import ipos.project.devkit.eca.zseq.ZoneSequenceArgs;
import ipos.project.devkit.iposext.IPosArguments;
import ipos.project.devkit.iposext.IPosExtension;
import ipos.project.devkit.utility.ProtoJsonMap;
import ipos.project.iposextension.orderpicker.eca.SeqPicklistCondition;
import ipos.project.iposextension.orderpicker.eca.SeqPicklistRule;
import org.apache.logging.log4j.LogManager;
import org.eclipse.emf.common.util.EList;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Extension of the IPos-FW specific to the usecase "OrderPicker".
 */
public class OFBizOrderPickerExt extends IPosExtension {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    List<SeqPicklistRule> spRules = new LinkedList<SeqPicklistRule>(); // One ComplexEventFilter is responsible for one picklist

    @Override
    public void handlePositionEvent(IposPositionEvent posEvent) {
        IPosArguments.HandlePosEventConf dummyArgument = new IPosArguments.HandlePosEventConf() {};
        handlePositionEvent(posEvent, dummyArgument);
    }

    private String extractZoneIdsAsString(EList<ZoneDescriptor> zoneDescriptors) {
        String zoneIds = "";
        for (ZoneDescriptor zoneDescriptor : zoneDescriptors){
            zoneIds += zoneDescriptor.getZoneId() + ", ";
        }
        return zoneIds.substring(0, zoneIds.length()-2);
    }

    /**
     * Function runs through all registered picklists (i.e., all ECARules that have been created) and
     * applies the received PositionEvent to it. It is not the responsibility of this function to determine
     * whether a given PositionEvent is applicable to a given ECARule. This decision is determined by the ECARule.
     * For handling a PositionEvent the classes SeqPicklistRule, SeqPicklistRule, SeqPicklistCondition, and UpdateFrontendAction are relevant.
     *
     * @param posEvent
     * @param _posEvtConf
     */
    @Override
    public void handlePositionEvent(IposPositionEvent posEvent, IPosArguments.HandlePosEventConf _posEvtConf) {
        LOG.info("OP:");
        LOG.info("OP:----OFBizOrderPickerExt--------------------------");
        LOG.info("OP: Received IposPositionEvent for agent " + posEvent.getAgentId() + ". Zones: " + extractZoneIdsAsString(posEvent.getZoneDescriptors()) + "; position: " + logPoint((Point3D) posEvent.getPosition().getPoint()));
        try {
            spRules.forEach(spRule -> spRule.apply(posEvent));
            /* above: lambda-expression, shortform of:

            spRules.forEach(new Consumer<SeqPicklistRule>(){
                @Override
                public void accept(SeqPicklistRule spRule){
                    spRule.apply(posEvent);
                }
            });
            */
        }catch (RuntimeException e){
            LOG.info("OP: PositionEvent could not be handled: " + e.getMessage());
            return;
        }
    }

    @Override
    public void handleRawdataEvent(IPosRawdataEvent iPosRawdataEvent) {}

    private String logPoint(Point3D point) {
        return "(x: " + point.getX() + ", y: " + point.getY() + ", z: " + point.getZ() + ")";
    }

    /*
     private void handleCefResult(String picklistId, CEFResult cefResult) {
         if(cefResult.isPosEventTriggeredStateChange()){
             List<ZoneDescriptor> zoneDescriptors = cefResult.getPosEvent().getZonedescriptors();
             String vesselId = zoneDescriptors.get(0).getZoneId();
             ofbizOPApp.onPickerStateChange(picklistId, vesselId, cefResult.isPosEventIsInOrder());
         }
     }


     private SeqPicklistRule getEcaRule(String picklistId) {
         ECARule cef = getEcaRuleByPicklistId(picklistId);
         if (! (cef instanceof SeqPicklistRule)){
             throw new RuntimeException("Received argument of unexpeted type: " + cef.getClass() + " Expected was: " + ZoneSequenceRule.class);
         }
         return (SeqPicklistRule) cef;
     }
 */
    /*
    private String readPicklistId(IPosArguments.HandlePosEventConf _posEvtConf) {
        if (! (_posEvtConf instanceof OFBizOrderPickerArgs.OFBizOPPosEvtConf)){
            throw new RuntimeException("Received argument of unexpeted type: " + _posEvtConf.getClass() + " Expected was: " + OFBizOrderPickerArgs.OFBizOPPosEvtConf.class);
        }
        OFBizOrderPickerArgs.OFBizOPPosEvtConf posEvtConf = (OFBizOrderPickerArgs.OFBizOPPosEvtConf) _posEvtConf;
        String picklistId = posEvtConf.getPicklistId();
        return picklistId;
    }
*/

    /**
     * Functions prepares a MonitoringRequest that causes the IPos-FW to update the OrderPicker extension with all positions
     * relevant for the OrderPicker-usecase.
     * @param iposConfigData
     * @return
     */
    @Override
    public SimpleScene.IposConfigWrapper prepareConfigWrapper(IPosArguments.IPosConfigData iposConfigData) {
            try {
                return ProtoJsonMap.fromJson(OFBizOrderPicker.OP_MONITORINGREQUEST, SimpleScene.IposConfigWrapper.class);
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException("Warning: Monitoring-request could not be parsed (JSON) and translated into Protobuf-data structures: ");
            }
        }

    /**
     * Function creates an ECARule that is able to detect whether PositionEvents have been received in
     * an order that conforms to the order that is specified by the Picklist (see testdata-file testdata_raw_orderpicker_readable.txt).
     * The picklist specifies picklistBins and InventoryItems. InventoryItems have to be placed in the picklistBins they belong to.
     * Picklistbins have to be handled in the order of their occurence in the textfile. InventoryItems have to be handled
     * in the order of their occurence in their picklistbin.
      * @param picklist
     */
    public void setupECARule(Picklist picklist){
        String pickerId = extractPickerId(picklist).orElseThrow(() -> new RuntimeException("Picklist did not contain the ID of the picker who is responsible for it"));
        SeqPicklistRule spRule = new SeqPicklistRule(picklist.getPicklistId(), pickerId); // complex event filter for verifying the picker
        List<ZoneDescriptor> zoneDescriptors = OFBizOrderPickerTrans.picklistToZoneDescriptors(picklist);
        ZoneSequenceArgs.ZoneSequenceConfigure zsConfigure = new ZoneSequenceArgs.ZoneSequenceConfigure(zoneDescriptors, new SeqPicklistCondition());
        spRule.configure(zsConfigure);
        LOG.info("OP: SeqPicklistRule has been created: \n" + spRule.toString());
        spRules.add(spRule);
    }

    private Optional<String> extractPickerId(Picklist picklist) {
        if (picklist.getPicklistRoles().size() == 0){
            return Optional.empty();
        }else {
            String pickerId = picklist.getPicklistRoles().get(0).getPartyId();
            return Optional.of(pickerId);
        }

    }

    /*
    public Optional<ECARule> getEcaRuleByPicklistId(String picklistId) {
        ECARule ecaRule = spRules.get(picklistId);
        if(ecaRule == null){
            return Optional.empty();
        } else {
            return Optional.of(ecaRule);
        }
    }
*/
}
