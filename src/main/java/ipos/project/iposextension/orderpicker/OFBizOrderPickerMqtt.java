package ipos.project.iposextension.orderpicker;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.OFBizOrderpicker;
import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.Picklist;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.trans.IPos2protoTransformer;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.List;

@MqttListener(OFBizOrderPicker.TOPIC_POSITION_EVENT)
public class OFBizOrderPickerMqtt implements Handler {
    // ich denke: die meiste MQTT-Funktionalität sollte von einer Klasse aus dem IPos-extension-package geerbt werden
    // hier sollte nur die Weiterleitung des PositionEvents mit der picklistId an die korrekte Klasse (OFBizOrderPickerExt.handlePositioneEvent) sichergestellt werden
    // reception: has to call OFBizOrderPickerExt.handlePositionEvent
    // reception: the name of the picklist shall equal the name of the topic that PositionEvents for this picklist are sent to
    // horchen auf RobolabMonitoringOPExtension (IPos-FW sendet dort gemäß monitoringrequest die PositionEvents hin)

    public static void handleJsonOpWrapper(String jsonMsg){
        try {
            OFBizOrderpicker.OrderpickerWrapper opWrapper = ProtoJsonMap.fromJson(jsonMsg, OFBizOrderpicker.OrderpickerWrapper.class);
            for (OFBizOrderpicker.OFBizPicklist proto_Picklist : opWrapper.getPicklistsList()){
                processPicklist(proto_Picklist);
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    private static void processPicklist(OFBizOrderpicker.OFBizPicklist proto_picklist) {
        Picklist picklist = OFBizOrderPickerTrans.protoPicklist2Internal(proto_picklist);
        OFBizOrderPicker.handlePicklist(picklist);
    }

    @Override
    public void handle(MqttMessage message) {
        handleIPosMonitoringWrapper(message.toString());
    }

    static void handleIPosMonitoringWrapper(String jsonMsg) {
        try {
            SimpleScene.IposMonitoringWrapper iPosMonitoringWrapper = ProtoJsonMap.fromJson(jsonMsg, SimpleScene.IposMonitoringWrapper.class);
            iPosMonitoringWrapper.getIposPositionEventsList().stream().forEach(iposPositionEvent_proto -> handleJsonPosEvt(iposPositionEvent_proto));
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    public static void handleJsonPosEvt(SimpleScene.IposPositionEvent proto_positionEvent){
        // SimpleScene.IposPositionEvent proto_positionEvent = ProtoJsonMap.fromJson(jsonPosEvt, SimpleScene.IposPositionEvent.class);
        List<IposPositionEvent> internal_iposPositionEvents = IPos2protoTransformer.transformToInternal(proto_positionEvent);
        for (IposPositionEvent iposPositionEvent : internal_iposPositionEvents){
            OFBizOrderPicker.ofbizOpIposExt.handlePositionEvent(iposPositionEvent);
        }
    }
}
