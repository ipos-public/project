package ipos.project.iposextension.orderpicker;

import ipos.models.OFBizOrderpicker;
import ipos.project.DataModellntegration.iPos_Datamodel.IPos_DatamodelFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.OFBiz.*;
import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;
import ipos.project.iposextension.orderpicker.frontend.FEInitRow;
import ipos.project.iposextension.orderpicker.frontend.FETable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class OFBizOrderPickerTrans {
    private static IPos_DatamodelFactory modelFactory = IPos_DatamodelFactory.eINSTANCE;
    private static OFBizFactory ofbizFactory = OFBizFactory.eINSTANCE;

    public static Picklist protoPicklist2Internal(OFBizOrderpicker.OFBizPicklist proto_picklist) {
        Picklist picklist = ofbizFactory.createPicklist();
        picklist.setPicklistId(proto_picklist.getPicklistId());
        List<PicklistRole> picklistRoles = proto_picklist.getPicklistRolesList().stream()
                        .map(OFBizOrderPickerTrans::protoPicklistRole2Internal)
                        .collect(Collectors.toList());
        picklist.getPicklistRoles().addAll(picklistRoles);
        List<PicklistBin> picklistBins = proto_picklist.getPicklistBinsList().stream()
                .map(OFBizOrderPickerTrans::protoPicklistBin2Internal)
                .collect(Collectors.toList());
        picklist.getPicklistBins().addAll(picklistBins);
        return picklist;
    }

    private static PicklistBin protoPicklistBin2Internal(OFBizOrderpicker.OFBizPicklistBin ofBizPicklistBin) {
        PicklistBin picklistBin = ofbizFactory.createPicklistBin();
        picklistBin.setPicklistId(ofBizPicklistBin.getPicklistId());
        picklistBin.setBinLocationNumber(ofBizPicklistBin.getBinLocationNumber());
        List<PicklistItem> picklistItems = ofBizPicklistBin.getPicklistItemsList().stream()
                        .map(OFBizOrderPickerTrans:: protoPicklistItem2Internal)
                        .collect(Collectors.toList());
        picklistBin.getPicklistItems().addAll(picklistItems);
        return picklistBin;
    }

    private static PicklistItem protoPicklistItem2Internal(OFBizOrderpicker.OFBizPicklistItem proto_picklistItem) {
        PicklistItem picklistItem = ofbizFactory.createPicklistItem();
        InventoryItem inventoryItem = ofbizFactory.createInventoryItem();
        OFBizOrderpicker.OFBizInventoryItem proto_inventoryItem = proto_picklistItem.getInventoryItem();
        inventoryItem.setInventoryItemId(proto_inventoryItem.getInventoryItemId());
        inventoryItem.setContainerId(proto_inventoryItem.getContainerId());
        inventoryItem.setProductId(proto_inventoryItem.getProductId());
        picklistItem.setInventoryItem(inventoryItem);
        return picklistItem;
    }

    private static PicklistRole protoPicklistRole2Internal(OFBizOrderpicker.OFBizPicklistRole ofbizPicklistRole) {
        PicklistRole picklistRole = ofbizFactory.createPicklistRole();
        picklistRole.setPicklistId(ofbizPicklistRole.getPicklistId());
        picklistRole.setPartyId(ofbizPicklistRole.getPartyId());
        picklistRole.setRoleTypeId(ofbizPicklistRole.getRoleTypeId());
        return picklistRole;
    }

    static OFBizOrderpicker.FEndWrapper transform2proto(FETable feTable) {
        OFBizOrderpicker.FEndWrapper.Builder proto_fendWrapper = OFBizOrderpicker.FEndWrapper.newBuilder();
        OFBizOrderpicker.FEndInitPicklist.Builder proto_fendInitPicklist = OFBizOrderpicker.FEndInitPicklist.newBuilder();
        proto_fendInitPicklist.setPicklistId(feTable.getPicklistId());
        proto_fendInitPicklist.setPickerId(feTable.getPickerId());
        List<OFBizOrderpicker.FEndInitRow> feInitRows = feTable.getFeInitRows().stream()
                        .map(OFBizOrderPickerTrans::transform2proto)
                        .collect(Collectors.toList());
        proto_fendInitPicklist.addAllFEndInitRows(feInitRows);
        proto_fendWrapper.addFEndInitPicklists(proto_fendInitPicklist);
        return proto_fendWrapper.build();
    }

    private static OFBizOrderpicker.FEndInitRow transform2proto(FEInitRow feInitRow) {
        OFBizOrderpicker.FEndInitRow.Builder proto_feInitRow = OFBizOrderpicker.FEndInitRow.newBuilder();
        proto_feInitRow.setIndex(feInitRow.getIndex());
        proto_feInitRow.setProductId(feInitRow.getProductId());
        proto_feInitRow.setInventoryItemSoll(feInitRow.getInventoryItem_Soll());
        proto_feInitRow.setShipmentBinNrSoll(feInitRow.getShipmentBinNr_Soll());
        return proto_feInitRow.build();
    }

    /**
     * Note to the implementation: A picklistBin knows multiple picklistItems, i.e., multiple tableRows,
     * and is therefore transformed into a list of tableRows ("feInitRow"). flatMap prevents us from
     * obtaining a List<List<FEInitRow>>.
     * Directly collecting the result of transformBin2Rows would result in such a nested list. Due to
     * flatMap we obtain a flattened version of it: List<FEInitRow>
     * @param picklist
     * @return
     */
    static FETable transformPicklistIntoFeTable(Picklist picklist) {
        String picklistId = picklist.getPicklistId();
        List<PicklistRole> pickerRoles = safelyExtractPickerRoles(picklist);
        String pickerId = pickerRoles.get(0).getPartyId();
        List<FEInitRow> feInitRows = picklist.getPicklistBins()
                .stream()
                .map(OFBizOrderPickerTrans::transformBin2Rows)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
       setRowIndices(feInitRows);
        return new FETable(picklistId, pickerId, feInitRows);
    }

    private static void setRowIndices(List<FEInitRow> feInitRows) {
        int rowIdx = 1;
        for (FEInitRow feInitRow : feInitRows){
            feInitRow.setRowIdx(rowIdx);
            rowIdx++;
        }
    }

    private static List<PicklistRole> safelyExtractPickerRoles(Picklist picklist) {
        List<PicklistRole> pickerRoles = picklist.getPicklistRoles()
                .stream()
                .filter(c -> c.getRoleTypeId().equals(OFBizOrderPicker.OP_PICKER_ROLE_TYPE))
                .collect(Collectors.toList());
        if (pickerRoles.size() == 0){
            throw new RuntimeException("There is no picker associated to the picklist");
        }else if (pickerRoles.size() > 1){
            throw new RuntimeException("There are multiple pickers associated to the picklist");
        }
        return pickerRoles;
    }

    private static List<FEInitRow> transformBin2Rows(PicklistBin picklistBin) {
        List<FEInitRow> feInitRows = new LinkedList<FEInitRow>();
        for (PicklistItem picklistItem : picklistBin.getPicklistItems()){
            InventoryItem inventoryItem = picklistItem.getInventoryItem();
            String productId = inventoryItem.getProductId();
            String inventoryItem_Soll = inventoryItem.getContainerId();
            String shipmentBinNr_Soll = picklistBin.getBinLocationNumber();
            FEInitRow feInitRow = new FEInitRow(-1, productId, inventoryItem_Soll, shipmentBinNr_Soll);
            feInitRows.add(feInitRow);
        }
        return feInitRows;
    }

    static List<ZoneDescriptor> picklistToZoneDescriptors(Picklist picklist) {
        FETable feTable = OFBizOrderPickerTrans.transformPicklistIntoFeTable(picklist); // the ECA-rule shall expect the position events to arrive exactly in the order displayed in the frontend-table
        List<ZoneDescriptor> zoneDescriptors = feTable.getFeInitRows().stream()
                .map(OFBizOrderPickerTrans::feTableRowToZoneDescriptors)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        return zoneDescriptors;
    }

    static List<ZoneDescriptor> feTableRowToZoneDescriptors(FEInitRow feInitRow) {
        List<ZoneDescriptor> zoneDescriptors = new LinkedList<>();
        zoneDescriptors.add(extractInvItemZoneDescFromRow(feInitRow));
        zoneDescriptors.add(extractShipmentBinZoneDescFromRow(feInitRow));
        return zoneDescriptors;
    }

    private static ZoneDescriptor extractShipmentBinZoneDescFromRow(FEInitRow feInitRow) {
        ZoneDescriptor shipBin_zoneDescriptor = modelFactory.createZoneDescriptor();
        shipBin_zoneDescriptor.setZoneId(feInitRow.getShipmentBinNr_Soll());
        return shipBin_zoneDescriptor;
    }

    private static ZoneDescriptor extractInvItemZoneDescFromRow(FEInitRow feInitRow) {
        ZoneDescriptor invItem_zoneDescriptor = modelFactory.createZoneDescriptor();
        invItem_zoneDescriptor.setZoneId(feInitRow.getInventoryItem_Soll());
        return invItem_zoneDescriptor;
    }

}
