# OrderPicker extension

## How to run?
1. Preparation
   - Make sure that IPos-FW can connect to an MQTT-server
     - Verify MQTT-configuration in `src\main\resources\application.yml`
   - Check logger configuration in `ipos.project.CustomLoggingFilter`
     - Returning `FilterReply.DENY` prevents messages from being logged, `FilterReply.ACCEPT` allows them to be logged
     - Logging all messages with relevance for this extension requires accepting all messages that contain the string `OP:`
   - Check that the *modules*-submodule has been checked out properly
     - folder `models` should not be empty
     - if empty, try to clone the repository together with checking out all submodules
       - `git clone -b SimpleScene --recurse-submodules https://git-st.inf.tu-dresden.de/ipos/project.git project4`
       - additionally: `git status` in folder `models` should return `Head detached`. To fix that: `git checkout main`
   - Make sure that an instance of the [frontend-app](https://git-st.inf.tu-dresden.de/ipos/front-end-app) is running and accessible by the IPos-FW
     - Remark: This is not necessary if you only want to debug the IPos-FW or examine its logging messages
2. Launch IPos-FW
    - main-function: `ipos.project.MainApp.main`
    - Console should print an overview of the available comments (if CustomLoggingFilter accepts messages containing string `SHELL:`)
    - Console can be made to reprint the overview by entering `help`
3. Enter `init op`
    - Initializes both, IPos-FW and OrderPicker extension
    - OrderPicker extension sends MonitoringRequest to IPos-FW that causes it to send *Position-Updates* to the OrderPicker that cover all zones relevant for the verification of order-picking  
4. Enter `oppl testdata`
    - Reads a test-picklist contained in JSON-file `testdata_raw_orderpicker.txt`
    - The logger should print the sequence of zones the picker is expected to visit
5. Enter `opevts testdata`
    - Reads the JSON-file `testdata_raw_orderpicker_posEvts.txt` containing a sequence of test-messages indicating to the *OrderPicker extension* the presence of the picker in certain zones
    - The OrderPicker verifies whether the picker follows the expected sequence. 
    - The *OrderPicker extension* informs the frontend-app about certain events by sending appropriate messages:
      - Picker entered a zone in order
      - Picker entered a zone out of order
    - The behaviour of the *OrderPicker extension* can be verified by inspecting the (exhaustive) logger-output
      - Messages sent are sent to the frontend-app are logged
      - Events that have occured but are not indicated to the frontend-app are logged

## Selected positions in source-code
### Execution of the SHELL-commands
- function `ipos.project.MainApp.processCommands`
### Initialization of the *OrderPicker extension*
- starting point: function `ipos.project.MainApp.initOp`
- The MonitoringReqeuest is prepared: function `ipos.project.iposextension.orderpicker.OFBizOrderPickerExt.prepareConfigWrapper`
### Information on the extension-mechanism of the IPos-FW
- see documentation of function `ipos.project.iposextension.orderpicker.OFBizOrderPicker.initialize`
### Information on the general behaviour of the `OrderPicker extension`
- see documentation of class `ipos.project.iposextension.orderpicker.OFBizOrderPicker`
### Testdata processing
- picklist testdata: function `ipos.project.iposextension.orderpicker.OFBizOrderPicker.processPicklistTestData`
- position testdata: function `ipos.project.iposextension.orderpicker.OFBizOrderPicker.processPosEvtTestData`
### Picklist-handling
- function: `ipos.project.iposextension.orderpicker.OFBizOrderPicker.handlePicklist`
- filter-behaviour: see documentation of function `ipos.project.iposextension.orderpicker.OFBizOrderPickerExt.setupECARule`
### Handling a PositionEvent sent from IPos-FW
- function: `ipos.project.iposextension.orderpicker.OFBizOrderPickerExt.handlePositionEvent(ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent, ipos.project.devkit.iposext.IPosArguments.HandlePosEventConf)`
- overall behaviour of each ECA-rule: `ipos.project.devkit.eca.ECARule.apply`