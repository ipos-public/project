package ipos.project.iposextension.orderpicker.eca;

import ipos.project.DataModellntegration.iPos_Datamodel.ZoneDescriptor;
import ipos.project.devkit.eca.zseq.StageChangeAction;
import ipos.project.devkit.eca.zseq.ZoneSequenceCondition;
import ipos.project.devkit.eca.zseq.ZoneSequenceStage;
import ipos.project.iposextension.orderpicker.OFBizOrderPicker;

public class SeqPicklistCondition extends ZoneSequenceCondition {

    int timesCalled = 0;

    /**
     *
     * @param zoneDescriptor describes the zone, i.e. the inventory- or shipmentBinItem, that the received Position(Event) has been found be located into by the IPos-FW
     * @return
     */
    @Override
    public StageChangeAction evaluateStageChangeIntoAction(ZoneDescriptor zoneDescriptor){
        timesCalled++;
        int index = (int) Math.ceil(timesCalled/2.0); // number of the line of the frontend-app table should be updated. Each second StageChange changes the line number
        String inventoryOrShipment = (timesCalled%2.0 == 1.0) ? OFBizOrderPicker.INVENTORYITEM_TYPE_NAME : OFBizOrderPicker.SHIPMENTBIN_TYPE_NAME; // uneven -> inventoryItem, even -> shipmentBin

        ZoneSequenceStage nextStage = currentStage.getNextStage(); // assumption: ZoneSequenceRule::isApplicable rejects the event if the corresponding picklist has been finished
        String nextItemId = nextStage.getEventDescriptors().get(0).getZoneId();
        boolean isCorrect = zoneDescriptor.getZoneId().equals(nextItemId);
        String itemId = zoneDescriptor.getZoneId();

        UpdateFrontendAction updateFrontendAction = new UpdateFrontendAction(index, isCorrect, itemId, inventoryOrShipment);
        return updateFrontendAction;
    }

}
