package ipos.project.iposextension.orderpicker.eca;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.devkit.eca.zseq.ZoneSequenceRule;

public class SeqPicklistRule extends ZoneSequenceRule {
    private String picklistId;
    private String pickerId;

    public SeqPicklistRule(String picklistId, String pickerId) {
        this.picklistId = picklistId;
        this.pickerId = pickerId;
    }

    public String getPicklistId() {
        return picklistId;
    }

    public String getPickerId() {
        return pickerId;
    }

    @Override
    public String toString() {
        String strRepresentation = "SeqPicklistRule: \n picklistId: " + picklistId + "\n pickerId: " + pickerId + "\n";
        strRepresentation += zsCondition.toString();
        return strRepresentation;

    }

    @Override
    public boolean isApplicable(IposPositionEvent posEvent){
        boolean picklistHasBeenFinished = zsCondition.getCurrentStage().getNextStage() == null;
        boolean isApplicable = this.pickerId.equals(posEvent.getAgentId()) & (!picklistHasBeenFinished);
        return super.isApplicable(posEvent) & isApplicable;
    }

    //@Override
    //public StageChangeAction createSCAction() {
    //    return null;
    //}
}
