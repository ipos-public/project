package ipos.project.iposextension.orderpicker.eca;

import ipos.models.OFBizOrderpicker;
import ipos.project.devkit.eca.zseq.StageChangeAction;
import ipos.project.devkit.utility.ProtoJsonMap;
import ipos.project.iposextension.orderpicker.OFBizOrderPicker;
import ipos.project.iposextension.orderpicker.frontend.FETransformer;
import ipos.project.iposextension.orderpicker.frontend.FEUpdateInventoryItem;
import ipos.project.iposextension.orderpicker.frontend.FEUpdateShipmentBin;
import org.apache.logging.log4j.LogManager;

public class UpdateFrontendAction extends StageChangeAction {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    private final int index;
    private final boolean isCorrect;
    private final String itemId;
    private final String inventoryOrShipment;

    // @Autowired
   // public static ExternalPubServiceImpl mqttService;

    public UpdateFrontendAction(int index, boolean isCorrect, String itemId, String inventoryOrShipment) {
        super();
        this.index = index;
        this.isCorrect = isCorrect;
        this.itemId = itemId;
        this.inventoryOrShipment = inventoryOrShipment;
    }

    @Override
    public void execute() {
        if (shouldBeWrittenInInventoryItemColumn(inventoryOrShipment)){
            FEUpdateInventoryItem feUpdInvItem = new FEUpdateInventoryItem(index, isCorrect, itemId);
            OFBizOrderpicker.FEndUpdateInventoryItem proto_invItem = FETransformer.transformIntoProto(feUpdInvItem);
            OFBizOrderpicker.FEndWrapper proto_invItemWrapper = FETransformer.putIntoWrapper(proto_invItem);
            String jsonString = ProtoJsonMap.toJson(proto_invItemWrapper);
            OFBizOrderPicker.mqttService.publish(OFBizOrderPicker.TOPIC_FRONTEND, jsonString, 0, false);
            LOG.info("OP: publishing JSON-InventoryItem mqtt update-message to OP-Frontend-app:" + jsonString + " on topic: " + OFBizOrderPicker.TOPIC_FRONTEND + ". isCorrect (proto): " + proto_invItem.getInventoryIsCorrect());
        }else if(shouldBeWrittenInShipmentBinColumn(inventoryOrShipment)) {
            FEUpdateShipmentBin feUpdShipBinNr = new FEUpdateShipmentBin(index, isCorrect, itemId);
            OFBizOrderpicker.FEndUpdateShipmentBin proto_shipBin = FETransformer.transformIntoProto(feUpdShipBinNr);
            OFBizOrderpicker.FEndWrapper proto_shipBinWrapper = FETransformer.putIntoWrapper(proto_shipBin);
            String jsonString = ProtoJsonMap.toJson(proto_shipBinWrapper);
            OFBizOrderPicker.mqttService.publish(OFBizOrderPicker.TOPIC_FRONTEND, jsonString, 0, false);
            LOG.info("OP: publishing JSON-ShipmentBin mqtt update-message to OP-Frontend-app:" + jsonString + " on topic: " + OFBizOrderPicker.TOPIC_FRONTEND+ ". isCorrect (proto): " + proto_shipBin.getShipmentIsCorrect());
        } else {
            LOG.info("OP: Warning: Could not react to StageChange, as vessel-type could not be determined");
            return;
        }
    }

    private int extractShipmentBinNr(String itemId) {
        return Integer.parseInt(itemId.split("_")[1]);
    }

    private boolean shouldBeWrittenInShipmentBinColumn(String itemId) {
        return itemId.equals(OFBizOrderPicker.SHIPMENTBIN_TYPE_NAME);
    }

    private boolean shouldBeWrittenInInventoryItemColumn(String itemId) {
        return itemId.equals(OFBizOrderPicker.INVENTORYITEM_TYPE_NAME);
    }
}
