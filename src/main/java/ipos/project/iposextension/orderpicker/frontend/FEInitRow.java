package ipos.project.iposextension.orderpicker.frontend;

public class FEInitRow {
    public FEInitRow(int index, String productId, String inventoryItem_Soll, String shipmentBinNr_Soll) {
        this.index = index;
        this.productId = productId;
        this.inventoryItem_Soll = inventoryItem_Soll;
        this.shipmentBinNr_Soll = shipmentBinNr_Soll;
    }

    int index;
    String productId;
    String inventoryItem_Soll;
    String shipmentBinNr_Soll;

    public int getIndex() {
        return index;
    }

    public String getProductId() {
        return productId;
    }

    public String getInventoryItem_Soll() {
        return inventoryItem_Soll;
    }

    public String getShipmentBinNr_Soll() {
        return shipmentBinNr_Soll;
    }

    public void setRowIdx(int rowIdx){
        this.index = rowIdx;
    }
}
