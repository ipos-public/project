package ipos.project.iposextension.orderpicker.frontend;

import java.util.List;

public class FETable {
    String picklistId;
    String pickerId;
    List<FEInitRow> feInitRows;

    public String getPicklistId() {
        return picklistId;
    }

    public String getPickerId() {
        return pickerId;
    }

    public List<FEInitRow> getFeInitRows() {
        return feInitRows;
    }

    public FETable(String picklistId, String pickerId, List<FEInitRow> feInitRows) {
        this.picklistId = picklistId;
        this.pickerId = pickerId;
        this.feInitRows = feInitRows;
    }
}
