package ipos.project.iposextension.orderpicker.frontend;

import ipos.models.OFBizOrderpicker;

public class FETransformer {
    public static OFBizOrderpicker.FEndUpdateInventoryItem transformIntoProto(FEUpdateInventoryItem feUpdInvItem) {
        OFBizOrderpicker.FEndUpdateInventoryItem.Builder proto_invItem = OFBizOrderpicker.FEndUpdateInventoryItem.newBuilder();
        proto_invItem.setIndex(feUpdInvItem.getIndex());
        proto_invItem.setInventoryIsCorrect(feUpdInvItem.isCorrect());
        proto_invItem.setInventoryItemIst(feUpdInvItem.getInventoryItem_Ist());
        return proto_invItem.build();
    }

    public static OFBizOrderpicker.FEndUpdateShipmentBin transformIntoProto(FEUpdateShipmentBin feUpdShipBinNr) {
        OFBizOrderpicker.FEndUpdateShipmentBin.Builder proto_shipBin = OFBizOrderpicker.FEndUpdateShipmentBin.newBuilder();
        proto_shipBin.setIndex(feUpdShipBinNr.getIndex());
        proto_shipBin.setShipmentIsCorrect(feUpdShipBinNr.isCorrect());
        proto_shipBin.setShipmentBinNrIst(feUpdShipBinNr.getShipmentBinNrIstColumn());
        return proto_shipBin.build();
    }

    public static OFBizOrderpicker.FEndWrapper putIntoWrapper(OFBizOrderpicker.FEndUpdateInventoryItem proto_invItem) {
        OFBizOrderpicker.FEndWrapper.Builder proto_fendWrapper = OFBizOrderpicker.FEndWrapper.newBuilder();
        proto_fendWrapper.addFEndUpdateInventoryItems(proto_invItem);
        return proto_fendWrapper.build();
    }

    public static OFBizOrderpicker.FEndWrapper putIntoWrapper(OFBizOrderpicker.FEndUpdateShipmentBin proto_shipBin) {
        OFBizOrderpicker.FEndWrapper.Builder proto_fendWrapper = OFBizOrderpicker.FEndWrapper.newBuilder();
        proto_fendWrapper.addFEndUpdateShipmentBins(proto_shipBin);
        return proto_fendWrapper.build();
    }

}
