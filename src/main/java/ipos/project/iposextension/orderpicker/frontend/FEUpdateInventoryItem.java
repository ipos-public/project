package ipos.project.iposextension.orderpicker.frontend;

public class FEUpdateInventoryItem extends FEUpdateRow {
    String inventoryItem_Ist;

    public FEUpdateInventoryItem(int index, boolean isCorrect, String inventoryItem_Ist) {
        super(index, isCorrect);
        this.inventoryItem_Ist = inventoryItem_Ist;
    }

    public String getInventoryItem_Ist() {
        return inventoryItem_Ist;
    }
}
