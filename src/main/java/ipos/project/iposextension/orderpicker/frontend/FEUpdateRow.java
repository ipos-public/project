package ipos.project.iposextension.orderpicker.frontend;

public class FEUpdateRow {
    int index;
    boolean isCorrect;

    public FEUpdateRow(int index, boolean isCorrect) {
        this.index = index;
        this.isCorrect = isCorrect;
    }

    public int getIndex() {
        return index;
    }

    public boolean isCorrect() {
        return isCorrect;
    }
}
