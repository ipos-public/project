package ipos.project.iposextension.orderpicker.frontend;

public class FEUpdateShipmentBin extends FEUpdateRow{
    String shipmentBinNrIstColumn;

    public FEUpdateShipmentBin(int index, boolean isCorrect, String shipmentBinNrIst) {
        super(index, isCorrect);
        this.shipmentBinNrIstColumn = shipmentBinNrIst;
    }

    public String getShipmentBinNrIstColumn() {
        return shipmentBinNrIstColumn;
    }
}
