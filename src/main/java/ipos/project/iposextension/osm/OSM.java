package ipos.project.iposextension.osm;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.VDA5050;
import ipos.project.DataModellntegration.VDA5050Processor.api.MqttRequestHandler;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject;
import ipos.project.devkit.iposext.IPosArguments;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.OtherUtility;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Scanner;

@Component
public class OSM {

    public static final String TOPIC_TOOZEXT_OUTPUT = "SmartGlassesBackend";
    public static final String SERIALIZATIONTYPE_TOOZEXT_OUTPUT = "json";
    public static final String OSM_MONREQ = "{\"monitoringRequests\": [{\"properties\": [\"%s\"], \"monitoringTaskId\": \"%s\", \"refSystemId\": \"ROOT\", \"serializationType\": \"json\"}]}";
    public static final String TOPIC_OSM_ENVIRONMENT = "OSM_ENVIRONMENT";
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    static private OSMExt osmExt = new OSMExt();
    public static final String OSM_WAYPOINT_OSMOBJECT = "{           \"agentId\":\"waypoint%s\",           \"agentType\":\"\",           \"publisher\":\"main\",           \"position\":{               \"refSystemId\":\"ROOT\",               \"point\":{                   \"x\":%s,                   \"y\":%s,                   \"z\":0               }           },           \"time\":\"2022-04-14T09:41:20+00:00\"       }";
    //public static final String OSM_WAYPOINT_OSMOBJECT = "{ \"waypoints\": [       {           \"agentId\":\"waypoint%s\",           \"agentType\":\"\",           \"publisher\":\"main\",           \"position\":{               \"refSystemId\":\"ROOT\",               \"point\":{                   \"x\":%s,                   \"y\":%s,                   \"z\":0               }           },           \"time\":\"2022-04-14T09:41:20+00:00\"       } ] }";
    // public static final String OSM_WAYPOINT_OSMOBJECT = "{           \"id\": \"waypoint%s\",           \"sensorId\": \"\",           \"type\": \"\",           \"sensorType\": \"\",           \"position\": {               \"refSystemId\": \"ROOT\",               \"point\": {               \"x\": %s,               \"y\": %s,               \"z\": 0               },               \"accuracy\": 1           },           \"orientation\": {               \"x\": 0,               \"y\": 0,               \"z\": 0,               \"w\": 0           },           \"lastPosUpdate\": \"2021-09-14T09:41:20+00:00\"           }";

    @Autowired
    public OSM(ExternalPubServiceImpl mqttService){
        osmExt.setMqttService(mqttService);
        OSMIPosMonitoringWrapperMqtt.setMqttService(mqttService);
        OSMExtInputMqtt.setMqttService(mqttService);
        OSMExtOutputMqtt.setMqttService(mqttService);
        osmExt.initOSM_drawWaypoints();
    }

    public static void handleOSMObject(OSMObject osmObject) {
        osmExt.handleOSMObject(osmObject);
    }

    public static void processMonTargetTestdata(String path_to_monTarget_file) {
        File monTargetFile = new File(path_to_monTarget_file);
        for (String line : OtherUtility.readLines(monTargetFile)) {
            OSMExtInputMqtt.handleOsmExtInput(line);
        }
    }

    public static void handleOSMMonitoringTarget(OSMMonitoringTarget osmMonitoringTarget_internal) {
        IPosArguments.IPosConfigData osmConf = new OSMArgs.OSMConf(OSMExt.TOPIC_OSM_EXT, osmMonitoringTarget_internal.getProtocol());
        osmExt.configureIpos(osmConf);
    }

    public static void processTestData(String path_to_test_data_file){
        File testDataFile = new File(path_to_test_data_file);
        Scanner scanner = new Scanner(System.in);
            for (String line : OtherUtility.readLines(testDataFile)) {
                OSMExtInputMqtt.handleOsmExtInput(line);
        }
    }

}
