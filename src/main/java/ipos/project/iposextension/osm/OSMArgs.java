package ipos.project.iposextension.osm;

import ipos.project.devkit.iposext.IPosArguments;

public class OSMArgs {

    public static class OSMConf implements IPosArguments.IPosConfigData {

        String monitoringTaskId;

        public String getProtocol() {
            return protocol;
        }

        String protocol;
        public String getMonitoringTaskId() {
            return monitoringTaskId;
        }

        public OSMConf(String monitoringTaskId, String protocol) {
            this.monitoringTaskId = monitoringTaskId;
            this.protocol = protocol;
        }

    }

}
