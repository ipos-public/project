package ipos.project.iposextension.osm;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosRawdataEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IposPositionEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject;
import ipos.project.devkit.iposext.IPosArguments;
import ipos.project.devkit.iposext.IPosExtension;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class OSMExt extends IPosExtension {
    public static final String TOPIC_OSM_EXT = "AGVViewer_VDA5050";
    public static final String TOPIC_OSM_EXT_OUTPUT = "ipos/client/position";
    public static final String TOPIC_OSM_EXT_OUT_SET_WAYPOINT = "ipos/client/waypoint";
    public static final String SERIALIZATION_TYPE_OSM_EXT = "json";

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    void initOSM_drawWaypoints() {
        createAndTransmitWaypointToOSMApp("1",18, 10);
        createAndTransmitWaypointToOSMApp("2",6, 3.12f);
        createAndTransmitWaypointToOSMApp("3",14, 3.12f);
        createAndTransmitWaypointToOSMApp("4",14, 16.92f);
        createAndTransmitWaypointToOSMApp("5",6, 16.92f);
        createAndTransmitWaypointToOSMApp("6",2, 10);
    }

    private void createAndTransmitWaypointToOSMApp(String waypointId, float x, float y) {
        try {
            //OSMObject waypointAsOSMObject = createWaypointAsOSMObject(waypointId, x, y);
            //OSMExtOutputMqtt.transmitOSMObject(waypointAsOSMObject, TOPIC_OSM_EXT_OUT_SET_WAYPOINT, SERIALIZATION_TYPE_OSM_EXT);
            ipos.models.OSM.OSMWaypoint waypoint_proto = createWaypoint_proto(waypointId, x, y);
            OSMExtOutputMqtt.transmitOSMOutput(waypoint_proto, TOPIC_OSM_EXT_OUT_SET_WAYPOINT, SERIALIZATION_TYPE_OSM_EXT);
        }catch(NullPointerException e){
            LOG.warn("Warning: Could not initialize OSM-app with waypoint. Id of the waypoint that could not be initialized: " + waypointId);
        }
    }

    private ipos.models.OSM.OSMWaypoint createWaypoint_proto(String waypointId, float x, float y) {
        String waypointAsJSONString = String.format(OSM.OSM_WAYPOINT_OSMOBJECT, waypointId, String.valueOf(x), String.valueOf(y));
        ipos.models.OSM.OSMWaypoint waypoint_proto = Objects.requireNonNull(parseIntoProtoWaypointOrNull(waypointAsJSONString));
        return waypoint_proto;
    }

    private ipos.models.OSM.OSMWaypoint parseIntoProtoWaypointOrNull(String wrappedWaypointAsJSONString) {
        try {
            return ProtoJsonMap.fromJson(wrappedWaypointAsJSONString, ipos.models.OSM.OSMWaypoint.class);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
    private OSMObject createWaypointAsOSMObject(String waypointId, float x, float y) {
        String waypointAsJSONString = String.format(OSM.OSM_WAYPOINT_OSMOBJECT, waypointId, String.valueOf(x), String.valueOf(y));
        SimpleScene.IposObject waypointAsOsmObjectProto = Objects.requireNonNull(parseIntoProtoObjectOrNull(waypointAsJSONString));
        OSMObject waypointAsOSMObject = OSMTrans.posEv_proto2OSMObject(waypointAsOsmObjectProto) ;
        return waypointAsOSMObject;
    }
    */
    @Nullable
    private SimpleScene.IposObject parseIntoProtoObjectOrNull(String waypointAsJSONString) {
        try {
            return ProtoJsonMap.fromJson(waypointAsJSONString, SimpleScene.IposObject.class);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void handlePositionEvent(IposPositionEvent posEvent) {}

    @Override
    public void handlePositionEvent(IposPositionEvent posEvent, IPosArguments.HandlePosEventConf posEventConf) {}

    @Override
    public void handleRawdataEvent(IPosRawdataEvent iPosRawdataEvent) {}

    public void handleOSMObject(OSMObject osmObject){
        OSMExtOutputMqtt.transmitOSMObject(osmObject, TOPIC_OSM_EXT_OUTPUT, SERIALIZATION_TYPE_OSM_EXT);
    }

    @Override
    public SimpleScene.IposConfigWrapper prepareConfigWrapper(IPosArguments.IPosConfigData iposConfigData) {
        OSMArgs.OSMConf osmConf = (OSMArgs.OSMConf) iposConfigData;
        String osmMonitoringTarget = osmConf.getMonitoringTaskId();
        String osmProtocol = osmConf.getProtocol();
        return createConfigWrapperFromOSMMonitoringTarget(osmMonitoringTarget, osmProtocol);
    }

    private SimpleScene.IposConfigWrapper createConfigWrapperFromOSMMonitoringTarget(String monitoringTaskId, String protocol) {
        String monitoringRequest = String.format(OSM.OSM_MONREQ, protocol, monitoringTaskId);
        try {
            return ProtoJsonMap.fromJson(monitoringRequest, SimpleScene.IposConfigWrapper.class);
        } catch (InvalidProtocolBufferException e) {
            LOG.info("Warning: IPos-IPS could not be configured properly, as there was a MonitoringTarget that no ConfigWrapper could be created from.");
            return null;
        }
    }
}
