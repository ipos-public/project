package ipos.project.iposextension.osm;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMMonitoringTarget;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;

@MqttListener(OSM.TOPIC_OSM_ENVIRONMENT)
public class OSMExtInputMqtt implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static ExternalPubServiceImpl mqttService;
    public static void setMqttService(ExternalPubServiceImpl mqttService) {
        OSMExtInputMqtt.mqttService = mqttService;
    }

  @Override
    public void handle(MqttMessage message) {
        handleOsmExtInput(message.toString());
    }

    public static void handleOsmExtInput(String jsonMsg) {
        ipos.models.OSM.OSMExtInputWrapper osmInputWrapper = null;
        try {
            LOG.info("OSM: Handling message in order to initialize OSM-application: " + jsonMsg);
            osmInputWrapper = ProtoJsonMap.fromJson(jsonMsg, ipos.models.OSM.OSMExtInputWrapper.class);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        osmInputWrapper.getMonitoringTargetsList().forEach(osmMonitoringTarget -> handleOsmMonitoringTarget(osmMonitoringTarget));
    }

    private static void handleOsmMonitoringTarget(ipos.models.OSM.OSMMonitoringTarget monitoringTarget_proto) {
        OSMMonitoringTarget osmMonitoringTarget_internal = OSMTrans.osmMonTarget_proto2internal(monitoringTarget_proto);
        OSM.handleOSMMonitoringTarget(osmMonitoringTarget_internal);
    }
}
