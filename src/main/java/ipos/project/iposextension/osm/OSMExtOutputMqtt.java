package ipos.project.iposextension.osm;

import com.google.protobuf.AbstractMessageLite;
import ipos.models.OSM;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.OtherUtility;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;

@MqttListener("invalid_topic")
public class OSMExtOutputMqtt implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static ExternalPubServiceImpl mqttService;
    public static void setMqttService(ExternalPubServiceImpl mqttService) {
        OSMExtOutputMqtt.mqttService = mqttService;
    }

    public static void transmitOSMOutput(AbstractMessageLite osmOutput, String topic, String serializationType) {
        OtherUtility.publishRespectingSerializationType(topic, serializationType, osmOutput);
    }

    @Override
    public void handle(MqttMessage message) {}

    public static void transmitOSMObject(OSMObject osmObject, String topic, String serializationType){
        OSM.OSMObject osmObject_proto = OSMTrans.osmObject_internal2proto(osmObject);
        OSM.OSMOutputWrapper osmOutputWrapper_proto = wrapIntoOutputWrapper(osmObject_proto);
        logging_osm(topic, serializationType, osmObject_proto);
        OtherUtility.publishRespectingSerializationType(topic, serializationType, osmOutputWrapper_proto);
    }

    private static OSM.OSMOutputWrapper wrapIntoOutputWrapper(OSM.OSMObject osmObject_proto) {
     OSM.OSMOutputWrapper.Builder osmOutputWrapper_proto = OSM.OSMOutputWrapper.newBuilder();
     osmOutputWrapper_proto.addObjects(osmObject_proto);
     return osmOutputWrapper_proto.build();
    }

    private static void logging_osm(String topic, String serializationType, OSM.OSMObject osmObject_proto) {
        LOG.info("OSM: Publishing " + osmObject_proto.getClass().getName() + " on topic " + topic + " with serialization type " + serializationType);
        LOG.info("OSM: ");
    }

}
