package ipos.project.iposextension.osm;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.OSMObject;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;

@MqttListener(OSMExt.TOPIC_OSM_EXT)
public class OSMIPosMonitoringWrapperMqtt implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static ExternalPubServiceImpl mqttService;
    public static void setMqttService(ExternalPubServiceImpl mqttService) {
        OSMIPosMonitoringWrapperMqtt.mqttService = mqttService;
    }

    // für den generierten code ggf. noch eine höhere Wrapper-Ebene vorsehen: IPosInputWrapper (enthält sowohl PositionWrapper, als auch RawdataWrapper)
    // für den generierten code auch vorsehen: eigene XMqtt-Klasse für jeden Wrapper generieren der empfangen werden können soll.
    // Also eine Klasse für den IPosInputWrapper und eine andere Klasse für den IPosToozExtInputWrapper. Für die Wrapper-Abbildung in EMF ggf. mit Packages arbeiten
    // Es gibt Wrapper die nur ausgehende Nachrichten beinhalten (z.B. bei der tooz-Anwendung: ToozExtOutputWrapper), man muss beim Generieren festlegen können ob
    // die Wrapper-Nachrichten überhaupt empfangen werden sollen
    @Override
    public void handle(MqttMessage message) {
        handleIPosMonitoringWrapper(message.toString());
    }

    // generierter code: grundsätzlich für jede mögliche Nachricht etwas generieren, nicht nur für IPosUWBEvent
    private void handleIPosMonitoringWrapper(String jsonMsg) {
        try {
            LOG.info("OSM-app Received Message: " + jsonMsg + " on topic " + OSMExt.TOPIC_OSM_EXT);
            SimpleScene.IposMonitoringWrapper iPosMonitoringWrapper = ProtoJsonMap.fromJson(jsonMsg, SimpleScene.IposMonitoringWrapper.class);
            iPosMonitoringWrapper.getIposMsgRcvEventsList().stream().forEach(iposMsgRcvEvent -> this.handleIPosMsgRcvEvent(iposMsgRcvEvent));
            iPosMonitoringWrapper.getIposPositionEventsList().stream().forEach(iposPositionEvent -> this.handleIPosPositionEvent(iposPositionEvent));
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    private void handleIPosMsgRcvEvent(SimpleScene.IposMsgRcvEvent iPosMsgRcvEvent_proto) {
        OSMObject osmObject_internal = OSMTrans.msgRcvEv_proto2OSMObject(iPosMsgRcvEvent_proto);
        LOG.info("OSM: Translated IPosMsgRcvEvent to OSMObject: " + osmObject_internal.toString());
        OSM.handleOSMObject(osmObject_internal);
    }

    private void handleIPosPositionEvent(SimpleScene.IposPositionEvent iPosPositionEvent_proto) {
        iPosPositionEvent_proto.getObjectsList().stream().forEach(iposObject -> handleIposObject(iposObject));
    }

    private void handleIposObject(SimpleScene.IposObject iposObject_proto) {
        OSMObject osmObject_internal = OSMTrans.posEv_proto2OSMObject(iposObject_proto);
        LOG.info("OSM: Translated IposObject into OSMObject: " + osmObject_internal.toString());
        OSM.handleOSMObject(osmObject_internal);
    }

}
