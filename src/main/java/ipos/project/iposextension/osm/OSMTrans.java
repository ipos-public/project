package ipos.project.iposextension.osm;

import ipos.models.OSM;
import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.OSM.*;
import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OSMTrans {
    private static IPosDevKitFactory devKitFactory = IPosDevKitFactory.eINSTANCE;
    private static OSMFactory osmFactory = OSMFactory.eINSTANCE;
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    public static OSMMonitoringTarget osmMonTarget_proto2internal(OSM.OSMMonitoringTarget osmMonitoringTarget_proto) {
        OSMMonitoringTarget osmMonitoringTarget_internal = osmFactory.createOSMMonitoringTarget();
        osmMonitoringTarget_internal.setMonitoringTaskId(osmMonitoringTarget_proto.getMonitorintTaskId());
        osmMonitoringTarget_internal.setProtocol(osmMonitoringTarget_proto.getProtocol());
        return osmMonitoringTarget_internal;
    }

   public static OSM.OSMObject osmObject_internal2proto(OSMObject osmObject_internal) {
       OSM.OSMObject.Builder osmObject_proto = OSM.OSMObject.newBuilder();
       if(null != osmObject_internal.getId()) osmObject_proto.setId(osmObject_internal.getId());
       if(null != osmObject_internal.getSensorId()) osmObject_proto.setSensorId(osmObject_internal.getSensorId());
       if(null != osmObject_internal.getType()) osmObject_proto.setType(osmObject_internal.getType());
       if(null != osmObject_internal.getSensorType()) osmObject_proto.setSensorType(osmObject_internal.getSensorType());
       if(null != osmObject_internal.getLastPosUpdate()) osmObject_proto.setLastPosUpdate(osmObject_internal.getLastPosUpdate());
       if(null != osmObject_internal.getOsmposition()) osmObject_proto.setPosition(osmposition_internal2proto(osmObject_internal.getOsmposition()));
       if(null != osmObject_internal.getOsmorientation()) osmObject_proto.setOrientation(osmorientation_internal2proto(osmObject_internal.getOsmorientation()));
       if(null != osmObject_internal.getOsmzonedescriptor()) osmObject_proto.addAllZoneDescriptors(osmObject_internal.getOsmzonedescriptor().stream().map(OSMTrans::zdescr_osm2proto).collect(Collectors.toList()));
       if(null != osmObject_internal.getOsmextractedattributes()) osmObject_proto.setExtractedAttributes(exAtr_internal2proto(osmObject_internal.getOsmextractedattributes()));
       return osmObject_proto.build();
   }

    private static OSM.OSMExtractedAttributes exAtr_internal2proto(OSMExtractedAttributes osmExtractedattributes_internal) {
        OSM.OSMExtractedAttributes.Builder osmExtractedAttributes_proto = OSM.OSMExtractedAttributes.newBuilder();
        osmExtractedAttributes_proto.setTheta(osmExtractedattributes_internal.getTheta()); // getTheta default-value: 0.0F
        osmExtractedAttributes_proto.setBatteryChargeLevel(osmExtractedattributes_internal.getBatteryChargeLevel()); // getBatteryChargeLevel default-value: 0.0F
        if(null != osmExtractedattributes_internal.getErrors()) {
            List<Integer> errorsAsList = Arrays.stream(osmExtractedattributes_internal.getErrors()).boxed().collect(Collectors.toList());
            osmExtractedAttributes_proto.addAllErrors(errorsAsList);
        }
        if (null != osmExtractedattributes_internal.getLoadedItems()) {
            List<Integer> loadsAsList = Arrays.stream(osmExtractedattributes_internal.getLoadedItems()).boxed().collect(Collectors.toList());
            osmExtractedAttributes_proto.addAllLoadedItems(loadsAsList);
        }
        return osmExtractedAttributes_proto.build();
    }

    private static OSM.OSMZoneDescriptor zdescr_osm2proto(OSMZoneDescriptor osmZoneDescriptor_internal) {
        OSM.OSMZoneDescriptor.Builder osmZoneDescriptor_proto = OSM.OSMZoneDescriptor.newBuilder();
        if( null != osmZoneDescriptor_internal.getZoneId()) osmZoneDescriptor_proto.setZoneId(osmZoneDescriptor_internal.getZoneId());
        if( null != osmZoneDescriptor_internal.getNotificationType()) osmZoneDescriptor_proto.setNotificationType(osmZoneDescriptor_internal.getNotificationType());
        return osmZoneDescriptor_proto.build();
    }

    private static OSM.OSMOrientation osmorientation_internal2proto(OSMOrientation osmOrientation_internal) {
        OSM.OSMOrientation.Builder osmOrientation_proto = OSM.OSMOrientation.newBuilder();
        osmOrientation_proto.setX(osmOrientation_internal.getX()); // getX default-value: 0.0F
        osmOrientation_proto.setY(osmOrientation_internal.getY()); // getY default-value: 0.0F
        osmOrientation_proto.setZ(osmOrientation_internal.getZ()); // getZ default-value: 0.0F
        osmOrientation_proto.setW(osmOrientation_internal.getW()); // getW default-value: 0.0F
        return osmOrientation_proto.build();
    }

    private static OSM.OSMPosition osmposition_internal2proto(OSMPosition osmposition_internal) {
        OSM.OSMPosition.Builder osmPosition_proto = OSM.OSMPosition.newBuilder();
        if (null != osmposition_internal.getRefSystemId()) osmPosition_proto.setRefSystemId(osmposition_internal.getRefSystemId());
        osmPosition_proto.setAccuracy(osmposition_internal.getAccuracy()); // accuracy-default value 0.0F
        if (null != osmposition_internal.getOsmpoint()) osmPosition_proto.setPoint(osmpoint_internal2proto(osmposition_internal.getOsmpoint()));
        return osmPosition_proto.build();
    }

    private static OSM.OSMPoint osmpoint_internal2proto(OSMPoint osmpoint_internal) {
        OSM.OSMPoint.Builder osmPoint_proto = OSM.OSMPoint.newBuilder();
        osmPoint_proto.setX(osmpoint_internal.getX());
        osmPoint_proto.setY(osmpoint_internal.getY());
        osmPoint_proto.setZ(osmpoint_internal.getZ());
        return osmPoint_proto.build();
    }

    public static OSMObject msgRcvEv_proto2OSMObject(SimpleScene.IposMsgRcvEvent iPosMsgRcvEvent_proto) {
        OSMObject osmObject_internal = osmFactory.createOSMObject();
        osmObject_internal.setId(iPosMsgRcvEvent_proto.getAgentId());
        osmObject_internal.setSensorId(iPosMsgRcvEvent_proto.getAgentId()); // Annahme: für VDA5050-artige Protokolle gilt: sensorId == agentId
        osmObject_internal.setType(""); // Annahme: VDA5050-artige Protokolle schicken zwar meist Nachrichten von AGVs, geben aber den Agententyp typischerweise nicht an
        osmObject_internal.setSensorType(""); // Annahme: VDA5050-artige Protokolle berechnen zwar die Position aber schicken nicht unbedingt die Information Sensortype mit
        osmObject_internal.setLastPosUpdate(iPosMsgRcvEvent_proto.getLastPosUpdate());
        if (null != iPosMsgRcvEvent_proto.getLastKnownPosition()) {
            OSMPosition osmPosition = position_proto2OSM(iPosMsgRcvEvent_proto.getLastKnownPosition());
            osmObject_internal.setOsmposition(osmPosition);
        }
        if (null != iPosMsgRcvEvent_proto.getLastKnownOrientation()) {
            OSMOrientation osmOrientation = orientation_proto2OSM(iPosMsgRcvEvent_proto.getLastKnownOrientation());
            osmObject_internal.setOsmorientation(osmOrientation);
        }
        if (null != iPosMsgRcvEvent_proto.getLastKnownZoneDescriptorsList()) osmObject_internal.getOsmzonedescriptor().addAll(iPosMsgRcvEvent_proto.getLastKnownZoneDescriptorsList().stream().map(OSMTrans::zoneDescr_proto2OSM).collect(Collectors.toList()));
        if (null != iPosMsgRcvEvent_proto.getExtractedAttributesList()) osmObject_internal.setOsmextractedattributes(exAtr_proto2OSM(iPosMsgRcvEvent_proto.getExtractedAttributesList()));
        return osmObject_internal;
    }

    private static OSMExtractedAttributes exAtr_proto2OSM(List<SimpleScene.Attribute> extractedAttributes_proto) {
        OSMExtractedAttributes osmExtractedAttributes = osmFactory.createOSMExtractedAttributes();
        for (SimpleScene.Attribute attribute : extractedAttributes_proto) {
            extractProtoAttributeIntoOSMAtr(osmExtractedAttributes, attribute);
        }
        return osmExtractedAttributes;
    }

    private static void extractProtoAttributeIntoOSMAtr(OSMExtractedAttributes osmExtractedAttributes, SimpleScene.Attribute attribute) {
        try {
            switch (attribute.getName()) {
                case "batteryCharge":
                    osmExtractedAttributes.setBatteryChargeLevel(Float.parseFloat(attribute.getData()));
                    break;
                case "theta":
                    osmExtractedAttributes.setTheta(Float.parseFloat(attribute.getData())); break;
                case "loads":
                    osmExtractedAttributes.setLoadedItems(parseIntArray(attribute.getData())); break;
                case "errors":
                    osmExtractedAttributes.setErrors(parseIntArray(attribute.getData())); break;
                default:
                    LOG.info("Unknown extracted attribute: " + attribute.getName()); break;
            }
        } catch(NumberFormatException e){
            LOG.info("Warning: Extracted attribute " + attribute.getName() + " could not be parsed: " + e.getMessage());
        }
    }

    private static int[] parseIntArray(String data) {
        String[] stringArray = data
                .replaceAll("\\[", "")
                .replaceAll("]", "")
                .replaceAll("\\s+","") // removing whitespace
                .split(",");
        int[] intArray = new int[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
             intArray[i] = Integer.parseInt(stringArray[i]);
        }
        return intArray;
    }

    private static OSMZoneDescriptor zoneDescr_proto2OSM(SimpleScene.IposZoneDescriptor iposZoneDescriptor) {
        OSMZoneDescriptor osmZoneDescriptor = osmFactory.createOSMZoneDescriptor();
        osmZoneDescriptor.setZoneId(iposZoneDescriptor.getZoneId());
        osmZoneDescriptor.setNotificationType(iposZoneDescriptor.getNotificationType());
        return osmZoneDescriptor;
    }

    private static OSMOrientation orientation_proto2OSM(SimpleScene.IposSimpleOrientation lastKnownOrientation) {
        OSMOrientation orientation = osmFactory.createOSMOrientation();
        orientation.setX(lastKnownOrientation.getX()); // getX-default: 0F
        orientation.setY(lastKnownOrientation.getY());
        orientation.setZ(lastKnownOrientation.getZ());
        orientation.setW(lastKnownOrientation.getW());
        return orientation;
    }

    private static OSMPosition position_proto2OSM(SimpleScene.IposPosition position_proto) {
        OSMPosition position = osmFactory.createOSMPosition();
        position.setRefSystemId(position_proto.getRefSystemId());
        position.setAccuracy(position_proto.getAccuracy());
        position.setOsmpoint(point_proto2OSM(position_proto.getPoint()));
        return position;
    }

    private static OSMPoint point_proto2OSM(SimpleScene.IposPoint3D point_proto) {
        OSMPoint osmPoint = osmFactory.createOSMPoint();
        osmPoint.setX(point_proto.getX());
        osmPoint.setY(point_proto.getY());
        osmPoint.setZ(point_proto.getZ());
        return osmPoint;
    }


    public static OSMObject posEv_proto2OSMObject(SimpleScene.IposObject iposObject_proto) {
        OSMObject osmObject_internal = osmFactory.createOSMObject();
        osmObject_internal.setId(iposObject_proto.getId());
        osmObject_internal.setSensorId(iposObject_proto.getSensorId());
        osmObject_internal.setType(iposObject_proto.getType());
        osmObject_internal.setSensorType(iposObject_proto.getSensorType());
        osmObject_internal.setLastPosUpdate(iposObject_proto.getLastPosUpdate());
        if (null != iposObject_proto.getPosition()) {
            OSMPosition osmPosition = position_proto2OSM(iposObject_proto.getPosition());
            osmObject_internal.setOsmposition(osmPosition);
        }
        if (null != iposObject_proto.getOrientation()) {
            OSMOrientation osmOrientation = orientation_proto2OSM(iposObject_proto.getOrientation());
            osmObject_internal.setOsmorientation(osmOrientation);
        }
        if (null != iposObject_proto.getZoneDescriptorsList()) {
            osmObject_internal.getOsmzonedescriptor().addAll(
                    iposObject_proto.getZoneDescriptorsList().stream().map(OSMTrans::zoneDescr_proto2OSM).collect(Collectors.toList()));
        }
        return osmObject_internal;
    }
}
