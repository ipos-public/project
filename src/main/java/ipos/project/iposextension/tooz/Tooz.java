package ipos.project.iposextension.tooz;

import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozFactory;
import ipos.project.devkit.iposext.IPosArguments;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.OtherUtility;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.*;

@Component
public class Tooz {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    public static final String TOPIC_TOOZEXT_OUTPUT = "SmartGlassesBackend";
    public static final String SERIALIZATIONTYPE_TOOZEXT_OUTPUT = "json";
    public static final String TOOZ_MONREQ = "{\"monitoringRequests\": [{\"id\": [\"%s\"], \"properties\": [\"distance\"], \"monitoringTaskId\": \"%s\", \"refSystemId\": \"ROOT\", \"serializationType\": \"json\"}]}";
    public static final String TOOZ_MONITORINGTASKID = "tooz_distances";
    // public static final String TOPIC_TOOZ_EXT = "TOOZ_TEST_MONITORING";
    public static final String TOPIC_TOOZ_ENVIRONMENT = "TOOZ_ENVIRONMENT";
    public static final String TOPIC_TOOZ_EXT_OUTPUT = "TOOZ_EXT_OUTPUT";
    public static final String STOMP_BLUETOOTH_NAME = "IPOS_IPS";
    public static final String SERIALIZATION_TYPE_JSON = "json";
    public static final String TOOZ_SERVICE_APPLICATION_DESTINATION_PREFIX = "/medical_ar_app";
    public static final String TOOZ_SERVICE_GLASS_ID = "/id_of_the_tooz_glass_the_service_should_send_information_to"; // see de.tu.dresden.swt.medical.ar.service.controller.PublicWebSocketMessagesController.glassChannelMessage
    public static final String TOOZ_STOMP_URI = "ws://localhost:8000/medical_ar-websocket"; // see C:\Users\Admin\Promotion\IPos\ar-support-in-medical-environments\Service\src\test\java\testutility\WSTest.java

    private static ToozFactory toozFactory = ToozFactory.eINSTANCE;
    private Timer timer;

    /**
     * This IPos extension manages for certain monitoring targets (so called local agents) a list of distant entities,
     * including their respective distance from the local agent, based on
     * rawdata information that is provided to this extension by the IPos-FW. Repeatedly, after a
     * regular interval, for each local agent the nearest distant entity is determined. The id of this entity is
     * provided to the Tooz backend service.
     *
     * ToozIPosRawdataMqtt.handleIPosUWBEvent(...): rawdata handling, distances are updated
     * ToozExtInputMqtt.handleToozExtInput(...): new local agents are registered
     * Tooz.updateToozBackend(...): for each local agent the closest distant entity is determined. Tooz backend service is updated
     * ToozExtOutputStomp.transmitDistantEntity(...) Tooz Backend service is updated
     *
     * @param mqttService
     */
    @Autowired
    public Tooz(ExternalPubServiceImpl mqttService) {
        toozExt.setMqttService(mqttService);
        ToozIPosRawdataMqtt.setMqttService(mqttService);
        ToozExtInputMqtt.setMqttService(mqttService);
        ToozExtOutputMqtt.setMqttService(mqttService);
        startUpdateTimer();
    }

//    ich könnte noch machen: gleich die NFC bzw. RFID-Rohdatenübertragung einbauen, dann muss ich mich später nicht erst lange einarbeiten

    private void startUpdateTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run(){
                updateToozBackend();
            }
        }, 0, 3000);
    }

    static private ToozExt toozExt = new ToozExt();

    public static void handleIPosBTEvent(IPosBTEvent iPosBTEvent){
        handleIPosBeaconEvent((IPosBeaconEvent) iPosBTEvent);
    }

    public static void handleIPosUWBEvent(IPosUWBEvent iPosUWBEvent) {
        handleIPosBeaconEvent((IPosBeaconEvent) iPosUWBEvent);
    }

    public static void handleIPosBeaconEvent(IPosBeaconEvent iPosBeaconEvent){
        try {
            toozExt.handleRawdataEvent(iPosBeaconEvent);
        } catch (RuntimeException e) {
            LOG.info(e.getMessage());
        }
    }

    public static void processMonTargetTestdata(String path_to_monTarget_file) {
        File monTargetFile = new File(path_to_monTarget_file);
        for (String line : OtherUtility.readLines(monTargetFile)) {
            ToozExtInputMqtt.handleToozExtInput(line);
        }
    }

    /**
     * This function configures the IPos-IPS to receive and forward location information concerning
     * a monitoring target (i.e., a position sensor with a particular ID). Location information
     * may be the distances of the monitoring target from particular distant entities.
     * @param monitoringTarget_internal
     */
    public static void handleMonitoringTarget(MonitoringTarget monitoringTarget_internal) {
        LOG.info("TOOZ: Attempting to configure IPos-IPS for monitoring the location of the sensor with id " + monitoringTarget_internal.getTargetSensorId());
        IPosArguments.IPosConfigData toozConf = new ToozArgs.ToozConf(monitoringTarget_internal);
        toozExt.configureIpos(toozConf);
        ToozLocalAgent newlyRegisteredTarget = new ToozLocalAgent(monitoringTarget_internal.getTargetSensorId());
        toozExt.getAllCurrentlyMonitoredTargets().add(newlyRegisteredTarget);
    }

    public static void updateToozBackend() {
        LOG.info("TOOZ: Updating tooz-backend");
        List<ToozLocalAgent> allCurrentlyMonitoredTargets = toozExt.getAllCurrentlyMonitoredTargets();
        allCurrentlyMonitoredTargets.stream().forEach(monitoredTarget -> updateToozBackendForTarget(monitoredTarget));
    }

    private static void updateToozBackendForTarget(ToozLocalAgent monitoredTarget) {
        if (!monitoredTarget.hasChangedSinceLastUpdate()){
            return;
        }
        List<Map.Entry<String, Double>> sortedDistances = extractSortedDistances(monitoredTarget);
        if (sortedDistances.size() == 0){
            LOG.info("Could not update Tooz-backend for monitoring Target " + monitoredTarget.getTargetSensorId() + " as no distant entities are known yet.");
            return;
        }
        Map.Entry<String, Double> nearestDistantEntity = sortedDistances.get(0);
        DistantEntity distantEntity = createDistantEntityFromMonitoredTarget(monitoredTarget, nearestDistantEntity);
        LOG.info("TOOZ: The closest distant entity of monitoring target " + monitoredTarget.getTargetSensorId() + " has changed. Id of closest entity: " + distantEntity.getDistantEntityId() + "; distance of closest entity: " + distantEntity.getDistance() + "; all known distances for this monitoring target: " + monitoredTarget.getCopyOfDistances().values());
        //ToozExtOutputMqtt.transmitDistantEntity(distantEntity, TOPIC_TOOZ_EXT_OUTPUT, SERIALIZATION_TYPE_JSON);
        ToozExtOutputStomp.transmitDistantEntity(distantEntity, TOPIC_TOOZ_EXT_OUTPUT);
        monitoredTarget.setHasChangedSinceLastUpdate(false); // Tooz-backend has been updated and is up-to-date now, so we regard the monitoredTarget as not having changed since last update
        return;
    }

    private static DistantEntity createDistantEntityFromMonitoredTarget(ToozLocalAgent monitoredTarget, Map.Entry<String, Double> nearestDistantEntity) {
        DistantEntity distantEntity = toozFactory.createDistantEntity();
        distantEntity.setLocalSensorId(monitoredTarget.getTargetSensorId());
        distantEntity.setLocalAgentId(monitoredTarget.getLocalAgentId());
        distantEntity.setDistantEntityId(nearestDistantEntity.getKey());
        distantEntity.setDistance(nearestDistantEntity.getValue());
        distantEntity.setProximityIndex(0);
        distantEntity.setEntityData(new HashMap<>());
        distantEntity.setTimeStamp(monitoredTarget.getTimestamp());
        return distantEntity;
    }

    private static List<Map.Entry<String, Double>> extractSortedDistances(ToozLocalAgent monitoredTarget) {
        List<Map.Entry<String, Double>> toBeSortedDistancesList = new LinkedList<>();
        toBeSortedDistancesList.addAll(monitoredTarget.getCopyOfDistances().entrySet());
        Collections.sort(toBeSortedDistancesList, (e1, e2) -> e1.getValue().compareTo(e2.getValue()));
        return toBeSortedDistancesList;
    }
}
