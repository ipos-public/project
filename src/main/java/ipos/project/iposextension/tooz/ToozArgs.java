package ipos.project.iposextension.tooz;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget;
import ipos.project.devkit.iposext.IPosArguments;

public class ToozArgs {

    public static class ToozConf implements IPosArguments.IPosConfigData {

        MonitoringTarget monitoringTarget;
        public MonitoringTarget getMonitoringTargets() {
            return monitoringTarget;
        }

        public ToozConf(MonitoringTarget monitoringTarget) {
            this.monitoringTarget = monitoringTarget;
        }

    }

}
