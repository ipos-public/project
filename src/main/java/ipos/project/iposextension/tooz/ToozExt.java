package ipos.project.iposextension.tooz;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.SimpleScene;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.*;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget;
import ipos.project.devkit.iposext.IPosArguments;
import ipos.project.devkit.iposext.IPosExtension;
import ipos.project.devkit.utility.ProtoJsonMap;
import ipos.project.iposextension.tooz.websocket.WebSocketService;
import org.apache.logging.log4j.LogManager;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static ipos.project.iposextension.tooz.Tooz.TOOZ_MONITORINGTASKID;

public class ToozExt extends IPosExtension {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    public ToozExt() {
        initStomp();
    }

    private WebSocketService webSocketService;

    protected List<ToozLocalAgent> getAllCurrentlyMonitoredTargets() {
        return allCurrentlyMonitoredTargets;
    }

    private List<ToozLocalAgent> allCurrentlyMonitoredTargets = new LinkedList<>();

    @Override
    public void handlePositionEvent(IposPositionEvent posEvent) {}

    @Override
    public void handlePositionEvent(IposPositionEvent posEvent, IPosArguments.HandlePosEventConf posEventConf) {}

    @Override
    public void handleRawdataEvent(IPosRawdataEvent iPosRawdataEvent) {
        if (iPosRawdataEvent instanceof IPosUWBEvent){
            handleIPosUWBEvent((IPosUWBEvent) iPosRawdataEvent);
        }
        if (iPosRawdataEvent instanceof IPosBTEvent){
            handleIPosBTEvent((IPosBTEvent) iPosRawdataEvent);
        }
    }

    private void handleIPosBTEvent(IPosBTEvent iPosRawdataEvent) {
        IPosBTEvent iPosBTEvent = iPosRawdataEvent;
        if ((iPosBTEvent.getDistances().size() == 0) && (iPosBTEvent.getRss().size() == 0)){
            LOG.info("Ignoring IPosBTEvent, as it neither contains distances nor rss-values");
            return;
        }
        if ((iPosBTEvent.getDistances().size() == 0) && (iPosBTEvent.getRss().size() > 0)) {
            iPosBTEvent.setDistances(translateRssToDistances(iPosBTEvent));
        }
        handleIPosBeaconEvent((IPosBeaconEvent) iPosBTEvent);
    }

    private void handleIPosUWBEvent(IPosUWBEvent iPosRawdataEvent) {
        IPosUWBEvent iPosUWBEvent = iPosRawdataEvent;
        if (iPosUWBEvent.getDistances().size() > 0) {
            handleIPosBeaconEvent((IPosBeaconEvent) iPosUWBEvent);
        }else{
            LOG.info("Ignoring IPosUWBEvent as it does not contain any distances");
        }
    }

    private Map<String, Double> translateRssToDistances(IPosBTEvent iPosBTEvent) {
        // currently rss is incorrectly translated into distances by creating a copy
        return iPosBTEvent.getRss();
    }

    @Override
    public SimpleScene.IposConfigWrapper prepareConfigWrapper(IPosArguments.IPosConfigData iposConfigData) {
        ToozArgs.ToozConf toozConf = (ToozArgs.ToozConf) iposConfigData;
        MonitoringTarget monitoringTarget = toozConf.getMonitoringTargets();
        return createConfigWrapperFromMonitoringTarget(monitoringTarget);
    }

    private SimpleScene.IposConfigWrapper createConfigWrapperFromMonitoringTarget(MonitoringTarget monitoringTarget) {
        String monitoringRequest = String.format(Tooz.TOOZ_MONREQ, monitoringTarget.getTargetSensorId(), TOOZ_MONITORINGTASKID);
        try {
            return ProtoJsonMap.fromJson(monitoringRequest, SimpleScene.IposConfigWrapper.class);
        } catch (InvalidProtocolBufferException e) {
            LOG.info("Warning: IPos-IPS could not be configured properly, as there was a MonitoringTarget that no ConfigWrapper could be created from.");
            return null;
        }
    }

    protected void handleIPosBeaconEvent(IPosBeaconEvent iPosBeaconEvent) {
        LOG.info("TOOZ: Attempting to handle IPosBeaconEvent (class: " + iPosBeaconEvent.getClass().getName());
        ToozLocalAgent toozLocalAgent = fetchMonitoringTarget(iPosBeaconEvent);
        toozLocalAgent.addOrUpdateDistances(iPosBeaconEvent.getDistances());
        toozLocalAgent.setLocalAgentId(iPosBeaconEvent.getAgentId());
        toozLocalAgent.setTimestamp(iPosBeaconEvent.getTimeStamp()); // the time when the distances to the distant entities have been updated the previous time
        LOG.info("TOOZ: ToozExt received rawdata event: " + iPosBeaconEvent.getClass().getName() +
                ". Monitoring target (sensorId): " + iPosBeaconEvent.getSensorId() +
                ", agent: " + iPosBeaconEvent.getAgentId() + ", timestamp: " +
                iPosBeaconEvent.getTimeStamp());
    }

    private ToozLocalAgent fetchMonitoringTarget(IPosBeaconEvent iPosUWBEvent) {
        String targetSensorId = iPosUWBEvent.getSensorId();
        ToozLocalAgent localAgent = allCurrentlyMonitoredTargets.stream().filter(
                monTarget -> monTarget.getTargetSensorId().equals(targetSensorId))
                .findFirst().
                orElseThrow(() -> new RuntimeException("Warning: ToozExt received rawdata from a position sensor that no MonitoringTarget was defined for"));
        return localAgent;
    }

    public void initStomp(){
        webSocketService = new WebSocketService();
        ToozExtOutputStomp.setToozExt(this);
        LOG.info("Tooz-extension of IPos-FW is attempting to connect to STOMP broker with URI " + Tooz.TOOZ_STOMP_URI + " ( ws://ip:port/endpoint )");
        webSocketService.reconnectWebSocket(Tooz.TOOZ_STOMP_URI, 1000);
    }
    public void requestPatientInfoFromServiceForGlasses(String _destination, String _patientId) {
        try {
            Objects.requireNonNull(webSocketService);
            String patientId = Objects.requireNonNull(_patientId);
            String destination = Objects.requireNonNull(_destination);
            _requestPatientInfoFromServiceForGlasses(destination, patientId);
        } catch (NullPointerException e) {
            LOG.info("Warning: Could not request patient information from tooz-backend for glasses. Possible reasons: (1) STOMP was not propertly initialized, (2) patient id was null.");
        }
    }

    private void _requestPatientInfoFromServiceForGlasses(String destination, String patientId){
        LOG.info("Tooz-extension of IPos-FW requests the tooz backend service to publish relevant information for distant agent with id " + patientId + " to the tooz smart glasses with id: " + Tooz.TOOZ_SERVICE_GLASS_ID + ". Destination prefix of the backend service: " + Tooz.TOOZ_SERVICE_APPLICATION_DESTINATION_PREFIX);
        LOG.info("Resulting destination of the tooz backend service: " + destination);
        webSocketService.requestPatientInfoFromServiceForGlasses(destination, patientId);
    }


}
