package ipos.project.iposextension.tooz;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;

@MqttListener(Tooz.TOPIC_TOOZ_ENVIRONMENT)
public class ToozExtInputMqtt implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static ExternalPubServiceImpl mqttService;
    public static void setMqttService(ExternalPubServiceImpl mqttService) {
        ToozExtInputMqtt.mqttService = mqttService;
    }

  @Override
    public void handle(MqttMessage message) {
        handleToozExtInput(message.toString());
    }

    public static void handleToozExtInput(String jsonMsg) {
        LOG.info("TOOZ: Handling ToozExtInput: " + jsonMsg);
        ipos.models.Tooz.IPosToozExtInputWrapper toozInputWrapper = null;
        try {
            toozInputWrapper = ProtoJsonMap.fromJson(jsonMsg, ipos.models.Tooz.IPosToozExtInputWrapper.class);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        toozInputWrapper.getMonitoringTargetsList().forEach(monitoringTarget -> handleMonitoringTarget(monitoringTarget));
    }

    private static void handleMonitoringTarget(ipos.models.Tooz.MonitoringTarget monitoringTarget_proto) {
        MonitoringTarget monitoringTarget_internal = ToozTrans.monTarget_proto2internal(monitoringTarget_proto);
        Tooz.handleMonitoringTarget(monitoringTarget_internal);
    }
}
