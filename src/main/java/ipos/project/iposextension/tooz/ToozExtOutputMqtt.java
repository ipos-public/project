package ipos.project.iposextension.tooz;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.OtherUtility;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;

@MqttListener(Tooz.TOPIC_TOOZ_EXT_OUTPUT)
public class ToozExtOutputMqtt implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static ExternalPubServiceImpl mqttService;
    public static void setMqttService(ExternalPubServiceImpl mqttService) {
        ToozExtOutputMqtt.mqttService = mqttService;
    }

   @Override
    public void handle(MqttMessage message) {}

    public static void transmitDistantEntity(DistantEntity distantEntity_internal, String topic, String serializationType){
        ipos.models.Tooz.DistantEntity distantEntity_proto = ToozTrans.distantEntity_internal2proto(distantEntity_internal);
        logging_tooz(topic, serializationType, distantEntity_proto);
        OtherUtility.publishRespectingSerializationType(topic, serializationType, distantEntity_proto);
    }

    private static void logging_tooz(String topic, String serializationType, ipos.models.Tooz.DistantEntity distantEntity_proto) {
        LOG.info("TOOZ: Publishing " + distantEntity_proto.getClass().getName() + " on topic " + topic + " with serialization type " + serializationType);
        LOG.info("TOOZ: ");
    }

}
