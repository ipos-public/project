package ipos.project.iposextension.tooz;

import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity;
import org.apache.logging.log4j.LogManager;

import java.util.Objects;
// import de.tu.dresden.swt.medical.ar.toozer_app_java.websocket.WebSocketService.StompSessionHandler;



// webSocketService.setBluetoothName(Tooz.IPOS_IPS);

public class ToozExtOutputStomp {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    static private ToozExt toozExt;

    /**
     * The web-socket-client to handle stomp-messages.
     */
   // private final ___StompSessionClient stompSessionClient = new ___StompSessionClient(new StompSessionHandler());

    static void setToozExt(ToozExt _toozExt){
        toozExt = Objects.requireNonNull(_toozExt);
    }

    static void transmitDistantEntity(DistantEntity _distantEntity, String _topicToozExtOutput) {
        try {
            Objects.requireNonNull(toozExt);
            DistantEntity distantEntity = Objects.requireNonNull(_distantEntity);
            String topicToozExtOutput = Objects.requireNonNull(_topicToozExtOutput);
            _transmitDistantEntity(distantEntity, topicToozExtOutput);
        }catch(NullPointerException e){
            LOG.info("Warning: Could not transmit information on distant entity to tooz-backend. Possible reasons: (1) STOMP was not propertly initialized, (2) distant entity or topic was null.");
        }
    }

    static void _transmitDistantEntity(DistantEntity distantEntity, String topicToozExtOutput) {
        String destination = Tooz.TOOZ_SERVICE_APPLICATION_DESTINATION_PREFIX + Tooz.TOOZ_SERVICE_GLASS_ID;
        String stompMessageContent = distantEntity.getDistantEntityId(); //patientId, see ipos.project.iposextension.tooz.Tooz.createDistantEntityFromMonitoredTarget
        LOG.info("TOOZ: Attempting to transmit STOMP message containing distant entity ID " + stompMessageContent + " to destination " + destination);
        toozExt.requestPatientInfoFromServiceForGlasses(destination, stompMessageContent);

        // Message patientIdMessage = new Message(MessageType.PATIENT_ID, stompMessageContent);
        // ToozController.sendMessageToService(destination, patientIdMessage);
        //webSocketService.getPatientWithBeaconId(distantEntity.getDistantEntityId())

    }
}
