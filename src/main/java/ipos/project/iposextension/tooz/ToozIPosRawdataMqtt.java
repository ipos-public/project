package ipos.project.iposextension.tooz;

import com.google.protobuf.InvalidProtocolBufferException;
import ipos.models.GenericSensor;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent;
import ipos.project.config.mqtt.Handler;
import ipos.project.config.mqtt.MqttListener;
import ipos.project.devkit.utility.ExternalPubServiceImpl;
import ipos.project.devkit.utility.ProtoJsonMap;
import org.apache.logging.log4j.LogManager;
import org.eclipse.paho.client.mqttv3.MqttMessage;

@MqttListener(Tooz.TOOZ_MONITORINGTASKID)
public class ToozIPosRawdataMqtt implements Handler {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    private static ExternalPubServiceImpl mqttService;
    public static void setMqttService(ExternalPubServiceImpl mqttService) {
        ToozIPosRawdataMqtt.mqttService = mqttService;
    }

    // für den generierten code ggf. noch eine höhere Wrapper-Ebene vorsehen: IPosInputWrapper (enthält sowohl PositionWrapper, als auch RawdataWrapper)
    // für den generierten code auch vorsehen: eigene XMqtt-Klasse für jeden Wrapper generieren der empfangen werden können soll.
    // Also eine Klasse für den IPosInputWrapper und eine andere Klasse für den IPosToozExtInputWrapper. Für die Wrapper-Abbildung in EMF ggf. mit Packages arbeiten
    // Es gibt Wrapper die nur ausgehende Nachrichten beinhalten (z.B. bei der tooz-Anwendung: ToozExtOutputWrapper), man muss beim Generieren festlegen können ob
    // die Wrapper-Nachrichten überhaupt empfangen werden sollen
    @Override
    public void handle(MqttMessage message) {
        LOG.info("TOOZ: ToozIPosRawdataMqtt received message: " + message.toString());
        handleIPosRawdataWrapper(message.toString());
    }

    // generierter code: grundsätzlich für jede mögliche Nachricht etwas generieren, nicht nur für IPosUWBEvent
    private void handleIPosRawdataWrapper(String jsonMsg) {
        try {
            GenericSensor.IPosRawdataEventWrapper iPosRawdataEventWrapper = ProtoJsonMap.fromJson(jsonMsg, GenericSensor.IPosRawdataEventWrapper.class);
            iPosRawdataEventWrapper.getIPosUWBEventList().stream().forEach(iPosUWBEvent -> this.handleIPosUWBEvent(iPosUWBEvent));
            iPosRawdataEventWrapper.getIPosBTEventList().stream().forEach(iPosBTEvent -> this.handleIPosBTEvent(iPosBTEvent));
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    private void handleIPosBTEvent(GenericSensor.IPosBTEvent iPosBTEvent_proto) {
        IPosBTEvent iPosBTEvent_internal = ToozTrans.bt_proto2IPosRawdata(iPosBTEvent_proto);
        LOG.info("TOOZ: BTEvent was transformed into internal data format: " + iPosBTEvent_internal.toString());
        Tooz.handleIPosBTEvent(iPosBTEvent_internal);
    }

    private void handleIPosUWBEvent(GenericSensor.IPosUWBEvent iPosUWBEvent_proto) {
        IPosUWBEvent iPosUWBEvent_internal = ToozTrans.uwb_proto2IPosRawdata(iPosUWBEvent_proto);
        LOG.info("TOOZ: UWBEvent was transformed into internal data format: " + iPosUWBEvent_internal.toString());
        Tooz.handleIPosUWBEvent(iPosUWBEvent_internal);
    }

}
