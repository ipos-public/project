package ipos.project.iposextension.tooz;

import java.util.HashMap;
import java.util.Map;

public class ToozLocalAgent {

    public ToozLocalAgent(String targetSensorId) {
        this.targetSensorId = targetSensorId;
        this.targetData = new HashMap<>();
        this.localAgentId = "";
        this.timestamp = "";
        this.hasChanged = false;
    }

    private Map<String, Double> distances = new HashMap<>();

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    private String timestamp; // the time when the distances to the distant entities have been updated the previous time

    /**
     * Returns true if this instance of ToozLocalAgent has to be regarded as having changed since the last
     * update of the Tooz-backend, and false otherwise.
     * @return
     */
    public boolean hasChangedSinceLastUpdate() {
        return hasChanged;
    }

    /**
     * Sets the boolean value that defines whether this instance of ToozLocalAgent has to be
     * regarde as having changed since last update of the Tooz-backend
     * @param hasChanged
     */
    public void setHasChangedSinceLastUpdate(boolean hasChanged) {
        this.hasChanged = hasChanged;
    }

    private boolean hasChanged;

    private String localAgentId;
    Map<String, String> targetData;

    protected Map<String, Double> getCopyOfDistances() {
        HashMap<String, Double> copyOfDistances = new HashMap<>();
        copyOfDistances.putAll(distances);
        return copyOfDistances;
    }

     protected void addDistance(String sensorId, Double distance){
        distances.put(sensorId, distance);
    }

    public String getTargetSensorId() {
        return targetSensorId;
    }

    private String targetSensorId;

    public void addOrUpdateDistances(Map<String, Double> map){
        if (map.size() > 0) {
            map.entrySet().forEach(entry -> distances.put(entry.getKey(), entry.getValue())); // put-function creates new entry, or updates existing one
            this.hasChanged = true;
        }
    }

    public String getLocalAgentId() {
        return localAgentId;
    }

    public void setLocalAgentId(String localAgentId) {
        this.localAgentId = localAgentId;
    }


    public Map<String, String> getTargetData() {
        return targetData;
    }

    public void setTargetData(Map<String, String> targetData) {
        this.targetData = targetData;
    }

}
