package ipos.project.iposextension.tooz;

import ipos.models.GenericSensor;
import ipos.models.Tooz;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBTEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosBeaconEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosDevKitFactory;
import ipos.project.DataModellntegration.iPos_Datamodel.IPosDevKit.IPosUWBEvent;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.DistantEntity;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.MonitoringTarget;
import ipos.project.DataModellntegration.iPos_Datamodel.Tooz.ToozFactory;

import java.util.HashMap;
import java.util.Map;

public class ToozTrans {
    private static IPosDevKitFactory devKitFactory = IPosDevKitFactory.eINSTANCE;
    private static ToozFactory toozFactory = ToozFactory.eINSTANCE;


    public static IPosUWBEvent uwb_proto2IPosRawdata(GenericSensor.IPosUWBEvent iPosUWBEvent_proto) {
        IPosUWBEvent iPosUWBEvent_internal = devKitFactory.createIPosUWBEvent();
        return ToozTrans.beacon_proto2IPosRawdata(iPosUWBEvent_internal, iPosUWBEvent_proto.getAgentId(), iPosUWBEvent_proto.getSensorId(), iPosUWBEvent_proto.getAgentType(), iPosUWBEvent_proto.getSensorType(), iPosUWBEvent_proto.getTimeStamp(), iPosUWBEvent_proto.getDistancesMap());
    }

    public static IPosBTEvent bt_proto2IPosRawdata(GenericSensor.IPosBTEvent iPosBTEvent_proto) {
        IPosBTEvent iPosBTEvent_internal = devKitFactory.createIPosBTEvent();
        iPosBTEvent_internal = ToozTrans.beacon_proto2IPosRawdata(iPosBTEvent_internal, iPosBTEvent_proto.getAgentId(), iPosBTEvent_proto.getSensorId(), iPosBTEvent_proto.getAgentType(), iPosBTEvent_proto.getSensorType(), iPosBTEvent_proto.getTimeStamp(), iPosBTEvent_proto.getDistancesMap());
        iPosBTEvent_internal.setRss(Map_String2Double(iPosBTEvent_proto.getRssMap()));
        return iPosBTEvent_internal;
    }

    private static <T extends IPosBeaconEvent> T beacon_proto2IPosRawdata(T iPosBeaconEvent_internal, String agentId, String sensorId, String agentType, String sensorType, String timeStamp, java.util.Map<String, String> distancesMap) {
        iPosBeaconEvent_internal.setAgentId(agentId);
        iPosBeaconEvent_internal.setSensorId(sensorId);
        iPosBeaconEvent_internal.setAgentType(agentType);
        iPosBeaconEvent_internal.setSensorType(sensorType);
        iPosBeaconEvent_internal.setTimeStamp(timeStamp);
        iPosBeaconEvent_internal.setDistances(Map_String2Double(distancesMap));
        return iPosBeaconEvent_internal;
    }

    private static Map<String, Double> Map_String2Double(Map<String, String> distances) {
        HashMap<String, Double> distancesDouble = new HashMap<>();
        for (Map.Entry<String, String> distanceEntry : distances.entrySet()){
            distancesDouble.put(distanceEntry.getKey(), Double.valueOf(distanceEntry.getValue()));
        }
        return distancesDouble;
    }

    public static MonitoringTarget monTarget_proto2internal(Tooz.MonitoringTarget monitoringTarget_proto) {
        MonitoringTarget monitoringTarget_internal = toozFactory.createMonitoringTarget();
        monitoringTarget_internal.setTargetSensorId(monitoringTarget_proto.getTargetSensorId());
        return monitoringTarget_internal;
    }

    public static Tooz.DistantEntity distantEntity_internal2proto(DistantEntity distantEntity_internal) {
        Tooz.DistantEntity.Builder distantEntity_proto = Tooz.DistantEntity.newBuilder();
        distantEntity_proto.setLocalSensorId(distantEntity_internal.getLocalSensorId());
        distantEntity_proto.setLocalAgentId(distantEntity_internal.getLocalAgentId());
        distantEntity_proto.setDistantEntityId(distantEntity_internal.getDistantEntityId());
        distantEntity_proto.setDistance(distantEntity_internal.getDistance());
        distantEntity_proto.setProximityIndex(distantEntity_internal.getProximityIndex());
        distantEntity_proto.putAllEntityData(distantEntity_internal.getEntityData());
        return distantEntity_proto.build();
    }

}
