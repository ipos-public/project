package ipos.project.iposextension.tooz.websocket;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Defined format for a message to send or receive with the {@link StompSessionClient}.
 */
public class Message {

    /**
     * Type of the message.
     */
    private MessageType messageType;

    /**
     * Message-content as a {@link List} of {@link String}.
     * Every list-item represents a singe page to show on the glasses.
     * If the message will be used as command, only one item needs to be in the {@link List}.
     */
    private List<String> content;

    /**
     * Defines a default-message.
     * Use this constructor only for initialisation while a reflection-process.
     * The default {@link MessageType} is {@link MessageType#UNDEFINED}.
     */
    private Message() {
        this(MessageType.UNDEFINED);
    }

    /**
     * Creates an empty message with a {@link MessageType}.
     *
     * @param messageType type for the message
     */
    public Message(final @NotNull MessageType messageType) {
        setMessageType(messageType);
        setContent(new ArrayList<>());
    }

    /**
     * Creates a message.
     * @param messageType type for the message.
     * @param message a message as an item in the message-content
     */
    public Message(final @NotNull MessageType messageType, final @NotNull String message) {
        this(messageType);
        addInformationToContent(message);
    }

    /**
     * Sets the {@link MessageType}.
     *
     * @param messageType new type for the message.
     */
    private void setMessageType(final @NotNull MessageType messageType) {
        this.messageType = messageType;
    }

    /**
     * Gives the {@link MessageType} of the message.
     *
     * @return the {@link MessageType} of the message.
     */
    public MessageType getMessageType() {
        return messageType;
    }

    /**
     * Adds a new item to the message-content.
     * @param information content for the new item in the message-content.
     */
    public void addInformationToContent(final @NotNull String information) {
        content.add(information);
    }

    /**
     * Sets a {@link List} with {@link String}s as new content for the message.
     * @param content a {@link List} of {@link String} as new content for the message.
     */
    private void setContent(final @NotNull List<String> content) {
        this.content = content;
    }

    /**
     * Gives the complete content of the message as {@link List} of {@link String}.
     * @return the complete content of the message.
     */
    public List<String> getContent() {
        return content;
    }
}
