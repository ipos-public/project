package ipos.project.iposextension.tooz.websocket;

/**
 * Types of message they can be send to the medical-ar-service.
 */
public enum MessageType {

    /**
     * Defines a message with a patient-id in the message-content.
     */
    PATIENT_ID,

    /**
     * Defines a emergency-message.
     */
    EMERGENCY,

    /**
     * Defines a broadcast-message. Broadcasts are send to all clients.
     */
    BROADCAST,

    /**
     * Defines a message with beacon-information.
     */
    BEACON,

    /**
     * Defines a button-pressed event-message.
     */
    BUTTON_PRESSED,


    /**
     * Defines a massage with content to display in the toozer-apps head.
     */
    DISPLAY_HEAD,

    /**
     * Defines a message with content to display in the toozer-apps text-container.
     */
    DISPLAY_TEXT,


    /**
     * Defines a test-message.
     */
    TEST,

    /**
     * Defines a undefined message. These messages should not be processed.
     */
    UNDEFINED
}
