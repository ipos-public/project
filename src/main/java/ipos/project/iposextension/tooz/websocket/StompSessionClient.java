package ipos.project.iposextension.tooz.websocket;

import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * <p>
 * A client to connect with a web-socket to the medical-ar-service.
 * This client creates a stomp-session within the web-socket to handle all messages and events.
 * </p>
 * <p>
 * Currently there is no authentication supported.
 * </p>
 */
public class StompSessionClient {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();
    // Atomic structures and semaphores to handling internal threading.
    private final AtomicBoolean disconnect = new AtomicBoolean(true);
    private final Semaphore connectionMutex = new Semaphore(1);

    /**
     * Map with stomp-subscriptions.
     */
    private final Map<String, StompSession.Subscription> subscriptions = new HashMap<>();

    /**
     * The web-socket to connect to the medical-ar-service.
     */
    private volatile StompSession webSocketSession;

    /**
     * The stomp-session.
     */
    private final StompSessionHandlerAdapter sessionHandler;

    /**
     * Creates a client to connect with a web-socket to a service.
     *
     * @param sessionHandler handler to manage stomp-messages as events.
     */
    StompSessionClient(final @NotNull StompSessionHandlerAdapter sessionHandler) {
        this.sessionHandler = sessionHandler;
    }

    /**
     * Connects to a given destination with a given timeout.
     *
     * @param uri        the destination to connect with.
     * @param waitMillis the timeout to wait for a response.
     */
    private void connect(final @NotNull String uri, final long waitMillis) {
        new Thread(new ConnectWebSocket(uri, createWebSocketStompClient(), waitMillis)).start();
    }

    /**
     * Disconnects the client from the destination.
     */
    void disconnect() {
        new Thread(new DisconnectWebSocket()).start();
    }

    /**
     * Disconnects the client first from the current destination
     * and try to reconnect to the given destination with a timeout.
     *
     * @param uri        the destination to reconnect with.
     * @param waitMillis the timeout to wait for a response.
     */
    void reconnect(final @NotNull String uri, final long waitMillis) {
        disconnect();
        connect(uri, waitMillis);
    }

    /**
     * Gives the connection-state of the client.
     *
     * @return {@code true}, if the client is currently connected to a destination.
     */
    boolean isConnected() {
        return webSocketSession.isConnected();
    }

    /**
     * Subscribe to a new destination, if the client is connected. It's a threaded function and
     * blocks the semaphore while working.
     *
     * @param destination the new destination to subscribe.
     */
    void subscribe(final @NotNull String destination) {
        new Thread(() -> {
            try {
                connectionMutex.acquire();

                if (webSocketSession != null && webSocketSession.isConnected() && !subscriptions.containsKey(destination)) {
                    LOG.info("subscribe to: %s", destination);
                    subscriptions.put(destination, webSocketSession.subscribe(destination, sessionHandler));
                }
            } catch (InterruptedException e) {
                LOG.info(e);
            } catch (Exception e) {
                LOG.info(e);
            } finally {
                connectionMutex.release();
            }
        }).start();
    }

    /**
     * Unsubscribe from a subscribed stomp-destination. It's a threaded function and
     * blocks the semaphore while working.
     *
     * @param destination the destination to unsubscribe.
     */
    void unsubscribe(final @NotNull String destination) {
        new Thread(() -> {
            try {
                connectionMutex.acquire();

                if (webSocketSession != null && webSocketSession.isConnected() && subscriptions.containsKey(destination)) {
                    LOG.info("unsubscribe to: %s", destination);
                    Objects.requireNonNull(subscriptions.remove(destination)).unsubscribe();
                }
            } catch (InterruptedException e) {
                LOG.info(e);
            } catch (Exception e) {
                LOG.info(e);
            } finally {
                connectionMutex.release();
            }
        }).start();
    }

    /**
     * Subscription-state for a given stomp-destination.
     *
     * @param destination the destination to check its subscription-state.
     * @return {@code true}, if the destination has been subscribed.
     */
    boolean hasSubscribedToDestination(final String destination) {
        return subscriptions.containsKey(destination);
    }

    /**
     * Sends a message to a given stomp-destination.
     * The destination should be supported by the service connected with.
     * It's a threaded function and blocks the semaphore while working.
     *
     * @param destination the destination to send a message.
     * @param message the message to send.
     *                The concrete class of the message-object should be supported by the service connected with.
     */
    void sendMessage(final @NotNull String destination, final @NotNull Object message) {
        new Thread(() -> {
            try {
                connectionMutex.acquire();

                if (webSocketSession != null && webSocketSession.isConnected()) {
                    webSocketSession.send(destination, message);
                    LOG.info("send message '%s' to '%s'", message.toString(), destination);
                }
            } catch (InterruptedException e) {
                LOG.info(e);
            } catch (Exception e) {
                LOG.info(e);
            } finally {
                connectionMutex.release();
            }
        }).start();
    }

    /**
     * Helper-function to create a web-socket client with an underlying stomp-session.
     * @return a web-socket-client wit underlying stomp-session.
     */
    private WebSocketStompClient createWebSocketStompClient() {
        final StandardWebSocketClient client = new StandardWebSocketClient();

        final List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(client));

        final SockJsClient sockJsClient = new SockJsClient(transports);

        final WebSocketStompClient webSocketStompClient = new WebSocketStompClient(sockJsClient);
        webSocketStompClient.setMessageConverter(new MappingJackson2MessageConverter());

        return webSocketStompClient;
    }

    /**
     * Inner class to create a web-socket-connection to handle stomp-messages.
     */
    private class ConnectWebSocket implements Runnable {

        /**
         * The destination to connect with.
         */
        final private String uri;

        /**
         * The web-socket-client to connect wit a service.
         */
        final private WebSocketStompClient webSocketStompClient;

        /**
         * Timeout for the connection-response.
         */
        final private long waitMillis;

        /**
         * Creates a web-socket with a given stomp-client.
         * @param uri the destination to connect with.
         * @param webSocketStompClient the stomp-client to handle stomp-messages.
         * @param waitMillis the timeout for the connection-response.
         */
        ConnectWebSocket(final String uri, final WebSocketStompClient webSocketStompClient,
                         final long waitMillis) {
            this.uri = uri;
            this.webSocketStompClient = webSocketStompClient;
            this.waitMillis = waitMillis;
        }

        /**
         * Start the connection with the destination and listen for stomp-messages to handle them.
         * This function should be run in a thread and blocks the semaphore while the connection-process.
         * The connection-process restarts every time a timeout exists.
         */
        public void run() {
            try {
                connectionMutex.acquire();
                disconnect.set(false);

                final AtomicBoolean isNotConnected = new AtomicBoolean(true);

                do {
                    if (disconnect.get()) {
                        LOG.info("disconnect occurred -> stop self");
                        break;
                    }

                    try {
                        webSocketSession = webSocketStompClient.connect(uri, sessionHandler).get();
                    } catch (ResourceAccessException e) {
                        LOG.info("can not connect to: %s", uri);
                    } catch (Exception e) {
                        LOG.info(e);
                    }

                    if (webSocketSession == null || !webSocketSession.isConnected()) {
                        LOG.info("not connected -> retry connect in %s seconds", 0.001f * waitMillis);

                        try {
                            Thread.sleep(waitMillis);
                        } catch (InterruptedException e) {
                            LOG.info("thread has been interrupted -> stop self");
                            break;
                        }
                    } else {
                        isNotConnected.set(false);
                    }
                } while (isNotConnected.get());
            } catch (InterruptedException e) {
                LOG.info(e);
            } finally {
                connectionMutex.release();
            }
        }
    }

    /**
     * Disconnects the client from the destination.
     * This function should be run in a thread and blocks the semaphore while the disconnection-process.
     */
    private class DisconnectWebSocket implements Runnable {

        public void run() {
            try {
                disconnect.set(true);
                connectionMutex.acquire();

                if (webSocketSession != null) {
                    if (webSocketSession.isConnected()) {
                        subscriptions.forEach((k, v) -> v.unsubscribe());
                        webSocketSession.disconnect();
                    }

                    subscriptions.clear();
                    webSocketSession = null;
                }
            } catch (InterruptedException e) {
                LOG.info(e);
            } catch (Exception e) {
                LOG.info(e);
            } finally {
                connectionMutex.release();
            }
        }
    }
}
