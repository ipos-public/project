package ipos.project.iposextension.tooz.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class ToozController {

    @Autowired
    private static SimpMessagingTemplate template;

    //@Autowired
    //public ToozController(SimpMessagingTemplate template){
        //this.template = template;
    //}

    public static void sendMessageToService(String destination, Message message){
        template.convertAndSend(destination, message);
    }

}
