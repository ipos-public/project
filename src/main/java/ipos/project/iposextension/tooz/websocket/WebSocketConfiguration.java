package ipos.project.iposextension.tooz.websocket;

import org.apache.logging.log4j.LogManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.concurrent.DefaultManagedTaskScheduler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Simple configuration to use WebSockets as server/de.tu.dresden.swt.medical.ar.service.
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {
    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    /**
     * Configures the MessageBroker-registry.
     *
     * @param config {@link MessageBrokerRegistry} to become configured
     */
    @Override
    public void configureMessageBroker(final MessageBrokerRegistry config) {
        // their service receives messages
        //config.enableSimpleBroker("/medical_ar_app")
          //  .setTaskScheduler(new DefaultManagedTaskScheduler())
          //  .setHeartbeatValue(new long[]{0, 0});
        //config.enableStompBrokerRelay("destprefixes");
        // ipos-fw receives nothing...
        // config.setApplicationDestinationPrefixes("/medical_ar_app");

       // LOG.debug("Simple STOMP-broker enabled.");
    }

    /**
     * Configures the STOMP-endpoint.
     *
     * @param registry {@link StompEndpointRegistry} to create endpoint for STOMP-messages
     */
    @Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
        registry.addEndpoint("/iposfw_stomp_endpoint").setAllowedOrigins("*").withSockJS();

        LOG.debug("Endpoints registered.");
    }
}
