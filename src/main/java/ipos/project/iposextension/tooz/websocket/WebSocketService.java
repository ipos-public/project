package ipos.project.iposextension.tooz.websocket;

import ipos.project.iposextension.tooz.Tooz;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;
import org.springframework.messaging.simp.stomp.*;

import java.lang.reflect.Type;



/**
 * A background-android-service to handle the web-socket-connection with an underlying stomp-connection.
 */
public class WebSocketService {

    private static org.apache.logging.log4j.Logger LOG = LogManager.getLogger();

    /**
     * The web-socket-client to handle stomp-messages.
     */
    private final StompSessionClient stompSessionClient = new StompSessionClient(new StompSessionHandler());


    /**
     * Name of the bluetooth-device.
     */
    private String bluetoothName = "glass_default";

    /**
     * The destination for the service to connect with.
     */
    private String ownDestination = null;

    /**
     * Sets the bluetooth-device-name to the background-service
     * It is used to identify the android-device at the medical-ar-service.
     *
     * @param bluetoothName
     */
    public void setBluetoothName(final @NotNull String bluetoothName) {
        if (ownDestination != null)
            return;

        this.bluetoothName = bluetoothName;
        ownDestination = "/medical_ar_app/" + bluetoothName;
    }

    /**
     * Gives the connection-state of the internal web-socket-connection.
     *
     * @return {@code true}, if a connection is established.
     */
    public boolean isConnected() {
        return stompSessionClient.isConnected();
    }

    /**
     * Reconnects the internal web-socket-client wit a given destination and timeout.
     * Before the new connection will be created, the current connection will be destroyed.
     *
     * @param uri        the new destination to connect with.
     * @param waitMillis timeout for the connection-response.
     */
    public void reconnectWebSocket(final @NotNull String uri, final long waitMillis) {
        stompSessionClient.reconnect(uri, waitMillis);
    }

    public void requestPatientInfoFromServiceForGlasses(String destination, String patientId){
        final Message message = new Message(MessageType.PATIENT_ID, patientId);

        stompSessionClient.sendMessage(destination,
                message);
    }

    /**
     * Sends a message to the service to get patient-information.
     *
     * @param patientId the patient-id of the patient to get the information about.
     */
    public void getPatient(final @NotNull String patientId) {
        final Message message = new Message(MessageType.PATIENT_ID, patientId);

        stompSessionClient.sendMessage(ownDestination,
                message);
    }

    /**
     * Sends a message to the service to get patient-information with a beacon-id.
     *
     * @param beackonId the beacon-id of the patient to get the information about.
     */
    public void getPatientWithBeaconId(final @NotNull String beackonId) {
        final Message message = new Message(MessageType.BEACON, beackonId);

        stompSessionClient.sendMessage(ownDestination,
                message);
    }

    /**
     * Sends a message to the service wit a button-pressed-event.
     */
    public void buttonPressed() {
        stompSessionClient.sendMessage(ownDestination,
                new Message(MessageType.BUTTON_PRESSED));
    }

    /**
     * Subscribes to a given stomp-destination.
     *
     * @param channelName the name of the stomp-destination to connect with.
     */
    public void subscribeToChannel(final @NotNull String channelName) {
        stompSessionClient.subscribe(channelName);
    }

    /**
     * Unsubscribe from a given stomp-destination.
     *
     * @param channelName the name of the stomp-destination to disconnect from.
     */
    public void unsubscribeToChannel(final @NotNull String channelName) {
        stompSessionClient.unsubscribe(channelName);
    }

    /**
     * An internal event-handling-class to handle events from a stomp-connection.
     */
    private class StompSessionHandler extends StompSessionHandlerAdapter {

        // Mutex for thread-handling.
        private final Object mutex = new Object();

        /**
         * OnAfterConnected-event-handle. Gets called after a stomp-connection has been established.
         * Subscribes to the broadcast-destination and an id-destination to get only messages for a single android-device.
         * Forwards the exception to all connected {@linkk WebSocketEventListener} as an OnConnectionEvent.
         *
         * @param session          the session that has been created.
         * @param connectedHeaders the connection-header send from the connected service.
         */
        @Override
        public void afterConnected(final StompSession session, final StompHeaders connectedHeaders) {
            synchronized (mutex) {
                LOG.info("session started: ID -> %s ", session.getSessionId());

                stompSessionClient.subscribe("/broadcast");
                stompSessionClient.subscribe(String.format("/glass/%s", bluetoothName));

                // webSocketEventListeners.forEach(listener -> listener.onConnectionEvent(true));
            }
        }

        /**
         * OnException-event-handle. Gets called if an exception has been thrown.
         * Forwards the exception to all connected {@linkk WebSocketEventListener} as an OnErrorEvent.
         *
         * @param session   the session where the exception has been thrown.
         * @param command   the stomp-command where the exception has been thrown.
         * @param headers   the stomp-header where the exception has been thrown.
         * @param payload   the stomp-message-content where the exception has been thrown.
         * @param exception the exception that has been thrown.
         */
        @Override
        public void handleException(final StompSession session, final StompCommand command,
                                    final StompHeaders headers, final byte[] payload,
                                    final Throwable exception) {
            //webSocketEventListeners.forEach(listener -> listener.onErrorEvent(exception));
        }

        /**
         * Sets the {@link Type} of the messages that should be handled by the stomp-client.
         * Currently the {@link Type} is {@link Message}.
         * The {@link StompHeaders} can be used to differentiate between types of messages in the future.
         *
         * @param headers the stomp-header to differentiate between types of messages.
         * @return the {@link Type} of the message to reflect.
         */
        @Override
        @NotNull
        public Type getPayloadType(final StompHeaders headers) {
            return Message.class;
        }

        /**
         * <p>
         * OnHandleMessage-event-handle. Gets called after a message has been received.
         * Only messages to a subscribed stomp-destination will be processed.
         * Forwards the {@link Message} to all connected {@linkk WebSocketEventListener} as an OnMessageReceivedEvent.
         * </p>
         *
         * <p>
         * Creates the payload with reflection based on the result of the {@link StompSessionHandler#getPayloadType} function.
         * If you're change this function, you need to include these changes here.
         * </p>
         *
         * @param headers the stomp-header to differentiate between types of messages.
         * @param payload the reflected message-content from the received frame.
         */
        @Override
        public void handleFrame(final StompHeaders headers, final Object payload) {
            synchronized (mutex) {
                final Message message = (Message) payload;
                final String destination = headers.getDestination();

                if (destination != null && stompSessionClient.hasSubscribedToDestination(destination)) {
                    // webSocketEventListeners.forEach(
                    //        listener -> listener.onMessageReceivedEvent(message));
                }
            }
        }

        /**
         * <p>
         * OnHandleSendError-event-handler. Gets called every time an exception while sending a message has been thrown.
         * </p>
         *
         * <P>
         * If a connection has been lost this function forwards the {@link Exception} to all connected
         * {@linkk WebSocketEventListener} as an OnConnectionEvent and OnDisconnectOccurredEvent.
         * </P>
         *
         * <p>
         * In any other cases this function forwards the {@link Exception} to all connected
         * {@linkk WebSocketEventListener} as an OnErrorEvent.
         * </p>
         *
         * @param session   the session where the exception has been thrown.
         * @param exception the exception that has been thrown.
         */
        @Override
        public void handleTransportError(final StompSession session, final Throwable exception) {
            if (exception instanceof ConnectionLostException) {
                LOG.info("connection lost -> create a new one");

                // webSocketEventListeners.forEach(listener -> listener.onConnectionEvent(false));
                // webSocketEventListeners.forEach(WebSocketEventListener::onDisconnectOccurredEvent);

                return;
            }

            //webSocketEventListeners.forEach(listener -> listener.onErrorEvent(exception));
        }
    }
}