{"picklists" : [{ 
	"picklistId" : "picklist_1",
	"picklistRoles" : [
		{ "picklistId" : "Picklist_1",
		  "partyId" : "Employee_1",
		  "roleTypeId" : "PICKER"
		
		}
	],
	"picklistBins" : [ 
		{
			"picklistId" : "picklist_1",
			"binLocationNumber" : "shipmentBin_1", 
			"picklistItems" : [
				{
					"inventoryItem" : 
					{
						"inventoryItemId" : "invit1",
						"containerId" : "box_1", 
						"productId" : "blau"
					}
				}
			]
		},
		{
			"picklistId" : "picklist_1",
			"binLocationNumber" : "shipmentBin_2", 
			"picklistItems" : [
				{
					"inventoryItem" : 
					{
						"inventoryItemId" : "invit3",
						"containerId" : "box_2", 
						"productId" : "grün"
					}
				},
				{
					"inventoryItem" : 
					{
						"inventoryItemId" : "invit3",
						"containerId" : "box_3", 
						"productId" : "rot"
					}
				}
			]
		
		}	
	]
	}]}